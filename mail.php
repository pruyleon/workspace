<?php
  include('./signed/php/login_state.php');
  checkLogin();
  include("./signed/php/db.php");
  if (isset($_POST['user_id'])) {
    $userID = $_POST['user_id'];
  }
  $info_query = "SELECT mail, acc_ID FROM account WHERE acc_ID='$userID'";
  $info_res = mysqli_query($con, $info_query);
  while ($res = mysqli_fetch_assoc($info_res)) {
    $mail_address = $res['mail'];
  }
?>

<html>

<head>
  <base href="/" />
  <?php include('./src/header.php'); ?>
  <title>E-Mail Adresse - Syncronight</title>

  <link type="text/css" rel="stylesheet" href="./css/register/mail.css" />
</head>

<body>
  <div class="login_top_line">
    <div class="nav_logo_wrapper">
      <a href="/home"><img src="/src/icns/logo_white.svg" alt="SYNCRONIGHT" class="nav_logo"></a>
    </div>
    <div class="login_wrapper">
      <!--<span class="nav_link_wrapper"><a href="/register/host" class="nav_link" >Du bist ein Club?</a></span>-->
      <span class="nav_link_wrapper"><a class="nav_link" href="/login">Einloggen</a></span>
    </div>
  </div>
  <div class="master_wrapper">
    <div class="header_line">
      <?php
            if(isset($_GET['info']) && $_GET['info'] == 1){
                echo "<h1>Done! Welcome to mypressk.it!</h1>";
            } else {
              echo "<h1>Fast fertig!</h1>";
            }
           ?>
    </div>

    <div id="infobox">
      <?php

                if(isset($_GET['info']) && $_GET['info'] != ""){
                    // Info gesetzt
                    $Info = $_GET['info'];

                    switch($Info){
                        case 0:
                                echo 'Deine E-Mail Adresse wurde bereits aktiviert.';
                            break;
                        case 1:
                                echo '<b>Deine E-Mail Adresse wurde erfolgreich bestätigt.</b><br><br>Du kannst dich nun in Deinen Account einloggen und Syncronight nutzen. Lege gleich los und melde Dich an!';
                            break;
                        case 2:
                                $token_set = false;
                                if(isset($_GET['token']) && $_GET['token'] != null){
                                    $token_set = true;
                                    $trueUser = decrypt(urldecode($_GET['token']), "linkKEY");
                                    if(isset($_GET['retry']) && $_GET['retry'] == "true"){
                                        $mailSQL = db_select("SELECT `mail` FROM `account` WHERE `acc_ID`='".$trueUser."'");
                                        if($mailSQL != false && sizeof($mailSQL) == 1){
                                            echo "<b>Eine neue Bestätigungsmail wurde an ".$mail_address." versandt.</b><br><br>";
                                        }
                                    }
                                }

                                echo "<p>Wir haben eine E-Mail zur Bestätigung deiner Mailadresse an $mail_address gesendet.</p>";

                                echo "<p>Bitte bestätige den Link dieser E-Mail um dich bei Syncronight anzumelden!</p>";
                                echo "<div class='resend'>";
                                echo ($token_set) ? "<br><br>Hast Du noch keine Nachricht erhalten? <a href='./php/register/resend.php?token=".$_GET['token']."'>nochmal senden.</a>": null;
                                echo "</div>";
                            break;
                    }
                }else{
                    //echo 'Die Anfrage liefert keine Rückgabewerte.';
                    echo 'Bitte bestätige deine Mailadresse!';
                }

                if(isset($Info) && $Info != 2){
                    echo '<br><a href="./login"  id="loginbtn"><button class="normal_button">Anmelden</button></a>';
                }
            ?>
    </div>
  </div>


  <?php

        ?>
  <?php //include('./signed/incl/footer/foot.php'); ?>
</body>

</html>
