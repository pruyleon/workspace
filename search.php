<html lang="de">
  <head>
    <?php
      include('./src/header.php');
      include('./signed/php/gen.php');
    ?>
    <meta charset="utf-8">
    <title>Artist Suche</title>

    <link rel="stylesheet" href="/css/search/search.css">
    <link rel="stylesheet" href="/css/responsive_fontsizes.css">
    <link rel="stylesheet" href="/css/search/rapture_header.css">

    <!-- Plugins -->
    <link rel="stylesheet" href="/src/slick/slick.css">
  </head>
  <body>
    <!-- Menü -->
    <?php include('./php/search/search_menue.php'); ?>

    <!-- Headslider -->
    <div class="searchbody">
      <div class="headslider_wrapper">
        <div class="headslider_content" style="background-image: url(/src/demo/10.jpg)">
          <div class="rapture_actions">
            <div class="rapture_header">
              LAST FREE DATES FOR 2019
            </div>
            <div class="rapture_buttons">
              <button type="button" name="button" id="book_now">Book now</button>
              <button type="button" name="button" id="freedates">Free dates</button>
              <button type="button" name="button" id="more_info">More info</button>
            </div>
          </div>
        </div>
        <div class="headslider_filter">

        </div>
      </div>

      <div class="slider_body">
        <!-- Sliders klein / groß -->
        <div class="sliderheader">
          Urban Highlights
        </div>
        <div class="sliderwrapper small frist_slider">
          <div class="sliderelement small first">
            <div class="sliderelement_content" style="background-image: url(/src/demo/9.jpg)">

            </div>
            <div class="sliderelement_filter">

            </div>
            <video class="sliderelement_video" src="/src/demo/rapture.mp4" loop="loop" muted="muted"></video>
            <div class="sliderelement_infos">
              <div class="dj_name resp_font_16">
                DJ Rapture
              </div>
              <div class="dj_location resp_font_11">
                <img src="/signed/src/icns/location.svg" alt=""> Mannheim | Germany
              </div>
              <div class="dj_genres resp_font_11">
                Hip Hop &nbsp; | &nbsp; RnB &nbsp; | &nbsp; House
              </div>
            </div>
        </div>
          <?php
            for ($i=0; $i < 20; $i++) {
                $rand = rand(0, 10);
                if ($i==0) {
                    echo '<div class="sliderelement small first"><div class="sliderelement_content" style="background-image: url(/src/demo/'.$rand.'.jpg)"></div><div class="sliderelement_filter"></div><video class="sliderelement_video" src="/src/demo/rapture.mp4" loop="loop" muted="muted"></video><div class="sliderelement_infos"><div class="dj_name resp_font_16">DJ Rapture</div><div class="dj_location resp_font_11"><img src="/signed/src/icns/location.svg" alt=""> Mannheim | Germany</div><div class="dj_genres resp_font_11">Hip Hop &nbsp; | &nbsp; RnB &nbsp; | &nbsp; House</div></div></div>';
                } else {
                    echo '<div class="sliderelement small"><div class="sliderelement_content" style="background-image: url(/src/demo/'.$rand.'.jpg)"></div><div class="sliderelement_filter"></div><video class="sliderelement_video" src="/src/demo/rapture.mp4" loop="loop" muted="muted"></video><div class="sliderelement_infos"><div class="dj_name resp_font_16">DJ Rapture</div><div class="dj_location resp_font_11"><img src="/signed/src/icns/location.svg" alt=""> Mannheim | Germany</div><div class="dj_genres resp_font_11">Hip Hop &nbsp; | &nbsp; RnB &nbsp; | &nbsp; House</div></div></div>';
                }
            }
           ?>


        </div>


        <div class="sliderheader">
          International Highlights
        </div>
        <div class="sliderwrapper small">
          <?php
            for ($i=0; $i < 20; $i++) {
                $rand = rand(0, 10);
                if ($i==9) {
                    echo '<div class="sliderelement small first"><div class="sliderelement_content" style="background-image: url(/src/demo/'.$rand.'.jpg)"></div><div class="sliderelement_filter"></div><video class="sliderelement_video" src="/src/demo/rapture.mp4" loop="loop" muted="muted"></video><div class="sliderelement_infos"><div class="dj_name resp_font_16">DJ Rapture</div><div class="dj_location resp_font_11"><img src="/signed/src/icns/location.svg" alt=""> Mannheim | Germany</div><div class="dj_genres resp_font_11">Hip Hop &nbsp; | &nbsp; RnB &nbsp; | &nbsp; House</div></div></div>';
                } else {
                    echo '<div class="sliderelement small"><div class="sliderelement_content" style="background-image: url(/src/demo/'.$rand.'.jpg)"></div><div class="sliderelement_filter"></div><video class="sliderelement_video" src="/src/demo/rapture.mp4" loop="loop" muted="muted"></video><div class="sliderelement_infos"><div class="dj_name resp_font_16">DJ Rapture</div><div class="dj_location resp_font_11"><img src="/signed/src/icns/location.svg" alt=""> Mannheim | Germany</div><div class="dj_genres resp_font_11">Hip Hop &nbsp; | &nbsp; RnB &nbsp; | &nbsp; House</div></div></div>';
                }
            }
           ?>
        </div>

        <div class="sliderheader">
          Urban Highlights
        </div>
        <div class="sliderwrapper large">
          <?php
            for ($i=0; $i < 20; $i++) {
                $rand = rand(0, 10);
                if ($i==9) {
                    echo '<div class="sliderelement large first"><div class="sliderelement_content" style="background-image: url(/src/demo/'.$rand.'.jpg)"></div><div class="sliderelement_filter"></div></div>';
                } else {
                    echo '<div class="sliderelement large"><div class="sliderelement_content" style="background-image: url(/src/demo/'.$rand.'.jpg)"></div><div class="sliderelement_filter"></div></div>';
                }
            }
           ?>
        </div>
      </div>

    </div>

  </body>
  <script src="/js/search/search_functions.js" charset="utf-8"></script>
  <script src="/js/search/search.js" charset="utf-8"></script>

  <!-- Slideshow -->
  <script type="text/javascript" src="/src/slick/slick.min.js"></script>

</html>
