<?php
    include('./signed/php/login_state.php');
    checkLogin();


    require('./signed/php/render_stars.php');

    $artist_per_page = 24;

    // Buchen für Veranstalterauswahl
    $selected_event = false;
    $selected_place = false; // für PLZ
    $selected_date = false; // für Datum
    $filter_genre = [];

    if (isset($_GET['for']) && $_GET['for'] != null && $_GET['for'] != "false") {
        $possible_event_id = db_escape($_GET['for']);

        $event_qr = db_select("SELECT `event_ID`, `city`, `timestamp`, `genre` FROM `event` WHERE `event_ID`='".$possible_event_id."'");
        if ($event_qr !== false && sizeof($event_qr) == 1) {
            $selected_event = $event_qr[0]['event_ID'];

            // PLZ aus Event laden
            $possiblePLZ = explode(" ", $event_qr[0]['city'])[0];
            if (intval($possiblePLZ) == $possiblePLZ) {
                $selected_place = $possiblePLZ;
            }

            // Datum aus Event laden
            $selected_date = $event_qr[0]['timestamp'];

            // Genre aus Event laden
            $filter_genre = explode(',', $event_qr[0]['genre']);
        }
    }

    // Schauen ob PLZ gesetzt
    if (isset($_GET['plz']) && $_GET['plz'] != null && $_GET['plz'] != 0) {
        $possiblePLZ = db_escape($_GET['plz']);
        if (intval($possiblePLZ) == $possiblePLZ) {
            $selected_place = $possiblePLZ;
        }
    }

    // Schauen ob Date Gesetzt
    if (isset($_GET['date']) && $_GET['date'] != null && intval($_GET['date']) != 0) {
        $possible_date = db_escape($_GET['date']);
        if (intval($possible_date) == $possible_date && $possible_date > 1420070400) {
            $selected_date = $possible_date;
        }
    }

    $genre_qr = db_select("SELECT * FROM `genre`");
    $full_genre_array = [];
    if ($genre_qr !== false) {
        foreach ($genre_qr as $genre) {
            $full_genre_array[$genre['ID']] = $genre['genre_type'];
        }
    }
    // Schauen ob Genre Gesetzt
    if (isset($_GET['genre']) && $_GET['genre'] != null && intval($_GET['genre']) != 0) {
        $possible_genres = explode(',', db_escape($_GET['genre']));
        foreach ($possible_genres as $genre_in) {
            if (array_key_exists($genre_in, $full_genre_array)) {
                array_push($filter_genre, $genre_in);
            }
        }
    }

    // Schauen ob Suchstring gesetzt
    $search_str = false;
    if (isset($_GET['search']) && $_GET['search'] != null && $_GET['search'] != "false") {
        $search_str = db_escape($_GET['search']);
    }



    // Sortierungsmethode
    $sort_str_arr = [
        ["name", ["name", "ASC"], "Künstlername"],
        ["rating", [false, "DESC"], "Bewertung"],
        ["popular", [false, "DESC"], "Beliebtheit"],
        ["experience", ["experience", "DESC"], "Erfahrung"],
        ["price_asc", ["min_price", "ASC"], "Preis aufsteigend"],
        ["price_desc",["min_price", "DESC"], "Preis absteigend"],
        ["place", [false, "ASC"], "Distanz"]
    ];

    $sorting=$sort_str_arr[2]; // standartwert noch setzten

    if (isset($_GET['sort']) && $_GET['sort'] != null && $_GET['sort'] != "false") {
        $possible_sorting = db_escape($_GET['sort']);

        foreach ($sort_str_arr as $sort_info) {
            if ($sort_info[0] == $possible_sorting) {
                if ($sort_info[0] != "place" || ($sort_info[0] == "place" && $selected_place != false)) {
                    $sorting = $sort_info;
                    break;
                }
            }
        }
    }

    // Künstler anzeigen
    $sql_insert = "";
    if ($sorting != false) {
        if ($sorting[1][0] != false) {
            $sql_insert = " ORDER BY `".$sorting[1][0]."` ".$sorting[1][1];
        }
    }

    $sql_str = "SELECT * FROM `artists` WHERE `state`='1'".$sql_insert;
    $full_arr = db_select($sql_str);

    if ($full_arr !== false && sizeof($full_arr > 0)) {
        $filtered_array = [];

        // Filter anweden
        foreach ($full_arr as $snglArt) {
            $add_artist_trigger = true;
//            echo "artist: ".$snglArt['name']."\n";

            //  -> Datum
            if ($selected_date != false) {
                // schauen ob Artist an diesem Datum bereits gebucht
                $booking_entry = db_select("SELECT `event_ID` FROM `bookings` WHERE `artist_ID`='".$snglArt['artist_ID']."'");
                if ($booking_entry !== false && sizeof($booking_entry) > 0) {
                    foreach ($booking_entry as $booking) {
                        $stamp_entry = db_select("SELECT `timestamp` FROM `event` WHERE `event_ID`='".$booking['event_ID']."'");
                        if ($stamp_entry !== false && sizeof($stamp_entry) == 1) {
//                            echo date('d.m.Y', $selected_date).' -> '.date('d.m.Y', $stamp_entry[0]['timestamp'])."\n";

                            if (date('d.m.Y', $selected_date) == date('d.m.Y', $stamp_entry[0]['timestamp'])) {
                                // Veranstaltungsdatum schneided sich mit vorherigem booking_Datum
                                // -> Künstler bereits gebucht
//                               echo $snglArt['name']." durch Datum aussortiert \n";
                                $add_artist_trigger = false;
                            }
                        }
                    }
                }
            }

            //  -> Genre
            if (sizeof($filter_genre) > 0) {
                $artist_genre = explode(',', $snglArt['genre']);
                $atleastone = false;
                foreach ($filter_genre as $sngl_filter_genre) {
                    if (in_array($sngl_filter_genre, $artist_genre)) {
                        $atleastone = true;
                    }
                }
                if ($atleastone == false) {
//                    echo $snglArt['name']." durch Genre aussortiert \n";
                    $add_artist_trigger = false;
                }
            }

            //  -> Suche
            if ($search_str != false) {
                // wenn Artistname nix mit suchstr zutun  hat -> add_artist_trigger = false
                // Suchstr an Leerzeichen teilen
                $hit = false;


                if (strpos($search_str, " ") != false) {
                    $split_search = explode($search_str, " ");
                } else {
                    $split_search = [$search_str];
                }

                $artist_name = replace_Umlaute($snglArt['name']);

                foreach ($split_search as $sub_search_str) {

                    // schauen ob Artistname größer als Suchstr
                    if (strlen($artist_name) > strlen($sub_search_str)) {
                        $sub_elm = strtolower($sub_search_str);
                        $main_elm = strtolower($artist_name);
                    } else {
                        $sub_elm = strtolower($artist_name);
                        $main_elm = strtolower($sub_search_str);
                    }

                    $loops = strlen($main_elm) - strlen($sub_elm) +1;
                    for ($z = 0; $z<$loops; $z++) {
                        $sub_match = substr($main_elm, $z, strlen($sub_elm));

                        if ($sub_match == $sub_elm) {
                            // Suchstr entspricht Künstlername irgendwo
                            $hit = true;
                        }
                    }
                }

                if ($hit == false) {
                    $add_artist_trigger = false;
                }
            }

            // |->  Aussortierten
            if ($add_artist_trigger == true) {
                array_push($filtered_array, $snglArt);
            }

//            echo "\n";
        }


        if ($sorting[1][0] == false) {
            // besondere Sortiermethode
            switch ($sorting[0]) {
                case "rating":
                        foreach ($filtered_array as $i => $artist) {
                            $star_summ = floatval(getAverageSumm($artist['rating'])[0]);
                            $added_sort_ind = "rating_sum";
                            $filtered_array[$i][$added_sort_ind] = $star_summ;
                        }

                    break;
                case "popular":
                        foreach ($filtered_array as $i => $artist) {
                            $book_count = db_select("SELECT COUNT(`artist_ID`) FROM `bookings` WHERE `artist_ID`='".$artist['artist_ID']."'");
                            $added_sort_ind = "popular";

                            if ($book_count !== false && sizeof($book_count) == 1) {
                                $popularity = $book_count[0]['COUNT(`artist_ID`)'];
                            } else {
                                $popularity = 0;
                            }

                            $filtered_array[$i][$added_sort_ind] = $popularity;
                        }
                    break;
                case "place":
                        if ($selected_place != false) {
                            $search_point = get_coord($selected_place);
                            ;
                            foreach ($filtered_array as $i => $artist) {
                                $artist_coord = get_coord($artist['plz']);

                                $order_distance = calc_coord_distance($search_point, $artist_coord);

                                $added_sort_ind = "distance";
                                $filtered_array[$i][$added_sort_ind] = floatval($order_distance);
                            }
                        }

                    break;
                default: $added_sort_ind = "name";
                    break;
            }

            $sort_line = [];
            foreach ($filtered_array as $one_line) {
                array_push($sort_line, $one_line[$added_sort_ind]);
            }

            $sort_str = ($sorting[1][1] == "ASC") ? SORT_ASC : SORT_DESC;
            array_multisort($sort_line, $sort_str, $filtered_array);
        }
    }

function get_coord($plz)
{
    $plz_mark = db_select("SELECT * FROM `plz_de` WHERE `plz`='".$plz."'");
    if ($plz_mark === false || sizeof($plz_mark) == 0) {
        return false;
    }

    if (sizeof($plz_mark) > 1) {
        $lng_base = 0;
        $lat_base = 0;
        foreach ($plz_mark as $matchedPLZ) {
            $lng_base += $matchedPLZ['longitude'];
            $lat_base += $matchedPLZ['latitude'];
        }
        $lng_base = $lng_base/sizeof($plz_mark);
        $lat_base = $lat_base/sizeof($plz_mark);
    } else {
        $lng_base = $plz_mark[0]["longitude"];
        $lat_base = $plz_mark[0]["latitude"];
    }

    $lng = $lng_base / 180 * M_PI;
    $lat = $lat_base / 180 * M_PI;

    return [$lng, $lat];
}

function calc_coord_distance($coord_1, $coord_2)
{
    $EarthRadius = 6371000; // radius in meters
    $phi1 = deg2rad($coord_1[1]);
    $phi2 = deg2rad($coord_2[1]);
    $deltaLat = deg2rad($coord_2[1] - $coord_1[1]);
    $deltaLon = deg2rad($coord_2[2] - $coord_1[2]);

    $a = sin($deltaLat/2) * sin($deltaLat/2) + cos($phi1) * cos($phi2) * sin($deltaLon / 2) * sin($deltaLon / 2);
    $c = 2 * atan2(sqrt($a), sqrt(1 - $a));

    $result = $EarthRadius * $c;

    return $result;
}

function replace_Umlaute($instr)
{
    $umlaute = array("ä", "ö", "ü","Ä","Ö","Ü","ß");
    $doppelbuchstaben = array("ae", "oe", "ue","AE","OE","UE","ss");
    return str_replace($umlaute, $doppelbuchstaben, $instr);
}


// Pages
$artist_count = sizeof($filtered_array);
$max_pages = ceil($artist_count/$artist_per_page);

if (isset($_GET['page']) && $_GET['page'] != "" && intval($_GET['page']) == $_GET['page']) {
    $possible_page = db_escape($_GET['page']);
    $page = ($possible_page < $max_pages) ? $possible_page : $max_pages-1;
} else {
    $page = 0;
}

$start_number = $page*$artist_per_page;
$end_number = ($start_number + $artist_per_page > $artist_count) ? $artist_count : ($page+1)*$artist_per_page;

?>
<html>

<head>
  <base href="/" />
  <?php include('./src/header.php'); ?>
  <title>Übersicht Künstler - Starbook</title>

  <link type="text/css" rel="stylesheet" href="./css/frontend/artist_list.css" />
  <script type="text/javascript" src="./js/frontend/artist_list.js"></script>

</head>

<body>
  <?php include('./signed/incl/menue/navigation.php'); ?>

  <div id="header" class="header_line">
    <div class="register_layout">
      <h1>Best artists for best partys</h1>
    </div>
  </div>

  <div class="register_layout space">
    <div id="top_line">
      <div onclick="toggleFilter();" id="show_btn">Filter einblenden</div>

      <div id="sort_drop" class="dropdown">
        <font id="sort_label">sortieren nach</font>
        <div class="dropdown_selection" onclick="toggleDropdown(this);">
          <font class="drop_label"><?php echo $sorting[2]; ?></font>
          <img src="./signed/src/icns/dropdown.svg" class="arrow_down" />
        </div>

        <div class="dropdown_window" state="hidden">
          <?php
            foreach ($sort_str_arr as $opt_sort) {
                $op_zs = ($opt_sort[0] == $sorting[0]) ? " selected" : '';

                if ($opt_sort[0] != "place" || ($opt_sort[0] == "place" && $selected_place != false)) {
                    $link = "../artist/overview/page=0/";
                    $link .= "event=";
                    $link .= ($selected_event == false) ? "false" : $selected_event;
                    $link .= "/sort=".$opt_sort[0]."/";
                    $link .= "date=";
                    $link .= ($selected_date == false) ? "0" : $selected_date;
                    $link .= "/plz=";
                    $link .= ($selected_place == false) ? "0" : $selected_place;
                    $link .= "/genre=";
                    $link .= ($filter_genre == false) ? "0" : implode(',', $filter_genre);
                    $link .= ($search_str != false && strlen($search_str) > 0) ? "/search=".$search_str : "";

                    echo '<a class="dropdown_option'.$op_zs.'" href="'.$link.'">
                                            <font class="drop_label">'.$opt_sort[2].'</font>
                                        </a>';
                }
            }
                        ?>
        </div>
      </div>

      <?php
                    $formlink = "./artist/overview";
                    $formlink .= ($selected_event != false) ? "/event=".$selected_event : '';
                ?>
      <form id="searchinput" onsubmit="return new_search();">
        <font id="search_label">Nach Künstlern suchen</font>
        <input type="text" id="search_input" value="<?php echo ($search_str != false) ? $search_str : ''; ?>" maxlength="30" />
        <img src="./src/icns/search.svg" onclick="new_search();" />
      </form>

    </div>

    <div id="toggle_box" state="hidden">

      <div id="input_layout">
        <!--                UMKREISSUCHE    -->
        <input type="hidden" id="sorting_constr" value="<?php echo $sorting[0]; ?>" />
        <input type="hidden" id="event_selection" value="<?php echo $selected_event; ?>" />
        <input type="hidden" id="search_input_hidden" value="<?php echo $search_str; ?>" />

        <div class="input_desc space_small">Sortierung nach Entfernung</div>

        <?php
          if ($selected_place == false) {
              $constr_edit_mode = false;
              $pre_PLZ = $pre_PLACE = "";
          } else {
              $constr_edit_mode = true;
              $pre_PLZ = $selected_place;
              $pre_PLACE = "";
          }
          $outlink = true;

          include('./signed/incl/place_search/place.php');

          echo ($selected_place != false) ? '<script> fillPlz('.$pre_PLZ.');</script>' : '';
        ?>

        <!--                DATUM       -->
        <div class="input_desc space_small">An diesem Datum nur noch nicht gebuchte Künstler anzeigen</div>
        <?php
            if ($selected_date == false) {
                $constr_edit_mode = false;
                $constr_date = "";
            } else {
                $constr_edit_mode = true;
                $constr_date = $selected_date;
            }
            $outlink = true;
            include('./signed/incl/date_input/date.php');
          ?>
        <!--                GENRE       -->
        <div class="input_desc space_small">Genre eingrenzen</div>
        <?php
          if (sizeof($filter_genre) == 0) {
              $constr_edit_mode = false;
              $pre_selected_genres = "";
          } else {
              $constr_edit_mode = true;
              $pre_selected_genres = implode(',', $filter_genre);
          }
          $outlink = true;
          include('./signed/incl/genre_search/genre.php');
        ?>
      </div>

      <div id="button_layout">
        <div class="space add_btn" title="Filter anwenden" onclick="gen_filter_link()" id="apply_filter">
          <img src="./signed/src/icns/settings.svg" alt="Filter anwenden" />
          Filter anwenden
        </div>

        <div class="space_small remove_btn" title="Filter zurücksetzen" onclick="reset_filter_link();">
          <img src="./signed/src/icns/remove_black.svg" alt="Filter zurücksetzen" />
          Filter zurücksetzen
        </div>

      </div>
    </div>
  </div>

  <div class="register_layout space" id="artist_grid">
    <?php
      if (sizeof($filtered_array) > 0) {
          for ($i = $start_number; $i < $end_number; $i++) {
              $artist = $filtered_array[$i];

              echo '<div class="artist_block space">';
              $artist_pic_link = "./signed/src/user/".$artist['acc_ID']."/artists/".$artist['artist_ID']."/profile_500.jpg";
              if (!is_file($artist_pic_link)) {
                  $artist_pic_link = "./src/pic/artist_default_profile.jpg";
              }

              echo    '<img class="artist_pic" preload="'.$artist_pic_link.'" />';

              $genreStr = "";
              $genre_exp = explode(',', $artist['genre']);
              foreach ($genre_exp as $snglGen) {
                  if (array_key_exists($snglGen, $full_genre_array)) {
                      $genreStr.= $full_genre_array[$snglGen].', ';
                  }
              }
              $genreStr = substr($genreStr, 0, (strlen($genreStr) -2));

              $artist_place_str = ($sorting[0] == "place") ? $artist['place']." (".number_format($artist['distance'], 2, ",", ".")." Km)" : $artist['place'];

              echo '<div class="artist_info">
                            <h6>'.$artist['name'].'</h6>'
                            .renderStars(getAverageSumm($artist['rating'])[0], 20, null, 'star_user').
                            '<font>'.$artist_place_str.'<br>'.$genreStr.'<br>ab '.number_format($artist['min_price'], 2, ",", ".").' €</font>';

              $artist_link = ($artist['call_url'] == "") ? "./artist/".$artist['artist_ID'] : "./".$artist['call_url'];

              $artist_link .= ($selected_event != false) ? "/book=".$selected_event : "";

              echo    '<a href="'.$artist_link.'" class="button neutral seeProfile space">Profil ansehen</a>';
              echo '</div>';

              echo '</div>';
          }
      } else {
          echo '<div id="no_result">Es konnten keine Künstler mit den aktuellen Sucheinstellungen gefunden werden.</div>';
      }
    ?>
  </div>

  <div class="register_layout space_enormous">
    <div id="site_buttons">
      <?php
          $link = "/event=";
          $link .= ($selected_event == false) ? "false" : $selected_event;
          $link .= "/sort=".$opt_sort[0]."/";
          $link .= "date=";
          $link .= ($selected_date == false) ? "0" : $selected_date;
          $link .= "/plz=";
          $link .= ($selected_place == false) ? "0" : $selected_place;
          $link .= "/genre=";
          $link .= ($filter_genre == false) ? "0" : implode(',', $filter_genre);
          $link .= ($search_str != false && strlen($search_str) > 0) ? "/search=".$search_str : "";

          if ($page > 0) {
              echo '<a class="site_btn button neutral" href="../artist/overview/page='.($page-1).$link.'">Zurück</a>';
          } else {
              echo '<div class="btn_space"></div>';
          }

          echo '<div id="site_state">Seite <b>'.($page+1).'</b> / '.$max_pages.'</div>';

          if ($page < $max_pages-1) {
              echo '<a class="site_btn button neutral" href="../artist/overview/page='.($page+1).$link.'">Weiter</a>';
          } else {
              echo '<div class="btn_space"></div>';
          }
        ?>
    </div>
  </div>


  <?php include('./signed/incl/footer/foot.php'); ?>
</body>

</html>
