<?php
include('./signed/php/login_state.php');
checkLogin();
?>
<html>
    <head>
        <base href="/" />
        <title>Error 404 - mypressk.it</title>

        <link type="text/css" rel="stylesheet" href="./css/frontend/error.css" />
    </head>

    <body>
        <div id="errorMsg" class="Master_wrapper_error">
            <div>
                <h1>Error 404 - Seite nicht gefunden</h1>
                <p>Tut uns leid, aber diese Seite ist wohl in den Sternen verloren gegangen...</p>
                <a class="button neutral space" href="./home">Zurück zur Startseite</a>
            </div>
        </div>

    </body>
</html>
