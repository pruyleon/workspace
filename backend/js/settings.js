
var inputName = ["vorname", "nachname", "mail"];
function checkInput(){
    // Allgemeine informationen checken
    
    var errStr = [];
    var s = 0;
    var trigger = false;
    
    while(inputName[s]){
        document.getElementById(inputName[s]).style.border = "solid 2px #C8C8C8";
        
        var txtVal = document.getElementById(inputName[s]).value.trim();
        if(txtVal == "" && (inputName[s] == "vorname" || inputName[s] == "nachname" || inputName[s] == "mail")){
            
            if(!trigger){
                trigger = true;
                errStr.push("Bitte füllen Sie alle Felder vollständig aus."); 
            }
            mark(inputName[s]);
            
        }else{
            // Name
            if(inputName[s] == "vorname" ||inputName[s] == "nachname"){
                var nameReg = /^[\u00C0-\u017Fa-zA-Z][\u00C0-\u017Fa-zA-Z- ]+[\u00C0-\u017Fa-zA-Z]$/;
                if(!txtVal.match(nameReg)){
                    errStr.push("Der eingegebene "+inputName[s].charAt(0).toUpperCase()+inputName[s].substr(1)+" enthält ungültige Zeichen.");
                    mark(inputName[s]);
                }
            }
            
            // Email
            if(inputName[s] == "mail"){
                if(txtVal.indexOf('@') < 0 || txtVal.indexOf('.') < 0 || txtVal.indexOf(' ') > -1){
                    errStr.push("Bitte geben Sie eine gültige E-Mailadresse an.");
                    mark(inputName[s]);
                }
                
                if(checkMailPW == true){
                    // Mail abgeändert
                    document.getElementById('mail_acces').style.border = "solid 2px #C8C8C8";
                    document.getElementById('showMailVary').style.border = "none";

                    var veryPW = document.getElementById('mail_acces').value;
                    if(veryPW == ""){
                         errStr.push("Bitte gib dein aktuelles Passwort ein um die E-Mail Adresse zu ändern.");
                        mark('mail_acces');
                        mark('showMailVary');
                    }else if(veryPW.length < 6){
                        errStr.push("Das angegebenen Passwort ist ungültig.");
                        mark('mail_acces');
                        mark('showMailVary');
                    }
                }
            }

        }
        s++;
    }
   
    // Passwörter checken
    document.getElementById('pw_new').style.border = "solid 2px #C8C8C8";
    document.getElementById('pw_new2').style.border = "solid 2px #C8C8C8";
    document.getElementById('pw').style.border = "solid 2px #C8C8C8";
    
    var pw_n1 = document.getElementById('pw_new').value;
    var pw_n2 = document.getElementById('pw_new2').value;
    var pw_old = document.getElementById('pw').value;
    
    if(pw_n1 != ""){
        if(pw_n1.length < 6){
           errStr.push("Das Passwort muss mindestens 6 Zeichen lang sein.");
           mark('pw_new');
        }else if(txtVal.length > 50){
            errStr.push("Das Passwort darf maximal 50 Zeichen lang sein.");
            mark('pw_new');
        }

        if(pw_n1 != pw_n2){
            errStr.push("Die Passwörter stimmen nicht überein.");
            mark('pw_new');
            mark('pw_new2');
        }else if(pw_old == ""){
            errStr.push("Bitte geben Sie ihr bisheriges Passwort zur Bestätigung ein.");
            mark('pw');
        }
    }
    
    // Bild checken
    document.getElementById('img_block').style.border = "none";
    if(allow == false){
        errStr.push("Das ausgewählte Bild Dateiformat ist leider nicht zulässig.");
        mark('img_block');
    }else{
        
        if(document.getElementById('picSelector').value != ""){
            if(currImgPX[0] > 2048 || currImgPX[1] > 2048){
                errStr.push("Die Auflösung des Bildes darf nicht mehr als 2048x2048px betragen.");
                mark('img_block');
            }
            var filesize = (document.getElementById('picSelector').files[0].size / 1024 / 1024).toFixed(2);
            
            if(filesize > 3){
                 errStr.push("Das ausgewählte Bild ist zu groß. ("+filesize+"MB / 3MB)");
                mark('img_block');
            }
        }
        
    }
    
    // Ausführen
    if(errStr.length == 0){
        return true;
    }else{
        document.getElementById('msger').setAttribute('style', '');
        document.getElementById('msger').innerHTML = "- "+errStr.join("<br>- ");
        document.getElementById('msger').style.display = "table";
        scrolltoreg('msger');
        return false;
    }
    
}

function mark(indexer){
    document.getElementById(indexer).style.borderBottom = "solid 2px #FF7D82";
}

var checkMailPW = false;

function alterMail(){
    var newMail = String(document.getElementById('mail').value);
    var oldMail = String(document.getElementById('showMailVary').getAttribute('org'));
    if(newMail == oldMail){
        checkMailPW = false;
        document.getElementById('showMailVary').style.display = "none";
    }else{
        checkMailPW = true;
        document.getElementById('showMailVary').style.display = "block";
    }
    
}

var allow = true;

function checkFile(inputF){
    
    if(inputF.files.length == 0){
        document.getElementById('picLabel').innerHTML = "Accountbild auswählen";
        document.getElementById('picLabel').className = "button neutral";
        
        document.getElementById('profile_picture').style.backgroundImage = "url('"+profileIMG_path+"')";
        allow = true;
        
        inputF.value = null;
           if(inputF.value){
            inputF.type = "text";
            inputF.type = "file";
        }

        return;
    }
    
    var zulaessig = [".jpg", ".jpeg", ".JPG", ".JPEG", ".png", ".PNG", ".gif", ".GIF"];
	
	if(validateFile(inputF, zulaessig)){
		
		console.log("datei zugelassen");
		allow = true;
        
         // Vorschaubild laden
        var reader = new FileReader();
        reader.onload = profileLoaded;
        reader.readAsDataURL(inputF.files[0]);

        document.getElementById('profile_picture').style.backgroundImage = "url('../src/pic/loading.svg')";
        document.getElementById('picLabel').innerHTML = "Bild ausgewählt...";
        document.getElementById('picLabel').className = "button positiv";
        
        // Löschenbutton wieder einblenden
        document.getElementById('del_btn_img').setAttribute('state', 'false');
        document.getElementById('del_btn_img').style.display = "block";
        document.getElementById('flag_del').checked = false;
        
		
	}else{

        console.log("Datei abgelehnt")

        document.getElementById('picLabel').innerHTML = "keine gültige Datei";
        document.getElementById('picLabel').className = "button negativ";

        allow = false;
    
        
        inputF.value = null;
           if(inputF.value){
            inputF.type = "text";
            inputF.type = "file";
        }
        
        if(!remove_image_entry){
            document.getElementById('profile_picture').style.backgroundImage = "url('"+profileIMG_path+"')";
        }else{
            document.getElementById('profile_picture').style.backgroundImage = "url('../src/pic/default_profile.jpg')";

        }

	}
}

function validateFile(sender, allowForm ) {
    var validExts = allowForm;
	
    var fileExt = sender.value;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      return false;
    }
    else return true;
}

var currImgPX = [0, 0];

function profileLoaded(e){     
    var image = new Image();    
    image.onload = function() {
        currImgPX[0] = this.width;
        currImgPX[1] = this.height;
        console.log(currImgPX);
    }
    image.src = e.target.result;

            
    document.getElementById('profile_picture').style.backgroundImage = "url("+e.target.result+")";
}

// Remove File
var remove_image_entry = false;

function flag_img(){
    var elm = document.getElementById('del_btn_img');
    
    if(elm.getAttribute('state') == "false"){
        elm.setAttribute('state', 'true');
        remove_image_entry = true;
        
        // Bild aus Auswahl löschen
        var fileSelector = document.getElementById('picSelector');
        fileSelector.value = null;
           if(fileSelector.value){
            fileSelector.type = "text";
            fileSelector.type = "file";
        }
        checkFile(fileSelector);
        
        elm.style.display = "none";
        document.getElementById('profile_picture').style.backgroundImage = "url('../src/pic/default_profile.jpg')";
    }
}