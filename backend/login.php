<?php
include('./php/login_state.php');
checkLogin();

    $prefill = "";

    if(isset($_GET['token']) && $_GET['token'] != null){
        $userCode = urldecode(strip_tags($_GET['token']));
        $userID = decrypt($userCode, "linkKEY");

        if(db_check($userID, 'acc_ID') == true){
            $userData =db_select("SELECT `mail` FROM `backend` WHERE `acc_ID`='".$userID."'");
            if($userData != false && isset($userData[0]['mail'])){
                $prefill = $userData[0]['mail'];

                makeCookie("admin", "fill", $prefill);
            }
        }
    }

    if($prefill == "" && isset($_COOKIE['admin']['fill']) && $_COOKIE['admin']['fill'] != null){
        $prefill = $_COOKIE['admin']['fill'];
    }
?>

<html>

    <head>
        <base href="/backend/" />
        <?php include('../src/header.php'); ?>
        <title>Backend - Starbook ADMIN</title>
        <link type="text/css" rel="stylesheet" href="../css/frontend/login.css" />
        <script type="text/javascript" src="./js/login.js"> </script>
    </head>

    <body>
        <?php //include('./inc/mini_menu/menu.php'); ?>

        <div class="header_line backend">
            <h1 id="login_headline">Backend anmelden</h1>

            <form id="loginform" action="./php/sign_in.php" method="POST" onsubmit="return checkInput();">

                <div class="message_negativ" id="errNote">
                <?php
                    $msg = [
                        "Ein technischer Fehler ist aufgetreten.",
                        "Die angegebenen E-Mail Adresse existiert nicht.",
                        "Das eingegebene Passwort ist falsch.",
                        "Die aktuelle Sitzung ist abgelaufen",
                        "Erfolgreich abgemeldet",
                        "Eine Mail zum zurücksetzten des Passworts wurde soeben versandt.",
                        "Dein Passwort wurde erfolgreich geändert.",
                        "Der Link ist bereits abgelaufen.",
                        "Bitte bestätige deine E-Mail-Adresse vor dem Login.",
                        "Dir Fehlen die nötigen Berechtigungen für diesen Bereich. Bitte melde dich erneut an.",
                        "Die angegebene Mailadresse ist bereits registriert. Bitte melde dich hier an."
                    ];

                    if(isset($_GET['re']) && $_GET['re'] != null){

                        if(isset($msg[strip_tags($_GET['re'])])){
                            echo $msg[strip_tags($_GET['re'])];
                            $showMSG = true;
                        }
                    }
                ?>
                </div>
                <?php
                    if(isset($showMSG) && $showMSG){
                        echo '<style type="text/css" rel="stylesheet">#errNote{display:block;}</style>';
                    }

                     if(isset($_GET['re']) && $_GET['re'] == 8){

                         if(isset($userCode)){
                            $userToken = $userCode;
                         }else{
                            if(isset($_COOKIE['admin']['fill']) && $_COOKIE['admin']['fill'] != null){
                                $userMail = urldecode($_COOKIE['admin']['fill']);
                                $db_return = db_select("SELECT `acc_ID` FROM `backend` WHERE `mail`='".$userMail."'");
                                if($db_return != false && sizeof($db_return) == 1){
                                    $userToken = encrypt(urlencode($db_return[0]['acc_ID']), "linkKEY");
                                }
                            }
                         }
                         echo isset($userToken) ? '<div id="resendMail">Bestätigungsmail <a href="./php/resend.php?token='.$userToken.'&second=true">erneut senden</a></div>' : '';
                     }
                ?>



                <input type="text" placeholder="E-Mail Adresse" name="usermail" id="usermail" value="<?php echo $prefill; ?>"/>
                <input type="password" placeholder="Passwort" name="userpw" id="userpw"/>
                <a href="./reset" id="new_pw" class="space_small">Passwort vergessen?</a>

                <input type="checkbox" id="remember" name="remember" onchange="setRemeber()" value="0" checked/>
                <label for="remember" id="remember_label" state="true" class="space_small">
                    <img src="../src/icns/checked_white.svg" />Angemeldet bleiben
                </label>


                <input type="submit" class="button neutral space" id="SbmBtn" value="Anmelden" />
            </form>

        </div>

        <?php include('../signed/incl/footer/foot.php'); ?>
    </body>

</html>
