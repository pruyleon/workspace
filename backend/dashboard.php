<?php
include('./php/login_state.php');
only_logged();
?>
<html>
    <head>
        <base href="/backend/" />
        <?php 
            include('../src/header.php'); 
            echo '<title>'.USER_NAME[0]."'s Adminbereich - Starbook ADMIN</title>";
        ?>
        
        <link type="text/css" rel="stylesheet" href="./css/dashboard.css"/>
    </head>
    
    <body>
        <?php include('./inc/mini_menu/menu.php'); ?>
        
        <div class="header_line backend">
            <div class="dashboard_layout">
                <h1>Backend</h1>
                <h3>Erfolgreich im Backend angemeldet</h3>
            </div>
        </div>
        
        <div class="dashboard_layout space_enormous">
            <a class="button neutral" href="./add_account.php">Admin-Account hinzufügen</a>
            <a class="button neutral" href="./settings">Accounteinstellungen</a>
        </div>
        
        
        <?php include('../signed/incl/footer/foot.php'); ?>  
    </body>
</html>