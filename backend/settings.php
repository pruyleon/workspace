<?php
include('./php/login_state.php');
only_logged();

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

?>
<html>
    <head>
        <base href="/backend/" />
        <?php
            include('../src/header.php'); 
            echo '<title>'.USER_NAME[0]."'s Admin - Starbook ADMIN</title>";
        ?>
        <link type="text/css" rel="stylesheet" href="../signed/css/settings/account.css"/>
        <link type="text/css" rel="stylesheet" href="../signed/css/settings/gen.css"/>
        
        <script type="text/javascript" src="./js/settings.js"></script>
    </head>
    
    <body>
        <?php include('./inc/mini_menu/menu.php'); ?>
            
        <div class="header_line">
            <h1 class="account_layout">Einstellungen</h1>
        </div>
                
        <form method="post" action="./php/settings.php" onsubmit="return checkInput();" id="update_form" enctype="multipart/form-data" class="account_layout space_big">
            
            <a class="button neutral" href="../monitor">Zurück zur Übersicht</a>

            
            <h2>Admin-Accounteinstellungen</h2>
            
            <input type="hidden" name="MAX_FILE_SIZE" value="52428800" />
            
            <div id="left">
                <div id="img_block">
                    <?php
                    $profileLink = "./src/".USER.".jpg";
                    if(!is_file($profileLink)){
                        $profileLink = "../src/pic/default_profile.jpg";
                    }
                    echo '<script>var profileIMG_path="'.$profileLink.'";</script>';
                    echo '<div style="background-image:url('.$profileLink.');" id="profile_picture" alt="dein Accountbild" ></div>';
                    ?>
                    
                    <input type="checkbox" id="flag_del" name="flag_del" onchange="flag_img()"/>
                    <label for="flag_del" id="del_btn_img" title="Bild entfernen" state="false">
                        <img src="../signed/src/icns/remove.svg"/>
                    </label>
                </div>
                
                <div id="pic_lowdown">
                    <input type="file" accept="image/gif, image/jpg, image/jpeg, image/png" onchange="checkFile(this);" id="picSelector" name="picSelector"/>
                    <label for="picSelector" class="button neutral" id="picLabel">
                    Accountbild auswählen</label>
                    
                    Maximale Auflösung: <b>2048x2048px</b><br>
                    Maximale Dateigröße: <b>3 Megabyte</b><br><br>
                    Unterstützte Dateiformate: <br><b>JPG, JPEG, GIF, PNG</b>
                </div>
            </div>
            
            <div id="right">
                <?php
                if(isset($_GET['re'])){
                    $ermsg = $_GET['re'];
                    $feedback = [
                        "Die Einstellungen wurden erfolgreich gespeichert.",
                        "Die Einstellungen konnten nicht übernommen werden.",
                        "Das Passwort stimmt leider nicht. Die E-Mail Adresse wurde nicht geändert.",
                        "Die Passwörter stimmen nicht überein. Das Passwort konnte nicht geändert werden.",
                        "Das Passwort stimmt leider nicht. Das neue Passwort konnte nicht übernommen werden.",
                        "Das neue Passwort entspricht deinem aktuellen Passwort. Es wurden keine Änderungen vorgenommen.",
                        "Die neue E-Mail Adresse ist bereits vergeben."
                    ];
                    
                    $styleADD = 'style="display:block;';
                    if($ermsg == 0) $styleADD .= " background-color: #9BDCAA;";
                    $styleADD .= '"';
                    
                     $exOutput = $feedback[$ermsg];
                }else{
                    $exOutput = $styleADD = "";
                }
               
                echo '<div class="message_negativ" id="msger" '.$styleADD.'>'.$exOutput.'</div>';
   
                // Daten aus DB laden
                    $userData = db_select("SELECT `mail` FROM `backend` WHERE `acc_ID`='".USER."'");
                    if($userData == false || sizeof($userData) > 1){
                        echo "Ein schwerwiegender Fehler ist aufgetreten...";
                        exit();
                    }else{
                        $user_mail = html_entity_decode($userData[0]['mail']);
                    }
                ?>
                
                <div class="input_head">Allgemeine Informationen</div>

                <div class="input_desc space_small">Name und Nachname</div>
                <input type="text" placeholder="Vorname" id="vorname" name="vorname" value="<?php echo USER_NAME[0]; ?>" />
                <input type="text" placeholder="Nachname" id="nachname" name="nachname" value="<?php echo USER_NAME[1]; ?>"/>
                
                <div class="input_desc space_small">Kontaktinformationen</div>
                <input type="email" placeholder="E-Mail Adresse" id="mail" name="mail" value="<?php echo $user_mail; ?>" onInput="alterMail()"/>
                <div id="showMailVary" org="<?php echo $user_mail; ?>">
                    <div class="input_desc">Die Bestätigung des Passworts ist notwendig für die Änderung der E-Mail Adresse.</div>
                    <input type="password" placeholder="Passwort" id="mail_acces" name="mail_acces"/>
                    
                </div>

                <div class="input_head space">Passwort ändern</div>
                <div class="input_desc space_small">neues Passwort eingeben</div>
                <input type="password" placeholder="neues Passwort" id="pw_new" name="pw_new" />
                <input type="password" placeholder="Passwort wiederholen" id="pw_new2" name="pw_new2" />
                <div class="input_desc space_small">bisheriges Passwort eingeben</div>
                <input type="password" placeholder="altes Passwort" id="pw" name="pw" />
                
                <input type="submit" value="Änderungen speichern" class="button button_right neutral space" id="submitForm"/>
            </div>
            
        </form>
        
        <?php include('../signed/incl/footer/foot.php'); ?>  
    </body> 
</html>