<?php
if(isset($_GET['token']) && $_GET['token'] != null){
    // Bestätigungsmail erneut senden
    $linkToken = $_GET['token'];
    require('../../php/crypt.php');
    $userID = decrypt(urldecode($linkToken), "linkKEY");
    $mailToken = urlencode(encrypt($userID, "mailKEY"));
    
    // Kontaktdaten herausfinden
    include('../../php/sql.php');
    if(db_check($userID, 'acc_ID')){
        $userData = db_select("SELECT `prename`, `mail`, `mail_active` FROM `backend` WHERE `acc_ID`='".$userID."'");
        if($userData != false && sizeof($userData) == 1){
            $preName = $userData[0]['prename'];
            $userMail = $userData[0]['mail'];
            
            if($userData[0]['mail_active'] == 1){
                // Account wurde bereits aktiviert
                // -> Mailsend abbrechen
                header('Location: ../../backend/mail/0');
                exit();
            }
        }else{
            tostart();
            return;
        }
    }else{
        tostart();
        return;
    }
}else{
    tostart();
    return;
}

// PFAD AUS INI laden
$iniPath = '../../php/path.ini';
if(file_exists($iniPath)){
    $iniContent = parse_ini_file($iniPath);
    $mainURL = $iniContent['HostName'];
    $secure = $iniContent['Secure'] == true ? "https://" : "http://";
}else{
    tostart();
    return;
}

$msg_firstline = (isset($_GET['second']) && $_GET['second'] == "true") ? "k" : "herzlich willkommen im Starbook-Backend! K";

$msg = $msg_firstline.'licke auf den untenstehenden Link, um Deine E-Mail Adresse zu bestätigen und den Admin-Account einzurichten.

<a href="'.$secure.$mainURL.'/backend/php/verify_mail.php?validate='.$mailToken.'" style="padding: 5px 15px;background-color: #BEA5FF;display:table;margin: 0 auto;color: #FFFFFF;text-decoration: none;">E-Mail Adresse bestätigen</a>';

require_once('../../php/mail/mail.php');
if(send_mail($userMail, $preName, "E-Mail Adresse bestätigen", $msg) == false){
    // Fehlerseite weiterleiten
    tostart();
    return;
}else{
    header('location: ../../backend/mail/2/'.$linkToken.'/true');
}

function tostart(){
    header('location: ../../admin');
}

?>