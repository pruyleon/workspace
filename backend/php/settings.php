<?php 

include('./login_state.php');
only_logged();

// TEXTVERARBEITUNG
    // Datenbank auslesen
    $userDATA = db_select("SELECT `mail` FROM `backend` WHERE `acc_ID`='".USER."'");
    if($userDATA != false && sizeof($userDATA) > 0){
        $user_mail = $userDATA[0]['mail'];
    }else{
        errorJump();
        return;
    }
    
    // Grundinformationen
    $estabVal = [USER_NAME[0], USER_NAME[1], $user_mail];
    $grundVar = ["vorname", "nachname", "mail"];
    $qrName   = ["prename", "name", "mail"];
    $qrString = "";
    foreach($grundVar as $i => $postVal){
        if(isset($_POST[$postVal])){
            
            $newVal = db_escape($_POST[$postVal]);
            
            if(($postVal == "vorname" || $postVal == "nachname" || $postVal == "mail") && $newVal == null){
                // Pflichtfelder leer
                errorJump();
                return;
            }
            
            if($newVal != $estabVal[$i]){
                // neue Variable
                if($postVal == "mail"){
                    // Passwort überprüfen 
                    if(isset($_POST['mail_acces']) && $_POST['mail_acces'] != null){
                        $userPW = $_POST['mail_acces'];
                        
                        $dbPW = getPWfromDB();
                        if($dbPW != false){
                            if($dbPW != $userPW){
                                // Passwort stimmt nicht überein
                                errorJump(2);
                                return;
                            }else{
                                if(db_check($newVal, 'mail') == true){
                                    // Mail bereits vergeben
                                    errorJump(6);
                                    return;
                                }else{
                                    // Cookies aktuallisieren
                                    $SessionKey = $cookieArr['key'];
                                    makeCookie("user", "mail", encrypt($newVal, $SessionKey), 0);
                                    makeCookie("user", "fill", $newVal, 0);
                                    
                                    // Bestätigungsmail erneut senden
                                    if(resendMail(USER, $newVal, USER_NAME[0])){
                                        $qrString .= " `mail_active`='0',";
                                    }
                                }
                            }
                        }else{
                            // Konnte passwort nicht finden
                            
                            errorJump();
                            return;
                        }
                    }else{
                        // Kein Sicherheitspasswort eingegeben

                        errorJump();
                        return;
                    }
                }
                
                $qrString .= " `".$qrName[$i]."`='".$newVal."',";
            }
                
        }else{
            errorJump();
            return;
        }
    }

    // Passwort ändern
    if(isset($_POST['pw_new']) && $_POST['pw_new'] != null){
         if((!isset($_POST['pw_new2']) || $_POST['pw_new2'] == null) || ($_POST['pw_new'] != $_POST['pw_new2'])){
             // Passwort 1&2 stimmen nicht überein
             errorJump(3);
             return;
         }else{
             if(!isset($dbPW)) $dbPW = getPWfromDB();
             
             if($dbPW != false){
                if($dbPW != $_POST['pw']){
                    // Passwort stimmt nicht überein
                    errorJump(4);
                    return;
                }else{
                    if($dbPW != $_POST['pw_new']){
                        $qrString.= " `password`=AES_ENCRYPT('".db_escape($_POST['pw_new'])."', '".USER."'),";
                    }else{
                        // Passwort bereits benutz
                        errorJump(5);
                        return;
                    }
                }
            }else{
                // Konnte passwort nicht finden
                errorJump();
                return;
            }
             // Schauen ob PWDB und PWUser übereinstimmen
         }
    }

    // Datenbank eintrag

    if($qrString!= ""){
        $qrString = substr($qrString, 0, -1);
        $updateQR = ["UPDATE `backend` SET", " WHERE `acc_ID`='".USER."'"];
        $qrStr = $updateQR[0].$qrString.$updateQR[1];
        
        if(db_query($qrStr) == false){
            // Fehlerseite weiterleiten
            errorJump();
            return;
        }
    }
        

// BILDVERARBEITUNG
ini_set('post_max_size', '64M');
ini_set('upload_max_filesize', '64M');
ini_set('memory_limit', '1024M');

if(isset($_POST['flag_del'])){
    // standart Bild als Profilbild verwenden
    if(!restorePIC() == true){
        errorJump();
        return;
    }
}else if(!empty($_FILES['picSelector']['name'])){
    if(!uploadPIC()){
        errorJump();
        return;
    }
}

header('Location: ../../backend/settings/0');

exit();

function restorePIC(){
    $profileDir = "../src/".USER.".jpg";
    
    if(is_file($profileDir)) unlink($profileDir);
    
    if(copy("../../src/pic/default_profile.jpg", $profileDir)){
        return true;
    }else{
        return false;
    }
}

function uploadPIC(){
    $FILE = $_FILES['picSelector'];
    $filename = "profile.".pathinfo($FILE['name'], PATHINFO_EXTENSION);
    
    $UploadDir = "../src/";
    $tmpDir = $UploadDir."bin/";

    if(!is_dir($UploadDir)) mkdir($UploadDir, 0755);
	if(!is_dir($tmpDir)) mkdir($tmpDir, 0755);
    
    
    if (move_uploaded_file($FILE['tmp_name'], $tmpDir.$filename)) {
        
        require_once('../../signed/php/picture/resize.php');
        
        if(createThumb($tmpDir.$filename, $UploadDir.USER.".jpg", 250, true)){
			// tmp Datei löschen
            if(unlink($tmpDir.$filename)) return true;
            else return false;
            
		}else return false;
        
    }else return false;
}

function getPWfromDB(){
    $dbPW = db_select("SELECT AES_DECRYPT(`password`, '".USER."') FROM `backend` WHERE `acc_ID`='".USER."'");
    if($dbPW == false || sizeof($dbPW) > 1 || sizeof($dbPW) < 1){
    //  Passwort nicht gefunden
        return false;
    }else{
        $dbPW = $dbPW[0]["AES_DECRYPT(`password`, '".USER."')"];
        return $dbPW;
    }
}

function resendMail($userID, $mail, $prename){
    // PFAD AUS INI laden
    $iniPath = '../../php/path.ini';
    if(file_exists($iniPath)){
        $iniContent = parse_ini_file($iniPath);
        $mainURL = $iniContent['HostName'];
        $secure = $iniContent['Secure'] == true ? "https://" : "http://";
    }else{
        return false;
    }
    
    $mailToken = urlencode(encrypt($userID, "mailKEY"));

    $msg = 'Du hast eine Änderung an deiner E-Mail Adresse vorgenommen! Klicke auf den untenstehenden Link, um Deine neue E-Mail Adresse zu bestätigen und deinen Admin-Account weiterhin wie gewohnt zu benutzen.

    <a href="'.$secure.$mainURL.'/backend/php/verify_mail.php?validate='.$mailToken.'" style="padding: 5px 15px;background-color: #BEA5FF;display:table;margin: 0 auto;color: #FFFFFF;text-decoration: none;">E-Mail Adresse bestätigen</a>';

    require_once('../../php/mail/mail.php');
    if(send_mail($mail, $prename, "E-Mail Adresse bestätigen", $msg) == false){
        // Fehlerseite weiterleiten
        return false;
    }else{
        return true;
    }

}

function errorJump($param = 1){
    header('Location: ../../backend/settings/'.$param);
    exit();
}
?>

