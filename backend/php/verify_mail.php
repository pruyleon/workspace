<?php
include('../../php/sql.php');
include('../../php/cookie.php');
require('../../php/crypt.php');

if(isset($_GET['validate']) && $_GET['validate'] != null){
    $trueUser = decrypt(urldecode($_GET['validate']), "mailKEY");
    $mailMode = true;
}else if(isset($_GET['token']) && $_GET['token'] != null){
    $trueUser = decrypt(urldecode($_GET['token']), "linkKEY");
    $mailMode = false;
}else{
    // Fehler -> Kein Account verbunden
    tostart();
    return;
}

// checken ob Account vorhanden
if(db_check($trueUser, 'acc_ID', 'backend')){

    if($mailMode){
        // Mailadresse bestätigen
        $check = db_select("SELECT `mail_active`, `mail` FROM `backend` WHERE `acc_ID`='".$trueUser."'");
        makeCookie('user', 'fill', $check[0]['mail']);
        if($check[0]['mail_active'] == 1){
            // Bereits aktiviert
            // -> Weiterleitung auf mail.php?info=0 + Login Button
            header('Location: ../../backend/mail/0');
            exit();
        }else{
            $try = db_query("UPDATE `backend` SET `mail_active`='1' WHERE `acc_ID`='".$trueUser."'");
            if($try == true){
                // Mail erfolgreich aktiviert
                // Weiterleitung auf mail.php?info=1 + Login Button
                header('Location: ../../backend/mail/1');
                exit();
            }else{
                tostart();
                return;
            }
        }

    }else{
        // Mail angefragt
        // Weiterleitung auf mail.php?info=2 + Resend button ($_GET['token'])
        header('Location: ../../backend/mail/2/'.$_GET['token']);
        exit();
    }
}else{
    // Fehler -> Account nicht in Datenbank
    tostart();
    return;
}

function tostart(){
    header('location: ../../admin/0');
    exit();
}

?>