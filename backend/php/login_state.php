<?php

define('LOGIN_STATE', false);

// Root definieren
$rootDir = explode('/',  realpath(__DIR__));
unset($rootDir[(sizeof($rootDir)-1)]);
unset($rootDir[(sizeof($rootDir)-1)]);
$root = join('/', $rootDir).'/';

// PFAD AUS INI laden
$iniPath = $root.'php/path.ini';
if(file_exists($iniPath)){
    $iniContent = parse_ini_file($iniPath);
    $mainURL = $iniContent['HostName'];
    $secure = $iniContent['Secure'] == true ? "https://" : "http://";
    define('URL_ROOT', $secure.$mainURL.'/');

    require($root.'php/cookie.php');
    require($root.'php/crypt.php');
    require($root.'php/sql.php');
}

function checkLogin(){

    if(getCookie('admin') != true){
        // Cookiegruppe nicht angelegt
        return false;
    }

    if(getCookie('admin','key') != false && getCookie('admin','mail') != false  && getCookie('admin','id') != false){
        $key = getCookie('admin','key');
        $cryptMail = getCookie('admin','mail');
        $cryptUser = getCookie('admin','id');
    }else{
        // keine Cookies gesetzt -> noch nicht angemeldet
        return false;
    }

    // decrypt mail with $key
    $userMail = decrypt($cryptMail, $key);

    // SessionToken aus DB laden
    $sqlResult = db_select("SELECT `acc_ID`, `session`, `name`, `prename` FROM `backend` WHERE `mail`='".$userMail."'");
    if($sqlResult == false || sizeof($sqlResult) > 1){
    //  Acc ID nicht gefunden
        return false;
    }else{
        $userID = $sqlResult[0]['acc_ID'];
        $dbSession = $sqlResult[0]['session'];
        $userPreName = $sqlResult[0]['prename'];
        $userName = $sqlResult[0]['name'];
    }

    // überprüfen ob password_verify(Cookie, DBtoken) = true
    if(!password_verify($key, $dbSession)){
        // Session Key und Schlüssel passen nicht zusammen
        return false;
    }

    // decrypt user with substr(DBtoken, -10)
    $storedUser = decrypt($cryptUser, substr($dbSession, -10));

    // überprüfen ob Session und User zusammenpassen
    if($storedUser != $userID){
        // User aus Datenbank entspricht nicht User aus Cookie
        return false;
    }else{
        define('USER', $userID);
        define('USER_NAME', array($userPreName, $userName));
        define('LOGIN_STATE', true);
        return true;

        
    }
    

}


function only_logged(){

    if(checkLogin() == false){
        makeCookie("admin", "key", "", 1);
        makeCookie("admin", "id", "", 1);
        makeCookie("admin", "mail", "", 1);
        
        header('Location: '.URL_ROOT."admin");
    }
    
    
}