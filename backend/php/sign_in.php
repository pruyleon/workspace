<?php
require('../../php/sql.php');
include('../../php/cookie.php');


if(isset($_POST['usermail']) && $_POST['usermail'] != null && isset($_POST['userpw']) && $_POST['userpw'] != null){
    $userMAIL = strip_tags($_POST['usermail']);
    
    makeCookie("admin", "fill", $userMAIL);
        
    $userPW = $_POST['userpw'];
}else{
//  Post nicht übergeben
    tostart(0);
    return;
}

// USER id aus db laden
$dbUserID = db_select("SELECT `acc_ID` FROM `backend` WHERE `mail`='".$userMAIL."'");
if($dbUserID == false || sizeof($dbUserID) > 1){
//  Acc ID nicht gefunden
    tostart(1);
    return;
}else{
    $userID = $dbUserID[0]['acc_ID'];
}

// PW aus db laden
$dbPW = db_select("SELECT AES_DECRYPT(`password`, '".$userID."') FROM `backend` WHERE `acc_ID`='".$userID."'");
if($dbPW == false || sizeof($dbPW) > 1){
//  Passwort nicht gefunden
    tostart(1);
    return;
}else{
    $dbPW = $dbPW[0]["AES_DECRYPT(`password`, '".$userID."')"];
    
    // Passwort aus DB und USERpw vergleichen
    if($dbPW != $userPW){
    //  Passwörter stimmen nicht überein
        tostart(2);
        return;
    }
}

// PW aus db laden
$dbActive = db_select("SELECT `mail_active` FROM `backend` WHERE `acc_ID`='".$userID."'");
if($dbActive == false || sizeof($dbActive) > 1){
//  Account nicht gefunden
    tostart(1);
    return;
}else{
    $mail_Active = $dbActive[0]['mail_active'];
    
    // Passwort aus DB und USERpw vergleichen
    if($mail_Active == 0){
    //  Passwörter stimmen nicht überein
        tostart(8);
        return;
    }
}


// Cookie Key generieren
$SessionKey = genSessionID();
$dbTOKEN = password_hash($SessionKey, PASSWORD_DEFAULT);

// In Datenbank eintragen
$result = db_query("UPDATE `backend` SET `session`='".$dbTOKEN."' WHERE `acc_ID`='".$userID."'");
if(!$result){
//  Fehler bei der Session in DB
    tostart(0);
    return;
}


require('../../php/crypt.php');

$expire = isset($_POST['remember']) ? (time()+(60*60*24*30)) : 0;

makeCookie("admin", "key", $SessionKey, $expire);
makeCookie("admin", "id", encrypt($userID, substr($dbTOKEN, -10)), $expire);
makeCookie("admin", "mail", encrypt($userMAIL, $SessionKey), $expire);

header('Location: ../../monitor');
exit();
// zum Dashboard


function tostart($msg){
    makeCookie("admin", "key", "", 1);
    makeCookie("admin", "id", "", 1);
    makeCookie("admin", "mail", "", 1);
    
    header('Location: ../../admin/'.$msg);
    exit();
}



function genSessionID(){
    
    $chars = "123456789abcdefghijklmnopqrstuvwxyz123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $returnID = "";
    for($n = 0; $n<12; $n++){
        if($n % 3 == 0 && $n > 0)$returnID .= "-";
        $returnID .= $chars[rand(0, strlen($chars)-1)];
    }
    
    return $returnID;
}

?>