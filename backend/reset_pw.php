<?php
include('./php/login_state.php');
checkLogin();

// login überprüfung
if(isset($_GET['token1']) && $_GET['token1'] != null && isset($_GET['token2']) && $_GET['token2'] != null){
    $tk1 = $_GET['token1'];
    $tk2 = $_GET['token2'];

    $tk1_decrypt = decrypt(urldecode($tk1), "PW_Reset_Key");
    $userID = decrypt(urldecode($tk2), $tk1_decrypt);
    
    // Datenbank überprüfung
    $db_token = db_select("SELECT `session` FROM `backend` WHERE `acc_ID`='".$userID."'");
    if($db_token != false && sizeof($db_token) == 1){
        
        if($db_token[0]['session'] != $tk1_decrypt){
            // Token stimmen nicht überein
            jumphome(7);
            return;
        }
        
    }else{
        jumphome();
        return;
    }
}else{
    jumphome();
    return;
}

function jumphome($n = 0){
    header('Location: ../admin/'.$n);
    exit();
}
?>
<html>
    
    <head>
        <base href="/backend/" />
        <?php include('../src/header.php'); ?>
        <title>Passwort zurücksetzen - Starbook</title>

        <link type="text/css" rel="stylesheet" href="../css/frontend/reset.css" />
        <script type="text/javascript" src="../js/frontend/reset_2.js"> </script>
    </head>
    
    <body>
        <?php include('../signed/incl/menue/navigation.php'); ?>

        <div class="header_line backend">
            <h1>Passwort zurücksetzen</h1>
        </div>
        
            <form action="./php/reset_pw.php" method="POST" onsubmit="return checkPW()" id="resetForm">
                
                <?php
                        $msg = [
                            "Ein technischer Fehler ist aufgetreten.",
                            "Das neue Passwort unterscheidet sich nicht zu deinem letzten Passwort.",
                            "Die Passwörter stimmen nicht überein.",
                        ];
                        
                        $styleMsg = 'style="display: none;"';
                        $msgTxt = "";
                        if(isset($_GET['re']) && $_GET['re'] != null){

                            if(isset($msg[strip_tags($_GET['re'])])){
                                $msgTxt = $msg[strip_tags($_GET['re'])];
                                
                                $styleMsg = 'style="display: block;"';
                            }

                        }
                    ?>
                
                <div class="message_negativ" <?php echo $styleMsg; ?> id="errNote">
                    <?php echo $msgTxt; ?>
                </div>
                
                <input type="password" placeholder="neues Passwort" name="user_pw_1" id="user_pw_1"/>
                <input type="password" placeholder="Passwort wiederholen" name="user_pw_2" id="user_pw_2"/>
                
                <input type="hidden" name="tk1" value="<?php echo $tk1; ?>" />
                <input type="hidden" name="tk2" value="<?php echo $tk2; ?>" />
                
                <input type="submit" class="button neutral" value="Passwort speichern" />
            </form>
        
    <?php include('../signed/incl/footer/foot.php'); ?>      
    </body>
    
</html>