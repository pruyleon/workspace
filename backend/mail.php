<?php
include('./php/login_state.php');
checkLogin();
?>

<html>
    <head>
        <base href="/backend/" />
        <?php include('../src/header.php'); ?>
        <title>E-Mail Adresse - Starbook ADMIN</title>

        <link type="text/css" rel="stylesheet" href="../css/register/mail.css" />
    </head>
    
    <body>
        <?php include('../signed/incl/menue/navigation.php'); ?>
        
        <div class="header_line">
            <h1>Email Adresse bestätigen</h1>
        </div>
        
        <div id="infobox">
            <img src="../src/icns/checked.svg" />
            <p>
            <?php
                
                if(isset($_GET['info']) && $_GET['info'] != ""){
                    // Info gesetzt
                    $Info = $_GET['info'];
                    
                    switch($Info){
                        case 0: 
                                echo 'Deine E-Mail Adresse wurde bereits aktiviert.';
                            break;
                        case 1:
                                echo '<b>Deine E-Mail Adresse wurde erfolgreich bestätigt.</b><br><br>Du kannst dich nun in Deinen Admin-Account einloggen und Starbook verwalten. Lege gleich los und melde Dich an!';
                            break;
                        case 2:
                                $token_set = false;
                                if(isset($_GET['token']) && $_GET['token'] != null){
                                    $token_set = true;
                                    $trueUser = decrypt(urldecode($_GET['token']), "linkKEY");
                                    if(isset($_GET['retry']) && $_GET['retry'] == "true"){
                                        $mailSQL = db_select("SELECT `mail` FROM `backend` WHERE `acc_ID`='".$trueUser."'");
                                        if($mailSQL != false && sizeof($mailSQL) == 1){
                                            echo "<b>Eine neue Bestätigungsmail wurde an ".$mailSQL[0]['mail']." versandt.</b><br><br>";
                                        }
                                    }
                                }
                                
                                echo "Dein Account wurde erfolgreich angelegt! Bitte bestätige Deine E-Mail Adresse. Schaue dazu einfach in Deinem E-Mail Postfach nach.";
                                
                                echo ($token_set) ? "<br><br>Hast Du noch keine Nachricht erhalten?<br>
                                Bestätigunsmail <a href='./php/resend.php?token=".$_GET['token']."'>erneut senden.</a>": null;
                            
                            break;
                    }
                }else{
                    echo 'Die Anfrage liefert keine Rückgabewerte.';
                }
                

            ?>
            </p> 
        </div>
        
        <?php
            if(isset($Info) && $Info != 2){
                echo '<a href="../admin" class="button neutral" id="loginbtn">Anmelden</a>';
            }
        ?>
        <?php include('./signed/incl/footer/foot.php'); ?>
    </body>
</html>