<?php
include('./php/login_state.php');
checkLogin();
?>
<html>
    
    <head>
        <base href="/backend/" />
        
        <?php include('../src/header.php'); ?>
        <title>Passwort zurücksetzen - Starbook ADMIN</title>

        <link type="text/css" rel="stylesheet" href="../css/frontend/reset.css" />
        <script type="text/javascript" src="../js/frontend/reset.js"> </script>
    </head>
    
    <body>
        <?php include('../signed/incl/menue/navigation.php'); ?>

        <div class="header_line backend">
            <h1>Passwort zurücksetzen</h1>
        </div>
        
            <form action="./php/reset_pw_mail.php" method="POST" onsubmit="return checkmail()" id="resetForm">
                
                <?php
                        $msg = [
                            "Ein technischer Fehler ist aufgetreten.",
                            "Die angegebenen E-Mail Adresse existiert nicht."
                        ];
                        
                        $styleMsg = 'style="display: none;"';
                        $msgTxt = "";
                        if(isset($_GET['re']) && $_GET['re'] != null){

                            if(isset($msg[strip_tags($_GET['re'])])){
                                $msgTxt = $msg[strip_tags($_GET['re'])];
                                
                                $styleMsg = 'style="display: block;"';
                            }

                        }
                    ?>
                
                <div class="message_negativ" <?php echo $styleMsg; ?> id="errNote">
                    <?php echo $msgTxt; ?>
                </div>
                


                <?php
                if(isset($_COOKIE['user']['fill']) && $_COOKIE['user']['fill'] != null){
                        $prefill = $_COOKIE['user']['fill'];
                }else $prefill = "";
                ?>
                <input type="text" placeholder="E-Mail Adresse" name="usermail" id="usermail" value="<?php echo $prefill; ?>"/>
                
                <input type="submit" class="button neutral" value="Passwort zurücksetzen" />
            </form>
    <?php include('../signed/incl/footer/foot.php'); ?>      
    </body>
    
</html>