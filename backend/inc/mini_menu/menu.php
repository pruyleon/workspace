<?php

if(strlen(USER) == 15){
    $signed = true;
    $logo_link = "monitor";
    
    
    $Acc_Types = ["Künstler", "Manager", "Veranstalter"];
    $profile_path = URL_ROOT.'backend/src/'.USER.'.jpg';
    $profile_pic = getimagesize($profile_path) ? $profile_path : URL_ROOT."src/pic/default_profile.jpg";
    
}else{
    $signed = false;
    $logo_link = "home";
}

$prefix = URL_ROOT;

?>


<script type="text/javascript" src="<?php echo $prefix; ?>signed/incl/menue/nav.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo $prefix; ?>signed/incl/menue/style.css"/>  

<div id="nav_spacer"></div>

<div id="navigation">
    <div id="nav_group1">
        <a id="nav_logo" href="<?php echo $prefix.$logo_link; ?>" title="Home"><img src="<?php echo $prefix; ?>src/icns/Logo.svg" alt="Starbook Logo" /></a>
        
        <div id="nav_toggle_mobile" onclick="toggle_men();"><img src="<?php echo $prefix; ?>src/icns/menu.svg" alt="Mobiles Menu Icon" /></div>
    </div>
    
    <div id="nav_group2" state="collapsed">
        
        <div id="nav_user">
            <a id="user_profile" href="<?php echo $prefix; ?>monitor" title="Zum Dashboard">
                <img src="<?php echo $profile_pic; ?>" alt="Dein Account-Profilbild"/>
            </a>
            
            <a id="user_info" href="<?php echo $prefix; ?>monitor" title="Zum Dashboard">
                <?php echo USER_NAME[0].' '.USER_NAME[1]; ?>
                <font><?php echo $Acc_Types[USER_TYPE]; ?></font>
            </a>

            <a href="<?php echo $prefix; ?>backend/php/logout.php" id="user_logout" title="Abmelden">Abmelden</a>
        </div>
        
    </div>
    
    <?php
    if(strlen(USER) != 15) echo '<style> #nav_group2{display:none !important;} </style>';
    ?>
</div>


