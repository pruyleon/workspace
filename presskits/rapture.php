<?php
  $dir_string = "../signed/php/profile_elements/presskit/";
  $text = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.';

 ?>

<html lang="de" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>mypressk.it | DJ Rapture</title>
    <link rel="stylesheet" href="/signed/css/profile_elements/presskit.css">
    <link rel="stylesheet" href="/signed/css/profile_elements/gen.css">
    <link rel="stylesheet" href="/signed/css/profile_elements/positions.css">
    <!-- <link rel="stylesheet" href="/signed/css/profile_elements/raw.css"> -->
    <link rel="stylesheet" href="/signed/css/profile_elements/preview.css">
    <link rel="stylesheet" href="/signed/css/file_archive.css">
    <link rel="stylesheet" href="/css/presskits/dj_rapture.css">
    <?php
      include("../src/header.php");
      include("../signed/php/gen.php");
     ?>
  </head>
  <body>
    <?php
      // H1
      $background = "/signed/src/demo_pics/demo-7.jpg";
      $artist_name = "DJ Rapture";
      $city = "Haßloch";
      $country = "Deutschland";
      $genre_string = "Hip-Hop | RnB | Black";
      include($dir_string."H1.php");

      // 3L
      $headline = "HEADLINE HEADLINE HEADLINE";
      $background_color = "#444";
      $background = "/signed/src/demo_pics/demo-5.jpg";
      include($dir_string."3L.php");

      // 10L
      $headline = "HEADLINE HEADLINE HEADLINE";
      $background_color = "#444";
      $background = "/signed/src/demo_pics/demo-2.jpg";
      include($dir_string."10L.php");
     ?>
  </body>
  <script src="/js/presskits/gen.js" charset="utf-8"></script>
</html>
