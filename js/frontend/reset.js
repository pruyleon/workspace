function checkmail(){
    var usermail = document.getElementById('usermail').value;
    
    document.getElementById('usermail').style.borderBottom = "solid 2px #FFF";
    
    if(usermail.indexOf('@') < 0 || usermail.indexOf('.') < 0 || usermail.indexOf(' ') > -1 || usermail.length <= 3){
        // keine Gülige Mailadresse
        document.getElementById('usermail').style.borderBottom = "solid 2px #FF7D82";
        document.getElementById('errNote').style.display = "block";
        document.getElementById('errNote').innerHTML = "Bitte gib eine gültige E-Mail Adresse ein.";
        return false;
    }
    
    return true;
}