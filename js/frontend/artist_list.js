
function toggleFilter(){
    if(document.getElementById('toggle_box').getAttribute('state') == "hidden"){
        document.getElementById('toggle_box').setAttribute('state', 'visible');
        document.getElementById('toggle_box').style.display = "table";
        document.getElementById('show_btn').innerHTML = "Filter ausblenden";
        scrolltoreg("input_layout");
        
    }else{
        document.getElementById('toggle_box').setAttribute('state', 'hidden');
        document.getElementById('toggle_box').style.display = "none";
        document.getElementById('show_btn').innerHTML = "Filter einblenden";

    }
}

function toggleDropdown(staticBox){

    var fullDrop = staticBox.parentNode;
    var flex_window = fullDrop.children[2];
    
    if(flex_window.getAttribute('state') == "hidden"){
        flex_window.style.display = "block";
        flex_window.setAttribute('state', 'visible');
        staticBox.children[1].style.transform = "rotate(180deg)";
    }else{
        flex_window.style.display = "none";
        flex_window.setAttribute('state', 'hidden');
        staticBox.children[1].style.transform = "rotate(0deg)";
    }
}

function gen_filter_link(){
    var href = "./artist/overview/page=0";
    
    href += "/event=";
    if(document.getElementById('event_selection').value.length > 0){
        if(
            document.getElementById('place_proposal').getAttribute('settled') == "true" && 
            document.getElementById('event_date').getAttribute('allowed') == "true" &&
            document.getElementById('genreIDarr').value.length > 0
        )href += document.getElementById('event_selection').value;
        else href += "false";
    }else{
        href += "false"
    }
    
    if(document.getElementById('place_proposal').getAttribute('settled') == "true"){
        href += "/sort=place";
    }else{
        href += "/sort="+document.getElementById('sorting_constr').value;
    }

    href += "/date=";
    if(document.getElementById('event_date').getAttribute('allowed') == "true"){
        href += document.getElementById('event_date').value;
    }else{
        href += "0"
    }
    
    href += "/plz=";
    if(document.getElementById('place_proposal').getAttribute('settled') == "true"){
        href += document.getElementById('artist_plz').value;
    }else{
        href += "0"
    }
    
    href += "/genre=";
    if(document.getElementById('genreIDarr').value.length > 0){
        href += document.getElementById('genreIDarr').value;
    }else{
        href += "0"
    }
    
    if(document.getElementById('search_input_hidden').value.length > 0){
        var searchstr = document.getElementById('search_input_hidden').value;
        href += "/search="+encode_link(searchstr);
    }
    
    document.location.href = href;
}

function reset_filter_link(){
    var href = "./artist/overview/page=0";
    
    href += "/event=";
    if(document.getElementById('event_selection').value.length > 0){
        href += document.getElementById('event_selection').value;
    }else{
        href += "false"
    }
    
    if(document.getElementById('search_input_hidden').value.length > 0){
        var searchstr = document.getElementById('search_input_hidden').value;
        href += "/search="+encode_link(searchstr);
    }
    
        document.location.href = href;
}

function new_search(){
    var href = "./artist/overview/page=0";
    
    if(document.getElementById('event_selection').value.length > 0){
        href += "/event="+document.getElementById('event_selection').value;
    }
    
    
    if(document.getElementById('search_input').value.length > 0){
        var searchstr = document.getElementById('search_input').value;
        href += "/search="+encode_link(searchstr);
    }
    
    document.location.href = href;
    return false;
}

function encode_link(link){
    var res = link.match(/([A-Za-z0-9\ \u00c4\u00e4\u00d6\u00f6\u00dc\u00fc\u00df]+)/g);
    var new_search_str = res.join('');
    new_search_str = new_search_str.split(' ').join('+');
    return encodeURI(new_search_str);
}

addLoadEvent(queryPics);

function queryPics(){
    var allPics = document.getElementsByClassName('artist_pic');

    for(var n in allPics){
        if(typeof allPics[n] == 'object'){
            
            var artist_img = new Image();
            var myDom = allPics[n];
            var pic_src = allPics[n].getAttribute('preload');
            
            artist_img.addEventListener('onload', display_src(myDom, pic_src));
            
            artist_img.src = pic_src;
        }
    }
}

function display_src(elms, img){
    elms.removeAttribute('preload');
    elms.setAttribute('src', img);
}
