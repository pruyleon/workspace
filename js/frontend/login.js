function checkInput(){
    
    var usermail = document.getElementById('usermail').value;
    
    document.getElementById('usermail').style.borderBottom = "solid 2px #FFF";
    if(usermail.indexOf('@') < 0 || usermail.indexOf('.') < 0 || usermail.indexOf(' ') > -1){
        // keine Gülige Mailadresse
        document.getElementById('usermail').style.borderBottom = "solid 2px #FF7D82";
        document.getElementById('errNote').style.display = "block";
        document.getElementById('errNote').innerHTML = "Bitte gib eine gültige E-Mail Adresse ein.";
        return false;
    }
    
    document.getElementById('userpw').style.borderBottom = "solid 2px #FFF";
    if(document.getElementById('userpw').value.length <= 4){
        document.getElementById('userpw').style.borderBottom = "solid 2px #FF7D82";
        document.getElementById('errNote').style.display = "block";
        document.getElementById('errNote').innerHTML = "Bitte gib ein gültiges Passwort an.";
        return false;
    }
    
    
    return true;
}

function setRemeber(){
    
    var currLabel = document.getElementById('remember_label');
    
    if(currLabel.getAttribute('state') == "false"){
        currLabel.setAttribute('state', 'true');
        currLabel.children[0].setAttribute('src', './src/icns/checked_white.svg');
    }else{
        currLabel.setAttribute('state', 'false');
        currLabel.children[0].setAttribute('src', './src/icns/unchecked_white.svg');
    }
    
}