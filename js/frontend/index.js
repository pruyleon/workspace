function searchInput(){
    var href = "./artist/overview/page=0/search=";
    var input_str = document.getElementById('short_search').value;
    document.location.href = href+encode_link(input_str);
}

function encode_link(link){
    var res = link.match(/([A-Za-z0-9\ \u00c4\u00e4\u00d6\u00f6\u00dc\u00fc\u00df]+)/g);
    var new_search_str = res.join('');
    new_search_str = new_search_str.split(' ').join('+');
    return encodeURI(new_search_str);
}
