function checkPW(){
    var pw1 = document.getElementById('user_pw_1');
    var pw2 = document.getElementById('user_pw_2');
    
    pw1.style.borderBottom = "solid 2px #fff";
    pw2.style.borderBottom = "solid 2px #fff";

    if(pw1.value.length < 6){
        pw1.style.borderBottom = "solid 2px #FF7D82";
        document.getElementById('errNote').style.display = "block";
        document.getElementById('errNote').innerHTML = "Das Passwort muss mindestens 6 Zeichen haben.";
        return false;
   }else if(pw1.value.length > 50){
        pw1.style.borderBottom = "solid 2px #FF7D82";
        document.getElementById('errNote').style.display = "block";
        document.getElementById('errNote').innerHTML = "Das Passwort darf maximal 50 Zeichen haben.";
        return false;
   }
    
    if(pw2.value != pw1.value){
        pw1.style.borderBottom = "solid 2px #FF7D82";
        pw2.style.borderBottom = "solid 2px #FF7D82";
        document.getElementById('errNote').style.display = "block";
        document.getElementById('errNote').innerHTML = "Die Passwörter stimmen nicht überein";
        return false;
    }
    
    return true;
}