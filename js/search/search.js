$(function() {
  //Navigationsleiste beim Scrollen abdunkeln
  $(document).on('scroll', function() {
    nav_transparency = ($(this).scrollTop() / $(this).width()) * 10;
    nav_transparency = parseFloat(nav_transparency);
    nav_transparency = nav_transparency >= 1 ? 1 : nav_transparency;
    // console.log(nav_transparency + '\n' + $(this).scrollTop() + '\n' + $(this).width() + '\n' + 'rgba(0,0,0,' + nav_transparency + ')');
    $('.search_menue_wrapper').css('background-color', 'rgba(21,21,21,' + nav_transparency + ')');
  });

  // Slider
  $('.sliderwrapper.small').slick({
    slidesToScroll: 6,
    slidesToShow: 6,
    infinite: false,
    variableWidth: true,
    swipe: false,
    swipeToSlide: true,
    touchThreshold: 10000,
    //Buttons Styling
    nextArrow: ' <button type="button" class="slick-next"><img src="/signed/src/icns/next.svg"></button> ',
    prevArrow: ' <button type="button" class="slick-prev"><img src="/signed/src/icns/previous.svg"></button> '
  });

  $('.sliderwrapper.large').slick({
    slidesToScroll: 1,
    slidesToShow: 6,
    infinite: false,
    variableWidth: true,
    swipe: false,
    swipeToSlide: true,
    touchThreshold: 18,
    //Buttons Styling
    nextArrow: ' <button type="button" class="slick-next"><img src="/signed/src/icns/next.svg"></button> ',
    prevArrow: ' <button type="button" class="slick-prev"><img src="/signed/src/icns/previous.svg"></button> '
  });

  $(document).on('click', '.sliderelement', function(){
    $('.sliderelement_video').each(function(){
      $(this)[0].muted = !$(this)[0].muted;
    });
  });

  //Hovereffekt
  $(document).on('mouseenter', '.sliderelement', function(e) {
    hovered_slider = $(this);
    hoverTimeout = setTimeout(function() {
      $(hovered_slider).addClass('zoomed');
      $(hovered_slider).find('.sliderelement_infos').stop(true, false).fadeIn(500);
      $(hovered_slider).closest('.sliderwrapper').addClass('zoomed');
      //wenn Maus in rechter Hälfte dann margin-links von erstem Slider auf -7vw
    }, 10);
    if (event.clientX > (5 * window.innerWidth) / 6) {
      marginTimeout = setTimeout(function() {
        $(hovered_slider).closest('.sliderwrapper').find('.sliderelement:first').addClass('far_right_hovered');
      }, 10);
    } else if (event.clientX > (1 * window.innerWidth) / 6) {
      marginTimeout = setTimeout(function() {
        $(hovered_slider).closest('.sliderwrapper').find('.sliderelement:first').addClass('right_hovered');
      }, 10);
    }

    videoTimeout = setTimeout(function() {
      //Video starten
      $(hovered_slider).children('video').fadeIn(500);
      $(hovered_slider).children('video').get(0).play();
    }, 1000);
    if ($(this).hasClass('large')) {
      clearTimeout(videoTimeout);
    }
  });

  $(document).on('mouseleave', '.sliderelement', function() {
    $(this).removeClass('zoomed');
    $(this).find('.sliderelement_infos').stop(true, false).fadeOut(500);
    $(this).closest('.sliderwrapper').removeClass('zoomed');
    //Videotimeout stoppen
    clearTimeout(hoverTimeout);
    clearTimeout(videoTimeout);
    $(hovered_slider).closest('.sliderwrapper').find('.sliderelement:first').removeClass('right_hovered far_right_hovered');
    $(hovered_slider).children('video').fadeOut(300);
    $(hovered_slider).children('video').trigger('pause');
    if ($(hovered_slider).children('video').length) {
      $(hovered_slider).children('video').get(0).currentTime = 0;
    }
  });


});
