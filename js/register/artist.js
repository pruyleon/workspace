$(function() {
	var redirect_link = '/complete_infos';
	$('#register').on('click', function() {
		$('.error_wrapper').html(' ');
		$('#agb_wrapper').css('color', '');
    $('#data_wrapper').css('color', '');
		if (!$('#agb_checkbox').is(':checked')) {
			$('#agb_wrapper').css('color', ' red');
		}
		if (!$('#dataprotect_checkbox').is(':checked')) {
			$('#data_wrapper').css('color', ' red');
		}

		var mail_address = $('#mail_address').val();
		var password = $('#password').val();
		var password_repeat = $('#password_repeat').val();

		var datacheck = true;
		var mail_check = true;
		var mailcheck2 = false;
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		mail_check = regex.test(mail_address);
		if (mail_check == false) {
			$('.error_wrapper').html('Bitte gebe eine gültige Email Adresse an!');
		}
		$.ajax({
			url: '/php/register/check_email.php',
			data: {
				mail_address: mail_address
			},
			type: 'post',
			success: function(response) {
				if (response == '1') {
					mailcheck2 = true;
				} else {
					$('.error_wrapper').html('Die Mailadresse ist bereits vergeben');
					mailcheck2 = false;
				}
				if (password != password_repeat) {
					var password_check = false;
					$('.error_wrapper').html('Die Passwörter stimmen nicht überein');
				} else {
					var password_check = true;
				}

				if (password.length < 7) {
					$('.error_wrapper').html('Dein Passwort muss mindestens sieben Zeichen lang sein!');
				}

				if ($('#dataprotect_checkbox').is(':checked') && $('#agb_checkbox').is(':checked') && mail_check == true && password_check == true && datacheck == true && mailcheck2 == true) {
					$('#popup_load_screen').fadeIn();
					$.ajax({
						url: '/php/register/artist/inserts/artist.php',
						type: 'post',
						data: {
							mail_address: mail_address,
							password: password
						},
						success: function(response) {
							$('#popup_load_screen').fadeOut();
							$('#usermail').val(mail_address);
							$('#userpw').val(password);
							$('#login_form').submit();
						}
					});
				}
			}
		});
	});
});
