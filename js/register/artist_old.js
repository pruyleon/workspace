var labelName = ["single_label", "multi_label"];

function setAccType(type) {
  var gg_type = type == 0 ? 1 : 0;

  document.getElementById(labelName[gg_type]).style.opacity = .5;
  document.getElementById(labelName[gg_type]).children[0].setAttribute('src', './src/icns/unchecked.svg');

  document.getElementById(labelName[type]).style.opacity = 1;
  document.getElementById(labelName[type]).children[0].setAttribute('src', './src/icns/checked.svg');
}

var boxName = ["agb_label", "data_label"];

function setRequirement(boxIdx) {

  var currLabel = document.getElementById(boxName[boxIdx]);

  if (currLabel.getAttribute('state') == "false") {
    currLabel.setAttribute('state', 'true');
    currLabel.children[0].setAttribute('src', './src/icns/checked.svg');
    currLabel.style.color = "#000";
  } else {
    currLabel.setAttribute('state', 'false');
    currLabel.children[0].setAttribute('src', './src/icns/unchecked.svg');
  }
}


var inputName = ["vorname", "nachname", "mail", "pw1", "pw2"];

function submit_form() {
  var errStr = [];
  var trigger = false;
  // auf Inhalt prüfen
  var s = 0;
  while (inputName[s]) {
    document.getElementById(inputName[s]).style.borderBottom = "solid 1px #000";
    var txtVal = document.getElementById(inputName[s]).value.trim();
    if (txtVal == "") {

      if (!trigger) {
        trigger = true;
        errStr.push("Bitte fülle alle Felder vollständig aus.");
      }
      mark(s);

    } else {
      // Name
      if (s == 0 || s == 1) {
        var nameReg = /^[\u00C0-\u017Fa-zA-Z][\u00C0-\u017Fa-zA-Z- ]+[\u00C0-\u017Fa-zA-Z]$/;
        if (!txtVal.match(nameReg)) {
          var nameType = ["Vorname", "Nachname"];
          errStr.push("Der eingegebene " + nameType[s] + " enthält ungültige Zeichen.");
          mark(s);
        }
      }

      // Email
      if (s == 2) {
        if (txtVal.indexOf('@') < 0 || txtVal.indexOf('.') < 0 || txtVal.indexOf(' ') > -1) {
          errStr.push("Bitte gib eine gültige E-Mailadresse an.");
          mark(s);
        }
      }


      // PW
      if (s == 3) {
        if (txtVal.length < 6) {
          errStr.push("Das Passwort muss mindestens 6 Zeichen lang sein.");
          mark(s);
        } else if (txtVal.length > 50) {
          errStr.push("Das Passwort darf maximal 50 Zeichen lang sein.");
          mark(s);
        }
      }

      if (s == 4) {
        var pw1 = document.getElementById(inputName[3]).value.trim();
        var pw2 = txtVal;
        if (pw2 != pw1) {
          errStr.push("Die Passwörter stimmen nicht überein.");
          mark(3);
          mark(4);
        }
      }


    }


    s++;
  }

  // Checkboxen
  if (document.getElementById('agb_label').getAttribute('state') == "false") {
    document.getElementById('agb_label').style.color = "#FF7D82";
    errStr.push("Akzeptiere die AGBs um fortzufahren.");
  }
  if (document.getElementById('data_label').getAttribute('state') == "false") {
    document.getElementById('data_label').style.color = "#FF7D82";
    errStr.push("Akzeptiere die Datenschutzbestimmungen um fortzufahren.");
  }

  if (errStr.length == 0) {
    return true;
  } else {
    document.getElementById('msger').innerHTML = "- " + errStr.join("<br>- ");
    document.getElementById('msger').style.display = "table";
    return false;
  }
}

function mark(indexer) {
  document.getElementById(inputName[indexer]).style.borderBottom = "solid 1px #FF7D82";
}

/*
function isValidIBANNumber(input) {
    var CODE_LENGTHS = {
        AD: 24, AE: 23, AT: 20, AZ: 28, BA: 20, BE: 16, BG: 22, BH: 22, BR: 29,
        CH: 21, CR: 21, CY: 28, CZ: 24, DE: 22, DK: 18, DO: 28, EE: 20, ES: 24,
        FI: 18, FO: 18, FR: 27, GB: 22, GI: 23, GL: 18, GR: 27, GT: 28, HR: 21,
        HU: 28, IE: 22, IL: 23, IS: 26, IT: 27, JO: 30, KW: 30, KZ: 20, LB: 28,
        LI: 21, LT: 20, LU: 20, LV: 21, MC: 27, MD: 24, ME: 22, MK: 19, MR: 27,
        MT: 31, MU: 30, NL: 18, NO: 15, PK: 24, PL: 28, PS: 29, PT: 25, QA: 29,
        RO: 24, RS: 22, SA: 24, SE: 24, SI: 19, SK: 24, SM: 27, TN: 24, TR: 26
    };
    var iban = String(input).toUpperCase().replace(/[^A-Z0-9]/g, ''), // keep only alphanumeric characters
            code = iban.match(/^([A-Z]{2})(\d{2})([A-Z\d]+)$/), // match and capture (1) the country code, (2) the check digits, and (3) the rest
            digits;
    // check syntax and length
    if (!code || iban.length !== CODE_LENGTHS[code[1]]) {
        return false;
    }
    // rearrange country code and check digits, and convert chars to ints
    digits = (code[3] + code[1] + code[2]).replace(/[A-Z]/g, function (letter) {
        return letter.charCodeAt(0) - 55;
    });
    // final check
    return mod97(digits);
}
function mod97(string) {
    var checksum = string.slice(0, 2), fragment;
    for (var offset = 2; offset < string.length; offset += 7) {
        fragment = String(checksum) + string.substring(offset, offset + 7);
        checksum = parseInt(fragment, 10) % 97;
    }
    return checksum;
}

*/
