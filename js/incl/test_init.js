function test(then, expected) {
	results.total++;
	if (then !== expected) {
        results.bad++;
		console.error('ERROR: Expected ' + expected + ', but was ' + then);
	}
}
var results = {
	total: 0,
	bad: 0
};
