$(function() {
  /**
   * ellipsizeTextBox - Kürzt die Texte in allen Elementen der angegebenene Klasse
   *
   * @param  {String} elementclass Klasse der Elemente in denen gekürzt werden soll
   * @return {boolean}              true
   */
  function ellipsizeTextBox(elementclass) {
    var elements = document.getElementsByClassName(elementclass);
    for (var i = 0; i < elements.length; i++) {
      el = elements[i];
      var wordArray = el.innerHTML.split(' ');
      while (el.scrollHeight > el.offsetHeight) {
        wordArray.pop();
        el.innerHTML = wordArray.join(' ');
      }
    }
    return true;
  }
  ellipsizeTextBox('textinput_textarea');
  ellipsizeTextBox('headline_input');

});
