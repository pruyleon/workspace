<?php
if(!isset($prefix)) $prefix = URL_ROOT;
?>

<link type="text/css" rel="stylesheet" href="/signed/incl/footer/style.css"/>

<div id="footer_content">
    <div class="footer_side_box">
        <img id="footer_logo" src="/src/icns/Logo.png" alt="Starbook Footer Logo"/>
    </div>

    <div class="footer_side_box footer_link_box">
        <a class="footer_link" href="#" title="Support kontaktieren">Support</a>
        <a class="footer_link" href="#" title="Kontakt herstellen">Kontakt</a>
    </div>

    <div class="footer_side_box footer_link_box">
        <a class="footer_link" href="/legal/impressum" title="Impressum aufrufen">Impressum</a>
        <a class="footer_link" href="/legal/datenschutz" title="Datenschutz einsehen">Datenschutz</a>
        <a class="footer_link" href="/legal/agb" title="AGB anzeigen">AGB</a>
    </div>

    <div class="footer_side_box footer_social_box">
        <div>
            <a class="footer_link_img" href="#" title="Starbook Instagram-Profil" target="_blank">
                <img src="/src/icns/social/instagram.svg" alt="Instagram Logo" />
            </a>
            <a class="footer_link_img" href="#" title="Starbook Facebook-Profil" target="_blank">
                <img src="/src/icns/social/facebook.svg" alt="Facebook Logo" />
            </a>
            <a class="footer_link_img" href="#" title="Starbook Google+ Profil" target="_blank">
                <img src="/src/icns/social/google.svg" alt="Google+ Logo" />
            </a>
        </div>
    </div>
</div>
