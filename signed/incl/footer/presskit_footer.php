<div class="footer_wrapper">
  <div class="footer_content">
    <div class="footer_top_border">

    </div>
    <div class="footer_box_left fontsize_12">
      <div class="footer_box_header bold">
        STAY CONNECTED
      </div>
      <div class="footer_content_wrapper">
        <div class="footer_content_box">
          Join our nightlife community to receive special offers, exclusive artists and ipdates on mypressk.it
        </div>
        <div class="footer_content_box">
          <input id="footer_mail" type="text" name="" placeholder="...E-mail address" value=""> <button id="footer_button" type="button" class="blue_btn normal_button_edgy" name="button">Sign up</button>
        </div>
        <div id="first_logos" class="footer_content_box">
          <a href="/"> <img id="footer_logo" src="/signed/src/logos/logo_full_white.svg" alt="mypressk.it"> </a>
          <a href="#"> <img class="social_links" src="/signed/src/icns/switch_icons/facebook_white.svg" alt="facebook"> </a>
          <a href="#"> <img class="social_links" src="/signed/src/icns/switch_icons/instagram_white.svg" alt="Instagram"> </a>
          <a href="#"> <img class="social_links" src="/signed/src/icns/switch_icons/youtube_white.svg" alt="Youtube"> </a>
        </div>
      </div>
    </div>
    <div class="footer_box_middle fontsize_12">
      <div class="footer_box_header bold">
        BECOME PART OF THE MOVEMENT
      </div>
      <div class="footer_content_wrapper">
        <div class="footer_content_box">
          Build your own pressk.it and join our global artist community. Promote your brand, grow your network and get found by thousands of potential bookers and venues.
        </div>
        <div class="footer_content_box">
          <a href="" id="buildyour" class="footer_link_color">Build your own pressk.it</a>
        </div>
        <div class="footer_content_box">
          It was never that easy to find and book the perfect artist for your events - 100% commission free.
        </div>
        <div class="footer_content_box">
          <a href="" class="footer_link_color">Try our mypressk.it artist search</a>
        </div>
      </div>
    </div>
    <div class="footer_box_right fontsize_12">
      <div class="footer_box_header bold">
        NAVIGATE
      </div>
      <div class="footer_content_wrapper navigate">
        <div class="">
          <a href="#" class="footer_link">Contact <img src="/signed/src/icns/arrow_right_gray.svg" alt=""></a>
        </div>
        <div class="">
          <a href="#" class="footer_link">Carreer <img src="/signed/src/icns/arrow_right_gray.svg" alt=""></a>
        </div>
        <div class="">
          <a href="#" class="footer_link">About us <img src="/signed/src/icns/arrow_right_gray.svg" alt=""></a>
        </div>
        <div class="">
          <a href="#" class="footer_link">Imprint <img src="/signed/src/icns/arrow_right_gray.svg" alt=""></a>
        </div>
        <div class="">
          <a href="#" class="footer_link">Privacy Policy <img src="/signed/src/icns/arrow_right_gray.svg" alt=""></a>
        </div>
      </div>
    </div>
    <div class="footer_bottom_desc fontsize_8">
      2019 Copyright&copy; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Syncronight UG (haftungsbeschränkt)
    </div>
  </div>
</div>
