<html lang="de">
  <head>
    <meta charset="utf-8">
    <?php
      include('./php/login_state.php');
      only_logged();
      include("../src/header.php");
      include("./php/gen.php");
      $user_id = USER;
      $artist_id = getArtistID($user_id);
      $artist_name = getArtistNameAccID($user_id);
     ?>
    <title>Choose your Style</title>
    <link rel="stylesheet" href="/signed/css/choose_style.css">
    <link rel="stylesheet" href="/signed/css/profile_elements/gen.css">
    <link rel="stylesheet" href="/signed/css/profile_elements/profile.css">
    <link rel="stylesheet" href="/signed/css/profile_elements/profile_mobile.css">
    <link rel="stylesheet" href="/signed/incl/menue/menue_min.css">
    <link rel="stylesheet" href="/signed/css/style_preview.css">
    <script type="text/javascript" src="/signed/js/choose_style.js"></script>
    <script type="text/javascript" src="/signed/js/style_preview.js"></script>
  </head>
  <body>
    <?php
      $menue_color = "white";
      include('./incl/menue/menue_min.php');
     ?>
     <input type="hidden" name="user_id" value="<?php echo $user_id ?>">
     <input type="hidden" name="artist_id" value="<?php echo $artist_id ?>">
     <div class="top_wrapper_fixed">
       <div class="progressbar_wrapper">
         <div class="progressbar_desc complete">
           Willkommen im Profileditor
         </div>
       </div>
       <div class="progress_header">
         Choose your Template
       </div>
     </div>
     <div class="outer_wrapper">
       <div class="template_outer_wrapper">
         <div class="template_wrapper left" data-id="miami">
           <div class="template_body">
             <div class="template_body_inner">
               <img src="/signed/php/profile_elements/preview_full/miami.png" alt="">
             </div>
           </div>
           <div class="template_desc">
             <div class="left">
               Miami
             </div>
             <div class="right">
               <button type="button" class="normal_button  preview" name="button">Vorschau</button>
              <a href="/builder/miami"><button type="button" class="normal_button choose" data-id="1" name="button">Wählen</button></a>
             </div>
           </div>
         </div>
         <div class="template_wrapper right" data-id="2">
           <div class="template_body">
             <div class="template_body_inner">

             </div>
           </div>
           <div class="template_desc">
             <div class="left">
               Template 2
             </div>
             <div class="right">
               <button type="button" class="normal_button  preview" name="button">Vorschau</button>
               <a href="/builder/NULL"><button type="button" class="normal_button  choose" data-id="2" name="button">Wählen</button></a>
             </div>
           </div>
         </div>
         <div class="template_wrapper left" data-id="3">
           <div class="template_body">
             <div class="template_body_inner">

             </div>
           </div>
           <div class="template_desc">
             <div class="left">
               Template 3
             </div>
             <div class="right">
               <button type="button" class="normal_button  preview" name="button">Vorschau</button>
               <a href="/builder/NULL"><button type="button" class="normal_button  choose" data-id="2" name="button">Wählen</button></a>
             </div>
           </div>
         </div>
         <div class="template_wrapper right" data-id="4">
           <div class="template_body">
             <div class="template_body_inner">

             </div>
           </div>
           <div class="template_desc">
             <div class="left">
               Template 4
             </div>
             <div class="right">
               <button type="button" class="normal_button  preview" name="button">Vorschau</button>
               <a href="/builder/NULL"><button type="button" class="normal_button  choose" data-id="2" name="button">Wählen</button></a>
             </div>
           </div>
         </div>
         <div class="template_wrapper left" data-id="5">
           <div class="template_body">
             <div class="template_body_inner">

             </div>
           </div>
           <div class="template_desc">
             <div class="left">
               Template 5
             </div>
             <div class="right">
               <button type="button" class="normal_button  preview" name="button">Vorschau</button>
               <a href="/builder/NULL"><button type="button" class="normal_button  choose" data-id="2" name="button">Wählen</button></a>
             </div>
           </div>
         </div>
         <div class="template_wrapper right" data-id="6">
           <div class="template_body">
             <div class="template_body_inner">

             </div>
           </div>
           <div class="template_desc">
             <div class="left">
               Template 6
             </div>
             <div class="right">
               <button type="button" class="normal_button  preview" name="button">Vorschau</button>
               <a href="/builder/NULL"><button type="button" class="normal_button  choose" data-id="2" name="button">Wählen</button></a>
             </div>
           </div>
         </div>
         <div class="template_wrapper left" data-id="7">
           <div class="template_body">
             <div class="template_body_inner">

             </div>
           </div>
           <div class="template_desc">
             <div class="left">
               Template 7
             </div>
             <div class="right">
               <button type="button" class="normal_button  preview" name="button">Vorschau</button>
               <a href="/builder/NULL"><button type="button" class="normal_button  choose" data-id="2" name="button">Wählen</button></a>
             </div>
           </div>
         </div>
         <div class="template_wrapper right" data-id="8">
           <div class="template_body">
             <div class="template_body_inner">

             </div>
           </div>
           <div class="template_desc">
             <div class="left">
               Template 8
             </div>
             <div class="right">
               <button type="button" class="normal_button  preview" name="button">Vorschau</button>
               <a href="/builder/NULL"><button type="button" class="normal_button  choose" data-id="2" name="button">Wählen</button></a>
             </div>
           </div>
         </div>
       </div>
     </div>

     <!-- Template-Vorschau -->
     <div class="" id="preview_wrapper">
       <div id="preview_body">

       </div>
       <div id="preview_controls">
         <div class="inner">
           <button type="button" class="back_selection gray_btn" id="back_selection" name="button">Zurück zur Auswahl</button>
           <div class="preview_controls_switch_wrapper">
             <button type="button" id="previous_template" name="button">back</button>
             <div class="mobile_switch_wrapper">
               <div class="mobile_switch_inner">
                 <button type="button" class="switch_button active" name="button" data-view="desktop"> <img src="/signed/src/icns/desktop.svg" alt="Desktop"> </button>
                 <button type="button" class="switch_button" name="button" data-view="mobile"> <img src="/signed/src/icns/mobile.svg" alt="Desktop"> </button>
               </div>
             </div>
             <button type="button" id="next_template" name="button">next</button>
           </div>
           <a href="#" id="choose_template_link"><button type="button" class="choose_template blue_btn" name="button">Vorlage wählen</button></a>
         </div>
       </div>
     </div>
  </body>
</html>
