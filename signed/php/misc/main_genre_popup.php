<?php

  //Level 1 Genres
  $level_1_genres = ['3', '14', '15', '16', '20', '21', '22', '33', '34', '40', '43', '45', '46', '48', '52', '54', '59', '63', '64', '65'];
  $level_1_genres_1 = ['3', '14', '15', '16'];
  $level_1_genres_2 = ['20', '21', '22', '33'];
  $level_1_genres_3 = ['34', '40', '43', '45'];
  $level_1_genres_4 = ['46', '48', '52', '54'];
  $level_1_genres_5 = ['59', '63', '64', '65'];

  $level_2_genres = [];
  $level_2_genres_types = [];
  $genre_row_1 = [];
  $genre_row_2 = [];
  $genre_row_3 = [];
  $genre_row_4 = [];
  $genre_row_5 = [];
  //Genres ziehen
  $query = "SELECT * FROM genre";
  $result = mysqli_query($con, $query);
  while ($res = mysqli_fetch_assoc($result)) {
      //Genre in level_1_genres? -> Skip
      $genre_types[$res['ID']] = $res['genre_type'];
      if (!in_array($res['ID'], $level_1_genres)) {
          $level_2_genres[] = $res['ID'];
      }
  }
  $genre_2_length = count($level_2_genres);
  $level_2_genres_sorted = array_fill(0, $genre_2_length, 'NULL');
  for ($i=0; $i < $genre_2_length; $i++) {
      if ($i <= $genre_2_length / 5) {
          $genre_row_0[] = $level_2_genres[$i];
      } elseif ($i <= ($genre_2_length / 5) * 2) {
          $genre_row_1[] = $level_2_genres[$i];
      } elseif ($i <= ($genre_2_length / 5) * 3) {
          $genre_row_2[] = $level_2_genres[$i];
      } elseif ($i <= ($genre_2_length / 5) * 4) {
          $genre_row_3[] = $level_2_genres[$i];
      } elseif ($i <= ($genre_2_length / 5) * 5) {
          $genre_row_4[] = $level_2_genres[$i];
      }
  }
?>

<div class="popup_background level_2_popup" id="main_genre_popup">
  <div class="popup_body" id="main_genre_popup_body">
    <div class="popup_title">
      <div class="popup_title_content">
        Wähle deine 3 Hauptgenre: <span id="selected_genres"></span>
      </div>
      <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
    </div>
    <?php
      echo '<div class="genre_row">';
      for ($i=0; $i < count($level_1_genres); $i++) {
          if ($i % 4 == 0) {
              echo '</div><div class="genre_row">';
          }
          echo '<div class="genre_button" data-id="'.$level_1_genres[$i].'">'.$genre_types[$level_1_genres[$i]].'</div>';
      }
      echo '</div>';

    ?>

    <div class="full_width">
      <button type="button" name="button" id="extend_genre_listing">Weitere Genre</button>
    </div>
    <div class="genre_wrapper additional_genres">
      <?php

      //Untere Genres
      echo '<div class="genre_row">';
      foreach ($genre_row_0 as $key) {
          echo '<div class="genre_button" data-id="'.$key.'">'.$genre_types[$key].'</div>';
      }
      echo '</div>';
      echo '<div class="genre_row">';
      foreach ($genre_row_1 as $key) {
          echo '<div class="genre_button" data-id="'.$key.'">'.$genre_types[$key].'</div>';
      }
      echo '</div>';
      echo '<div class="genre_row">';
      foreach ($genre_row_2 as $key) {
          echo '<div class="genre_button" data-id="'.$key.'">'.$genre_types[$key].'</div>';
      }
      echo '</div>';
      echo '<div class="genre_row">';
      foreach ($genre_row_3 as $key) {
          echo '<div class="genre_button" data-id="'.$key.'">'.$genre_types[$key].'</div>';
      }
      echo '</div>';
      echo '<div class="genre_row">';
      foreach ($genre_row_4 as $key) {
          echo '<div class="genre_button" data-id="'.$key.'">'.$genre_types[$key].'</div>';
      }
      echo '</div>';
      echo '<div class="genre_row">';
      foreach ($genre_row_5 as $key) {
          echo '<div class="genre_button" data-id="'.$key.'">'.$genre_types[$key].'</div>';
      }
      echo '</div>';
     ?>

    </div>
  </div>
</div>
