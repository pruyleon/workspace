<div class="mobile_header mobile_element" style="background-image: url(/signed/php/profile_elements/previews_full/src/rapture6.jpg)">
  <div class="header_menue">
      <div class="menue_left">
        <img src="/signed/src/logos/logo_full_white.svg" alt="">
      </div>
      <div class="menue_right">
        <img src="/signed/src/icns/burger_white.svg" alt="">
      </div>
    </div>
    <div class="name_wrapper">
      DJ RAPTURE
    </div>
    <div class="arrow_wrapper">
      <img src="/signed/src/icns/arrow_down_white.svg" alt="">
    </div>
  </div>
  <div class="mobile_infos">
    <div class="loc_wrapper">
      Mannheim | Germany
    </div>
    <div class="genre_wrapper">
      | Hip Hop
      <br>
      | RnB
      <br>
      | Reggaeton
    </div>
    <div class="actions">
      <button type="button" name="button" class="presspack_btn transparent presspack_popup_trigger">Presspack</button>
      <button type="button" name="button" class="request_button request_popup_trigger">Anfragen</button>
    </div>
  </div>
