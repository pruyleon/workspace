<div class="profile_element size_L  input_parent header relative" data-id="H1">
  <?php
    $logo = true;
    include('../signed/php/profile_elements/presskit/comps/header_menue_lang.php');
   ?>
  <div class="profile_element_content relative placement_wrapper element_background h1">
    <div class="menue_parent full_size_background full_size background absolute input_parent" style="background-image: url(<?php echo $background ?>)">
      <div class="header_colorgradient">

      </div>
      <!-- <div class="full_size_background full_size absolute background hover_blue_inlineshadow format_parent darkelement" data-format="full">

      </div> -->
    </div>

    <div class="name_city_wrapper h1 content hover_blue_inlineshadow" title="Name positionieren" data-font-size="49">
      <div class="name">
        <div class="name_wrapper">
          <?php echo strtoupper($artist_name); ?>
        </div>
        <div class="city_wrapper">
          <?php echo "$city | $country" ?>
        </div>
      </div>
      <div class="logo">

      </div>
    </div>
    <div class="bottom_H1_wrapper content">
      <div class="bottom_wrapper">
        <div class="social_media_wrapper social_media_trigger hover_blue_inlineshadow cursor_pointer">
          <?php
            include('../signed/php/profile_elements/presskit/comps/social_media_bw_round.php');
           ?>
           <div class="social_media_tooltip fixed_tooltip fontsize_14">
             Social Media bearbeiten
           </div>
        </div>
        <div class="genre_listing cursor_pointer fontsize_12">
          <?php
            echo $genre_string
           ?>
        </div>
        <div class="presspack_request fontsize_12">
          <?php
            $no_switch = true;
            include('../signed/php/profile_elements/presskit/comps/presspack_request.php');
            $no_switch = false;
           ?>
        </div>
      </div>

    </div>
  </div>
</div>
