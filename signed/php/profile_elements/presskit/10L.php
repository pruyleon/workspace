<div class="profile_element size_L" data-id="10L">
 <div class="profile_element_content">
   <div class="inner_profile_element_content">
     <div class="full_size full_size_background" style="background-image: url(<?php echo $background ?>)">
     </div>
     <div class="headline_input absolute horz_centered height_3 width_60 top_77 hover_blue_inlineshadow">
       <?php echo $headline ?>
     </div>
     <div class="textinput_textarea absolute horz_centered height_10 width_60 top_82 hover_blue_inlineshadow">
       <?php echo $text ?>
     </div>

   </div>
 </div>
</div>
