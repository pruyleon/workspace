<div class="profile_element size_L font_color_white" data-id="3L">
  <div class="profile_element_content">
    <div class="inner_profile_element_content">
      <div class="one_third absolute full_height right background_light">
        <div class="absolute_inner_content text_color_parent hover_blue_inlineshadow" style="background-color: <?php echo $background_color ?>">
          <div class="headline_input absolute top_9 width_74 height_3 horz_centered hover_blue_inlineshadow">
            <?php echo $headline ?>
          </div>
          <div class="textinput_textarea absolute width_74 height_78 top_13 horz_centered hover_blue_inlineshadow">
              <?php echo $text ?>
          </div>
        </div>

      </div>
      <div class="two_third absolute full_height left darkelement menue_parent input_parent">
        <div class="absolute_inner_content content format_parent hover_blue_inlineshadow"  data-format="twothird_l" style="background-image: url(<?php echo $background ?>)">
        </div>
      </div>
    </div>
  </div>
</div>
