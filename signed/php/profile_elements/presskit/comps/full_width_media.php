<div class="media_wrapper absolute menue_parent format_parent input_parent" data-format="<?php echo $format ?>">
   
<input type="hidden" name="value" maxlength="9999999999" value="">
  <input type="hidden" name="originals" value="">
  <input type="hidden" name="thumbnails" maxlength="9999999999" value="">
  <input type="hidden" name="type" value="">
  <input type="hidden" name="icons" value="">
  <div class="element_menue">

  </div>
  <div class="full_width_media relative content hover_blue_inlineshadow darkelement">
    <div class="builder_icons_wrapper absolute full_center">
      <?php if (in_array('ph', $media)): ?>
        <img src="/signed/src/icns/filter/photo.svg" class="builder_icon" title="Foto hinzufügen"data-type="photo" data-action="media" alt="Foto">
      <?php endif; ?>
      <?php if (in_array('ss', $media)): ?>
        <img src="/signed/src/icns/filter/slideshow.svg" class="builder_icon" title="Slideshow hinzufügen" data-type="slideshow" data-format="<?php echo $slideshow_format ?>" data-action="media" alt="Slideshow">
      <?php endif; ?>
      <?php if (in_array('vd', $media)): ?>
        <img src="/signed/src/icns/filter/video.svg" class="builder_icon" title="Video hinzufügen" data-type="video" data-action="media" alt="Video">
      <?php endif; ?>
    </div>
  </div>
</div>
