<div class="header_top_line left color_change">
  <?php if ($logo): ?>
    <div class="logo_wrapper">
      <img src="/signed/src/logos/logo_full_white.png"  data-fileformat="png" alt="mypressk.it">
    </div>
  <?php endif; ?>
</div>

<div class="header_top_line right fontsize_10 color_change">
  <div class="burger_menue_wrapper">
    <img src="/signed/src/icns/burger_white.svg" alt="">
  </div>
  <div class="lang_wrapper">
    <div class="language_trigger cursor_pointer color_change font_color_white hover_blue_inlineshadow">
      <span class="language">DE</span><img src="/signed/src/icns/dropdown_white.svg" alt="">
    </div>
    <div class="language_dropdown">
      <div class="language_dropdown_line">
        DE
      </div>
      <div class="language_dropdown_line">
        EN
      </div>
      <div class="language_dropdown_line">
        ES
      </div>
      <div class="language_dropdown_line">
        FR
      </div>
      <div class="language_dropdown_line">
        IT
      </div>
      <div class="language_dropdown_line">
        JA
      </div>
      <div class="language_dropdown_line">
        NL
      </div>
      <div class="language_dropdown_line">
        PL
      </div>
      <div class="language_dropdown_line">
        RU
      </div>
      <div class="language_dropdown_line">
        ZH
      </div>
    </div>
  </div>
</div>
