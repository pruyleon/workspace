<div class="absolute audio_full_bottom audio_element_wrapper" data-audiotype="audio_full_bottom" data-activeplayer="a">
  <div class="arrow">
    <img src="/signed/src/icns/arrow_left_white_simple.svg" class="left" alt="">
  </div>
  <div class="audio_element_wrapper empty">
    <div class="audio_title">
      TITLE TITLE TITLE
    </div>
    <div class="audio_wave_wrapper">
      <img src="/signed/src/icns/audio/full.svg" data-state="pause" alt="">
    </div>
  </div>
  <div class="arrow">
    <img src="/signed/src/icns/arrow_right_white_simple.svg" class="right" alt="">
  </div>
</div>
