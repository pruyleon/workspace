<?php
    if (isset($_POST['ajax'])) {
      $prefix = "../../../";
      $artist_name = $_POST['artist_name'];
      $genre_string = $_POST['genre_string'];
      $artist_city_country = $_POST['artist_city_country'];
      $lorem_ipsum_placeholder = $_POST['lorem_ipsum_placeholder'];
      $headline_placeholder = $_POST['headline_placeholder'];
    }else {
      $prefix = "./";
    }
 ?>
<div class="profile_element size_L font_color_white" data-id="A4L">
  <div class="profile_element_content">
    <div class="inner_profile_element_content">
      <div class="full_size full_size_background menue_parent input_parent color_switch" data-color="white">

        <input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
        <input type="hidden" name="originals" value="">
        <input type="hidden" name="type" value="">
        <input type="hidden" name="icons" value="">
        <div class="element_menue">

        </div>
        <div class="full_size background  hover_blue_inlineshadow lightelement">
          <div class="builder_icons_wrapper left_center absolute">
            <img src="/signed/src/icns/filter/photo.svg" class="builder_icon" title="Foto hinzufügen" data-action="background" data-type="photo" alt="photo">
          </div>
        </div>
      </div>
      <div class="content free_text_wrapper height_82 absolute one_third top_9 right_7 background_blur_dark">
        <div class="absolute_inner_content text_color_parent">
          <div class="horz_centered top_6 width_74 absolute fontsize_14 font_weight_heavy">
            <?php echo $artist_name ?>
          </div>
          <div class="horz_centered top_9 width_74 absolute">
            <?php echo $artist_city_country ?>
          </div>
          <div class="horz_centered top_14 width_74 absolute">
            <?php echo $genre_string ?>
          </div>
          <div class="textinput_textarea absolute top_19 height_68 width_74 horz_centered hover_blue_inlineshadow">
            <?php echo $lorem_ipsum_placeholder ?>
          </div>
          <div class="presspack_request_wrapper large horz_centered bottom_5">
            <?php
              include($prefix.'php/profile_elements/raw/comps/presspack_request.php');
             ?>
          </div>

        </div>
      </div>

    </div>
      <input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
      <input type="hidden" name="titles" value="">
      <input type="hidden" name="covers" value="">
      <input type="hidden" name="wave_colors" value="">
      <input type="hidden" name="type" value="">
      <input type="hidden" name="icons" value="">
      <input type="hidden" name="file_names" value="">
      <input type="hidden" name="originals" value="">
  </div>
</div>
