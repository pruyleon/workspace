<?php
    if (isset($_POST['ajax'])) {
      $prefix = "../../../";
      $lorem_ipsum_placeholder = $_POST['lorem_ipsum_placeholder'];
      $headline_placeholder = $_POST['headline_placeholder'];
    }else {
      $prefix = "./";
    }
 ?>
 <div class="profile_element size_L" data-id="9L">
  <div class="profile_element_content input_parent">
    <div class="inner_profile_element_content ">
      <div class="full_size full_size_background input_parent menue_parent">
        <input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
        <input type="hidden" name="type" value="">
        <input type="hidden" name="icons" value="">
        <input type="hidden" name="originals" value="">
        <div class="element_menue">

        </div>
        <div class="full_size background hover_blue_inlineshadow lightelement">
          <div class="builder_icons_wrapper top_position horz_centered">
            <img src="/signed/src/icns/filter/photo.svg" class="builder_icon" title="Foto hinzufügen" data-action="background" data-type="photo" alt="Audio">
            <img src="/signed/src/icns/filter/color.svg" class="builder_icon" title="Farbe hinzufügen" data-action="background" data-type="color" alt="Farbe">
          </div>
        </div>
      </div>
      <div class="content full_center height_82 width_82 absolute menue_parent input_parent format_parent"  data-format="full">
        <input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
        <input type="hidden" name="thumbnails" maxlength="9999999999" value="">
        <input type="hidden" name="originals" value="">
        <input type="hidden" name="type" value="">
        <input type="hidden" name="icons" value="">
        <div class="element_menue">

        </div>
        <div class="absolute_inner_content content hover_blue_inlineshadow darkelement video_div">
          <div class="builder_icons_wrapper vert_centered absolute horz_centered">
            <img src="/signed/src/icns/filter/photo.svg" class="builder_icon" title="Foto hinzufügen" data-action="media" data-type="photo" alt="Foto">
            <img src="/signed/src/icns/filter/slideshow.svg" class="builder_icon" title="Slideshow hinzufügen" data-action="media" data-type="slideshow" alt="Slideshow">
            <img src="/signed/src/icns/filter/video.svg" class="builder_icon" title="Video hinzufügen" data-action="media" data-type="video" alt="Video">
            <img src="/signed/src/icns/filter/gif.svg" class="builder_icon" title="Video hinzufügen" data-action="media" data-type="gif" alt="Video">
          </div>
        </div>
      </div>

      <input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
      <input type="hidden" name="titles" value="">
      <input type="hidden" name="type" value="">
      <input type="hidden" name="icons" value="">
      <input type="hidden" name="originals" value="">
    </div>
  </div>
</div>
