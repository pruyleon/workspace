<?php
    if (isset($_POST['ajax'])) {
      $prefix = "../../../";
      $lorem_ipsum_placeholder = $_POST['lorem_ipsum_placeholder'];
      $headline_placeholder = $_POST['headline_placeholder'];
    }else {
      $prefix = "./";
    }
    $audio_class = "absolute width_40 vert_centered right_9 sizeM";
 ?>
<div class="profile_element size_S font_color_white" data-id="19S">
  <div class="profile_element_content input_parent menue_parent">
    <div class="element_menue">

  </div>
    <div class="inner_profile_element_content menue_parent audioplayer_parent text_color_parent hover_blue_inlineshadow"   data-audiotype="audio_full_dropdown" data-audioplayer_id="NULL"  data-audioclass="<?php echo $audio_class ?>" data-coverformat="full_s">
      <div class="full_size background  darkelement">
        <div class="builder_icons_wrapper left_center vert_centered edit_icns_wrapper">
          <img src="/signed/src/icns/filter/audio.svg" class="builder_icon" title="Audio / Mixtape hinzufügen" data-action="media" data-type="audio" alt="Audio">
        </div>
      </div>
        <?php
        include($prefix.'php/profile_elements/raw/comps/audio_full_dropdown.php');
        ?>
      </div>
       
<input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
      <input type="hidden" name="titles" value="">
      <input type="hidden" name="covers" value="">
      <input type="hidden" name="wave_colors" value="">
      <input type="hidden" name="type" value="">
      <input type="hidden" name="icons" value="">
      <input type="hidden" name="file_names" value="">
      <input type="hidden" name="originals" value="">
  </div>
</div>
