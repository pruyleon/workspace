<div class="header_top_line left color_change">
  <?php if ($logo): ?>
    <div class="logo_wrapper">
      <img src="/signed/src/logos/logo_full_white.png"  data-fileformat="png" alt="mypressk.it">
    </div>
  <?php endif; ?>
</div>

<div class="header_top_line right fontsize_10 color_change">
  <div class="burger_menue_wrapper">
    <img src="/signed/src/icns/burger_white.svg" id="burger_header" alt="">
    <img src="/signed/src/icns/search_white.svg" id="search_header" alt="">
  </div>
</div>
