<?php
  $social_format = "full";
?>
<div class="social_media_bar_transparent content wide social_media_trigger hover_blue_inlineshadow fontsize_12 cursor_pointer <?php echo $social_format ?>">
  <div class="social_wrapper first">
    <div class="color_change">
      <img src="/signed/src/icns/switch_icons/facebook_white.svg" alt="" class=""> <span>Facebook</span>
    </div>
  </div>
  <div class="social_wrapper second">
    <div class="color_change">
      <img src="/signed/src/icns/switch_icons/instagram_white.svg" alt="" class=""> <span>Instagram</span>
    </div>
  </div>
  <div class="social_wrapper third">
    <div class="color_change">
      <img src="/signed/src/icns/switch_icons/youtube_white.svg" alt="" class=""> <span>Youtube</span>
    </div>
  </div>
  <div class="social_media_tooltip fixed_tooltip">
    Social Media bearbeiten
  </div>
</div>
