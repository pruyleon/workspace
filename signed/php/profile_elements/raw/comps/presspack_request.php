<?php
  if (isset($no_switch) && $no_switch == true) {
      $color_change = "";
  } else {
      $color_change = "color_change";
  }
 ?>

<button type="button" name="button" class="presspack_btn transparent presspack_popup_trigger blue_border_btn hover_blue_inlineshadow <?php echo $color_change ?> no_add_plus">
  <img src="/signed/src/icns/switch_icons/presspack_white.svg" alt="" class="color_switch_svg">
  <div class="presspack_tooltip fixed_tooltip fontsize_12">
    Presspack bearbeiten
  </div>
</button>

<button type="button" name="button" class="request_button request_popup_trigger blue_border_btn hover_blue_inlineshadow <?php echo $color_change ?> no_add_plus">
  <img src="/signed/src/icns/switch_icons/request_white.svg" alt="" class="color_switch_svg">
  <div class="request_tooltip fixed_tooltip fontsize_12">
    Anfrage bearbeiten
  </div>
</button>
