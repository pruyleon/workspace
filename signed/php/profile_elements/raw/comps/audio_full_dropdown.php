<?php
    if (isset($_POST['audio_class'])) {
      $audio_class = $_POST['audio_class'];
    }
 ?>
<div class="audio_element_wrapper empty <?php echo $audio_class ?> audio_full_dropdown" data-audiotype="audio_full_dropdown" data-activeplayer="a">
  <div class="audio_title">
    <img src="/signed/src/icns/playbutton_gray.svg" class="playbutton" alt="" data-active_playerid="NULL">
    <div class="playbutton" data-active_playerid="NULL" data-state="pause"></div>
    <div class="audio_dropdown cursor_pointer">
      <div class="title"> TITLE TITLE TITLE </div><img src="/signed/src/icns/dropdown_white.svg" class="dropdown_img" alt="">
      <div class="dropdown">
      </div>
    </div>
  </div>
  <div class="audio_wave_wrapper">
    <img src="/signed/src/icns/audio/full.svg" alt="">
  </div>
</div>
