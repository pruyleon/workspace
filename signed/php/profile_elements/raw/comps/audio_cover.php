<?php
    if (isset($_POST['audio_class'])) {
      $audio_class = $_POST['audio_class'];
    }
 ?>
 <div class="audio_element_wrapper empty <?php echo $audio_class ?> audio_full_cover" data-audiotype="audio_full_cover">
   <div class="element_audio_cover">
     <div class="cover relative">
       <img src="/signed/src/icns/playbutton_gray.svg" class="playbutton full_center absolute" data-state="pause" alt="">
     </div>
   </div>
   <div class="audio_wave">
     <div class="audio_title">
       <div class="audio_dropdown cursor_pointer">
         <div class="title"> TITLE TITLE TITLE </div><img src="/signed/src/icns/dropdown_white.svg" class="dropdown_img" alt="">
         <div class="dropdown">
         </div>
       </div>
     </div>
     <div class="audio_wave_wrapper">
       <img src="/signed/src/icns/audio/full.svg" alt="">
     </div>
   </div>
</div>
