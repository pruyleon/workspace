<?php
    if (isset($_POST['ajax'])) {
        $prefix = "../../../";
        $artist_name = $_POST['artist_name'];
        $genre_string = $_POST['genre_string'];
        $artist_city_country = $_POST['artist_city_country'];
        $lorem_ipsum_placeholder = $_POST['lorem_ipsum_placeholder'];
        $headline_placeholder = $_POST['headline_placeholder'];
    } else {
        $prefix = "./";
    }
 ?>
<div class="profile_element size_L font_color_white" data-id="A3L">
  <div class="profile_element_content">
    <div class="inner_profile_element_content">
      <div class="one_third absolute full_height right background_light lightelement background">
        <div class="absolute_inner_content text_color_parent">
          <div class="full_size_background full_size" data-color="white">
            <div class="full_size full_size_background input_parent menue_parent color_switch">

              <input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
              <input type="hidden" name="originals" value="">
              <input type="hidden" name="type" value="">
              <input type="hidden" name="icons" value="">
              <div class="element_menue">
              </div>
              <div class="full_size full_size_background background">
                <div class="builder_icons_wrapper top horz_centered absolute">
                  <img src="/signed/src/icns/filter/color.svg" class="builder_icon" title="Farbe hinzufügen" data-action="background" data-type="color" alt="Color">
                </div>
              </div>
            </div>
          </div>
          <div class="horz_centered top_9 width_74 absolute fontsize_14 font_weight_heavy content">
            <?php echo $artist_name ?>
          </div>
          <div class="horz_centered top_12 width_74 absolute">
            <?php echo $artist_city_country ?>
          </div>

          <div class="textinput_textarea absolute width_74 height_59 top_18 horz_centered hover_blue_inlineshadow content">
            <?php echo $lorem_ipsum_placeholder ?>
          </div>
          <div class="horz_centered top_80 width_74 absolute text_align_center font_weight_heavy">
            <?php echo $genre_string ?>
          </div>

        </div>
      </div>
      <div class="two_third absolute full_height left darkelement content menue_parent input_parent format_parent" data-format="twothird_l">

        <input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
        <input type="hidden" name="thumbnails" maxlength="9999999999" value="">
        <input type="hidden" name="originals" value="">
        <input type="hidden" name="type" value="">
        <input type="hidden" name="icons" value="">
        <div class="element_menue">

        </div>
        <div class="absolute_inner_content hover_blue_inlineshadow content video_div">
          <div class="builder_icons_wrapper full_center top absolute">
            <img src="/signed/src/icns/filter/photo.svg" class="builder_icon" title="Foto hinzufügen" data-action="media" data-type="photo" alt="photo">
            <img src="/signed/src/icns/filter/slideshow.svg" class="builder_icon" title="Slideshow hinzufügen" data-action="media" data-type="slideshow" alt="slideshow">
            <img src="/signed/src/icns/filter/video.svg" class="builder_icon" title="Video hinzufügen" data-action="media" data-type="video" alt="Video">
          </div>
        </div>
      </div>
      <div class="presspack_request_wrapper large right_third_centered bottom_centered">
        <?php
          include($prefix.'php/profile_elements/raw/comps/presspack_request.php');
         ?>
      </div>
    </div>


<input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
      <input type="hidden" name="titles" value="">
      <input type="hidden" name="covers" value="">
      <input type="hidden" name="wave_colors" value="">
      <input type="hidden" name="type" value="">
      <input type="hidden" name="icons" value="">
      <input type="hidden" name="file_names" value="">
      <input type="hidden" name="originals" value="">
  </div>
</div>
