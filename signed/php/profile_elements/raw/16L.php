<?php
    if (isset($_POST['ajax'])) {
      $prefix = "../../../";
      $lorem_ipsum_placeholder = $_POST['lorem_ipsum_placeholder'];
      $headline_placeholder = $_POST['headline_placeholder'];
    }else {
      $prefix = "./";
    }
    $common_pos_classes = "absolute width_40 right_9";
    $audio_class = "top_76 $common_pos_classes";
 ?>
<div class="profile_element size_L font_color_white" data-id="16L">
  <div class="profile_element_content input_parent menue_parent">
    <div class="element_menue">

    </div>
    <div class="inner_profile_element_content menue_parent audioplayer_parent text_color_parent"  data-audiotype="audio_full_dropdown" data-audioplayer_id="NULL"  data-audioclass="<?php echo $audio_class ?>" data-coverformat="full_l">
      <div class="full_size background hover_blue_inlineshadow darkelement">
        <div class="builder_icons_wrapper left_center vert_centered edit_icns_wrapper">
          <img src="/signed/src/icns/filter/audio.svg" class="builder_icon" title="Audio / Mixtape hinzufügen" data-action="media" data-type="audio" alt="Audio">
        </div>
      </div>
      <div class="full_size background_filter hidden">

      </div>
        <?php
          $headline_class = "top_8  $common_pos_classes";
          include($prefix.'php/profile_elements/raw/comps/headline.php');
          $text_class = "top_17 height_50 $common_pos_classes";
          include($prefix.'php/profile_elements/raw/comps/text_full.php');
          include($prefix.'php/profile_elements/raw/comps/audio_full_dropdown.php');
        ?>
    </div>
     
<input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
    <input type="hidden" name="titles" value="">
    <input type="hidden" name="covers" value="">
    <input type="hidden" name="wave_colors" value="">
    <input type="hidden" name="type" value="">
    <input type="hidden" name="icons" value="">
    <input type="hidden" name="file_names" value="">
    <input type="hidden" name="originals" value="">
  </div>
</div>
