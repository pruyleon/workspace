<?php
    if (isset($_POST['ajax'])) {
      $prefix = "../../../";
      $artist_name = $_POST['artist_name'];
      $genre_string = $_POST['genre_string'];
      $artist_city_country = $_POST['artist_city_country'];
      $lorem_ipsum_placeholder = $_POST['lorem_ipsum_placeholder'];
      $headline_placeholder = $_POST['headline_placeholder'];
    }else {
      $prefix = "./";
    }
 ?>
<div class="profile_element size_M font_color_white" data-id="4M">
  <div class="profile_element_content">
    <div class="inner_profile_element_content">
      <div class="one_third absolute full_height left darkelement menue_parent input_parent format_parent" data-format="onethird_m">

        <input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
        <input type="hidden" name="originals" value="">
        <input type="hidden" name="type" value="">
        <input type="hidden" name="icons" value="">
        <div class="element_menue">

        </div>
        <div class="absolute_inner_content hover_blue_inlineshadow content">
          <div class="builder_icons_wrapper full_center top absolute">
            <img src="/signed/src/icns/filter/photo.svg" class="builder_icon" title="Foto hinzufügen" data-action="media" data-type="photo" alt="photo">
            <img src="/signed/src/icns/filter/slideshow.svg" class="builder_icon" title="Slideshow hinzufügen" data-action="media" data-type="slideshow" alt="slideshow">
          </div>
        </div>
      </div>
      <div class="one_third absolute full_height third_middle  background color_switch" data-color="white">
        <div class="absolute_inner_content text_color_parent lightelement">
          <div class="full_size_background full_size input_parent menue_parent">

            <input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
            <input type="hidden" name="originals" value="">
            <input type="hidden" name="type" value="">
            <input type="hidden" name="icons" value="">
            <div class="element_menue">
              <div class="action_wrapper" data-type''>
                <span class="switch_color">
                  <img src="/signed/src/icns/switch_color_bw.svg" alt="Farbe">
                </span>
              </div>
            </div>
            <div class="full_size background hover_blue_inlineshadow">
              <div class="builder_icons_wrapper top horz_centered absolute">
                <img src="/signed/src/icns/filter/color.svg" class="builder_icon" title="Farbe hinzufügen" data-action="background" data-type="color" alt="Color">
              </div>
            </div>
          </div>
          <div class="absolute top_12 width_74 horz_centered font_weight_heavy fontsize_14 content">
            <?php echo $artist_name ?>
          </div>
          <div class="absolute top_16 width_74 horz_centered content">
            <?php echo $artist_city_country ?>
          </div>
          <div class="textinput_textarea absolute width_74 height_46 top_23 horz_centered hover_blue_inlineshadow">
            <?php echo $lorem_ipsum_placeholder ?>
          </div>
          <div class="absolute top_74 width_74 height_3 text_align_center horz_centered">
            <?php echo $genre_string ?>
          </div>

          <div class="presspack_request_wrapper large top_83 horz_centered bottom_12">
            <?php
              include($prefix.'php/profile_elements/raw/comps/presspack_request.php');
             ?>
          </div>
        </div>
      </div>
      <div class="one_third absolute full_height right darkelement menue_parent input_parent format_parent" data-format="onethird_m">

        <input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
        <input type="hidden" name="originals" value="">
        <input type="hidden" name="type" value="">
        <input type="hidden" name="icons" value="">
        <div class="element_menue">

        </div>
        <div class="absolute_inner_content hover_blue_inlineshadow content">
          <div class="builder_icons_wrapper full_center top absolute">
            <img src="/signed/src/icns/filter/photo.svg" class="builder_icon" title="Foto hinzufügen" data-action="media" data-type="photo" alt="photo">
            <img src="/signed/src/icns/filter/slideshow.svg" class="builder_icon" title="Slideshow hinzufügen" data-action="media" data-type="slideshow" alt="slideshow">
          </div>
        </div>
      </div>

    </div>


      <input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
      <input type="hidden" name="titles" value="">
      <input type="hidden" name="covers" value="">
      <input type="hidden" name="wave_colors" value="">
      <input type="hidden" name="type" value="">
      <input type="hidden" name="icons" value="">
      <input type="hidden" name="file_names" value="">
      <input type="hidden" name="originals" value="">
  </div>
</div>
