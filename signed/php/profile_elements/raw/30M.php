<?php
    if (isset($_POST['ajax'])) {
      $prefix = "../../../";
      $headline_placeholder = $_POST['headline_placeholder'];
    }else {
      $prefix = "./";
    }
 ?>
 <div class="profile_element size_M" data-id="30M">
  <div class="profile_element_content">
    <div class="inner_profile_element_content" data-format="full_m">
      <div class="full_size full_size_background menue_parent input_parent">

<input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
        <input type="hidden" name="originals" value="">
        <input type="hidden" name="type" value="">
        <input type="hidden" name="icons" value="">
        <div class="element_menue">

        </div>
        <div class="full_size background content lightelement">
          <div class="builder_icons_wrapper horz_centered bottom_7 media_icns_wrapper">
            <img src="/signed/src/icns/filter/color.svg" class="builder_icon" title="Farbe hinzufügen" data-action="background" data-type="color" alt="Color">
          </div>
        </div>
      </div>

      <div class="media_wrapper absolute content darkelement input_parent menue_parent format_parent width_69 height_69 top_15 horz_centered" data-format="full_m">

        <input type="hidden" name="originals" value="">
        <input type="hidden" name="type" value="">
        <input type="hidden" name="icons" value="">
        <div class="element_menue">

        </div>
        <div class="full_width_media relative content hover_blue_inlineshadow video_div">
          <div class="builder_icons_wrapper absolute full_center">
              <img src="/signed/src/icns/filter/photo.svg" class="builder_icon" title="Foto hinzufügen"data-type="photo" data-action="media" alt="Foto">
              <img src="/signed/src/icns/filter/slideshow.svg" class="builder_icon" title="Slideshow hinzufügen" data-type="slideshow" data-format="full_m" data-action="media" alt="Slideshow">
              <img src="/signed/src/icns/filter/video.svg" class="builder_icon" title="Video hinzufügen" data-type="video" data-format="full_m" data-action="media" alt="Video">
          </div>
        </div>
      </div>

    </div>

    <input type="hidden" name="thumbnails" maxlength="9999999999" value="">
    <input type="hidden" name="type" value="">
    <input type="hidden" name="icons" value="icons">
    <input type="hidden" name="original" value="">
  </div>
</div>
