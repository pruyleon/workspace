<?php
    if (isset($_POST['ajax'])) {
        $prefix = "../../../";
        $artist_name = $_POST['artist_name'];
        $genre_string = $_POST['genre_string'];
        $artist_city_country = $_POST['artist_city_country'];
        $lorem_ipsum_placeholder = $_POST['lorem_ipsum_placeholder'];
        $headline_placeholder = $_POST['headline_placeholder'];
    } else {
        $prefix = "./";
    }
 ?>
 <div class="profile_element size_M" data-id="8M">
  <div class="profile_element_content">
    <div class="inner_profile_element_content background text_color_parent">
      <div class="absolute_inner_content text_color_parent">
        <div class="full_size_background full_size input_parent menue_parent color_switch" data-color="white">

          <input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
          <input type="hidden" name="originals" value="">
          <input type="hidden" name="type" value="">
          <input type="hidden" name="icons" value="">
          <div class="element_menue">
          </div>
          <div class="full_size background hover_blue_inlineshadow lightelement">
            <div class="builder_icons_wrapper absolute horz_centered bottom_2">
              <img src="/signed/src/icns/filter/color.svg" class="builder_icon" title="Farbe hinzufügen" data-action="background" data-type="color" alt="Farbe">
            </div>
          </div>
        </div>


      <div class="width_50 absolute full_height left background">
        <div class="absolute_inner_content">
          <div class="absolute top_12 width_74 height_3 horz_centered font_weight_heavy fontsize_14 color_">
            <?php echo $artist_name ?>
          </div>
          <div class="textinput_textarea absolute width_74 height_68 top_20 horz_centered hover_blue_inlineshadow">
            <?php echo $lorem_ipsum_placeholder ?>
          </div>
        </div>
      </div>


      <div class="top_20 right_9 width_38 height_52 darkelement content absolute input_parent format_parent menue_parent"  data-format="full">
        <input type="hidden" name="originals" value="">
        <input type="hidden" name="thumbnails" maxlength="9999999999" value="">

        <input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
        <input type="hidden" name="type" value="">
        <input type="hidden" name="icons" value="">
        <div class="element_menue">

        </div>
        <div class="content absolute_inner_content hover_blue_inlineshadow video_div">
          <div class="builder_icons_wrapper vert_centered absolute horz_centered">
            <img src="/signed/src/icns/filter/photo.svg" class="builder_icon" title="Foto hinzufügen" data-action="media" data-type="photo" alt="Foto">
            <img src="/signed/src/icns/filter/slideshow.svg" class="builder_icon" title="Slideshow hinzufügen" data-action="media" data-type="slideshow" alt="Slideshow">
            <img src="/signed/src/icns/filter/video.svg" class="builder_icon" title="Video hinzufügen" data-action="media" data-type="video" alt="Video">
          </div>
        </div>
      </div>
      <div class="top_77 width_38 absolute right_9 text_align_center">
        <?php echo $genre_string ?>
      </div>

      <div class="presspack_request_wrapper large right_27-7 bottom_12">
        <?php
          include($prefix.'php/profile_elements/raw/comps/presspack_request.php');
         ?>
      </div>

    </div>
  </div>

    <input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
    <input type="hidden" name="titles" value="">
    <input type="hidden" name="type" value="">
    <input type="hidden" name="icons" value="">
    <input type="hidden" name="original" value="">
  </div>
</div>
