<?php
    if (isset($_POST['ajax'])) {
        $prefix = "../../../";
        $lorem_ipsum_placeholder = $_POST['lorem_ipsum_placeholder'];
        $headline_placeholder = $_POST['headline_placeholder'];
    } else {
        $prefix = "./";
    }
 ?>
 <div class="profile_element size_M" data-id="A1M">
  <div class="profile_element_content">
    <div class="inner_profile_element_content" data-format="full_m">
      <div class="full_size full_size_background input_parent menue_parent  color_switch" data-color="white">

        <input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
        <input type="hidden" name="originals" value="">
        <input type="hidden" name="type" value="">
        <input type="hidden" name="icons" value="">
        <div class="element_menue">
        </div>
        <div class="full_size background content hover_blue_inlineshadow darkelement video_div">
          <div class="builder_icons_wrapper full_center media_icns_wrapper">
            <img src="/signed/src/icns/filter/photo.svg" class="builder_icon" title="Foto hinzufügen" data-action="background" data-type="photo" alt="Audio">
            <img src="/signed/src/icns/filter/slideshow.svg" class="builder_icon" title="Slideshow hinzufügen" data-action="background" data-type="slideshow" alt="Audio">
            <img src="/signed/src/icns/filter/video.svg" class="builder_icon" title="Video hinzufügen" data-action="background" data-type="video" alt="Audio">
          </div>
        </div>
      </div>
    <div class="presspack_request_wrapper middle bottom_centered horz_centered">
      <?php
        include($prefix.'php/profile_elements/raw/comps/presspack_request.php');
       ?>
    </div>
  </div>

<input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
    <input type="hidden" name="thumbnails" maxlength="9999999999" value="">
    <input type="hidden" name="type" value="">
    <input type="hidden" name="icons" value="icons">
    <input type="hidden" name="original" value="">
  </div>
</div>
