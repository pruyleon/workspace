<?php
    if (isset($_POST['ajax'])) {
      $prefix = "../../../";
      $headline_placeholder = $_POST['headline_placeholder'];
    }else {
      $prefix = "./";
    }
 ?>
 <div class="profile_element size_L" data-id="1L">
  <div class="profile_element_content">
    <div class="inner_profile_element_content menue_parent input_parent format_parent" data-format="full">
      <input type="hidden" name="originals" value="">

      <input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
      <input type="hidden" name="type" value="">
      <input type="hidden" name="icons" value="">
      <input type="hidden" name="thumbnails" maxlength="9999999999" value="">
      <div class="element_menue">

      </div>
      <div class="full_size content hover_blue_inlineshadow darkelement video_div">
        <div class="builder_icons_wrapper full_center media_icns_wrapper">
          <img src="/signed/src/icns/filter/photo.svg" class="builder_icon" title="Foto hinzufügen" data-action="media" data-type="photo" alt="Audio">
          <img src="/signed/src/icns/filter/slideshow.svg" class="builder_icon" title="Slideshow hinzufügen" data-action="media" data-type="slideshow" alt="Audio">
          <img src="/signed/src/icns/filter/video.svg" class="builder_icon" title="Video hinzufügen" data-action="media" data-type="video" alt="Audio">
        </div>
      </div>
    </div>

    <input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
    <input type="hidden" name="thumbnails" maxlength="9999999999" value="">
    <input type="hidden" name="type" value="">
    <input type="hidden" name="icons" value="icons">
    <input type="hidden" name="original" value="">
  </div>
</div>
