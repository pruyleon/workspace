<?php
    if (isset($_POST['ajax'])) {
      $prefix = "../../../";
      $lorem_ipsum_placeholder = $_POST['lorem_ipsum_placeholder'];
      $headline_placeholder = $_POST['headline_placeholder'];
    }else {
      $prefix = "./";
    }
 ?>
 <div class="profile_element size_S" data-id="11S">
  <div class="profile_element_content input_parent">
    <div class="inner_profile_element_content  text_color_parent ">
      <div class="full_size full_size_background input_parent menue_parent">
        <input type="hidden" name="originals" value="">
         
<input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
        <input type="hidden" name="type" value="">
        <input type="hidden" name="icons" value="">
        <div class="element_menue">

        </div>
        <div class="full_size background hover_blue_inlineshadow lightelement">
          <div class="builder_icons_wrapper top_position horz_centered">
            <img src="/signed/src/icns/filter/photo.svg" class="builder_icon" title="Foto hinzufügen" data-action="background" data-type="photo" alt="Foto">
            <img src="/signed/src/icns/filter/color.svg" class="builder_icon" title="Farbe hinzufügen" data-action="background" data-type="color" alt="Farbe">
          </div>
        </div>
      </div>
      <div class="headline_input absolute horz_centered height_3-5 width_40 top_10 hover_blue_inlineshadow">
        <?php echo $headline_placeholder ?>
      </div>
      <?php
        $media = ['ph','ss','vd'];
        $format = "full";
        include($prefix.'php/profile_elements/raw/comps/full_width_media.php');
      ?>

    </div>
     
<input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
    <input type="hidden" name="titles" value="">
    <input type="hidden" name="type" value="">
    <input type="hidden" name="icons" value="">
    <input type="hidden" name="original" value="">
  </div>
</div>
