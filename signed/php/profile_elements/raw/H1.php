<?php
  if (isset($loaded) && $loaded == true) {
      $previewed = "";
  } else {
      $previewed = "previewed";
  }
  include('./php/profile_elements/raw/comps/genre_listing.php');
 ?>
<div class="profile_element <?php echo $previewed ?> size_L  input_parent header relative" data-id="H1">
  <div class="profile_element_content relative placement_wrapper element_background h1" style="background-image: url(<?php echo $background ?>)">
    <div class="menue_parent full_size_background full_size background absolute input_parent">
      <?php
        $logo = true;
        include('./php/profile_elements/raw/comps/header_menue_lang.php');
       ?>
      <input type="hidden" name="value" maxlength="9999999999" value="">
      <input type="hidden" name="config" value="Test">
      <input type="hidden" name="type" value="">
      <input type="hidden" name="icons" value="">
      <input type="hidden" name="original" value="">
      <input type="hidden" name="originals" value="">
      <div class="header_colorgradient">

      </div>
      <div class="element_menue header">
        <div class="action_wrapper">
          <span class="reposition_name">NAME</span>
          <!-- <span class="element_dropdown"> EDIT </span> -->
        </div>
        <div class="dropdown">
          <!-- <div class="dropdown_line" data-action="edit">
            Bearbeiten
          </div>
          <div class="dropdown_line" data-action="delete">
            Entfernen
          </div> -->
        </div>
      </div>
      <div class="full_size_background full_size absolute background hover_blue_inlineshadow format_parent darkelement" data-format="full">
        <div class="edit_icon_wrapper absolute icon_wrapper  full_center middle">
          <img src="/signed/src/icns/filter/photo.svg" class="builder_icon" title="Foto hinzufügen" data-type="photo" data-action="background" alt="Foto">
          <img src="/signed/src/icns/filter/slideshow.svg" class="builder_icon" title="Slideshow hinzufügen" data-type="slideshow" data-format="L" data-action="background" alt="Slideshow">
        </div>
      </div>
    </div>
    <div class="placement_border">

    </div>

    <div class="name_city_wrapper h1 content hover_blue_inlineshadow" title="Name positionieren" id="name_city_wrapper" data-font-size="49">
      <div class="name">
        <div class="name_wrapper">
          <?php
          echo mb_strtoupper($artist_name, 'UTF-8');
          ?>
        </div>
        <div class="city_wrapper">
          <?php echo "$city | $country" ?>
        </div>
      </div>
      <div class="logo">

      </div>
    </div>
    <div class="bottom_H1_wrapper content">
      <div class="bottom_wrapper">
        <div class="social_media_wrapper social_media_trigger hover_blue_inlineshadow cursor_pointer no_add_plus">
          <?php
            include('./php/profile_elements/raw/comps/social_media_bw_round.php');
           ?>
          <div class="social_media_tooltip fixed_tooltip fontsize_12">
            Social Media bearbeiten
          </div>
        </div>
        <div class="genre_listing <?php echo $genre_class ?> cursor_pointer fontsize_12 tooltip_parent hover_blue_inlineshadow no_add_plus">
          <span id="genres_inner">
            <?php echo $genre_string; ?>
          </span>
          <div class="genre_tooltip fixed_tooltip fontsize_12">
            Genre bearbeiten
          </div>
        </div>
        <div class="presspack_request fontsize_12 no_add_plus">
          <?php
            $no_switch = true;
            include('./php/profile_elements/raw/comps/presspack_request.php');
            $no_switch = false;
           ?>
        </div>
      </div>

    </div>
  </div>

  <input type="hidden" name="type" value="">
  <input type="hidden" name="value" maxlength="9999999999" value="">
  <input type="hidden" name="config" value="Test">
  <input type="hidden" name="icons" value="">
  <input type="hidden" name="original" value="">
  <input type="hidden" name="originals" value="">
  <input type="hidden" name="name_info" value="">
  <input type="hidden" name="name_type" value="">
  <input type="hidden" name="name_size" value="">
  <input type="hidden" name="name_uppercase" value="">
  <input type="hidden" name="name_centered" value="">
  <input type="hidden" name="name_uppercase" value="">
  <input type="hidden" name="name_shadow" value="">
  <input type="hidden" name="name_font" value="">  
  <input type="hidden" name="name_color" value="">
  <input type="hidden" name="sociallinks" value="youtube.com, twitter.com, instagram.com">
  <input type="hidden" name="socialselected" value="1,4,2">
  <input type="file" name="logo_file" class="hidden" value="">
</div>
