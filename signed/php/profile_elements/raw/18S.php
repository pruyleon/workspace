<?php
    if (isset($_POST['ajax'])) {
      $prefix = "../../../";
      $lorem_ipsum_placeholder = $_POST['lorem_ipsum_placeholder'];
      $headline_placeholder = $_POST['headline_placeholder'];
    }else {
      $prefix = "./";
    }
 ?>
<div class="profile_element size_S font_color_white" data-id="18S">
  <div class="profile_element_content">
    <div class="inner_profile_element_content">
        <div class="full_size full_size_background menue_parent input_parent">
           
<input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
          <input type="hidden" name="titles" value="">
          <input type="hidden" name="covers" value="">
          <input type="hidden" name="wave_colors" value="">
          <input type="hidden" name="type" value="">
          <input type="hidden" name="icons" value="">
          <input type="hidden" name="file_names" value="">
          <input type="hidden" name="originals" value="">
          <div class="element_menue">

          </div>
          <div class="full_size content audioplayer_parent hover_blue_inlineshadow darkelement" data-coverformat="full_s" data-audiotype="audio_full_bottom" data-audioplayer_id="NULL">
            <div class="builder_icons_wrapper full_center edit_icns_wrapper">
              <img src="/signed/src/icns/filter/audio.svg" class="builder_icon" title="Audio / Mixtape hinzufügen" data-action="media" data-type="audio" alt="Audio">
            </div>
            <?php
              include($prefix.'php/profile_elements/raw/comps/audio_full_bottom.php');
            ?>
          </div>
        </div>
      </div>
  </div>
</div>
