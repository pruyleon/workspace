<?php
    if (isset($_POST['ajax'])) {
      $prefix = "../../../";
      $lorem_ipsum_placeholder = $_POST['lorem_ipsum_placeholder'];
      $headline_placeholder = $_POST['headline_placeholder'];
    }else {
      $prefix = "./";
    }
    $common_pos_classes = "sizeS";
    $audio_class = "$common_pos_classes";
 ?>
<div class="profile_element size_S font_color_white" data-id="20S">
  <div class="profile_element_content">
    <div class="inner_profile_element_content">
      <div class="inner_profile_element_content">
          <div class="full_size full_size_background menue_parent input_parent">
             
<input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
            <input type="hidden" name="titles" value="">
            <input type="hidden" name="covers" value="">
            <input type="hidden" name="wave_colors" value="">
            <input type="hidden" name="type" value="">
            <input type="hidden" name="icons" value="">
            <input type="hidden" name="file_names" value="">
            <input type="hidden" name="originals" value="">
            <div class="element_menue">

            </div>
            <div class="full_size full_size_background background darkelement hover_blue_inlineshadow absolute_inner_content">
              <div class="builder_icons_wrapper top_position horz_centered edit_icns_wrapper">
                <img src="/signed/src/icns/filter/color.svg" class="builder_icon" title="Farbe hinzufügen" data-action="background" data-type="color" alt="Audio">
                <img src="/signed/src/icns/filter/photo.svg" class="builder_icon" title="Foto hinzufügen" data-action="background" data-type="photo" alt="Audio">
              </div>
            </div>
          </div>
          <div class="height_50 width_74 vert_centered horz_centered menue_parent input_parent content zindex_5 absolute audio_menue_wrapper">
             
<input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
            <input type="hidden" name="titles" value="">
            <input type="hidden" name="covers" value="">
            <input type="hidden" name="wave_colors" value="">
            <input type="hidden" name="type" value="">
            <input type="hidden" name="icons" value="">
            <input type="hidden" name="file_names" value="">
            <input type="hidden" name="originals" value="">
            <div class="element_menue">

            </div>
            <div class="background audioplayer_parent" data-audioclass="<?php echo $audio_class ?>" data-audiotype="audio_full_cover" data-audioplayer_id="NULL" data-coverformat="square">
              <div class="builder_icons_wrapper horz_centered bottom_13 edit_icns_wrapper">
                <img src="/signed/src/icns/filter/audio.svg" class="builder_icon" title="Audio / Mixtape hinzufügen" data-action="media" data-type="audio" alt="Audio">
              </div>
              <?php
              include($prefix.'php/profile_elements/raw/comps/audio_cover.php');
              ?>
            </div>
          </div>
          <div class="full_size background_filter hidden">

          </div>
      </div>
    </div>
  </div>
</div>
