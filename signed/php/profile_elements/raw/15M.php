<?php
    if (isset($_POST['ajax'])) {
      $prefix = "../../../";
      $lorem_ipsum_placeholder = $_POST['lorem_ipsum_placeholder'];
      $headline_placeholder = $_POST['headline_placeholder'];
    }else {
      $prefix = "./";
    }
 ?>
 <div class="profile_element size_M" data-id="15M">
  <div class="profile_element_content input_parent">
    <div class="inner_profile_element_content menue_parent text_color_parent">
      <div class="full_size full_size_background menue_parent input_parent">
        <input type="hidden" name="originals" value="">
         
<input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
        <input type="hidden" name="type" value="">
        <input type="hidden" name="icons" value="">
        <div class="element_menue">

        </div>
        <div class="full_size_background full_size background hover_blue_inlineshadow lightelement">
          <div class="builder_icons_wrapper absolute left_full_center">
            <img src="/signed/src/icns/filter/photo.svg" class="builder_icon" title="Foto hinzufügen" data-action="background" data-type="photo" alt="Foto">
          </div>
        </div>
      </div>


      <div class="content top_19 right_9 width_38 height_50 absolute menue_parent format_parent input_parent"  data-format="full">
        <input type="hidden" name="originals" value="">
        <input type="hidden" name="thumbnails" maxlength="9999999999" value="">
         
<input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
        <input type="hidden" name="type" value="">
        <input type="hidden" name="icons" value="">
        <div class="element_menue">

        </div>
        <div class="absolute_inner_content content format_parent hover_blue_inlineshadow darkelement video_div"  data-format="full">
          <div class="builder_icons_wrapper vert_centered absolute horz_centered">
            <img src="/signed/src/icns/filter/photo.svg" class="builder_icon" title="Foto hinzufügen" data-action="media" data-type="photo" alt="Foto">
            <img src="/signed/src/icns/filter/slideshow.svg" class="builder_icon" title="Slideshow hinzufügen" data-action="media" data-type="slideshow" alt="Slideshow">
            <img src="/signed/src/icns/filter/video.svg" class="builder_icon" title="Video hinzufügen" data-action="media" data-type="video" alt="Video">
          </div>
        </div>
      </div>

      <div class="headline_input absolute  height_3-5 width_38 top_78 right_9 hover_blue_inlineshadow">
        <?php echo $headline_placeholder ?>
      </div>

    </div>
     
<input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
    <input type="hidden" name="titles" value="">
    <input type="hidden" name="type" value="">
    <input type="hidden" name="icons" value="">
    <input type="hidden" name="original" value="">
  </div>
</div>
