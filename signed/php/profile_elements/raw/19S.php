<?php
    if (isset($_POST['ajax'])) {
      $prefix = "../../../";
      $lorem_ipsum_placeholder = $_POST['lorem_ipsum_placeholder'];
      $headline_placeholder = $_POST['headline_placeholder'];
    }else {
      $prefix = "./";
    }
    $common_pos_classes = "absolute width_40 height_20 overflow_hidden vert_centered left_9 sizeS";
    $audio_class = "$common_pos_classes";

 ?>
<div class="profile_element size_S font_color_white" data-id="19S">
  <div class="profile_element_content">
    <div class="inner_profile_element_content">
      <div class="element_menue">

      </div>
      <div class="full_size full_size_background menue_parent input_parent">
         
<input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
        <input type="hidden" name="titles" value="">
        <input type="hidden" name="covers" value="">
        <input type="hidden" name="wave_colors" value="">
        <input type="hidden" name="type" value="">
        <input type="hidden" name="icons" value="">
        <input type="hidden" name="file_names" value="">
        <input type="hidden" name="originals" value="">
        <div class="element_menue">

        </div>
        <div class="full_size full_size_background background audioplayer_parent hover_blue_inlineshadow darkelement paddingtop_2 margintop_-1" data-audioclass="<?php echo $audio_class ?>" data-audiotype="audio_full_dropdown" data-audioplayer_id="NULL" data-coverformat="full_s">
          <div class="builder_icons_wrapper right_center vert_centered edit_icns_wrapper">
            <img src="/signed/src/icns/filter/audio.svg" class="builder_icon" title="Audio / Mixtape hinzufügen" data-action="media" data-type="audio" alt="Audio">
          </div>
          <?php
          include($prefix.'php/profile_elements/raw/comps/audio_full_dropdown.php');
          ?>
        </div>
        <div class="full_size background_filter hidden">

        </div>

      </div>

    </div>
  </div>
</div>
