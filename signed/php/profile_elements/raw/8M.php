<?php
    if (isset($_POST['ajax'])) {
      $prefix = "../../../";
      $lorem_ipsum_placeholder = $_POST['lorem_ipsum_placeholder'];
      $headline_placeholder = $_POST['headline_placeholder'];
    }else {
      $prefix = "./";
    }
 ?>
 <div class="profile_element size_M" data-id="8M">
  <div class="profile_element_content input_parent">
    <div class="inner_profile_element_content">
      <div class="full_size background full_size_background input_parent menue_parent hover_blue_inlineshadow lightelement">

        <input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
        <input type="hidden" name="originals" value="">
        <input type="hidden" name="type" value="">
        <input type="hidden" name="icons" value="">
        <div class="element_menue">

        </div>
        <div class="builder_icons_wrapper absolute horz_centered bottom_2">
          <img src="/signed/src/icns/filter/color.svg" class="builder_icon" title="Farbe hinzufügen" data-action="background" data-type="color" alt="Farbe">
        </div>
      </div>

      <div class="width_50 absolute full_height left background">
        <div class="absolute_inner_content text_color_parent">
          <div class="absolute top_12 width_74 height_3 horz_centered font_weight_heavy fontsize_14 color_">
            <?php echo $artist_name ?>
          </div>
          <div class="textinput_textarea absolute width_74 height_68 top_20 horz_centered hover_blue_inlineshadow">
            <?php echo $lorem_ipsum_placeholder ?>
          </div>
        </div>
      </div>


      <div class="content top_20 right_9 width_38 height_52 absolute menue_parent input_parent format_parent"  data-format="full">

        <input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
        <input type="hidden" name="thumbnails" maxlength="9999999999" value="">
        <input type="hidden" name="originals" value="">
        <input type="hidden" name="type" value="">
        <input type="hidden" name="icons" value="">
        <div class="element_menue">

        </div>
        <div class="absolute_inner_content content hover_blue_inlineshadow darkelement video_div">
          <div class="builder_icons_wrapper vert_centered absolute horz_centered">
            <img src="/signed/src/icns/filter/photo.svg" class="builder_icon" title="Foto hinzufügen" data-action="media" data-type="photo" alt="Foto">
            <img src="/signed/src/icns/filter/slideshow.svg" class="builder_icon" title="Slideshow hinzufügen" data-action="media" data-type="slideshow" alt="Slideshow">
            <img src="/signed/src/icns/filter/video.svg" class="builder_icon" title="Video hinzufügen" data-action="media" data-type="video" alt="Video">
          </div>
        </div>
      </div>
      <div class="headline_input absolute  height_3 width_38 top_78 right_9 hover_blue_inlineshadow text_align_center">
        HEADLINE
      </div>

    </div>

<input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
    <input type="hidden" name="titles" value="">
    <input type="hidden" name="type" value="">
    <input type="hidden" name="icons" value="">
    <input type="hidden" name="originals" value="">
  </div>
</div>
