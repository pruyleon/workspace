<?php
    if (isset($_POST['ajax'])) {
      $prefix = "../../../";
      $lorem_ipsum_placeholder = $_POST['lorem_ipsum_placeholder'];
      $headline_placeholder = $_POST['headline_placeholder'];
    }else {
      $prefix = "./";
    }
 ?>
<div class="profile_element size_S font_color_white" data-id="4S">
  <div class="profile_element_content input_parent">
    <div class="inner_profile_element_content menue_parent">
      <div class="one_third absolute full_height left background_dark content menue_parent input_parent">
         
<input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
        <input type="hidden" name="originals" value="">
        <input type="hidden" name="type" value="">
        <input type="hidden" name="icons" value="">
        <div class="element_menue ">

        </div>
        <div class="absolute_inner_content content  format_parent hover_blue_inlineshadow darkelement"  data-format="onethird_s">
          <div class="builder_icons_wrapper full_center top absolute">
            <img src="/signed/src/icns/filter/photo.svg" class="builder_icon" title="Foto hinzufügen" data-action="media" data-type="photo" alt="photo">
            <img src="/signed/src/icns/filter/slideshow.svg" class="builder_icon" title="Slideshow hinzufügen" data-action="media" data-type="slideshow" alt="slideshow">
          </div>
        </div>
      </div>
      <div class="one_third absolute full_height third_middle background_light background menue_parent input_parent">
        <div class="absolute_inner_content text_color_parent hover_blue_inlineshadow lightelement">
          <div class="full_size full_size_background input_parent menue_parent">
             
<input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
            <input type="hidden" name="originals" value="">
            <input type="hidden" name="type" value="">
            <input type="hidden" name="icons" value="">
            <div class="element_menue">

            </div>
            <div class="full_size full_size_background background">
              <div class="builder_icons_wrapper top horz_centered absolute">
                <img src="/signed/src/icns/filter/color.svg" class="builder_icon" title="Farbe hinzufügen" data-action="background" data-type="color" alt="Color">
              </div>
            </div>
          </div>
          <div class="textinput_textarea absolute width_74 height_64 top_13 horz_centered hover_blue_inlineshadow">
            <?php echo $lorem_ipsum_placeholder ?>
          </div>
        </div>
      </div>
      <div class="one_third absolute full_height right background_dark content menue_parent input_parent">
         
<input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
        <input type="hidden" name="originals" value="">
        <input type="hidden" name="type" value="">
        <input type="hidden" name="icons" value="">
        <div class="element_menue">

        </div>
        <div class="absolute_inner_content content  format_parent hover_blue_inlineshadow darkelement"  data-format="onethird_s">
          <div class="builder_icons_wrapper full_center top absolute">
            <img src="/signed/src/icns/filter/photo.svg" class="builder_icon" title="Foto hinzufügen" data-action="media" data-type="photo" alt="photo">
            <img src="/signed/src/icns/filter/slideshow.svg" class="builder_icon" title="Slideshow hinzufügen" data-action="media" data-type="slideshow" alt="slideshow">
          </div>
        </div>
      </div>

    </div>

       
<input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
      <input type="hidden" name="titles" value="">
      <input type="hidden" name="covers" value="">
      <input type="hidden" name="wave_colors" value="">
      <input type="hidden" name="type" value="">
      <input type="hidden" name="icons" value="">
      <input type="hidden" name="file_names" value="">
      <input type="hidden" name="originals" value="">
  </div>
</div>
