<?php
    if (isset($_POST['ajax'])) {
      $prefix = "../../../";
      $lorem_ipsum_placeholder = $_POST['lorem_ipsum_placeholder'];
      $headline_placeholder = $_POST['headline_placeholder'];
    }else {
      $prefix = "./";
    }
 ?>
<div class="profile_element size_M font_color_white" data-id="6M">
  <div class="profile_element_content input_parent">
    <div class="inner_profile_element_content">
      <div class="full_size full_size_background input_parent menue_parent">
         
<input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
        <input type="hidden" name="originals" value="">
        <input type="hidden" name="type" value="">
        <input type="hidden" name="icons" value="">
        <div class="element_menue">

        </div>
        <div class="full_size background full_size_background absolute hover_blue_inlineshadow lightelement">
          <div class="builder_icons_wrapper left_center absolute">
            <img src="/signed/src/icns/filter/photo.svg" class="builder_icon" title="Foto hinzufügen" data-action="background" data-type="photo" alt="photo">
          </div>
        </div>
      </div>
      <div class="content free_text_wrapper height_76 absolute one_third top_13 right_7 relative">
        <div class="background_blur_dark">

        </div>
        <div class="absolute_inner_content text_color_parent">
          <div class="headline_input absolute top_9 height_3-7 width_78 horz_centered hover_blue_inlineshadow">
            <?php echo $headline_placeholder ?>
          </div>
          <div class="textinput_textarea absolute top_19 height_70 width_78 horz_centered hover_blue_inlineshadow">
            <?php echo $lorem_ipsum_placeholder ?>
          </div>
        </div>
      </div>

    </div>
       
<input type="hidden" name="value" maxlength="9999999999" value=""><input type="hidden" name="config" value="">
      <input type="hidden" name="titles" value="">
      <input type="hidden" name="covers" value="">
      <input type="hidden" name="wave_colors" value="">
      <input type="hidden" name="type" value="">
      <input type="hidden" name="icons" value="">
      <input type="hidden" name="file_names" value="">
      <input type="hidden" name="originals" value="">
  </div>
</div>
