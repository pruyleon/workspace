<?php
  $query = $_POST['query'];
  $artist_id = $_POST['artist_id'];
  if (isset($_POST['disabled'])) {
    $disabled_array = $_POST['disabled'];
  }else {
    $disabled_array = [];
  }
  include('../db.php');

  $result = mysqli_query($con, $query);
  $i = 0;
  while ($res = mysqli_fetch_assoc($result)) {
    if ($i == 6 || $i == 0) {
      echo "<div class='listing_row'>";
    }
    in_array($res['acc_ID'], $disabled_array) ? $disabled = "disabled" : $disabled = "draggable";

    echo '<div class="dj_wrapper '. $disabled .' search"><div class="dj_pic '. $disabled .'"  data-artistid="'.$res['artist_ID'].'" data-accid="'.$res['acc_ID'].'" data-name="'.$res['name'].'" style="background-image: url(/signed/src/user/'.$res['acc_ID'].'/profile.jpg)"></div><div class="dj_desc">'.$res['name'].'</div></div>';
    if ($i == 11 || $i == 5) {
      echo "</div>";
    }
    $i++;
  }

 ?>
