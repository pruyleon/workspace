<div class="popup_background level_1_popup" id="carousel_popup">
  <div class="popup_body" id="carousel_popup_inner">
    <div class="popup_title">
      <div class="popup_title_content">
        Suche deine TOP 6 aus
      </div>
      <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
    </div>
    <div class="popup_content" id="carousel_popup_content">
      <div class="top_wrapper">
        <div class="dj_wrapper empty  listing">
          <div class="dj_pic listing" data-accid="none" data-artistid="none" data-name="none">

          </div>
          <div class="dj_desc">
            Artist hinzufügen
          </div>
        </div>
        <div class="dj_wrapper empty listing">
          <div class="dj_pic listing" data-accid="none" data-artistid="none" data-name="none">

          </div>
          <div class="dj_desc">
            Artist hinzufügen
          </div>
        </div>

        <div class="dj_wrapper empty  listing">
          <div class="dj_pic listing" data-accid="none" data-artistid="none" data-name="none">

          </div>
          <div class="dj_desc">
            Artist hinzufügen
          </div>
        </div>

        <div class="dj_wrapper empty  listing">
          <div class="dj_pic listing" data-accid="none" data-artistid="none" data-name="none">

          </div>
          <div class="dj_desc">
            Artist hinzufügen
          </div>
        </div>

        <div class="dj_wrapper empty  listing">
          <div class="dj_pic listing" data-accid="none" data-artistid="none" data-name="none">

          </div>
          <div class="dj_desc">
            Artist hinzufügen
          </div>
        </div>

        <div class="dj_wrapper empty  listing">
          <div class="dj_pic listing" data-accid="none" data-artistid="none" data-name="none">

          </div>
          <div class="dj_desc">
            Artist hinzufügen
          </div>
        </div>
      </div>
      <div class="border_div"></div>
      <div class="carousel_search_wrapper">
        <input type="text" name="carousel_search" value="" placeholder="Artist Name eingeben...">
      </div>
      <div class="carousel_listing_wrapper">
        <div class="artist_search_listing">

        </div>
        <div class="invite_wrapper">
          Passenden Artist nicht gefunden?
          <br>
          <button type="button" class="blue_btn normal_button" name="button" id="open_inivte_popup">Jetzt einladen</button>
        </div>
      </div>
      <div class="carousel_empty error_message">
        Bitte wähle für jedes Feld einen Artist aus!
      </div>
    </div>
    <div class="bottom_right_btns">
      <button type="button" class="gray_btn menue_button" id="abort_carousel" name="button">Abbrechen</button>
      <button type="button" class="blue_btn menue_button " id="save_carousel" name="button">Speichern</button>
    </div>
  </div>
</div>
<div class="popup_background level_2_popup" id="invite_popup">
  <div class="popup_body" id="invite_popup_inner">
    <div class="popup_title">
      <div class="popup_title_content">
        Artist einladen
      </div>
      <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
    </div>
    <div class="popup_content" id="invite_popup_content">
      <div class="font_weight_heavy">
        Dein Einladungs-Link:
      </div>
      <div class="invite_link_wrapper">
        <span id="copy_invite_link" class="cursor_pointer">mypressk.it/invite/<?php echo $user_id ?></span>
        <div id="copy_message"> <img src="/signed/src/icns/checked_green.svg" alt=""> Kopiert!</div>
      </div>
      <div class="arrow_wrapper">
        <div>
          Kopieren
        </div>
        <img src="/signed/src/icns/arrow_right_big.svg" alt="">
        <div>
          Verschicken
        </div>
        <img src="/signed/src/icns/arrow_right_big.svg" alt="">
        <div>
            1 Monat <br>
            geschenkt!
        </div>
      </div>
    </div>
    <textarea name="name" id="invite_link_string" class="hidden">mypressk.it/invite/<?php echo $user_id ?></textarea>
    <div class="info_message">
      * Nur bei erfolgreicher Anmeldung und Abschluss des DJ-Rapture Bonus-Pakets bestehend aus 5 Waschmaschinen und 9 Trocknern
    </div>
  </div>
</div>
