<base href="/signed" target="_blank">
<?php
  if (isset($_POST['user_id'])) {
    $user_id = $_POST['user_id'];
  }
 ?>
<div class="popup_background" id="file_archive_popup">
  <div class="popup_body" id="file_archive_popup_inner">
    <div class="popup_title">
      Foto auswählen
      <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
    </div>

    <div class="popup_content">
      <div class="archive_upload_wrapper">
        <img src="/signed/src/icns/add_pic_black.svg" alt="">
        <div>
            Neues Foto hochladen
        </div>
      </div>
      <div class="file_listing">
        <span id="file_listing_title">Archiv</span>
        <div class="files">
          <?php
            $path = $_SERVER['DOCUMENT_ROOT'];
            $path .= "/signed/src/user/$user_id/archive/";
            foreach (get_files($path) as $value): ?>
              <div class="archive_file" data-name="<?php echo $value ?>" style="background-image: url(/signed/src/user/<?php echo $user_id.'/archive/'.$value ?>)">
                <div class="content">
                  <div class="delete_file">
                    <img src="/signed/src/icns/garbage_white.svg" alt="löschen">
                  </div>
                </div>
              </div>
          <?php endforeach; ?>
          <div class="no_archive_files">
            Noch keine Dateien im Archiv
          </div>
        </div>
      </div>
      <form class="" id="archive_form" method="post">
        <input type="file" accept="image/png, image/jpeg" id="archive_input" name="image" value="">
        <input type="hidden" name="user_id" value="<?php echo $user_id ?>">
      </form>
    </div>

  </div>
</div>


<?php
/*  function get_files($images_dir,$exts = array('jpg', 'png')) {
    $files = array();
    $times = array();
    if($handle = opendir($images_dir)) {
        while(false !== ($file = readdir($handle))) {
            $extension = strtolower(get_file_extension($file));
            if($extension && in_array($extension,$exts)) {
                $files[] = $file;
                $times[] = filemtime($images_dir . '/' . $file);
            }
        }
        closedir($handle);
    }
    array_multisort($files, SORT_ASC, $times);
    return $files;
  }

  function get_file_extension($file) {
    $array = explode(".", $file);
    $extension = end($array);
    return $extension ? $extension : false;
  }*/

 ?>
