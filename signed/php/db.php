<?php
  if (!defined('SQL_INI')) {
      define('SQL_INI', $_SERVER['DOCUMENT_ROOT'].'/php/sql.ini');
  }
  if (file_exists(SQL_INI)) {
      $config = parse_ini_file(SQL_INI);
  }
  $con = mysqli_connect($config['server'], $config['username'], $config['password'], $config['dbname']);
  //echo "Konfig: ".$config['server'].$config['username'].$config['password'].$config['dbname'];
  mysqli_query($con, "SET NAMES UTF8");


  // if(!function_exists("debug_to_console")) {
  //   function debug_to_console( $data ) {
  //     $output = $data;
  //     if ( is_array( $output ) )
  //         $output = implode( ',', $output);
  //     echo "<script>console.log( 'Debug Objects: " . $output . "' );</script>";
  //   }
  // }
?>
