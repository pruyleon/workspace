<?php
  if (!defined('SQL_INI')) {
      define('SQL_INI', $_SERVER['DOCUMENT_ROOT'].'/php/sql.ini');
  }
  if (file_exists(SQL_INI)) {
      $config = parse_ini_file(SQL_INI);
  }
  $con = mysqli_connect($config['server'], $config['username'], $config['password'], $config['dbname']);
  mysqli_query($con, "SET NAMES UTF8");

  function getArtistNameAccID($acc_ID){
    global $con;
    $query = "SELECT acc_ID, name FROM artists WHERE acc_ID='$acc_ID' LIMIT 1";
    $result = mysqli_query($con, $query);
    while ($res = mysqli_fetch_assoc($result)) {
      $name = $res['name'];
    }
    return $name;
  }

  function getArtistNameArtsitID($artist_ID){
    global $con;
    $query = "SELECT artist_ID, name FROM artists WHERE artist_ID='$artist_ID' LIMIT 1";
    $result = mysqli_query($con, $query);
    while ($res = mysqli_fetch_assoc($result)) {
      $name = $res['name'];
    }
    return $name;
  }

  function getArtistID($acc_ID){
    global $con;
    $query = "SELECT acc_ID, artist_ID FROM artists WHERE acc_ID='$acc_ID' LIMIT 1";
    $result = mysqli_query($con, $query);
    while ($res = mysqli_fetch_assoc($result)) {
      $artist_ID = $res['artist_ID'];
    }
    return $artist_ID;
  }

  function get_files($images_dir,$exts = array('jpg', 'png')) {
    $files = array();
    $times = array();
    if($handle = opendir($images_dir)) {
        while(false !== ($file = readdir($handle))) {
            $extension = strtolower(get_file_extension($file));
            if($extension && in_array($extension,$exts)) {
                $files[] = $file;
                $times[] = filemtime($images_dir . '/' . $file);
            }
        }
        closedir($handle);
    }
    array_multisort($files, SORT_ASC, $times);
    return $files;
  }

  function get_file_extension($file) {
    $array = explode(".", $file);
    $extension = end($array);
    return $extension ? $extension : false;
  }
 ?>
