<?php
  $template_id = $_POST['template_id'];
  $device = $_POST['device'];
  $format = $_POST['format'];
 ?>
<input type="hidden" name="template_id" value="<?php echo $template_id ?>">
<div class="preview_inner" <?php if ($device != 'desktop') {
     echo "style='display: none'";
 } ?> id="desktop_preview">
  Desktop <?php echo "Template: ".$template_id ?>
</div>
<div class="preview_inner" <?php if ($device != 'mobile') {
     echo "style='display: none'";
 } ?> id="mobile_preview">

  <div class="wrapper">
    <div class="" <?php if ($format != 'portrait') {
     echo "style='display: none'  data-active='false'";
 } else {
     echo "data-active='true'";
 } ?> id="portrait">
      <!-- <div class="phone_border_top">
        <img src="/signed/src/borders/phone_top.svg" alt="">
      </div> -->
      <div class="mobile_preview_body portrait">
        <?php echo $template_id; ?><?php echo "Format: $format"; ?>
      </div>
      <!-- <div class="phone_border_bottom">
        <img src="/signed/src/borders/phone_bottom.svg" alt="">
      </div> -->
    </div>
    <div class="" <?php if ($format != 'landscape') {
     echo "style='display: none'  data-active='false'";
 } else {
     echo "data-active='true'";
 } ?> id="landscape" data-active='false'>
      <div class="mobile_preview_body landscape">
        <?php echo $template_id; ?><?php echo "Format: $format"; ?>
      </div>
    </div>

    <button type="button" name="button" class="" id="format_switcher"> <img src="/signed/src/icns/format_switch_<?php echo $format ?>.svg" alt="umschalten"> </button>

  </div>

</div>
