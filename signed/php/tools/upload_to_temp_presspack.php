<?php
  $gif = $_FILES['gif'];
  $user_id = $_POST['user_id'];
  $audio_dir = '../../src/user/' . $user_id . '/temp_presspack/';
  if (!is_dir($audio_dir)) {
      mkdir($audio_dir, 0755, true);
  }

  $file = $_FILES["gif"]["name"];
  $tmp = $_FILES["gif"]["tmp_name"];
  $file_info = pathinfo($file);
  $file = $file_info['filename'];

  $extension = explode(".", $file);
  $extension = end($extension);

  $file = str_replace(" ", "-", $file);
  $file = str_replace(".", "_", $file).".gif";

  if (move_uploaded_file($tmp, $audio_dir.$file)) {
    echo $file;
  }else {
    echo "failure";
  }

 ?>
