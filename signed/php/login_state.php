<?php

define('LOGIN_STATE', false);

// Root definieren
$root = $_SERVER['DOCUMENT_ROOT'];

// PFAD AUS INI laden
$iniPath = $root . '/php/path.ini';
if (file_exists($iniPath)) {
    $iniContent = parse_ini_file($iniPath);
    $mainURL = $iniContent['HostName'];
    $secure = $iniContent['Secure'] == true ? "https://" : "http://";
    define('URL_ROOT', $secure . $mainURL . '/');

    require($root . '/php/cookie.php');
    require($root . '/php/crypt.php');
    require($root . '/php/sql.php');
}

function checkLogin()
{
    if (getCookie('user') != true) {
        // Cookiegruppe nicht angelegt
        return false;
    }

    if (getCookie('user', 'key') != false && getCookie('user', 'mail') != false  && getCookie('user', 'id') != false) {
        $key = getCookie('user', 'key');
        $cryptMail = getCookie('user', 'mail');
        $cryptUser = getCookie('user', 'id');
    } else {
        // keine Cookies gesetzt -> noch nicht angemeldet
        return false;
    }

    // decrypt mail with $key
    $userMail = decrypt($cryptMail, $key);

    // SessionToken aus DB laden
    $sqlResult = db_select("SELECT `acc_ID`, `session`, `name`, `prename`, `acc_type`, `language` FROM `account` WHERE `mail`='" . $userMail . "'");
    if ($sqlResult == false || sizeof($sqlResult) > 1) {
        //  Acc ID nicht gefunden
        return false;
    } else {
        $userID = $sqlResult[0]['acc_ID'];
        $dbSession = $sqlResult[0]['session'];
        $userPreName = $sqlResult[0]['prename'];
        $userName = $sqlResult[0]['name'];
        $userType = $sqlResult[0]['acc_type'];
        $userLang = $sqlResult[0]['language'];
    }

    setcookie("LanguageCookie", $userLang, time() + 2592000); //Gültigkeit = 30 Tage

    // überprüfen ob password_verify(Cookie, DBtoken) = true
    if (!password_verify($key, $dbSession)) {
        // Session Key und Schlüssel passen nicht zusammen
        return false;
    }

    // decrypt user with substr(DBtoken, -10)
    $storedUser = decrypt($cryptUser, substr($dbSession, -10));

    // überprüfen ob Session und User zusammenpassen
    if ($storedUser != $userID) {
        // User aus Datenbank entspricht nicht User aus Cookie
        return false;
    } else {
        define('USER', $userID);
        define('USER_NAME', array($userPreName, $userName));
        define('USER_TYPE', $userType);
        define('USER_LANG', $userLang);
        if (!defined('LOGIN_STATE')) {
            define('LOGIN_STATE', true);
        }
        return true;
    }
}


function only_logged()
{
    if (checkLogin() == false) {
        makeCookie("user", "key", "", 1);
        makeCookie("user", "id", "", 1);
        makeCookie("user", "mail", "", 1);
        //Redirect auf Base URL
        header('Location: ' . '/');
    }
}
