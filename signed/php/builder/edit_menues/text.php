<div class="edit_menue" id="texteditor">
    <div class="picker wrapper">
        <div id="text_colorpicker">

        </div>
        <div class="picker_buttons">
            <button type="button" class="normal_button blue_btn" name="button">Übernehmen</button>
        </div>
    </div>
    <div class="edit_menue_header">
        Text Editor
    </div>
    <div class="edit_menue_inner">
        <div class="text_editor_toolbar">
            <!-- Add a bold button -->
            <button class="ql-bold" title="Auswahl fett"></button>
            <!-- Add subscript and superscript buttons -->
            <img src="/signed/src/icns/text_color.svg" class="cursor_pointer" alt="Farbe wählen" title="Textfarbe" id="text_color">
            <select class="ql-align" title="Text-Align">
                <option class="ql-align" selected="selected"></option>
                <option class="ql-align" value="center"></option>
                <!-- <option value="right"></option> -->
                <option class="ql-align" value="justify"></option>
            </select>
            <button type="button" class="ql-list" value="ordered" name="button"></button>
            <button type="button" class="ql-list" value="bullet" name="button"></button>
        </div>
    </div>
    <div class="buttons">
        <button type="button" class="normal_button blue_btn" id="save_text" name="button">Fertig</button>
        <!-- <button type="button" class="normal_button gray_btn" name="button">Abbrechen</button> -->
    </div>
</div>