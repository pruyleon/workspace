<div class="edit_menue" id="headlineeditor">
    <div class="picker wrapper">
        <div id="headline_colorpicker">

        </div>
        <div class="picker_buttons">
            <button type="button" class="normal_button blue_btn" name="button">Übernehmen</button>
        </div>
    </div>
    <div class="edit_menue_header">
        Headline Editor
    </div>
    <div class="edit_menue_inner">
        <div class="headline_editor_toolbar">
            <img src="/signed/src/icns/text_color.svg" class="cursor_pointer" alt="Farbe wählen" title="Textfarbe" id="headline_color">
            <button class="ql-bold" title="Auswahl Fett"></button>
            <select class="ql-align" title="Text-Align">
                <option selected="selected"></option>
                <option value="center"></option>
                <option value="justify"></option>
            </select>
        </div>
    </div>
    <div class="buttons">
        <button type="button" class="normal_button blue_btn" id="save_headline" name="button">Fertig</button>
        <!-- <button type="button" class="normal_button gray_btn" name="button">Abbrechen</button> -->
    </div>
</div>