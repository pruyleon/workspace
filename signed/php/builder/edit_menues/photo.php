<div class="edit_menue" id="cropper">
    <div class="edit_menue_header">
        Foto Editor
    </div>
    <div class="edit_menue_inner" id="cropper_slider">
    </div>
    <div class="buttons">
        <button type="button" class="normal_button blue_btn" id="save_cropped_pic" name="button">Fertig</button>
        <!-- <button type="button" class="normal_button gray_btn" id="abort_cropped" name="button">Abbrechen</button> -->
    </div>
</div>