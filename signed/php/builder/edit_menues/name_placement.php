<div class="edit_menue" id="placement_menue">
    <div class="picker wrapper">
        <div id="name_colorpicker">

        </div>
        <div class="picker_buttons">
            <button type="button" class="normal_button blue_btn" id="" name="button">Fertig</button>
        </div>
    </div>
    <div class="edit_menue_header">
        Name / Logo Editor
    </div>
    <div class="edit_menue_inner">
        <select class="darktrans_select left" name="name_selector" title="">
            <option value="0">Name verbergen</option>
            <option value="2">Logo</option>
            <option value="1">Name</option>
            <option value="3">Name + Stadt</option>
            <option value="4">Name + Land</option>
            <option value="5" selected="selected">Name + Stadt + Land</option>
        </select>
        <div class="inner_edit name left">
            <span>Größe: </span>
            <button type="button" data-action="descrease" class="name_resize_button" name="button" title="Namen verkleinern"> -
            </button>
            <button type="button" data-action="increase" class="name_resize_button" name="button" title="Namen vergrößern"> +
            </button>
            <input type="hidden" name="namesize" value="49">
            <button type="button" id="name_uppercase" data-state="uppercase" name="button" title="Großschrift ein/aus"></button>
            <img src="/signed/src/icns/text_color.svg" class="cursor_pointer" alt="Farbe wählen" id="name_color" title="Textfarbe">
            <button type="button" id="make_name_shadow" data-state="NULL" name="button" title="Schriftschatten ein/aus"> S </button>
            <button type="button" id="name_center" data-state="left" name="button" title="Schrift zentrieren">C</button>
        </div>
        <div class="inner_edit logo left">
            <button type="button" id="upload_logo" name="button" title="">Bild hochladen</button>
            <span>Größe: </span>
            <button type="button" id="descrease_logo_size" name="button" title="Logo verkleinern"> - </button>
            <button type="button" id="inscrease_logo_size" name="button" title="Logo vergrößern"> + </button>
            <input type="hidden" name="logo_multiplicator" value="1">
        </div>
        <select name="name_font_type" class="darktrans_select" id="" title="">
            <option value="Comic Sans MS">Comic Sans</option>
            <option value="Arial">Arial</option>
            <option value="Times New Roman">Times New Roman</option>
            <option value="Courier New">Courier New</option>
            <option value="Impact">Impact</option>
        </select>
    </div>
    <div class="buttons">
        <!-- <button type="button" class="normal_button gray_btn" id="abort_name" name="button">Abbrechen</button> -->
        <button type="button" class="normal_button blue_btn" id="save_name" name="button">Fertig</button>
    </div>
</div>