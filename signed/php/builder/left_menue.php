<div id="left_menue" class="width_transition">
  <div id="left_menue_content">
    <div class="filter_wrapper">
      <div class="filter_toggle_wrapper">
        <button type="button" class="switch_button size" name="button" data-size="S">S</button>
        <button type="button" class="switch_button size active" name="button" data-size="M">M</button>
        <button type="button" class="switch_button size" name="button" data-size="L">L</button>
      </div>
      <div class="filter_options_wrapper">
        <div class="cursor_pointer" id="toggle_filter_options">
          Filter wählen <span id="element_filter_count"><img src="/signed/src/icns/dropdown_white.svg"></span>
        </div>
        <div class="options_wrapper">
          <div class="options_header">
            Filtern nach <button type="button" id="close_filter" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
          </div>
          <div class="filter_line">
            <input type="checkbox" name="filter" value="tx">
            <button type="button" class="filter_button" name="button">
              Text
            </button>
          </div>
          <div class="filter_line">
            <input type="checkbox" name="filter" value="ph">
            <button type="button" class="filter_button" name="button">
              Foto
            </button>
          </div>
          <div class="filter_line">
            <input type="checkbox" name="filter" value="ss">
            <button type="button" class="filter_button" name="button">
              Slideshow
            </button>
          </div>
          <div class="filter_line">
            <input type="checkbox" name="filter" value="vd">
            <button type="button" class="filter_button" name="button">
              Video
            </button>
          </div>
          <div class="filter_line">
            <input type="checkbox" name="filter" value="au">
            <button type="button" class="filter_button" name="button">
              Audio
            </button>
          </div>
          <div class="filter_line">
            <input type="checkbox" name="filter" value="sm">
            <button type="button" class="filter_button" name="button">
              Social Media
            </button>
          </div>
          <div class="filter_line">
            <input type="checkbox" name="filter" value="bo">
            <button type="button" class="filter_button" name="button">
              Booking
            </button>
          </div>
          <div class="filter_line">
            <input type="checkbox" name="filter" value="gf">
            <button type="button" class="filter_button" name="button">
              GIF
            </button>
          </div>
          <div class="filter_options_button_wrapper">
            <button class="cursor_pointer filter_menue_button" id="delete_filters">
              Filter löschen
            </button>
            <button class="cursor_pointer filter_menue_button blue_btn" id="apply_filters">
              Anwenden
            </button>
          </div>
        </div>
      </div>
    </div>
    <div class="element_listing_filter">
      <?php
        include("./php/builder/element_listing.php");
      ?>
    </div>
  </div>
</div>
