<!-- Start / Welcome -->
<div class="popup_background" id="tutorial_start_popup">
  <div class="popup_body builder_popup tutorial_popup" id="tutorial_start_popup_inner">
    <div class="popup_title">
      <div class="popup_title_content">
        Welcome
      </div>
      <!-- <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button> -->
    </div>
    <div class="popup_content" id="tutorial_popup_content">
      im Presskit Builder!
      <br>
      Bereit deinen Header anzulegen?
      <br>
      <button type="button" class="normal_button blue_btn" name="button" id="tutorial_start_button">Let´s go</button>
    </div>
  </div>
</div>

<!-- Icon-Erklärung -->
<div class="popup_background" id="tutorial_icons_popup">
  <div class="popup_body builder_popup tutorial_popup" id="tutorial_icons_popup_inner">
    <div class="popup_title">
      <div class="popup_title_content">
        Klicke auf das entsprechende Icon weils cool ist!
      </div>
      <!-- <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button> -->
    </div>
    <div class="popup_content" id="">

      <button type="button" class="normal_button blue_btn" name="button" id="tutorial_icons_button">Leon hats drauf</button>
    </div>
  </div>
</div>

<!-- Namensplatzierung Erklärung -->
<div class="popup_background" id="tutorial_name_popup">
  <div class="popup_body builder_popup tutorial_popup" id="tutorial_name_popup_inner">
    <div class="popup_title">
      <div class="popup_title_content">
        Lass uns deinen Namen bearbeiten
      </div>
      <!-- <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button> -->
    </div>
    <div class="popup_content" id="">

      <button type="button" class="normal_button blue_btn" name="button" id="tutorial_name_button">Skrrrt!</button>
    </div>
  </div>
</div>

<!-- Genre Erklärung -->
<div class="popup_background" id="tutorial_genre_popup">
  <div class="popup_body builder_popup tutorial_popup" id="tutorial_name_popup_inner">
    <div class="popup_title">
      <div class="popup_title_content">
        Wähle drei fancy Genres!
      </div>
      <!-- <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button> -->
    </div>
    <div class="popup_content" id="">

      <button type="button" class="normal_button blue_btn" name="button" id="tutorial_genre_button">Fancy 4 Life!</button>
    </div>
  </div>
</div>

<!-- Profilelemente Erklärung -->
<div class="popup_background" id="tutorial_element_popup">
  <div class="popup_body builder_popup tutorial_popup" id="tutorial_element_popup_inner">
    <div class="popup_title">
      <div class="popup_title_content">
        Lass uns nun dein erstes Profilelement auswählen und hinzufügen (auch Fancy!)
      </div>
      <!-- <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button> -->
    </div>
    <div class="popup_content" id="">
      <button type="button" class="normal_button blue_btn" name="button" id="tutorial_element_button">Okidok</button>
    </div>
  </div>
</div>

<!-- Symbol-Erklärung -->
<div class="popup_background" id="tutorial_symbols_popup">
  <div class="popup_body builder_popup tutorial_popup" id="tutorial_symbols_popup_inner">
    <div class="popup_title">
      <div class="popup_title_content">
        Wir haben super fancy Icons
      </div>
      <!-- <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button> -->
    </div>
    <div class="popup_content" id="">
      <button type="button" class="normal_button blue_btn" name="button" id="tutorial_symbols_button">Alla!</button>
    </div>
  </div>
</div>

<!-- Filter-Erklärung -->
<div class="popup_background" id="tutorial_filter_popup">
  <div class="popup_body builder_popup tutorial_popup" id="tutorial_filter_popup_inner">
    <div class="popup_title">
      <div class="popup_title_content">
        Und Filter!
      </div>
      <!-- <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button> -->
    </div>
    <div class="popup_content" id="">
      <button type="button" class="normal_button blue_btn" name="button" id="tutorial_filter_button">Crazyyyy!</button>
    </div>
  </div>
</div>

<!-- Elemente Verschieben-Erklärung -->
<div class="popup_background" id="tutorial_edit_popup">
  <div class="popup_body builder_popup tutorial_popup" id="tutorial_edit_popup_inner">
    <div class="popup_title">
      <div class="popup_title_content">
        Letzter Hinweis
      </div>
      <!-- <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button> -->
    </div>
    <div class="popup_content" id="">
      <button type="button" class="normal_button blue_btn" name="button" id="tutorial_edit_button">Okidoki</button>
    </div>
  </div>
</div>
