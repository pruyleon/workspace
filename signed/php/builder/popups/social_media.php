<!-- Popup Social Media -->
<div class="popup_background" id="socials_popup">
  <div class="popup_body builder_popup" id="socials_popup_inner">
    <div class="popup_title">
      <div class="popup_title_content">
        Social Media Links bearbeiten
      </div>
      <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
    </div>
    <div class="popup_content" id="socials_popup_content">
      <div class="embedded_header">
        Social Media Links:
      </div>

      <div class="social_link_wrapper">
        <div class="counter">
          1
        </div>
        <select class="social_link_select" name="social_1_select">
          <option value="1" selected="selected">Facebook</option>
          <option value="2">Youtube</option>
          <option value="3">Twitter</option>
          <option value="4">Instagram</option>
        </select>
        <input type="text" name="social_1_link" class="social_link_input" placeholder="Link einfügen..." value="">
        <span class="social_preview"><img src="/signed/src/icns/eye_gray.svg" alt="öffnen" title="Link öffnen"></span>
      </div>
      <div class="social_link_wrapper">
        <div class="counter">
          2
        </div>
        <select class="social_link_select" name="social_2_select">
          <option value="1">Facebook</option>
          <option value="2">Youtube</option>
          <option value="3">Twitter</option>
          <option value="4"  selected="selected">Instagram</option>
        </select>
        <input type="text" name="social_2_link" class="social_link_input" placeholder="Link einfügen..." value="">
        <span class="social_preview"><img src="/signed/src/icns/eye_gray.svg" alt="öffnen" title="Link öffnen"></span>
      </div>
      <div class="social_link_wrapper">
        <div class="counter">
          3
        </div>
        <select class="social_link_select" name="social_3_select">
          <option value="1">Facebook</option>
          <option value="2"  selected="selected">Youtube</option>
          <option value="3">Twitter</option>
          <option value="4">Instagram</option>
          <option value="0">entfernen</option>
        </select>
        <input type="text" name="social_3_link" class="social_link_input" placeholder="Link einfügen..." value="">
        <span class="social_preview"><img src="/signed/src/icns/eye_gray.svg" alt="öffnen" title="Link öffnen"></span>
      </div>
    </div>
    <div class="bottom_right_btns">
      <button type="button" class="gray_btn menue_button" id="abort_social" name="button">Abbrechen</button>
      <button type="button" class="blue_btn menue_button " id="save_social" name="button">Fertig</button>
    </div>
  </div>

</div>
