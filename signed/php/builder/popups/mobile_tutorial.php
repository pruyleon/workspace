<!-- Start / "Queransicht = Normales Presskit" Hinweis -->
<div class="popup_background" id="mobile_tutorial_start_popup">
    <div class="popup_body builder_popup tutorial_popup" id="mobile_tutorial_start_popup_inner">
        <div class="popup_title">
            <div class="popup_title_content">
                Handy Ansicht
            </div>
            <!-- <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button> -->
        </div>
        <div class="popup_content" id="tutorial_popup_content">
            Quer-Ansicht ergibt sich aus deinem fancy 
            <br>
            vorherigen Design!
            <button type="button" class="normal_button blue_btn" name="button" id="mobile_tutorial_start_button">Verstehe Bruda!</button>
        </div>
    </div>
</div>

<!-- Portrait Erklärung -->
<div class="popup_background" id="mobile_tutorial_portrait_popup">
    <div class="popup_body builder_popup tutorial_popup" id="mobile_tutorial_portrait_popup_inner">
        <div class="popup_title">
            <div class="popup_title_content">
                Hochkant Design Optimieren
            </div>
            <!-- <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button> -->
        </div>
        <div class="popup_content" id="tutorial_popup_content">
            Hochkant kannst auch was machen
            <br>
            Wir wissen das wir Fancy sind #vollcool
            <button type="button" class="normal_button blue_btn" name="button" id="mobile_tutorial_portrait_button">Verstehe Bruda!</button>
        </div>
    </div>
</div>

<!-- Portrait Erklärung -> keine Doppelte Arbeit! -->
<div class="popup_background" id="mobile_tutorial_hint_popup">
    <div class="popup_body builder_popup tutorial_popup" id="mobile_tutorial_hint_popup_inner">
        <div class="popup_title">
            <div class="popup_title_content">
                Keine Sorge
            </div>
            <!-- <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button> -->
        </div>
        <div class="popup_content" id="tutorial_popup_content">
            Keine Doppelte Arbeit vallah!!!!
            <button type="button" class="normal_button blue_btn" name="button" id="mobile_tutorial_hint_button">Verstehe Bruda!</button>
        </div>
    </div>
</div>