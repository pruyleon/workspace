<!-- Template-Auswahl -->
<div class="popup_background dark_popup" id="choose_template_popup">
    <div class="popup_body builder_popup" id="choose_template_popup_inner">
        <div class="popup_title">
            <div class="popup_title_content">
                Vorlage wählen
            </div>
            <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close_white.svg" alt=""> </button>
        </div>
        <div class="popup_content" id="choose_template_popup_content">
            <?php
            $query = "SELECT * FROM template_contents";
            $result = mysqli_query($con, $query);
            $counter = 0;
            $counter_goal = mysqli_num_rows($result);
            while ($res = mysqli_fetch_assoc($result)) {
                if ($counter % 2 == 0) {
                    echo '<div class="template_line">';
                }
            ?>
                <div class="template_wrapper" data-templateid="<?php echo $res['ID'] ?>" data-templatename="<?php echo $res['name'] ?>">
                    <div class="template_img">
                        <div class="template_img_inner"></div>
                    </div>
                    <div class="template_desc">
                        <div class="template_name">
                            <?php echo $res['name'] ?>
                        </div>
                        <div class="template_buttons">
                            <button class="normal_button gray_btn preview_template">Vorschau</button>
                            <button class="normal_button blue_btn choose_template">Wählen</button>
                        </div>
                    </div>
                </div>
            <?php
                if ($counter % 2 == 1) {
                    echo '</div>';
                }
                $counter++;
            }
            if ($counter == $counter_goal) {
                echo '</div>';
            }
            ?>

        </div>
    </div>
</div>

<!-- TODO: Überschreiben / Speichern (Slotauswahl) -->
<div class="popup_background" id="overwrite_slot_popup">
    <div class="popup_body builder_popup" id="overwrite_slot_popup_inner">
        <div class="popup_title">
            <div class="popup_title_content">
                Slot zum überschreiben auswählen
            </div>
            <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
        </div>
        <div class="popup_content" id="overwrite_slot_popup_content">
            <div><img src="/signed/src/icns/red_excl.svg" alt=""> Dein Speicher ist voll. Wähle welches Presskit du überschreiben möchtest</div>
            <div id="slot_overwrite_listing">
                <?php
                $wrapper_class = "";
                $row_class = "";
                $slot_class = "";
                include('./php/builder/incl/presskit_slots_listing.php');
                ?>
            </div>

        </div>
    </div>
</div>

<!-- TODO: Speichern unter (Namenseingabe) -->
<div class="popup_background dark_popup" id="_popup">
    <div class="popup_body builder_popup" id="_popup_inner">
        <div class="popup_title">
            <div class="popup_title_content">
                Mixcloud einbinden
            </div>
            <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
        </div>
        <div class="popup_content" id="_popup_content">

        </div>
    </div>
</div>

<!-- Änderungen speichern? (Bspw. bei Verlassen und Co) -->
<div class="popup_background" id="save_changes_popup">
    <div class="popup_body builder_popup" id="save_changes_popup_inner">
        <div class="popup_title">
            <div class="popup_title_content">
                Änderungen speichern?
            </div>
            <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
        </div>
        <div class="popup_content" id="save_changes_popup_content">
            <div class="text_align_center bold">
                Möchtest du deine aktuellen Änderungen speichern?
                <br>
                Alle Änderungen gehen dadurch verloren
            </div>
            <div class="left_buttons">
                <button class="normal_button red_btn" id="discard_changes">Verwerfen</button>
                <button class="normal_button blue_btn" id="save_changes">Speichern</button>
            </div>
        </div>
    </div>
</div>

<!-- TODO: Presskit nun online -->
<div class="popup_background dark_popup" id="_popup">
    <div class="popup_body builder_popup" id="_popup_inner">
        <div class="popup_title">
            <div class="popup_title_content">
                Mixcloud einbinden
            </div>
            <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
        </div>
        <div class="popup_content" id="_popup_content">

        </div>
    </div>
</div>

<!-- TODO: International Player? (Presskit in zweiter Sprache) -->
<div class="popup_background dark_popup" id="_popup">
    <div class="popup_body builder_popup" id="_popup_inner">
        <div class="popup_title">
            <div class="popup_title_content">
                Mixcloud einbinden
            </div>
            <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
        </div>
        <div class="popup_content" id="_popup_content">

        </div>
    </div>
</div>

<!-- TODO: Sprachkürzel Wahl -->
<div class="popup_background" id="choose_language_popup">
    <div class="popup_body builder_popup" id="choose_language_popup_inner">
        <div class="popup_title">
            <div class="popup_title_content">
                Wähle die Sprachkürzel
            </div>
            <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
        </div>
        <div class="popup_content" id="choose_language_popup_content">
            <div class="full_width">
                <div class="bold">Bisheriges Presskit</div>
                <div>mypressk.it/[djname]</div>
                <select name="primary_language" class="language_selector" id="">
                    <?php
                    foreach ($language_array as $lang) {
                        echo '<option value="' . $lang . '">' . $lang . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
</div>

<!-- TODO: Presskit ersetzen -->
<div class="popup_background" id="overwrite_presskit_popup">
    <div class="popup_body builder_popup" id="overwrite_presskit_popup_inner">
        <div class="popup_title">
            <div class="popup_title_content">
                Presskit ersetzen
            </div>
            <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
        </div>
        <div class="popup_content" id="overwrite_presskit_popup_content">
            <div class="text_align_center bold">
                Du überschreibst ein aktuell veröffentlichtes Pressk.it!
                <br>
                Möchtest du fortfahren?
            </div>
            <div class="left_buttons">
                <button class="normal_button gray_btn" id="abort_overwrite">Verwerfen</button>
                <button class="normal_button blue_btn" id="overwrite_presskit">Speichern</button>
            </div>
        </div>
    </div>
</div>

<!-- TODO: Vervollständigung nötig -->
<div class="popup_background dark_popup" id="_popup">
    <div class="popup_body builder_popup" id="_popup_inner">
        <div class="popup_title">
            <div class="popup_title_content">
                Mixcloud einbinden
            </div>
            <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
        </div>
        <div class="popup_content" id="_popup_content">

        </div>
    </div>
</div>

<!-- TODO: Speichern unter -->
<div class="popup_background" id="save_as_popup">
    <div class="popup_body builder_popup" id="save_as_popup_inner">
        <div class="popup_title">
            <div class="popup_title_content">
                Speichern unter
            </div>
            <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
        </div>
        <div class="popup_content" id="save_as_popup_content">
            <input type="text" id="save_as_name_input" placeholder="Name eingeben...">
            <div class="left_buttons">
                <button class="normal_button gray_btn" id="abort_save_as">Abbrechen</button>
                <button class="normal_button blue_btn" id="save_presskit_as">Speichern</button>
            </div>
        </div>
    </div>
</div>