<!-- Afragepopup -->
<div class="popup_background" id="request_popup">
  <div class="popup_body builder_popup" id="request_popup_inner">
    <div class="popup_title">
      <div class="popup_title_content">
        Anfragepopup bearbeiten
      </div>
      <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
    </div>
    <div class="popup_content" id="socials_popup_content">
      <div class="profile_pic_wrapper">
        <div class="profile_pic_request" style="background-image: url(/signed/src/user/<?php echo $user_id ?>/profile.jpg)">

        </div>
        <?php echo getArtistNameAccID($user_id); ?>
      </div>
      <div class="request_info_wrapper">
        <div class="request_top_line">
          <div class="full_width request_line ">
            <div class="request_line_desc date">
              Auftrittsdatum:*
            </div>
            <div class="request_line_info">
              <input type="text" class="datepicker" value="<?php echo strftime(" %d.%m.%Y", time()); ?>">
              <div class="request_playtime">
                <div class="font_weight_heavy">
                  Playtime:*
                </div>
                <select class="playtime_select" disabled id="playtime_start" name="playtime_start">
                  <?php
                      for ($i=0; $i < 24; $i++) {
                          for ($j=0; $j < 46; $j+=15) {
                              if ($j<10) {
                                  $j = "0".$j;
                              }
                              if ($i<10) {
                                  $i = "0".$i;
                              }
                              if ("$i.$j" == "23.00") {
                                  $selected = "selected";
                              } else {
                                  $selected = "";
                              }
                              echo '<option '.$selected.' value="'.$i.':'.$j.'">'.$i.':'.$j.'</option>';
                          }
                      }
                     ?>
                </select>
                bis
                <select class="playtime_select" disabled id="playtime_end" name="playtime_end">
                  <?php
                      for ($i=0; $i < 24; $i++) {
                          for ($j=0; $j < 46; $j+=15) {
                              if ($j<10) {
                                  $j = "0".$j;
                              }
                              if ($i<10) {
                                  $i = "0".$i;
                              }
                              if ("$i:$j" == "03:00") {
                                  $selected = "selected";
                              } else {
                                  $selected = "";
                              }
                              echo '<option '.$selected.' value="'.$i.':'.$j.'">'.$i.':'.$j.'</option>';
                          }
                      }
                     ?>
                </select>
              </div>
            </div>
          </div>


          <div class="full_width request_line name">
            <div class="request_line_desc">
              Name:*
            </div>
            <div class="request_line_info">
              <input type="text" class="full_width_input" disabled placeholder="Ansprechpartner angeben..." name="" value="">
            </div>
          </div>
          <div class="full_width request_line">
            <div class="request_line_desc">
              Location:*
            </div>
            <div class="request_line_info">
              <input type="text" class="full_width_input" disabled placeholder="Location angeben..." name="" value="">
            </div>
          </div>
          <div class="full_width request_line ">
            <div class="request_line_desc">
              Ort:*
            </div>
            <div class="request_line_info">
              <input type="text" class="full_width_input" disabled placeholder="Ort der Location angeben..." name="city" value="">
            </div>
          </div>


          <div class="full_width request_line mail">
            <div class="request_line_desc">
              E-Mail:*
            </div>
            <div class="request_line_info">
              <input type="text" class="full_width_input" disabled placeholder="E-Mail angeben..." name="" value="">
            </div>
          </div>
          <div class="full_width request_line">
            <div class="request_line_desc">
              Telefon:*
            </div>
            <div class="request_line_info">
              <input type="text" class="full_width_input" disabled placeholder="Telefonnummer angeben..." name="" value="">
            </div>
          </div>
        </div>



        <div class="full_width additional_infos_wrapper">
          <span class="font_weight_heavy">Weitere Informationen:</span>
          <div class="additional_questions_scroll">
            <div class="additional_questions_append">
              <div class="full_width additional_question_wrapper">
                <div class="full_width">
                  <input type="text" name="additional_question" class="additional_question_input" value="Kapazität">
                  <div class="right">
                    <input type="checkbox" name="required" checked> Pflichtfeld <button type="button" class="button_img delete_question" name="button"> <img src="/signed/src/icns/trash.svg" alt="löschen"> </button>
                  </div>
                </div>
                <div class="full_width">
                  <input type="text" name="additional_answer" disabled class="additional_answer_input" value="">
                </div>
              </div>

              <div class="full_width additional_question_wrapper">
                <div class="full_width">
                  <input type="text" name="additional_question" class="additional_question_input" value="Ticketpreis">
                  <div class="right">
                    <input type="checkbox" name="required" checked> Pflichtfeld <button type="button" class="button_img delete_question" name="button"> <img src="/signed/src/icns/trash.svg" alt="löschen"> </button>
                  </div>
                </div>
                <div class="full_width">
                  <input type="text" name="additional_answer" disabled class="additional_answer_input" value="">
                </div>
              </div>

              <div class="full_width additional_question_wrapper">
                <div class="full_width">
                  <input type="text" name="additional_question" class="additional_question_input" placeholder="Frage eingeben... ">
                  <div class="right">
                    <input type="checkbox" name="required" checked> Pflichtfeld <button type="button" class="button_img delete_question" name="button"> <img src="/signed/src/icns/trash.svg" alt="löschen"> </button>
                  </div>
                </div>
                <div class="full_width">
                  <input type="text" name="additional_answer" disabled class="additional_answer_input" value="">
                </div>
              </div>
            </div>

            <div class="full_width" id="add_question">
              <img src="/signed/src/icns/plus.svg" class="add_question_img" alt=""> weitere Frage hinzufügen
            </div>
            <div class="message_wrapper">
              <div class="message_desc font_weight_heavy">
                Nachricht:
              </div>
              <div class="message_text">
                <textarea name="message" placeholder="Zusatzinformationen eingeben" disabled></textarea>
              </div>
            </div>
          </div>
        </div>
      </div>


    </div>

    <div class="bottom_right_btns">
      <button type="button" class="gray_btn menue_button" id="abort_request" name="button">Abbrechen</button>
      <button type="button" class="blue_btn menue_button " id="save_request" name="button">Speichern</button>
    </div>
  </div>
</div>
