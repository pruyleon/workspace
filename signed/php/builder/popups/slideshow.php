<!-- Slideshow Popup -->
<!-- TODO: Geschwindigkeit (effecttime) und Effekttyp speichern -->
<div class="popup_background" id="slideshow_popup" data-format="">
  <div class="popup_body builder_popup" id="slideshow_popup_inner">
    <div class="popup_title">
      <div class="popup_title_content">
        Slideshow erstellen
      </div>
      <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
    </div>
    <div class="popup_content" id="slideshow_popup_content">
      <input type="hidden" name="slideshow_element_id" value="">
      <div class="slideshow_inner_wrapper">
        <div class="slideshow_element_wrapper" data-format="">
          <div class="slideshow_desc">
            1
          </div>
          <input type="hidden" class="slideshow_input" name="picture_input" position="1" data-position="one" value="">
          <input type="hidden" class="slideshow_input_base64" name="picture_input_base64" data-position="one" value="">
          <div class="slideshow_element empty" position="1" data-format="">
            <div class="slideshow_menue">
              <div class="slideshow_menue_btn button_img">
                <img src="/signed/src/icns/menue_dots.svg" alt="Optionen">
              </div>
              <div class="slideshow_menue_inner">
                <div class="slideshow_menue_point upload_pic">
                  Foto hochladen
                </div>
                <div class="slideshow_menue_point position_pic">
                  Foto positionieren
                </div>
                <div class="slideshow_menue_point delete_pic">
                  Entfernen
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="slideshow_element_wrapper" data-format="">
          <div class="slideshow_desc">
            2
          </div>
          <input type="hidden" class="slideshow_input" name="picture_input" position="2" data-position="two" value="">
          <input type="hidden" class="slideshow_input_base64" name="picture_input_base64" data-position="two" value="">
          <div class="slideshow_element empty" position="2" data-format="">
            <div class="slideshow_menue">
              <div class="slideshow_menue_btn button_img">
                <img src="/signed/src/icns/menue_dots.svg" alt="Optionen">
              </div>
              <div class="slideshow_menue_inner">
                <div class="slideshow_menue_point upload_pic">
                  Foto hochladen
                </div>
                <div class="slideshow_menue_point position_pic">
                  Foto positionieren
                </div>
                <div class="slideshow_menue_point delete_pic">
                  Entfernen
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="slideshow_element_wrapper" data-format="">
          <div class="slideshow_desc">
            3
          </div>
          <input type="hidden" class="slideshow_input" name="picture_input" position="3" data-position="three" value="">
          <input type="hidden" class="slideshow_input_base64" name="picture_input_base64" data-position="three" value="">
          <div class="slideshow_element empty" position="3" data-format="">
            <div class="slideshow_menue">
              <div class="slideshow_menue_btn button_img">
                <img src="/signed/src/icns/menue_dots.svg" alt="Optionen">
              </div>
              <div class="slideshow_menue_inner">
                <div class="slideshow_menue_point upload_pic">
                  Foto hochladen
                </div>
                <div class="slideshow_menue_point position_pic">
                  Foto positionieren
                </div>
                <div class="slideshow_menue_point delete_pic">
                  Entfernen
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="slideshow_element_wrapper" data-format="">
          <div class="slideshow_desc">
            4
          </div>
          <input type="hidden" class="slideshow_input" name="picture_input" position="4" data-position="four" value="">
          <input type="hidden" class="slideshow_input_base64" name="picture_input_base64" data-position="four" value="">
          <div class="slideshow_element empty" position="4" data-format="">
            <div class="slideshow_menue">
              <div class="slideshow_menue_btn button_img">
                <img src="/signed/src/icns/menue_dots.svg" alt="Optionen">
              </div>
              <div class="slideshow_menue_inner">
                <div class="slideshow_menue_point upload_pic">
                  Foto hochladen
                </div>
                <div class="slideshow_menue_point position_pic">
                  Foto positionieren
                </div>
                <div class="slideshow_menue_point delete_pic">
                  Entfernen
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
      <div class="spacer_line">

      </div>
      <div class="slideshow_preview">
        <div class="preview_top_line">
          <div class="preview_left left font_weight_heavy">
            Effektvorschau:
          </div>
          <div class="preview_right left font_weight_heavy">
            Slideshow Effekt:
          </div>
        </div>
        <div class="slidehow_preview_content">
          <div class="preview_left left">
            <div class="slideshow_demo">
              <div class="slides one" style="background-image: url(/signed/src/demo/demo1.jpg)">

              </div>
              <div class="slides two" style="background-image: url(/signed/src/demo/demo2.jpg)">

              </div>
              <div class="slides three" style="background-image: url(/signed/src/demo/demo3.jpg)">

              </div>
              <div class="slides four" style="background-image: url(/signed/src/demo/demo4.jpg)">

              </div>
            </div>
          </div>
          <div class="preview_right left">
            <div>
              <select class="slideshow_effect_select" name="slideshow_effect">
                <!-- <option value="0">Überlagern</option> -->
                <option value="1">Erscheinen</option>
                <option value="2" selected>Wischen</option>
              </select>
            </div>
            <div class="slideshow_effect_desc font_weight_heavy">
              Anzeigezeit:
            </div>
            <div class="slideshow_effect slider">
              <div class="effect_time"></div>
            </div>
          </div>
        </div>
        <div class="cropper_wrapper slideshow">
          <div class="croppie_slider_wrapper slideshow">

          </div>
        </div>

      </div>
    </div>
    <div class="bottom_right_btns">
      <button type="button" class="gray_btn menue_button" id="abort_slideshow" name="button">Abbrechen</button>
      <button type="button" class="blue_btn menue_button " id="save_slideshow" name="button">Fertig</button>
    </div>
  </div>
</div>