<!-- Popup Video-->
<div class="popup_background" id="video_popup">
  <div class="popup_body builder_popup" id="video_popup_inner">
    <div class="popup_title">
      <div class="popup_title_content">
        Video Editor
      </div>
      <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
    </div>
    <div class="popup_content relative" id="video_popup_content">
      <div class="toggle_button_wrapper" id="video_popup_mode_toggle">
        <div class="inner_wrapper">
          <button class="toggle_button left_toggle active no_hover video_popup_mode_toggle" data-type="video">
            Video Upload
          </button>
          <button class="toggle_button right_toggle no_hover video_popup_mode_toggle" data-type="youtube">
            Youtube
          </button>
        </div>
      </div>
      <div id="video_popup_video_wrapper">
        <div class="full_width">
          <div class="video_inner_content first tooltip">
            <span role="tooltip">
              mp4 mit max. 50MB
            </span>
            <button type="button" name="button" id="upload_video" class="button_invisible tooltip_img empty"><img src="/signed/src/icns/video_upload.svg" class="upload_img" alt=""> Video hochladen</button>
             <div class="error_message" id="video_too_big_message">
               Video zu groß! Max. 50MB
             </div>
          </div>
        </div>
        <div class="full_width">
          <div class="empty" id="video_thumbnail">

          </div>
        </div>
        <div class="" id="video_error_message_wrapper">
          <div class="error_message" id="video_error_message">
            Ein Videoelement muss Thumbnail und eine Datei im MP4-Format beinhalten um angezeigt zu werden!
          </div>
        </div>
      </div>
      <div id="video_popup_youtube_wrapper">
        <div class="full_width">
          <div class="absolute horz_centered">
            <img src="/signed/src/icns/filter/youtube.svg" alt=""> Youtube URL einfügen:
          </div>
        </div>
        <div class="full_width" id="youtube_coming_soon">
          Coming soon!
        </div>
      </div>
      <div class="bottom_right_btns">
        <button type="button" class="gray_btn menue_button" id="abort_video" name="button">Abbrechen</button>
        <button type="button" class="blue_btn menue_button " id="save_video" name="button">Fertig</button>
      </div>
    </div>
    <form id="video_form" method="post">
      <input type="hidden" name="user_id" value="<?php echo $user_id ?>">
      <input type="hidden" name="artist_id" value="<?php echo $artist_id ?>">
      <input type="file" accept="video/mp4" name="file" id="video_file" class="hidden" value="">
    </form>

    <input type="hidden" name="video_file_name" class="hidden" value="">

    <input type="hidden" name="" id="video_thumbnail_file" class="hidden video_pic" value="">
    <input type="hidden" id="video_thumbnail_base64" name="video_thumbnail_base64" value="">
  </div>
</div>
