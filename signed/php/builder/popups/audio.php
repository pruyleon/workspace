<!-- Popup Audio-->
<div class="popup_background nofadeout" id="audio_popup">
  <div class="popup_body builder_popup" id="audio_popup_inner">
    <div class="popup_title">
      <div class="popup_title_content">
        Audio upload
      </div>
      <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
    </div>
    <div class="popup_content" id="audio_popup_content">
      <div class="sortable_audios_wrapper">
        <div class="sortable_audios">
          <div class="audio_wrapper empty active" data-position="1">
            <div class="headline">
              <span class="position_ind">1</span>
            </div>
            <div class="audio">

            </div>
            <div class="title">
              Titel
            </div>
            <input type="hidden" name="audio_name" value="">
            <input type="hidden" name="cover" value="">
            <input type="hidden" name="file_name" id="file_name_1" value="">
            <input type="hidden" name="title" id="audio_title_1" value="">
            <input type="hidden" name="wave_color" value="#FFFFFF,#CCCCCC">
          </div>

          <div class="audio_wrapper empty" data-position="2">
            <div class="headline">
              <span class="position_ind">2</span>
            </div>
            <div class="audio">

            </div>
            <div class="title">
              Titel
            </div>
            <input type="hidden" name="audio_name" value="">
            <input type="hidden" name="cover" value="">
            <input type="hidden" name="file_name" id="file_name_2" value="">
            <input type="hidden" name="title" id="audio_title_2" value="">
            <input type="hidden" name="wave_color" value="#FFFFFF,#CCCCCC">
          </div>

          <div class="audio_wrapper empty" data-position="3">
            <div class="headline">
              <span class="position_ind">3</span>
            </div>
            <div class="audio">

            </div>
            <div class="title">
              Titel
            </div>
            <input type="hidden" name="audio_name" value="">
            <input type="hidden" name="cover" value="">
            <input type="hidden" name="file_name" id="file_name_3" value="">
            <input type="hidden" name="title" id="audio_title_3" value="">
            <input type="hidden" name="wave_color" value="#FFFFFF,#CCCCCC">
          </div>

          <div class="audio_wrapper empty" data-position="4">
            <div class="headline">
              <span class="position_ind">4</span>
            </div>
            <div class="audio">

            </div>
            <div class="title">
              Titel
            </div>
            <input type="hidden" name="audio_name" value="">
            <input type="hidden" name="cover" value="">
            <input type="hidden" name="file_name" id="file_name_4" value="">
            <input type="hidden" name="title" id="audio_title_4" value="">
            <input type="hidden" name="wave_color" value="#FFFFFF,#CCCCCC">
          </div>
        </div>
        <div class="audio_edit_wrapper full_absolute">

        </div>
      </div>
      <div class="divider">

      </div>
      <div class="audio_upload_wrapper empty" data-active="1">
          <div class="audio_cover">
            <div class="background_filter">
            </div>
            <div class="audio_wave">
              <div class="tooltip">
                Farbe festlegen
              </div>
              <div class="left">
                <div class="full_width">
                  <?php include('./src/icns/audio/left_top.svg') ?>
                </div>
                <div class="full_width">
                  <?php include('./src/icns/audio/left_bottom.svg') ?>
                </div>
              </div>
              <div class="right">
                <div class="full_width">
                  <?php include('./src/icns/audio/right_top.svg') ?>
                </div>
                <div class="full_width">
                  <?php include('./src/icns/audio/right_bottom.svg') ?>
                </div>
              </div>
            </div>
          </div>
          <div id="audio_wave_colorpicker">

          </div>
          <div class="audio_title_wrapper relative">
            <input type="text" name="audio_title" placeholder="Titel eingeben">
            <button type="button" id="upload_audio" name="button">
              <img src="/signed/src/icns/filter/audio.svg" alt="">
              <span id="audio_upload_status_1">Audiodatei hochladen</span>
              <span id="audio_upload_status_2">Audiodatei hochladen</span>
              <span id="audio_upload_status_3">Audiodatei hochladen</span>
              <span id="audio_upload_status_4">Audiodatei hochladen</span>
            </button>
            <!-- <span id="audio_upload_status"></span> -->
            <div class="full_absolute" id="cropper_slider_audio">

            </div>
            <div class="" id="audio_error_message_wrapper">
              <div class="error_message hidden" id="audio_error_message">
                Ein Audioelement muss Titel, Thumbnail und eine Audiodatei beinhalten um angezeigt zu werden!
              </div>
            </div>
          </div>
      </div>

      <div class="audio_button_cover">

      </div>
    </div>
    <div class="bottom_right_btns">
      <button type="button" class="gray_btn menue_button" id="abort_audio" name="button">Abbrechen</button>
      <button type="button" class="blue_btn menue_button " id="save_audio" name="button">Fertig</button>
    </div>
    <form id="audio_form" method="post">
      <input type="hidden" name="user_id" value="<?php echo $user_id ?>">
      <input type="hidden" name="artist_id" value="<?php echo $artist_id ?>">
    </form>

    <input type="file" accept="audio/mp3" name="" id="audio_file_1" class="hidden audio_file" value="">
    <input type="file" accept="audio/mp3" name="" id="audio_file_2" class="hidden audio_file" value="">
    <input type="file" accept="audio/mp3" name="" id="audio_file_3" class="hidden audio_file" value="">
    <input type="file" accept="audio/mp3" name="" id="audio_file_4" class="hidden audio_file" value="">

    <input type="hidden" name="" id="audio_pic_1" class="hidden audio_pic" value="">
    <input type="hidden" name="" id="audio_pic_2" class="hidden audio_pic" value="">
    <input type="hidden" name="" id="audio_pic_3" class="hidden audio_pic" value="">
    <input type="hidden" name="" id="audio_pic_4" class="hidden audio_pic" value="">

  </div>
</div>
