<!-- Popups Soundcloud -->
<div class="popup_background" id="soundcloud_popup">
  <div class="popup_body builder_popup" id="soundcloud_popup_inner">
    <div class="popup_title">
      <div class="popup_title_content">
        Soundcloud einbinden
      </div>
      <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
    </div>
    <div class="popup_content" id="soundcloud_popup_content">
      <div class="full_width soundcloud_topline">
        <div class="half left">
          <div class="embedded_header">
            <img src="/signed/src/icns/soundcloud.svg" alt=""> Soundcloud Sharelink einfügen
            <div class="embedded_input">
              <input type="text" name="soundcloud_input" id="soundcloud_link" placeholder="Hier einfügen...">
              <div class="error_message" id="soundcloud_empty">
                Bitte füge einen Link ein!
              </div>
            </div>
          </div>
        </div>
        <div class="half right soundcloud">
          <div class="embedded_header">
            <span class="soundcloud_where">Wo finde ich den Sharelink? </span><button type="button" class="link_btn" id="show_soundcloud_info" name="button">Mehr Infos</button>
          </div>
          <div class="embedded_content">
            In Soundcloud: Auf beliebigen Song, Album, Playlist,<br>
            Künstler oder Podcast und wähle „Teilen“.<br>
            Kopiere den Sharelink und füge Ihn hier ein
          </div>
        </div>
        <div class="full_width">
          <div class="embedded_header soundcloud_preview">
            Vorschau des Players:
          </div>
          <div class="embed" id="soundcloud_embedded_player">
            <!-- <iframe width="100%" scrolling="no" id="soundcloud_lightmode" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/166239133&color=%23231c31&auto_play=false&hide_related=true&show_comments=false&show_user=true&show_reposts=false&show_teaser=false"></iframe> -->
          </div>
        </div>
      </div>
    </div>
    <div class="bottom_right_btns">
      <button type="button" class="gray_btn menue_button" id="abort_soundcloud" name="button">Abbrechen</button>
      <button type="button" class="blue_btn menue_button " id="save_soundcloud" name="button">Fertig</button>
    </div>
  </div>

</div>

<div class="popup_background" id="soundcloud_info_popup">
  <div class="popup_body" id="soundcloud_info_inner">
    <div class="popup_title no_border">
      <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
    </div>
    <div class="popup_content">
      <img src="/signed/src/icns/soundcloud_info.png" class="soundcloud_info_img" alt="">
      <div class="soundcloud_infotext embeded_infotext">
        Rechtsklick auf beliebigen Song, Album, Playlist, Künstler oder Podcast bei Spotify und wähle „Teilen“. <br>
        Klicke auf „Spotify URI kopieren“ und füge die URI unten ein
      </div>
    </div>
    <div class="bottom_right_btns">
      <button type="button" class="menue_button blue_btn" id="close_soundcloud_info" name="button">Schließen</button>
    </div>
  </div>
</div>
