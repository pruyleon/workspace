<!-- Popups Spotify -->
<div class="popup_background" id="spotify_popup">
  <div class="popup_body  builder_popup" id="spotify_popup_inner">
    <div class="popup_title">
      <div class="popup_title_content">
        Spotify einbinden
      </div>
      <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
    </div>
    <div class="popup_content" id="spotify_popup_content">
      <div class="half left">
        <div class="embedded_header">
          <img src="/signed/src/icns/spotify.svg" alt="" class="spotify_logo"> Spotify URI einfügen
          <div class="embedded_input">
            <input type="text" name="spotify_input" id="spotify_uri" placeholder="Hier einfügen...">
            <div class="error_message" id="spotify_empty">
              Bitte füge eine gültige URI ein!
            </div>
          </div>
        </div>
      </div>
      <div class="half right">
        <div class="embedded_header spotify_preview">
          Vorschau des Players:
        </div>
        <div class="embed" id="spotify_embedded_player">
          <!-- <iframe class="track" src="https://open.spotify.com/embed/track/5WvAo7DNuPRmk4APhdPzi8" frameborder="0" height="75" allowtransparency="true" allow="encrypted-media"></iframe>
            <iframe class="album" src="https://open.spotify.com/embed/album/4JBZ0QHveEpESepanNBG8A" frameborder="0" height="280" width="189" allowtransparency="true" allow="encrypted-media"></iframe> -->
        </div>


      </div>
      <div class="half left" id="spotify_uri_info">
        <div class="embedded_header">
          Wo finde ich die URI?
        </div>
        <div class="embedded_content">
          In Spotify: Auf beliebigen Song, Album, Playlist,<br>
          Künstler oder Podcast und wähle „Teilen“.<br>
          Klicke auf "Spotify URI kopieren" und füge die URI unten ein
          <br>
          <button type="button" class="link_btn" id="show_spotify_info" name="button">Mehr Infos</button>
        </div>
      </div>

    </div>
    <div class="bottom_right_btns">
      <button type="button" class="gray_btn menue_button" id="abort_spotify" name="button">Abbrechen</button>
      <button type="button" class="blue_btn menue_button " id="save_spotify" name="button">Fertig</button>
    </div>
  </div>

</div>

<div class="popup_background" id="spotify_info_popup">
  <div class="popup_body" id="spotify_info_inner">
    <div class="popup_title no_border">
      <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
    </div>
    <div class="popup_content">
      <img src="/signed/src/icns/spotify_info.png" class="spotify_info_img" alt="">
      <div class="spotify_infotext">
        Rechtsklick auf beliebigen Song, Album, Playlist, Künstler oder Podcast bei Spotify und wähle „Teilen“. <br>
        Klicke auf „Spotify URI kopieren“ und füge die URI unten ein
      </div>
    </div>
    <div class="bottom_right_btns">
      <button type="button" class="menue_button blue_btn" id="close_spotify_info" name="button">Schließen</button>
    </div>
  </div>
</div>
