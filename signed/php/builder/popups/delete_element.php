<div class="popup_background" id="delete_element_popup">
  <div class="popup_body" id="delete_element_popup_inner">
    <div class="popup_title">
      <div class="popup_title_content text_align_center">
        Element unwiderruflich löschen?
      </div>
      <!-- <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button> -->
    </div>
    <!-- <div class="text_wrapper">
      Element kann nicht wiederhergestellt werden
    </div> -->
    <div class="button_wrapper">
      <button type="button" class="gray_btn normal_button popup_close_btn_nostyle" id="delete_element_abort" name="button">Abbrechen</button>
      <button type="button" class="red_btn normal_button" id="delete_element" name="button">Löschen</button>
    </div>
  </div>
</div>
