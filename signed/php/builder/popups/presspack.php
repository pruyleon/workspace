<!-- Presspack Popup -->
<div class="popup_background" id="presspack_popup">
  <div class="popup_body builder_popup" id="presspack_popup_inner">
    <div class="popup_title">
      <div class="popup_title_content">
        Presspack bearbeiten
      </div>
      <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
    </div>
    <div class="popup_content" id="presspack_popup_content">
      <form class="" id="presspack_form" method="post">

        <!-- Bilder -->
        <div class="presspack_type active picture" data-type="picture">
          <div class="presspack_type_name">
            <span>Bilder</span> <button type="button" class="button_img delete_presspack_type" name="button"> <img src="/signed/src/icns/trash.svg" alt="löschen"> </button>
          </div>
          <div class="presspack_pic_list picture presspack_list">
            <?php
                $path = $_SERVER['DOCUMENT_ROOT'];
                $path .= "/signed/src/user/$user_id/presspack/picture/";
                foreach (get_files($path) as $value): ?>
            <div class="presspack_img picture" style="background-image: url(/signed/src/user/<?php echo $user_id.'/presspack/picture/'.$value ?>)">
              <button type="button" class="button_img img_delete_btn" name="button" data-name="<?php echo $value ?>"> <img src="/signed/src/icns/delete_presspack.svg" alt="löschen"> </button>
              <input type="file" class="empty hidden" name="picture" accept="image/*" value="">
            </div>
            <?php endforeach; ?>
            <div class="presspack_img picture add_img">
              <button type="button" class="button_img img_delete_btn" name="button" data-name=""> <img src="/signed/src/icns/delete_presspack.svg" alt="löschen"> </button>
              <input type="file" class="empty hidden" name="picture" accept="image/*" value="">
            </div>
          </div>
        </div>

        <!-- Videos -->
        <div class="presspack_type active video" data-type="video">
          <div class="presspack_type_name">
            <span>Videos</span> <button type="button" class="button_img delete_presspack_type" name="button"> <img src="/signed/src/icns/trash.svg" alt="löschen"> </button>
          </div>
          <div class="presspack_pic_list video">
            <?php
                $path = $_SERVER['DOCUMENT_ROOT'];
                $path .= "/signed/src/user/$user_id/presspack/video/";
                foreach (get_files($path) as $value): ?>
            <div class="presspack_img video add_img" style="background-image: url(/signed/src/user/<?php echo $user_id.'/presspack/video/'.$value ?>)">
              <button type="button" class="button_img img_delete_btn" name="button" data-name="<?php echo $value ?>"> <img src="/signed/src/icns/delete_presspack.svg" alt="löschen"> </button>
              <input type="file" class="empty hidden" name="video" accept="video/*" value="">
            </div>
            <?php endforeach; ?>
            <div class="presspack_img video add_img">
              <button type="button" class="button_img img_delete_btn" name="button" data-name=""> <img src="/signed/src/icns/delete_presspack.svg" alt="löschen"> </button>
              <input type="file" class="empty hidden" name="video" accept="video/*" value="">
            </div>
          </div>
        </div>

        <!-- Logos -->
        <div class="presspack_type logo" data-type="logo">
          <div class="presspack_type_name">
            <span>Logos</span> <button type="button" class="button_img delete_presspack_type" name="button"> <img src="/signed/src/icns/trash.svg" alt="löschen"> </button>
          </div>
          <div class="presspack_pic_list logo">
            <?php
                $path = $_SERVER['DOCUMENT_ROOT'];
                $path .= "/signed/src/user/$user_id/presspack/logo/";
                foreach (get_files($path) as $value): ?>
            <div class="presspack_img logo" style="background-image: url(/signed/src/user/<?php echo $user_id.'/presspack/picture/'.$value ?>)">
              <button type="button" class="button_img img_delete_btn" name="button" data-name="<?php echo $value ?>"> <img src="/signed/src/icns/delete_presspack.svg" alt="löschen"> </button>
              <input type="file" class="empty hidden" name="logo" accept="images/*" value="">
            </div>
            <?php endforeach; ?>
            <div class="presspack_img picture add_img">
              <button type="button" class="button_img img_delete_btn" name="button" data-name=""> <img src="/signed/src/icns/delete_presspack.svg" alt="löschen"> </button>
              <input type="file" class="empty hidden" name="logo" accept="image/*" value="">
            </div>
          </div>
        </div>

        <!-- Biografien -->
        <div class="presspack_type bio" data-type="bio">
          <div class="presspack_type_name">
            <span>Biografien</span> <button type="button" class="button_img delete_presspack_type" name="button"> <img src="/signed/src/icns/trash.svg" alt="löschen"> </button>
          </div>
          <div class="presspack_pic_list bio">
            <?php
                $path = $_SERVER['DOCUMENT_ROOT'];
                $path .= "/signed/src/user/$user_id/presspack/bio/";
                foreach (get_files($path) as $value): ?>
            <div class="presspack_img bio" style="background-image: url(/signed/src/user/<?php echo $user_id.'/presspack/picture/'.$value ?>)">
              <button type="button" class="button_img img_delete_btn" name="button" data-name="<?php echo $value ?>"> <img src="/signed/src/icns/delete_presspack.svg" alt="löschen"> </button>
              <input type="file" class="empty hidden" name="bio" accept="image/*" value="">
            </div>
            <?php endforeach; ?>
            <div class="presspack_img bio add_img">
              <button type="button" class="button_img img_delete_btn" name="button" data-name=""> <img src="/signed/src/icns/delete_presspack.svg" alt="löschen"> </button>
              <input type="file" class="empty hidden" name="bio" value="">
            </div>
          </div>
        </div>

        <!-- Sonstiges -->
        <div class="presspack_type misc" data-type="misc">
          <div class="presspack_type_name">
            <span>Sonstiges</span> <button type="button" class="button_img delete_presspack_type" name="button"> <img src="/signed/src/icns/trash.svg" alt="löschen"> </button>
          </div>
          <div class="presspack_pic_list misc">
            <?php
                $path = $_SERVER['DOCUMENT_ROOT'];
                $path .= "/signed/src/user/$user_id/presspack/misc/";
                foreach (get_files($path) as $value): ?>
            <div class="presspack_img misc" data-type="misc" style="background-image: url(/signed/src/user/<?php echo $user_id.'/presspack/picture/'.$value ?>)">
              <button type="button" class="button_img img_delete_btn" name="button" data-name="<?php echo $value ?>"> <img src="/signed/src/icns/delete_presspack.svg" alt="löschen"> </button>
              <input type="file" class="empty hidden" name="misc" value="">
            </div>
            <?php endforeach; ?>
            <div class="presspack_img misc add_img">
              <button type="button" class="button_img img_delete_btn" name="button" data-name=""> <img src="/signed/src/icns/delete_presspack.svg" alt="löschen"> </button>
              <input type="file" class="empty hidden" name="misc" value="">
            </div>
          </div>
        </div>


      </form>
      <div class="dropdown_desc add_presspack_type">
        <img src="/signed/src/icns/plus.svg" alt=""> Weitere Rubirk
      </div>
      <div class="manual_dropdown presspack_rubrik">
        <div class="dropdown_point picture hidden" data-type="picture">
          Bilder
        </div>
        <div class="dropdown_point video hidden" data-type="video">
          Video
        </div>
        <div class="dropdown_point logo" data-type="logo">
          Logos
        </div>
        <div class="dropdown_point bio" data-type="bio">
          Biografien
        </div>
        <div class="dropdown_point misc" data-type="misc">
          Sonstiges
        </div>

      </div>
      <div class="spacer_50">

      </div>
    </div>
    <div class="bottom_left_div">
      <div class="font_weight_heavy presspack_info">
        Wer darf dein Presspack öffnen/downloaden?
      </div>
      <div class="presspack_info_select">
        <select class="" name="visibility">
          <option value="0">Öffentlich</option>
          <option value="1">Nur registrierte Clubs/Veranstalter</option>
        </select>
      </div>
    </div>
    <div class="bottom_right_btns">
      <!-- <button type="button" class="gray_btn menue_button" id="abort_presspack" name="button">Abbrechen</button> -->
      <button type="button" class="blue_btn menue_button " id="save_presspack" name="button">Fertig</button>
    </div>
  </div>
</div>
<div class="popup_background" id="delete_presspack_popup_outer">
  <div class="popup_body" id="delete_presspack_popup_inner">
    <div class="text_wrapper">
      Bist du dir sicher dass du diese Rubrik löschen möchtest?<br>Alle darin gespeicherten Dateien werden gelöscht!
    </div>
    <div class="button_wrapper">
      <button type="button" class="gray_btn normal_button" id="presspack_delete_abort" name="button">Abbrechen</button>
      <button type="button" class="red_btn normal_button" id="delete_presspack" name="button">Löschen</button>
    </div>
  </div>
</div>
