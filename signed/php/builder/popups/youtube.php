<!-- Popup Youtube -->
<div class="popup_background" id="youtube_popup">
  <div class="popup_body builder_popup" id="youtube_popup_inner">
    <div class="popup_title">
      <div class="popup_title_content">
        Youtube einbinden
      </div>
      <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
    </div>
    <div class="popup_content" id="youtube_popup_content">
      <div class="half left">
        <div class="embedded_header">
          <img src="/signed/src/icns/youtube.svg" alt="" class="youtube_logo"> Youtube URI einfügen
          <div class="embedded_input">
            <input type="text" name="youtube_input" id="youtube_link" placeholder="Hier einfügen...">
            <div class="error_message" id="youtube_empty">
              Bitte füge eine gültige URL ein!
            </div>
          </div>
        </div>
        <div class="checkbox_line youtube">
          <input type="checkbox" name="youtube_autoplay"> Autoplay
        </div>
        <div class="checkbox_line youtube">
          <input type="checkbox" name="youtube_thumbnail_infos"> Videoname und Accountbild im Thumbnail anzeigen
        </div>
      </div>

    </div>
    <div class="bottom_right_btns">
      <button type="button" class="gray_btn menue_button" id="abort_youtube" name="button">Abbrechen</button>
      <button type="button" class="blue_btn menue_button " id="save_youtube" name="button">Fertig</button>
    </div>
  </div>

</div>
