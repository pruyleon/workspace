<!-- Popups Mixcloud-->
<div class="popup_background" id="mixcloud_popup">
  <div class="popup_body builder_popup" id="mixcloud_popup_inner">
    <div class="popup_title">
      <div class="popup_title_content">
        Mixcloud einbinden
      </div>
      <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
    </div>
    <div class="popup_content" id="mixcloud_popup_content">
      <div class="full_width mixcloud_topline">
        <div class="half left">
          <div class="embedded_header">
            <img src="/signed/src/icns/mixcloud.svg" alt="Mixcloud" class="mixcloud_logo"> Sharelink einfügen
            <div class="embedded_input">
              <input type="text" name="soundcloud_input" id="mixcloud_link" placeholder="Hier einfügen...">
              <div class="error_message" id="mixcloud_empty">
                Bitte füge einen Link ein!
              </div>
            </div>
          </div>
        </div>
        <div class="half right mixcloud">
          <div class="embedded_header">
            Wo finde ich den Sharelink? <button type="button" class="link_btn" id="show_mixcloud_info" name="button">Mehr Infos</button>
          </div>
          <div class="embedded_content">
            In Mixcloud: Auf beliebigen Song, Album, Playlist,<br>
            Künstler oder Podcast und wähle „Teilen“.<br>
            Kopiere den Sharelink und füge Ihn hier ein
          </div>
        </div>
        <div class="full_width">
          <div class="embedded_header soundcloud_preview">
            Vorschau des Players:
          </div>
          <div class="embed" id="mixcloud_embedded_player">
            <!-- <iframe width="100%" id="mixcloud_darkmode" class="mixcloud_darkmode" src="https://www.mixcloud.com/widget/iframe/?hide_cover=1&hide_artwork=1&mini=1&feed=%2Fdjcity%2Fdj-rapture-djcity-de-podcast-150616%2F" frameborder="0" ></iframe>
              <iframe width="100%" id="mixcloud_lightmode" class="mixcloud_lightmode" src="https://www.mixcloud.com/widget/iframe/?hide_cover=1&hide_artwork=1mini=1&light=1&feed=%2Fdjcity%2Fdj-rapture-djcity-de-podcast-150616%2F" frameborder="0" ></iframe>
              <iframe width="100%" id="mixcloud_darkmode_artwork" class="mixcloud_darkmode" src="https://www.mixcloud.com/widget/iframe/?hide_cover=1&mini=1&feed=%2Fdjcity%2Fdj-rapture-djcity-de-podcast-150616%2F" frameborder="0" ></iframe> -->
            <!-- <iframe width="100%" id="mixcloud_lightmode_artwork" class="mixcloud_lightmode" src="https://www.mixcloud.com/widget/iframe/?hide_cover=1&mini=1&light=1&feed=%2Fdjcity%2Fdj-rapture-djcity-de-podcast-150616%2F" frameborder="0"></iframe> -->
          </div>

          <div class="full_width">
            <div class="toggle_btn_wrapper" id="mixcloud_mode_toggle">
              <button type="button" class="toggle_btn mixcloud active light_toggle" data-mode="light" id="mixcloud_light" name="button">Light</button>
              <button type="button" class="toggle_btn mixcloud dark_toggle" data-mode="dark" id="mixcloud_dark" name="button">Dark</button>
            </div>
            <input type="checkbox" name="mixcloud_artwork" id="mixcloud_artwork"> Artwork
          </div>
        </div>
      </div>
    </div>
    <div class="bottom_right_btns">
      <button type="button" class="gray_btn menue_button" id="abort_mixcloud" name="button">Abbrechen</button>
      <button type="button" class="blue_btn menue_button " id="save_mixcloud" name="button">Fertig</button>
    </div>
  </div>

</div>

<div class="popup_background" id="mixcloud_info_popup">
  <div class="popup_body" id="mixcloud_info_inner">
    <div class="popup_title no_border">
      <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
    </div>
    <div class="popup_content">
      <img src="/signed/src/icns/mixcloud_info.png" class="mixcloud_info_img" alt="">
      <div class="mixcloud_infotext embeded_infotext">
        Rechtsklick auf beliebigen Song, Album, Playlist, Künstler oder Podcast bei Spotify und wähle „Teilen“. <br>
        Klicke auf „Spotify URI kopieren“ und füge die URI unten ein
      </div>
    </div>
    <div class="bottom_right_btns">
      <button type="button" class="menue_button blue_btn" id="close_mixcloud_info" name="button">Schließen</button>
    </div>
  </div>
</div>
