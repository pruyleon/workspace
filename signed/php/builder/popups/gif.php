<div class="popup_background nofadeout" id="gif_popup">
  <div class="popup_body builder_popup" id="gif_popup_inner">
    <div class="popup_title">
      <div class="popup_title_content">
        GIF hochladen
      </div>
      <button type="button" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt=""> </button>
    </div>
    <div class="popup_content" id="gif_popup_content">
      <button type="button" id="upload_gif" name="button"> <img src="/signed/src/icns/upload.svg" alt=""> GIF hochladen </button>
      <div class="popup_footer">
        <div class="left_popup_footer">
          <a href="#">Video to GIF converter</a>
          <br>
          <a href="#">Photo to GIF converter</a>
        </div>
        <div class="right_popup_footer">

        </div>
      </div>
    </div>
    <div class="bottom_right_btns">
      <button type="button" class="gray_btn menue_button" id="abort_gif" name="button">Abbrechen</button>
      <button type="button" class="blue_btn menue_button " id="save_gif" name="button">Fertig</button>
    </div>
    <form id="gif_form" method="post">
      <input type="file" name="" accept="image/gif" id="gif_input" value="">
    </form>
  </div>
</div>
