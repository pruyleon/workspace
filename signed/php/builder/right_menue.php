<button type="button" class="" id="fullscreen" data-screen="edit" name="button"> <img src="/signed/src/icns/arrow_right_gray.svg" alt=""> </button>
<div class="right_menue width_transition" id="right_menue">
  <img src="/signed/src/logos/logo_full_color.png" id="right_menue_logo" alt="mypressk.it">
  <div class="builder_menue_top">
    <div class="menue_inner">
      <div class="mobile_switch_wrapper edit_disable_fadeout">
        <div class="mobile_switch_inner">
          <button type="button" class="switch_button view active" name="button" data-view="desktop"> <img src="/signed/src/icns/desktop.svg" alt="Desktop"> </button>
          <button type="button" class="switch_button view" name="button" data-view="mobile"> <img src="/signed/src/icns/mobile.svg" alt="Mobile"> </button>
        </div>
      </div>
      <button type="button" class="menue_button gray_btn edit_disable_fadeout" id="show_preview" name="button">Vorschau</button>
      <span id="Infos"></span>
    </div>
  </div>
  <div class="builder_menue_middle">
    <!-- <div class="menue_desc">
      Element auswählen
    </div> -->
    <button type="button" name="button" class="menue_button gray_btn" id="abort_element_mode">Abbrechen</button>
    <button type="button" name="button" class="menue_button blue_btn" id="choose_element">Hinzufügen</button>
  </div>
  <div class="builder_menue_bottom edit_disable_fadeout">
    <div class="menue_inner">
      <div class="presskit_lang_name">
        <div class="presskit_name" title="<?php echo $presskitname ?>">
          <?php echo $presskitname ?>
        </div>
        <!-- <div class="presskit_lang">
          <div class="lang_wrapper">
            <div class="language_trigger cursor_pointer color_change font_color_white hover_blue_inlineshadow">
              <span class="language">DE</span><img src="/signed/src/icns/dropdown_top.svg" alt="">
            </div>
            <div class="language_dropdown">
              <div class="language_dropdown_line">
                DE
              </div>
              <div class="language_dropdown_line">
                EN
              </div>
              <div class="language_dropdown_line">
                ES
              </div>
              <div class="language_dropdown_line">
                FR
              </div>
              <div class="language_dropdown_line">
                IT
              </div>
              <div class="language_dropdown_line">
                JA
              </div>
              <div class="language_dropdown_line">
                NL
              </div>
              <div class="language_dropdown_line">
                PL
              </div>
              <div class="language_dropdown_line">
                RU
              </div>
              <div class="language_dropdown_line">
                ZH
              </div>
            </div>
          </div>
        </div> -->
      </div>
      <button type="button" class="menue_button darkgray_btn" id="presskit_options" name="button">Optionen</button>
      <button type="button" class="menue_button blue_btn" id="publish" name="button">Veröffentlichen</button>
      <button type="button" class="menue_button darkestgray_btn" id="leave_builder" name="button">Verlassen</button>
      <div id="presskit_options_menue">
        <div class="options_line" data-action="new_presskit">
          <span class="image"><img src="/signed/src/icns/new_presskit.svg" alt=""></span><span class="text">Neues pressk.it</span>
        </div>
        <div class="options_line" data-action="open_presskit">
          <span class="image"><img src="/signed/src/icns/open.svg" alt=""></span><span class="text">Öffnen</span>
        </div>
        <div class="options_line" data-action="save_presskit">
          <span class="image"><img src="/signed/src/icns/save.svg" alt=""></span><span class="text">Speichern</span>
        </div>
        <div class="options_line" data-action="save_as_presskit">
          <span class="image"><img src="/signed/src/icns/save.svg" alt=""></span><span class="text">Speichern unter</span>
        </div>
      </div>
    </div>
  </div>
</div>
