<?php
//Komponenten
// id (Auto-generiert)
// presskit_id
// component_id
// element_id
// value
// type
// icons
// originals
// name_info
// name_size
// social_links
// social_selected
// logo_file_name
// logo_file_size
// thumbnails
// wave_colors
// file_names

include('../../db.php');

$presskit_id = $_POST['presskit_id'];
$component_id = $_POST['component_id'];
$element_id = $_POST['element_id'];
$artist_id = $_POST['artist_id'];
$user_id = $_POST['user_id'];
$html =  "<br>Presskit-id:      " . $presskit_id;
$html .=  "<br> Komponenten-id: " . $component_id;
$html .=  "<br>Element-id:      " . $element_id;

echo '<h2>Response ' . $component_id . ': </h2>';

$user_dir = '../../../src/user/' . $user_id;
$presskit_dir = $user_dir . '/presskits/' . $presskit_id . '/';

include('../data_base64.php');

// Falls Presskit noch nicht vorhanden entsprechenden Ordner erstellen
if (!file_exists($presskit_dir) && !is_dir($presskit_dir)) {
  mkdir($presskit_dir);
}

// INSERT String schreiben
$query = "INSERT INTO presskit_elements (";
$query_names = "`presskit_ID`, `artist_ID`, `element_ID`, `component_ID`";
$query_values = "$presskit_id, '$artist_id', '$element_id', $component_id";

/*DONE*/
if (isset($_POST['type'])) {
  $type = $_POST['type'];
  $type_string = json_encode($type);
  $query_names .= ", `type`";
  $query_values .= ", '$type_string'";
  if (count($_POST['type']) > 1) {
    echo "<br><b>TYPE</b>: " . json_encode($_POST['type']);
    $is_multi = true;
  } else {
    $is_multi = false;
  }
}


/*DONE*/
if (isset($_POST['value'])) {
  $value = json_encode($_POST['value']);
  $value_arr = $_POST['value'];
  $value_query = [];
  $i = 1;
  foreach ($value_arr as $base64) {
    // Slideshow durch | getrennt
    $value_exploded = explode("|", $base64);
    $value_exploded_length = count($value_exploded);
    if ($value_exploded_length > 1) {
      for ($j = 0; $j < $value_exploded_length; $j++) {
        $j1 = $j + 1;
        $file_name = "slideshow_" . $component_id . "_" . $i . "_" . $j1;
        if (makeFile($presskit_dir, $file_name, $value_exploded[$j])) {
          array_push($value_query, $file_name);
        } else {
          array_push($value_query, $value_exploded[$j]);
        }
      }
    } else {
      if ($type[$i - 1] == 'video') {
        echo "<br>VIDEO: ";
        //Datei aus user/video in presskits/id verschieben!
        copy($user_dir . '/video/' . $base64, $presskit_dir . $base64);
        array_push($value_query, $base64);
      } else {
        $file_name = "picture_" . $component_id . "_" . $i;
        if (makeFile($presskit_dir, $file_name, $base64)) {
          array_push($value_query, $file_name);
        } else {
          array_push($value_query, $base64);
        }
      }
    }
    $i++;
  }
  // echo "<br><b>VALUE</b>: " . json_encode($value_query);
  $query_names .= ", `value`";
  $query_values .= ", '" . json_encode($value_query) . "'";
}

if (isset($_POST['config'])) {
  $config = json_encode($_POST['config']);
  $html .=  "<br>config:    " . json_encode($config);
  $query_names .= ", `config`";
  $query_values .= ", '$config'";
  echo "<h1>CONFIG! $config</h1>";
}


echo '<h3>Types: ' . json_encode($_POST['type']) . '<br>Values: ' . json_encode($value_query) . '</h3>';
echo "<br>Thumbnails";


/*DONE*/
if (isset($_POST['thumbnails'])) {
  $thumbnails_arr = $_POST['thumbnails'];
  $thumbnails = json_encode($thumbnails_arr);
  $html .=  "<br>Thumbnails:      " . json_encode($thumbnails);
  $thumbnail_query = [];
  $i = 1;
  foreach ($thumbnails_arr as $base64) {
    $file_name = "thumbnail_" . $component_id . "_" . $i;
    if (makeFile($presskit_dir, $file_name, $base64)) {
      array_push($thumbnail_query, $file_name);
    } else {
      array_push($thumbnail_query, $base64);
    }
    echo "<br>" . json_encode($thumbnail_query);
    $i++;
  }
  $query_names .= ", `thumbnails`";
  $query_values .= ", '" . json_encode($thumbnail_query) . "'";
}


if (isset($_POST['covers'])) {
  $covers_arr = $_POST['covers'];
  $covers = json_encode($covers_arr);
  $html .=  "<br>Covers:      " . json_encode($covers);
  $query .= ", covers='" . $covers . "'";
  $covers_query = [];

  foreach ($covers_arr as $base64) {
    $i++;
    $file_name = "cover_" . $component_id . "_" . $i;
    if (makeFile($presskit_dir, $file_name, $base64)) {
      array_push($covers_query, $file_name);
    } else {
      array_push($covers_query, $base64);
    }

    echo "<br>" . json_encode($covers_query);
  }
  $query_names .= ", `covers`";
  $query_values .= ", '" . json_encode($covers_query) . "'";
}

/*if (isset($_POST['icons'])) {
  $icons = json_encode($_POST['icons']);
  $html .=  "<br>Icons:           " . json_encode($icons);
}*/

/*DONE*/
if (isset($_POST['originals'])) {
  $originals = json_encode($_POST['originals']);
  $html .=  "<br>Originals:       " . json_encode($originals);
  $query_names .= ", `originals`";
  $query_values .= ", '$originals'";
}

/*DONE*/
if (isset($_POST['name_info'])) {
  $name_info = json_encode($_POST['name_info']);
  $html .=  "<br>Name-Info:       " . json_encode($name_info);
  $query_names .= ", `name_info`";
  $query_values .= ", '$name_info'";
}

/*DONE*/
if (isset($_POST['name_color'])) {
  $name_color = json_encode($_POST['name_color']);
  $html .=  "<br>Name-Color:       " . json_encode($name_color);
  $query_names .= ", `name_color`";
  $query_values .= ", '$name_color'";
}

/*DONE*/
if (isset($_POST['name_size'])) {
  $name_size = json_encode($_POST['name_size']);
  $html .=  "<br>Name-Size:       " . json_encode($name_size);
  $query_names .= ", `name_size`";
  $query_values .= ", '$name_size'";
}

if (isset($_POST['social_links'])) {
  $social_links = json_encode($_POST['social_links']);
  $html .=  "<br>Social-Links:    " . json_encode($social_links);
  $query_names .= ", `social_links`";
  $query_values .= ", '$social_links'";
}

if (isset($_POST['social_selected'])) {
  $social_selected = json_encode($_POST['social_selected']);
  $html .=  "<br>Socials:         " . json_encode($social_selected);
  $query_names .= ", `social_selected`";
  $query_values .= ", '$social_selected'";
}

if (isset($_POST['logo_file_name'])) {
  $logo_file_name = json_encode($_POST['logo_file_name']);
  $html .=  "<br>Logofile Name:   " . json_encode($logo_file_name);
  $query_names .= ", `logo_file_name`";
  $query_values .= ", '$logo_file_name'";
}

if (isset($_POST['logo_file_size'])) {
  $logo_file_size = json_encode($_POST['logo_file_size']);
  $html .=  "<br>Logo File Size:  " . json_encode($logo_file_size);
  $query_names .= ", `logo_file_size`";
  $query_values .= ", '$logo_file_size'";
}

/*DONE*/
if (isset($_POST['titles']) && implode(",", $_POST['file_names']) != 'NULL') {
  $titles = json_encode($_POST['titles']);
  echo "<h4>" . json_encode($titles) . "</h4>";
  $html .=  "<br>Titles:      " . $titles;
  $query_names .= ", `titles`";
  $query_values .= ", '$titles'";
} else {
  echo "<h4>No Titles!</h4>";
}

/*DONE*/
if (isset($_POST['wave_colors'])) {
  $wave_colors = json_encode($_POST['wave_colors']);
  $html .=  "<br>Wavecolors:      " . json_encode($wave_colors);
  $query_names .= ", `wave_colors`";
  $query_values .= ", '$wave_colors'";
}

/*DONE*/
if (isset($_POST['file_names']) && implode(",", $_POST['file_names']) != "NULL") {
  $file_names = $_POST['file_names'];
  $file_names_string = json_encode($file_names);
  $query_names .= ", `file_names`";
  $query_values .= ", '$file_names_string'";
  //Dateien in Presskit Verzeichnis verschieben
  //array 1
  foreach ($file_names as $file) {
    copy($user_dir . '/audio/' . $file, $presskit_dir . $file);
    copy($user_dir . '/audio/' . $file . ".json", $presskit_dir . $file . ".json");
  }
}

// Headlines
if (isset($_POST['headlines'])) {
  $headlines_string = json_encode($_POST['headlines']);
  $headlines_string = str_replace("\\", "", $headlines_string);
  $query_names .= ", `headlines`";
  $query_values .= ", '$headlines_string'";
}

// Text
if (isset($_POST['text_inputs'])) {
  $text_string = json_encode($_POST['text_inputs']);
  $query_names .= ", `text`";
  $query_values .= ", '$text_string'";
}

// echo "<br>Query: " . $query;
// echo "<br>query_names: " . $query_names;
// echo "<br>query_values: " . $query_values;
$query .= $query_names . ") VALUES (" . $query_values;
$query .= ");";
// echo "<br>Query: " . $query;

mysqli_query($con, $query);

//Alle Bilder (Cropper, Slideshow, Audio, usw.) aus base64 in eigene Datei und bennen (presskit_id+_+component_id+_+type+$i)
// Typenkennung aus base64 STRING entfernen
// makeFile($presskit_dir, "pic2", $data);

function makeFile($dir, $filename, $base64)
{
  if (count(explode(';', $base64)) == 1) {
    echo "<br> ---- __" . $base64 . "__ is no valid base64";
    return false;
  }

  list($type, $base64) = explode(';', $base64);
  list(, $base64)      = explode(',', $base64);
  //Dekodieren
  $base64 = base64_decode($base64);

  file_put_contents($dir . $filename . ".png", $base64);
  return true;
}
