<?php
// $wrapper_class = "";
// $row_class ="";
// $slot_class = "";

if (isset($_POST['artist_id'])) {
    include('../../db.php');
    $wrapper_class = $_POST['wrapper_class'];
    $row_class = $_POST['row_class'];
    $slot_class = $_POST['slot_class'];
    $artist_id = $_POST['artist_id'];
}

?>

<div class="slot_listing_wrapper">
    <?php
    $counter = 0;
    $query = "SELECT * FROM presskits WHERE artist_ID= '" . $artist_id . "' ORDER BY presskit_slot ASC";
    $result = mysqli_query($con, $query);
    while ($res = mysqli_fetch_assoc($result)) {
        if ($counter % 2 == 0) {
            echo '<div class="slot_listing_row">';
        }
        if ($res['published'] == 1) {
            $hover_source = "published_msg.svg";
            $info_class = "published";
            $slot_class .= " published";
        } else {
            $hover_source = "overwrite_msg_hover.svg";
            $info_class = "overwrite";
            $slot_class .= " overwriteable";
        }
    ?>
        <div class="presskit_slot_wrapper <?php echo $slot_class; ?>" data-presskitid="<?php echo $res['presskit_ID'] ?>">
            <div class="presskit_img_wrapper">
                <div class="presskit_slot_preview">
                    Vorschau
                </div>
                <div class="slot_hover_info <? echo $info_class ?>" style="background-image: url(/signed/src/layer/<?php echo $hover_source ?>)">
                </div>
                <?php if ($res['published'] == 0) : ?>
                    <div class="slot_action">
                        Überschriebenes Presskit wird unwiderruflich gelöscht, bist du dir sicher?
                        <div>
                            <button class="normal_button gray_btn abort_slot_overwrite" id="">Verwerfen</button>
                            <button class="normal_button blue_btn overwrite_slot" id="">Speichern</button>
                        </div>

                    </div>
                <?php endif; ?>

            </div>
            <div class="presskit_slot_desc">
                <div class="listing_presskit_name"><?php echo $res['presskit_name']; ?></div>
                <div class="listing_presskit_last_edited"><?php echo strftime("%d.%m.%y", $res['last_edited']); ?></div>
            </div>
        </div>
    <?php
        if ($counter % 2 != 0) {
            echo '</div>';
        }

        $counter++;
    }
    if ($counter % 2 == 0) {
        echo '</div>';
    }
    ?>
</div>
</div>