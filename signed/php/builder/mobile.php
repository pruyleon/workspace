<div class="builder_content" id="mobile">
  <div class="mobile_container">
    <div class="mobile_helper">
      <div class="mobile_inner_wrapper">
        <img src="/signed/src/borders/phone_top.svg" alt="">
        <div class="mobile_inner">
          <div class="mobile_content" id="mobile_portrait">
            <div class="mobile_header_element menue_parent">
              <div class="mobile_element_menue">
                <div class="action_wrapper" data-type="' + type + '">
                  <span class="reposition_name_mobile">NAME</span>
                </div>
              </div>
              <div class="mobile_element_content">
                <?php include('./incl/menue/min_menue_mobile.php') ?>
                <img src="/signed/src/icns/filter/photo.svg" class="" id="mobile_photo_edit_icn" alt="">
                <div class="placement_border"></div>
                <div id="mobile_name_wrapper" title="Name bearbeiten" class="hover_blue_inlineshadow" data-font-size="56">
                  <div class="name">
                    <?php
                    echo mb_strtoupper($artist_name, 'UTF-8');
                    ?>
                  </div>
                  <div class="logo">

                  </div>
                </div>
              </div>
              <input type="hidden" name="icons" value="">
              <input type="hidden" name="value" value="">
              <input type="hidden" name="type" value="">
              <input type="hidden" name="original" value="">
            </div>
            <div class="mobile_footer">
              <button type="button" class="no_hover button_invisible" name="button"> <img src="/signed/src/icns/switch_icons/presspack.svg" alt=""> </button>
              <button type="button" class="no_hover button_invisible" name="button"> <img src="/signed/src/icns/switch_icons/request.svg" alt=""> </button>
            </div>
            <div class="mobile_flip">
              <div class="inner">

              </div>
            </div>
          </div>
          <div class="mobile_content" id="mobile_landscape">
            <div class="inner" id="">

            </div>
          </div>
        </div>
        <img src="/signed/src/borders/phone_bottom.svg" alt="">
      </div>
    </div>
    <div class="mobile_switch">
      <button type="button" id="switch_mobile_format" name="button" data-format="portrait"> <img src="/signed/src/icns/format_switch_portrait.svg" alt=""> </button>
    </div>
  </div>
  <!-- Mobiles Bearbeitungsmenü -->
  <div id="mobile_edit_menue">
    <div class="full_absolute hidden" id="mobile_name">
      <div class="edit_menue_header">
        Name / Logo Editor
      </div>
      <div class="mobile_edit_inner">
        <div class="relative">
          <div class="picker wrapper" visible="false">
            <div id="mobile_name_colorpicker">

            </div>
            <div class="picker_buttons">
              <button type="button" class="normal_button blue_btn" name="button">Übernehmen</button>
            </div>
          </div>
        </div>
        <select class="darktrans_select left" name="mobile_name_selector">
          <option value="0">Name verbergen</option>
          <option value="1" selected>Artist Name</option>
          <option value="2">Logo</option>
        </select>
        <div class="inner_edit mobile_name left">
          <span>Größe: </span>
          <button type="button" data-action="descrease" class="mobile_name_resize_button" name="button" title="Name verkleinern"> - </button>
          <button type="button" data-action="increase" class="mobile_name_resize_button" name="button" title="Name vergrößern"> + </button>
          <input type="hidden" name="mobile_namesize" value="56">
          <button type="button" id="mobile_name_uppercase" data-state="uppercase" name="button" title="Großschreibung ein/aus"></button>
          <img src="/signed/src/icns/text_color.svg" class="cursor_pointer" alt="Farbe wählen" id="mobile_name_color" title="Namensfarbe">
          <button type="button" id="make_mobile_name_shadow" data-state="NULL" name="button" title="Schriftschatten ein/aus"> S </button>
        </div>
        <div class="inner_edit mobile_logo left">
          <button type="button" id="upload_mobile_logo" name="button" title="Logo hochladen">Logo hochladen</button>
          <span>Größe: </span>
          <button type="button" id="descrease_mobile_logo_size" name="button" title="Logo verkleinern"> - </button>
          <button type="button" id="inscrease_mobile_logo_size" name="button" title="Logo vergrößern"> + </button>
          <input type="hidden" name="mobile_logo_multiplicator" value="1">
        </div>
      </div>
      <div class="buttons">
        <!-- <button type="button" class="normal_button gray_btn" id="abort_name" name="button">Abbrechen</button> -->
        <button type="button" class="normal_button blue_btn" id="save_mobile_name" name="button">Fertig</button>
      </div>
    </div>
    <div class="full_absolute hidden" id="mobile_photo">
      <div class="edit_menue_header">
        Foto Editor
      </div>
      <div class="mobile_edit_inner">
        <img src="/signed/src/icns/minus_white.svg" class="minus">
        <div class="" id="mobile_cropper_slider">

        </div>
        <img src="/signed/src/icns/plus_white.svg" class="plus">
      </div>
      <div class="buttons">
        <button type="button" class="normal_button blue_btn" id="save_mobile_cropped_pic" name="button">Fertig</button>
        <!-- <button type="button" class="normal_button gray_btn" id="abort_cropped" name="button">Abbrechen</button> -->
      </div>
    </div>
  </div>
</div>