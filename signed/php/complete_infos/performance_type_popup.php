<div class="popup popup_background clickfadeout" id="performance_popup">
  <div class="popup_body" id="performance_popup_content">
    <div class="genre_content additional performance">
      <div class="genre_header performance">
        Wähle deine Art der performance
      </div>
      <div class="genre_desc small performance">
        Mehrfachauswahl möglich
      </div>
      <div class="genre_wrapper performance">
        <div class="genre_inner left"> <input type="checkbox" class="performance_type" name="performance_type[]" id="performance_type_1" value="1"> <label for="performance_type_1">DJ ohne Hosting</label></div>
        <div class="genre_inner right"> <input type="checkbox" class="performance_type" name="performance_type[]" id="performance_type_2" value="2"> <label for="performance_type_2">DJ mit Hosting</label></div>
        <div class="genre_inner left"> <input type="checkbox" class="performance_type" name="performance_type[]" id="performance_type_3" value="3"> <label for="performance_type_3">Rapper</label></div>
        <div class="genre_inner right"> <input type="checkbox" class="performance_type" name="performance_type[]" id="performance_type_4" value="4"> <label for="performance_type_4">MC / Hosting</label></div>
        <div class="genre_inner left"> <input type="checkbox" class="performance_type" name="performance_type[]" id="performance_type_5" value="5"> <label for="performance_type_5">Sänger</label></div>
        <div class="genre_inner right"> <input type="checkbox" class="performance_type" name="performance_type[]" id="performance_type_6" value="6"> <label for="performance_type_6">Musiker</label></div>
        <div class="genre_inner left"> <input type="checkbox" class="performance_type" name="performance_type[]" id="performance_type_7" value="7"> <label for="performance_type_7">Instrumentalist</label></div>
        <div class="genre_inner right"> <input type="checkbox" class="performance_type" name="performance_type[]" id="performance_type_8" value="8"> <label for="performance_type_8">Sonstige Künstler</label></div>
        <div class="missing_category">
          Welche Kategorie hat gefehlt? <input type="text" name="missing_category">
        </div>
      </div>
      <div class="bottom_right_btns">
        <button type="button" class="blue_btn normal_button" id="save_performance" name="button">übernehmen</button>
      </div>

    </div>
  </div>
</div>
