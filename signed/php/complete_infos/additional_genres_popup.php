<div class="popup popup_background clickfadeout" id="additional_genres_popup">
  <div class="popup_body" id="additional_genres_popup_content">
    <div class="genre_content additional">
      <div class="genre_header">
        Lass Clubs wissen welche weiteren Genre du spielst
      </div>
      <!-- <div class="genre_desc">
         wähle hier alles was du spielen kannst
       </div> -->
      <div class="genre_wrapper">
        <?php
          $i = 0;
          $row_1_content = "";
          $row_2_content = "";
          $row_3_content = "";
          $row_4_content = "";
           foreach ($genre_array as $key=>$value) {
               if ($i < $row_1) {
                   $row_1_content .= '<div class="genre_inner"><div class="second"><input type="checkbox" name="additional_genres[]" id="add_'.$key.'" value="'.$key.'"><label for="add_'.$key.'">'.$value.'</label></div></div>';
               } elseif ($i < $row_2) {
                   $row_2_content .= '<div class="genre_inner"><div class="second"><input type="checkbox" name="additional_genres[]" id="add_'.$key.'" value="'.$key.'"><label for="add_'.$key.'">'.$value.'</label></div></div>';
               } elseif ($i < $row_3) {
                   $row_3_content .= '<div class="genre_inner"><div class="second"><input type="checkbox" name="additional_genres[]" id="add_'.$key.'" value="'.$key.'"><label for="add_'.$key.'">'.$value.'</label></div></div>';
               } else {
                   $row_4_content .= '<div class="genre_inner"><div class="second"><input type="checkbox" name="additional_genres[]" id="add_'.$key.'" value="'.$key.'"><label for="add_'.$key.'">'.$value.'</label></div></div>';
               }
               $i++;
           }
           ?>
        <div class="genre_row first">
          <?php echo $row_1_content ?>
        </div>
        <div class="genre_row second">
          <?php echo $row_2_content ?>
        </div>
        <div class="genre_row third">
          <?php echo $row_3_content ?>
        </div>
        <div class="genre_row fourth">
          <?php echo $row_4_content ?>
        </div>
      </div>
      <div class="bottom_right_btns">
        <button type="button" class="darkgray_btn normal_button" id="abort_additional_genre" name="button">abbrechen</button><button type="button" class="blue_btn normal_button" id="save_additional_genres" name="button">Speichern</button>
      </div>

    </div>
  </div>
</div>
