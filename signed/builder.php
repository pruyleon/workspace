<?php
include('./php/login_state.php');
only_logged(); //_userid();
?>
<html lang="de" dir="ltr">

<head>
  <?php
  include("../src/header.php");
  include("./php/gen.php");
  $user_id = USER;

  $artist_id = getArtistID($user_id);
  $artist_name = getArtistNameAccID($user_id);

  //Tutorial
  $query = "SELECT account.acc_ID, account.country, countries.code, countries.de, account.tutorial, account.mobile_tutorial FROM account INNER JOIN countries ON account.country=countries.code WHERE account.acc_ID='" . $user_id . "'";
  $result = mysqli_query($con, $query);
  while ($res = mysqli_fetch_assoc($result)) {
    $country = $res['de'];
    $tutorial = $res['tutorial'];
    $mobile_tutorial = $res['mobile_tutorial'];
  }

  if ($tutorial == 0) {
    $tutorial = true;
    $tutorial_string = 'true';
  } else {
    $tutorial = false;
    $tutorial_string = 'false';
  }

  if ($mobile_tutorial == 0) {
    $mobile_tutorial = true;
    $mobile_tutorial_string = 'true';
  } else {
    $mobile_tutorial = false;
    $mobile_tutorial_string = 'false';
  }

  /*$tutorial = true;
  $tutorial_string = 'true';
  $mobile_tutorial = true;
  $mobile_tutorial_string = 'true';*/

  $query = "SELECT acc_ID, hometown FROM artists WHERE acc_ID='" . $user_id . "'";
  $result = mysqli_query($con, $query);
  while ($res = mysqli_fetch_assoc($result)) {
    $city = $res['hometown'];
  }

  $artist_city_country = $city . " | " . $country;

  //Genre-Listing für genre_string
  include('./php/profile_elements/raw/comps/genre_listing.php');

  //Checken ob Template  vorhanden
  $is_presskit = false;
  if (isset($_GET['template']) && $_GET['template']) {
    $template = urldecode($_GET['template']);
    $query = "SELECT * FROM template_contents WHERE name='" . $template . "'";
    $result = mysqli_query($con, $query);
    if (mysqli_num_rows($result) > 0) {
      while ($res = mysqli_fetch_assoc($result)) {
        $raw_array = $res['elements'];
      }
      $presskitname = "Presskit";
    } else {
      //ID des Künstler-Presskits
      $query = "SELECT * FROM presskits WHERE presskit_ID='" . $template . "' AND artist_ID='" . $artist_id . "'";
      $result = mysqli_query($con, $query);
      if (mysqli_num_rows($result) > 0) {
        $db_elements = true;
        //Presskit mit ID vorhanden
        while ($res = mysqli_fetch_assoc($result)) {
          //Presskit Elemente laden
          $is_presskit = true;
          $presskit_id = $res['presskit_ID'];
          $presskitname = $res['presskit_name'];
          $is_published = $res['published'];
          $created = $res['created'];
          $last_edited = $res['last_edited'];
        }
      } else {
        //Kein gespeichertes Presskit mit ID
        $presskit_id = 'NULL';
        $presskitname = "Presskit";
        $is_published = 0;
        $created = 0;
        $last_edited = 0;
        $raw_array = "H1";
      }
    }
    if (!$is_presskit) {
      $raw_array = explode(',', $raw_array);
    }
  } else {
    //Miami
    $raw_array = ['H1'];
    $db_elements = false;
    $presskitname = "Presskit";
    $presskit_id = 'NULL';
  }
  // $raw_array = ['H1'];
  //$raw_array = ['H1', '8L', '12L'];

  $language_array = ['DE', 'EN', 'ES', 'FR', 'IT', 'JA', 'NL', 'PL', 'RU', 'ZH'];


  $mobile_menue_color = 'white';
  $headline_placeholder = '<div class="placeholder">HEADLINE HEADLINE HEADLINE</div>';
  $lorem_ipsum_placeholder = '<div class="placeholder">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</div>';

  $no_switch = false;
  ?>
  <meta charset="utf-8">
  <title>Presskit Builder <?php echo $user_id ?></title>

  <link rel="stylesheet" href="/signed/css/builder/builder.css">
  <link rel="stylesheet" href="/signed/css/builder/responsive_builder.css">
  <link rel="stylesheet" href="/signed/css/builder/edit.css">
  <link rel="stylesheet" href="/signed/css/builder/mobile_edit.css">
  <link rel="stylesheet" href="/signed/css/builder/preview.css">
  <link rel="stylesheet" href="/signed/css/builder/dj_carousel.css">
  <link rel="stylesheet" href="/signed/css/presskit_footer.css">
  <link rel="stylesheet" href="/signed/css/builder/options.css">
  <link rel="stylesheet" href="/signed/css/builder/builder_popups.css">
  <link rel="stylesheet" href="/signed/css/builder/builder_popups/audio.css">
  <link rel="stylesheet" href="/signed/css/builder/builder_popups/gif.css">
  <link rel="stylesheet" href="/signed/css/builder/builder_popups/presspack.css">
  <link rel="stylesheet" href="/signed/css/builder/builder_popups/request.css">
  <link rel="stylesheet" href="/signed/css/builder/builder_popups/slideshow.css">
  <link rel="stylesheet" href="/signed/css/builder/builder_popups/social_media.css">
  <link rel="stylesheet" href="/signed/css/builder/builder_popups/video.css">
  <link rel="stylesheet" href="/signed/css/builder/builder_popups/presskit_options.css">
  <?php if ($tutorial || $mobile_tutorial) : ?>
    <link rel="stylesheet" href="/signed/css/builder/tutorial.css">
  <?php endif; ?>
  <?php if ($mobile_tutorial) : ?>
    <link rel="stylesheet" href="/signed/css/builder/mobile_tutorial.css">
  <?php endif; ?>
  <link rel="stylesheet" href="/signed/css/builder/builder_filter.css">
  <link rel="stylesheet" href="/signed/css/builder/mobile_name_placement.css">
  <link rel="stylesheet" href="/signed/css/name_placement.css">
  <!-- Edit Menues -->
  <link rel="stylesheet" href="/signed/css/builder/edit_menues/colorpicker.css">
  <link rel="stylesheet" href="/signed/css/builder/edit_menues/edit_menues.css">
  <link rel="stylesheet" href="/signed/css/builder/edit_menues/name_placement.css">
  <link rel="stylesheet" href="/signed/css/builder/edit_menues/cropper.css">
  <link rel="stylesheet" href="/signed/css/builder/edit_menues/text_headline.css">
  <link rel="stylesheet" href="/signed/css/profile_elements/gen.css">
  <link rel="stylesheet" href="/signed/css/profile_elements/mobile_gen.css">
  <link rel="stylesheet" href="/signed/css/profile_elements/positions.css">
  <link rel="stylesheet" href="/signed/css/profile_elements/raw.css">
  <link rel="stylesheet" href="/signed/css/profile_elements/preview.css">
  <link rel="stylesheet" href="/signed/css/file_archive.css">
  <link rel="stylesheet" href="/signed/css/misc/main_genre_popup.css">
  <link rel="stylesheet" href="/signed/incl/menue/menue_min.css">
  <!-- <link rel="stylesheet" href="/signed/css/misc/colorpicker.css"> -->

  <!-- <link rel="stylesheet" href="/signed/css/profile_elements/presentation.css"> -->
  <!-- <link rel="stylesheet" href="/signed/css/profile_elements/templates.css"> -->

  <!-- colorpicker -->
  <link rel="stylesheet" href="/src/colorpicker/dist/pickr.min.css">
  <script src="/src/colorpicker/dist/pickr.min.js" charset="utf-8"></script>

  <link rel="stylesheet" href="/signed/css/popups.css">
  <link rel="stylesheet" href="/signed/css/misc/loadingscreen.css">
  <link rel="stylesheet" href="/src/crop/croppie.css">
  <link rel="stylesheet" href="/src/slick/slick.css">
  <link rel="stylesheet" href="/src/quill/quill.snow.css">
  <link rel="stylesheet" href="/src/quill/quill.core.css">
  <link rel="stylesheet" href="/src/slick/slick-theme.css">
  <link rel="stylesheet" href="/src/video-js/video-js.css">

  <!-- Tests -->
  <!-- <link rel="stylesheet" href="https://code.jquery.com/qunit/qunit-2.9.2.css"> -->
</head>

<body>
  <input type="hidden" name="user_id" value="<?php echo $user_id ?>">
  <input type="hidden" name="artist_id" value="<?php echo $artist_id ?>">
  <input type="hidden" name="user_tutorial" value="<?php echo $tutorial_string ?>">
  <input type="hidden" name="mobile_tutorial" value="<?php echo $mobile_tutorial_string ?>">
  <input type="hidden" name="artist_name" value="<?php echo $artist_name ?>">
  <input type="hidden" name="genre_string" value="<?php echo $genre_string ?>">
  <input type="hidden" name="genre_ids" value="<?php echo $genre_ids ?>">
  <input type="hidden" name="artist_city_country" value="<?php echo $artist_city_country ?>">
  <input type="hidden" name="artist_country" value="<?php echo $country ?>">
  <input type="hidden" name="artist_city" value="<?php echo $city ?>">
  <input type="hidden" name="lorem_ipsum_placeholder" value='<?php echo $lorem_ipsum_placeholder ?>'>
  <input type="hidden" name="headline_placeholder" value='<?php echo $headline_placeholder ?>'>
  <input type="hidden" name="presskit_id" value="<?php echo $presskit_id ?>">
  <input type="hidden" name="presskit_name" value="<?php echo $presskit_id ?>">
  <input type="hidden" name="is_published" value="<?php echo $presskit_id ?>">
  <input type="hidden" name="created" value="<?php echo $presskit_id ?>">
  <input type="hidden" name="last_edited" value="<?php echo $presskit_id ?>">

  <input type="hidden" name="presskit_id" value="NULL">
  <!-- Linkes Menü -->
  <?php include('./php/builder/left_menue.php'); ?>
  <!-- Rechtes Menü -->
  <?php include('./php/builder/right_menue.php'); ?>
  <!-- Builder-Body -->
  <div class="builder_body">
    <button type="button" id="abort_preview" class="normal_button gray_btn" name="button">Vorschau schließen</button>
    <!-- Grauer Layer -->
    <div class="builder_cover width_transition">

    </div>
    <!-- Profil-Rahmen (Positionshilfe) -->
    <div class="builder_viewport width_transition">
      <!-- Div für Profil -->
      <div class="builder_content width_transition" id="desktop">
        <?php
        $background = "";
        $loaded = true;
        //include('./php/profile_elements/raw/1L.php');

        //Zu inkludierende Elemente (Gewähltes Template bzw. geladener Entwurf)
        if (!$is_presskit) {
          foreach ($raw_array as $element_id) {
             include('./php/profile_elements/raw/' . $element_id . '.php');
          }
        }
        
        //include('./php/builder/data_base64.php');
        //include('./php/builder/incl/demo_elements.php');

        ?>
        <div class="carousel_footer_wrapper">
          <?php
          //Standard Includes
          include('./php/dj_carousel/dj_carousel_listing.php');
          include('./incl/footer/presskit_footer_builder.php');
          ?>
          <div class="footer_filter">
            <div class="message">
              <img src="/signed/src/icns/arrow_top.svg" alt="">
              <br>
              Element hinzufügen
            </div>
          </div>
        </div>
        <div class="spacer_desktop width_transition"></div>
      </div>
      <div class="edit_menue_wrapper">
        <?php
        include('./php/builder/edit_menues/headline.php');
        include('./php/builder/edit_menues/text.php');
        include('./php/builder/edit_menues/color.php');
        include('./php/builder/edit_menues/name_placement.php');
        include('./php/builder/edit_menues/photo.php');
        ?>
      </div>
      <?php include('./php/builder/mobile.php'); ?>
    </div>


  </div>
  <!-- Tutorial Overlay -->
  <?php //echo $tutorial_overlay;
  ?>

  <!-- Popup Includes -->
  <?php
  include('./php/builder/popups/presskit_options.php');
  include('./php/builder/popups/audio.php');
  include('./php/builder/popups/video.php');
  include('./php/builder/popups/gif.php');
  include('./php/builder/popups/presspack.php');
  include('./php/builder/popups/request.php');
  include('./php/builder/popups/slideshow.php');
  include('./php/builder/popups/social_media.php');
  include('./php/builder/popups/delete_element.php');

  //DJ-Karousell
  include('./php/dj_carousel/dj_carousel_popup.php');

  //Archiv
  include('./php/file_archive/archive_popup.php');

  //Genre Popups
  include('./php/misc/main_genre_popup.php');

  //Ladescreen
  include('./php/misc/loadingscreen.php');

  // Tutorial
  if ($tutorial) {
    include('./php/builder/popups/tutorial.php');
  }

  // Mobiles Tutorial
  if ($mobile_tutorial) {
    include('./php/builder/popups/mobile_tutorial.php');
  }
  ?>
  <?php //include('./php/invitation_popup.php')
  ?>
  <button type="button" id="photo_cropper_trigger" name="button"></button>

  <!-- Tests -->
  <!-- <script src="/js/incl/qunit.js"></script> -->
  <!-- <script src="/js/incl/gen.js"></script> -->
  <!-- <script src="/js/incl/test_init.js"></script> -->
  <!-- Start js includes -->
  <script src="/signed/js/dj_carousel.js" charset="utf-8"></script>
  <!-- <script type="text/javascript" src="/signed/js/builder/fontsizes.js"></script> -->
  <script type="text/javascript" src="/src/crop/croppie.js"></script>
  <script type="text/javascript" src="/signed/js/builder/vars.js"></script>
  <script type="text/javascript" src="/signed/js/builder/builder_functions.js"></script>
  <script type="text/javascript" src="/signed/js/builder/element_edit_functions.js"></script>
  <script type="text/javascript" src="/signed/js/builder/profile_edit_functions.js"></script>
  <script type="text/javascript" src="/signed/js/builder/name_placement_functions.js"></script>
  <script type="text/javascript" src="/signed/js/builder/presskit_options_functions.js"></script>
  <script type="text/javascript" src="/signed/js/builder/name_placement.js"></script>
  <script type="text/javascript" src="/signed/js/builder/mobile_name_placement_functions.js"></script>
  <script type="text/javascript" src="/signed/js/builder/mobile_name_placement.js"></script>
  <script type="text/javascript" src="/signed/js/builder/element_edit.js"></script>
  <script type="text/javascript" src="/signed/js/builder/slideshow.js"></script>
  <script type="text/javascript" src="/signed/js/builder/profile_edit.js"></script>
  <script type="text/javascript" src="/signed/js/file_archive/archive.js"></script>
  <script type="text/javascript" src="/signed/js/builder/mobile_edit_functions.js"></script>
  <script type="text/javascript" src="/signed/js/builder/mobile_edit.js"></script>
  <script type="text/javascript" src="/signed/js/builder/preview.js"></script>
  <script type="text/javascript" src="/signed/js/builder/builder.js"></script>
  <script type="text/javascript" src="/signed/js/popups.js"></script>
  <script type="text/javascript" src="/signed/js/builder/filter.js"></script>
  <script type="text/javascript" src="/signed/js/builder/presskit_options.js"></script>
  <script type="text/javascript" src="/signed/js/builder/presskit_save/save_presskit.js"></script>
  <script type="text/javascript" src="/signed/js/builder/presskit_save/save_presskit_functions.js"></script>
  <script type="text/javascript" src="/signed/js/builder/genre_popup.js"></script>
  <script type="text/javascript" src="/signed/js/builder/request_popup.js"></script>
  <script type="text/javascript" src="/signed/js/builder/presspack_popup.js"></script>
  <script type="text/javascript" src="/signed/js/file_archive/archive_zoom.js"></script>
  <?php if ($tutorial) : ?>
    <script type="text/javascript" src="/signed/js/builder/tutorial.js"></script>
  <?php endif; ?>
  <?php if ($mobile_tutorial) : ?>
    <script type="text/javascript" src="/signed/js/builder/mobile_tutorial.js"></script>
  <?php endif; ?>
  <?php if ($mobile_tutorial || $tutorial) : ?>
    <script type="text/javascript" src="/signed/js/builder/tutorial_functions.js"></script>
  <?php endif; ?>
  <!-- Slideshow -->
  <script type="text/javascript" src="/src/slick/slick.min.js"></script>
  <!-- Audipplayer -->
  <script src="/src/wavesurfer/wavesurfer.js"></script>
  <!-- Videoplayer -->
  <script src='/src/video-js/video-js.js'></script>

  <script src="/src/quill/quill.js" charset="utf-8"></script>

  <!-- <script src="/signed/js/builder/tests/var_tests.js"></script> -->
</body>

</html>
