$(function() {
	/*var city_array;$.ajax({url: "/signed/php/tools/get_cities.php",type: "POST",success: function(response) {city_array = response.split(",");console.log(city_array);//Städte Autocomplete$('input[name=city]').autocomplete({minLength: 3,source: city_array,autoFocus: true});}});*/
	var artist_id = $('input[name=artist_id]').val();
	var user_id = $('input[name=user_id]').val();
	$('#continue').on('click', function() {
		artist_name = $('input[name=artist_name]').val();
		url = $('input[name=url]').val();
		address = $('input[name=address]').val();
		country_code = $('input[name=country_code]').val();
		city = $('input[name=city]').val();

		if (!url_check) {
			console.log('Keine gültige URL!');
			return;
		}

		if (artist_name.length < 3) {
			console.log('Kein gültiger Name! -> ' + artist_name.length);
			return;
		}
		if (!city_chosen) {
			console.log('Keine Stadt!');
			return;
		}

		$.ajax({
			url: '/signed/php/complete_infos/insert.php',
			type: 'POST',
			data: {
				artist_id: artist_id,
				user_id: user_id,
				artist_name: artist_name,
				url: url,
				address: address,
				country_code: country_code,
				city: city
			},
			success: function(response) {
				if (response == 'SUCCESS') {
					window.location.replace('/builder');
				} else {
					alert(response);
				}
			}
		});
	});

	//Change der url
	var url_check = false;
	$('input[name=url]').on('keyup', function() {
		url_check = urlCheck();
	});

	function urlCheck() {
		var url = $('input[name=url]').val();
		if (url.length < 5) {
			$('#url_check_icn').fadeIn();
			$('#url_check_icn').attr('src', '/signed/src/icns/forbidden_red.svg');
			$('#url_check_icn')
				.closest('.tooltip')
				.find('span[role=tooltip]')
				.text('Zulässige Zeichen: 0-9 a-z _ -');
			$('#url_check_icn').removeClass('disabled');
			$('input[name=url]').removeClass('required_red');
			$('input[name=url_check]').val('false');
			return false;
		}
		if (!/^[a-zA-Z0-9\-\_]+$/.test(url)) {
			$('#url_check_icn').fadeIn();
			$('#url_check_icn').attr('src', '/signed/src/icns/forbidden_red.svg');
			$('#url_check_icn')
				.closest('.tooltip')
				.find('span[role=tooltip]')
				.text('Zulässige Zeichen: 0-9 a-z _ -');
			$('#url_check_icn').removeClass('disabled');
			$('input[name=url]').removeClass('required_red');
			$('input[name=url_check]').val('false');
			return false;
		} else {
			$.ajax({
				url: '/signed/php/tools/check_url.php',
				type: 'POST',
				data: {
					artist_id: artist_id,
					url: url
				},
				success: function(response) {
					console.log(response);
					if (response == 'free') {
						$('#url_check_icn').fadeIn();
						$('#url_check_icn').attr('src', '/signed/src/icns/checked_green.svg');
						//$(this).closest('.tooltip').find('span[role=tooltip]').fadeOut(0);
						$('#url_check_icn')
							.closest('.tooltip')
							.find('span[role=tooltip]')
							.css('visibility', 'hidden');
						$('#url_check_icn').addClass('disabled');
						$('input[name=url_check]').val('true');
						$('input[name=url]').removeClass('required_red');
						url_check = true;
					} else {
						$('#url_check_icn').fadeIn();
						$('#url_check_icn').attr('src', '/signed/src/icns/forbidden_red.svg');
						$('#url_check_icn')
							.closest('.tooltip')
							.find('span[role=tooltip]')
							.css('visibility', 'unset');
						$('#url_check_icn')
							.closest('.tooltip')
							.find('span[role=tooltip]')
							.text('Diese URL ist bereits vergeben!');
						$('#url_check_icn').removeClass('disabled');
						$('input[name=url]').addClass('required_red');
						$('input[name=url_check]').val('false');
						url_check = false;
					}
					return url_check;
				}
			});
		}
	}
});
//
