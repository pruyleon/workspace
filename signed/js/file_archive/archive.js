$(function() {
	//archiv
	//Datei löschen
	$(document).on('click', '.delete_file', function(e) {
		name = $(this)
			.parent()
			.parent()
			.attr('data-name');
		$.ajax({
			url: '/signed/php/file_archive/delete_file_by_name.php',
			type: 'POST',
			data: {
				name: name,
				user_id: user_id
			},
			success: function(response) {
				if (response == 'deleted') {
					$('.archive_file_preview').remove();
					$('.archive_file[data-name="' + name + '"]').remove();
					checkArchiveFilesLength();
				}
			}
		});
	});

	$('.archive_upload_wrapper').on('click', function() {
		$('#archive_input').trigger('click');
	});

	//Datei hochladen
	$('#archive_input').change(function() {
		$('#loadingscreen').fadeIn(200);
		$.ajax({
			url: '/signed/php/file_archive/upload_file.php',
			type: 'POST',
			data: new FormData($('#archive_form')[0]),
			contentType: false,
			cache: false,
			processData: false,
			success: function(response) {
				if (response != 'failure') {
					//Bild in archiv-popup hinzufügen
					addToArchive(response, user_id);
					recently_uploaded = response;
					//Cropper auslösen
					if (edit_trigger == 'photo') {
						$('#loadingscreen').fadeOut(500);
						$('#photo_cropper_trigger').trigger('click');
					} else if (edit_trigger == 'slideshow') {
						// Slideshow-Cropper
						$(slideshow_element_clicked)
							.parent()
							.children('.slideshow_input')
							.val(recently_uploaded);
						if (croppie_position != 0) {
							//Bereits bestehende Instanz speichern und zerstören
							slideshow_element = $('.slideshow_element[position=' + croppie_position + ']');
							result = $(slideshow_element)
								.croppie('result', {
									type: 'base64',
									size: 'original',
									format: 'png'
								})
								.then(function(dataImg) {
									$(slideshow_element)
										.parent()
										.children('.slideshow_input_base64')
										.val(dataImg);
									$(slideshow_element).css('background-image', 'url(' + dataImg + ')');
									$(slideshow_element).croppie('destroy');
									$(slideshow_element).removeClass('active_cropping');
									$('.cropper_wrapper.slideshow').fadeOut();
									$('.croppie_slider_wrapper.slideshow').html('');
									newSlideshowCropper(
										$(slideshow_element_clicked)
											.parent()
											.children('.slideshow_input'),
										user_id
									);
								});
						} else {
							newSlideshowCropper(
								$(slideshow_element_clicked)
									.parent()
									.children('.slideshow_input'),
								user_id
							);
						}
					} else if (edit_trigger == 'presspack') {
						$('#loadingscreen').fadeOut(500);
						//presspack_clicked
						$('.presspack_pic_list.picture').append('<div class="presspack_img picture add_img" data-type="picture"><button type="button" class="button_img img_delete_btn" name="button" data-name=""> <img src="/signed/src/icns/delete_presspack.svg" alt="Menü"> </button><input type="file" class="empty hidden" name="picture" accept="image/*" value=""></div>');
						$.ajax({
							url: '/signed/php/presspack/copy_from_archive.php',
							data: {
								user_id: user_id,
								file_name: recently_uploaded
							},
							type: 'POST',
							success: function(response) {
								$(presspack_clicked).removeClass('add_img');
								$(presspack_clicked).css('background-image', 'url("/signed/src/user/' + user_id + '/archive/' + recently_uploaded + '")');
								$(presspack_clicked)
									.children('button')
									.attr('data-name', recently_uploaded);
							}
						});
					} else if (edit_trigger == 'logo_placement') {
						$('#loadingscreen').fadeOut(500);
						bindLogoPlacement(recently_uploaded, user_id);
					} else if (edit_trigger == 'video') {
						$('#loadingscreen').fadeOut(500);
						$('#video_thumbnail_file').val(recently_uploaded);
						makeVideoCropper();
					} else if (edit_trigger == 'audio') {
						$('#loadingscreen').fadeOut(500);
						makeAudioCropper(active_audio, recently_uploaded, user_id);
					}
					checkArchiveFilesLength();
					$('#file_archive_popup').fadeOut();
				}
			}
		});
	});

	//Archiv Klick auf Datei
	croppie_position = 0;

	$(document).on('click', '.archive_file', function(data, handler) {
		if (data.target == this) {
			recently_uploaded = $(this).attr('data-name');
			if (edit_trigger == 'photo') {
				$('#photo_cropper_trigger').trigger('click');
			} else if (edit_trigger == 'slideshow') {
				//Ladescreen einfaden
				$('#loadingscreen').fadeIn(200);
				if ($(slideshow_element_clicked).hasClass('empty')) {
					$(slideshow_element_clicked).addClass('was_empty');
				}
				$(slideshow_element_clicked)
					.parent()
					.children('.slideshow_input')
					.val(recently_uploaded);
				if (croppie_position != 0) {
					//Bereits bestehende Instanz speichern und zerstören
					slideshow_element = $('.slideshow_element[position=' + croppie_position + ']');
					result = $(slideshow_element)
						.croppie('result', {
							type: 'base64',
							size: 'original',
							format: 'png'
						})
						.then(function(dataImg) {
							$(slideshow_element)
								.parent()
								.children('.slideshow_input_base64')
								.val(dataImg);
							$(slideshow_element).css('background-image', 'url(' + dataImg + ')');
							$(slideshow_element).croppie('destroy');
							$(slideshow_element).removeClass('active_cropping');
							$('.cropper_wrapper.slideshow').fadeOut();
							$('.croppie_slider_wrapper.slideshow').html('');
							newSlideshowCropper(
								$(slideshow_element_clicked)
									.parent()
									.children('.slideshow_input'),
								user_id
							);
						});
				} else {
					newSlideshowCropper(
						$(slideshow_element_clicked)
							.parent()
							.children('.slideshow_input'),
						user_id
					);
				}
			} else if (edit_trigger == 'presspack') {
				//presspack_clicked
				$('.presspack_pic_list.picture').append('<div class="presspack_img picture add_img" data-type="picture"><button type="button" class="button_img img_delete_btn" name="button" data-name=""> <img src="/signed/src/icns/delete_presspack.svg" alt="Menü"> </button><input type="file" class="empty hidden" name="picture" accept="image/*" value=""></div>');
				$.ajax({
					url: '/signed/php/presspack/copy_from_archive.php',
					data: {
						user_id: user_id,
						file_name: recently_uploaded
					},
					type: 'POST',
					success: function(response) {
						$(presspack_clicked).removeClass('add_img');
						$(presspack_clicked).css('background-image', 'url("/signed/src/user/' + user_id + '/archive/' + recently_uploaded + '")');
						$(presspack_clicked)
							.children('button')
							.attr('data-name', recently_uploaded);
					}
				});
			} else if (edit_trigger == 'logo_placement') {
				bindLogoPlacement(recently_uploaded, user_id, 'header');
			} else if (edit_trigger == 'mobile_logo_placement') {
				bindLogoPlacement(recently_uploaded, user_id, 'mobile');
			} else if (edit_trigger == 'audio') {
				makeAudioCropper(active_audio, recently_uploaded, user_id);
			} else if (edit_trigger == 'video') {
				$('#video_thumbnail_file').val(recently_uploaded);
				makeVideoCropper();
			} else if (edit_trigger == 'mobile_header') {
				disableMobileEdit();
				url_file = '/signed/src/user/' + user_id + '/archive/' + recently_uploaded;
				$('.mobile_header_element>input[name=original]').val(recently_uploaded);
				var mobile_croppie = $('.mobile_element_content').croppie({
					url: url_file,
					viewport: {
						width: '100%',
						height: '100%'
					},
					showZoomer: true,
					mouseWheelZoom: false
				});
				appendPositionNoteFixed($('.mobile_element_content'), '');
				croppie_slider = $('.croppie-container').children('.cr-slider-wrap');
				$('#mobile_cropper_slider').html('');
				$(croppie_slider).appendTo('#mobile_cropper_slider');
				$('#mobile_photo').fadeIn(0);
				setTimeout(function() {
					$('#mobile_edit_menue').fadeIn(500);
				}, 500);
			}
			$('#file_archive_popup').fadeOut();
		}
	});

	$(document).on('mouseenter', '.archive_file>.content>.delete_file', function() {
		$(this)
			.closest('.archive_file')
			.addClass('red_border');
	});

	$(document).on('mouseleave', '.archive_file>.content>.delete_file', function() {
		$(this)
			.closest('.archive_file')
			.removeClass('red_border');
	});
});
