$(function() {
	$(document).on('mouseenter', '.archive_file', function() {
		archive_file = $(this).attr('data-name');
		hovered_archive_file = $(this);
		archive_file_pos = $(hovered_archive_file).position();
		filearchive_timeout = setTimeout(function() {
			archive_file_top_pos = archive_file_pos.top - 110;
			archive_file_left_pos = archive_file_pos.left + 50;
			$(hovered_archive_file)
				.closest('.popup_body')
				.append('<div class="archive_file_preview" style="top: ' + archive_file_top_pos + '; left: ' + archive_file_left_pos + '"><img src="' + archive_path + archive_file + '" alt=""></div>');
			$('.archive_file_preview').fadeIn(200);
		}, 700);
	});

	$(document).on('mouseleave', '.archive_file', function() {
		clearTimeout(filearchive_timeout);
		$('.archive_file_preview').remove();
	});
});
