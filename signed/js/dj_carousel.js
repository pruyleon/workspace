$(function() {
	var disabled = [];
	refresh_carousel(disabled);

	$('input[name=carousel_search]').on('keyup', function() {
		refresh_carousel(disabled);
	});
	$('.dj_wrapper:not(.search)').droppable({
		hoverClass: 'drop-hover',
		over: function(event, ui) {
			$(this).css({
				border: '1px dashed green',
				'background-color': ' rgba(12,200,12,0.3)'
			});
		},
		drop: function(event, ui) {
			//voherigen Artist wieder freischalten
			prev_artist_id = $(this)
				.find('.dj_pic')
				.attr('data-artistid');

			//Artist in Droppable Daten speichern
			if (
				$(ui.draggable)
					.parent()
					.hasClass('listing')
			) {
				acc_id_drop = $(this)
					.children('.dj_pic')
					.attr('data-accid');
				artist_id_drop = $(this)
					.children('.dj_pic')
					.attr('data-artistid');
				artist_name_drop = $(this)
					.children('.dj_pic')
					.attr('data-name');
			}
			//Gedroppten Artist Daten speichern und in
			dragged_artist_id = $(ui.draggable).attr('data-artistid');
			acc_id = $(ui.draggable).attr('data-accid');
			name = $(ui.draggable).attr('data-name');
			// if ($(this).children('.dj_pic').attr('data-accid') != "none") {
			//   //Aus Disabled-Array entfernen
			//   var index = disabled.indexOf($(this).children('.dj_pic').attr('data-accid'));
			//   if (index !== -1) disabled.splice(index, 1);
			// }
			$(this)
				.children('.dj_pic')
				.css('background-image', 'url(/signed/src/user/' + acc_id + '/profile.jpg)');
			$(this)
				.children('.dj_pic')
				.addClass('draggable');
			$(this)
				.children('.dj_desc')
				.text(name);
			$(this)
				.children('.dj_pic')
				.attr('data-accid', acc_id);
			$(this)
				.children('.dj_pic')
				.attr('data-artistid', dragged_artist_id);
			$(this)
				.children('.dj_pic')
				.attr('data-name', name);
			$(this).removeClass('empty');
			if (
				$(ui.draggable)
					.parent()
					.hasClass('listing')
			) {
				if (
					$(ui.draggable)
						.parent()
						.hasClass('listing') &&
					acc_id_drop == 'none'
				) {
					$(ui.draggable)
						.parent()
						.removeClass('draggable')
						.addClass('empty')
						.removeAttr('style');
				}
				$(ui.draggable).removeAttr('style');
				$(ui.draggable)
					.next('.dj_desc')
					.text('DJ wählen');
				$(ui.draggable).attr('data-accid', 'none');
				$(ui.draggable).attr('data-artistid', 'none');
				$(ui.draggable).attr('data-name', 'none');
			} else {
				if (prev_artist_id != 'none') {
					$('.dj_pic[data-artistid=' + prev_artist_id + ']:not(.listing)')
						.removeClass('disabled')
						.addClass('draggable')
						.draggable('enable');
					$('.dj_pic[data-artistid=' + prev_artist_id + ']:not(.listing)')
						.closest('.dj_wrapper')
						.removeClass('disabled');
				}
			}
			$('.dj_pic[data-artistid=' + dragged_artist_id + ']:not(.listing)')
				.closest('.dj_wrapper')
				.addClass('disabled');
			//Zu Disabled-Array hinzufügen    $(ui.draggable).attr('data-accid')
			disabled.indexOf(acc_id) === -1 ? disabled.push(acc_id) : false;

			//Artist "drop" daten in draggable schreiben
			if (
				$(ui.draggable)
					.parent()
					.hasClass('listing') &&
				acc_id_drop != 'none'
			) {
				console.log('Listing!');
				$(ui.draggable).css('background-image', 'url(/signed/src/user/' + acc_id_drop + '/profile.jpg)');
				$(ui.draggable).addClass('draggable');
				$(ui.draggable)
					.parent()
					.children('.dj_desc')
					.text(artist_name_drop);
				$(ui.draggable).attr('data-accid', acc_id_drop);
				$(ui.draggable).attr('data-artistid', artist_id_drop);
				$(ui.draggable).attr('data-name', artist_name_drop);
			}
			makePicsDraggable();
			$(this).css({
				border: '1px dashed transparent',
				'background-color': 'unset'
			});
			$('.dj_pic[data-artistid=' + dragged_artist_id + ']:not(.listing)')
				.addClass('disabled')
				.removeClass('draggable')
				.draggable('disable');
		},
		out: function() {
			$(this).css({
				border: '1px dashed transparent',
				'background-color': 'unset'
			});
		}
	});

	$(document).on('click', '.dj_wrapper:not(.disabled)', function() {
		if ($('.dj_wrapper.empty').length == 0) {
			// console.log("Kein Leerer Wrapper");
			return false;
		}
		if (
			$(this)
				.parent()
				.hasClass('top_wrapper')
		) {
			// console.log("Top Wrapper");
			return false;
		}

		empty_carousel = $('.dj_wrapper.empty')[0];
		//Artist in Droppable Daten speichern
		acc_id_drop = $(this)
			.children('.dj_pic')
			.children('.dj_pic')
			.attr('data-accid');
		artist_id_drop = $(this)
			.children('.dj_pic')
			.children('.dj_pic')
			.attr('data-artistid');
		artist_name_drop = $(this)
			.children('.dj_pic')
			.children('.dj_pic')
			.attr('data-name');
		//Gedroppten Artist Daten speichern und in
		dragged_artist_id = $(this)
			.children('.dj_pic')
			.attr('data-artistid');
		acc_id = $(this)
			.children('.dj_pic')
			.attr('data-accid');
		name = $(this)
			.children('.dj_pic')
			.attr('data-name');

		$(empty_carousel)
			.children('.dj_pic')
			.css('background-image', 'url(/signed/src/user/' + acc_id + '/profile.jpg)');
		$(empty_carousel)
			.children('.dj_pic')
			.addClass('draggable');
		$(empty_carousel)
			.children('.dj_desc')
			.text(name);
		$(empty_carousel)
			.children('.dj_pic')
			.attr('data-accid', acc_id);
		$(empty_carousel)
			.children('.dj_pic')
			.attr('data-artistid', dragged_artist_id);
		$(empty_carousel)
			.children('.dj_pic')
			.attr('data-name', name);
		$(empty_carousel).removeClass('empty');

		$('.dj_pic[data-artistid=' + dragged_artist_id + ']:not(.listing)')
			.closest('.dj_wrapper')
			.addClass('disabled');
		//Zu Disabled-Array hinzufügen    $(ui.draggable).attr('data-accid')
		disabled.indexOf(acc_id) === -1 ? disabled.push(acc_id) : false;
		makePicsDraggable();
		$(empty_carousel).css({
			border: '1px dashed transparent',
			'background-color': 'unset'
		});
		$('.dj_pic[data-artistid=' + dragged_artist_id + ']:not(.listing)')
			.addClass('disabled')
			.removeClass('draggable')
			.draggable('disable');
	});

	$('#save_carousel').on('click', function() {
		makeCarousel();
	});

	$('#abort_carousel').on('click', function() {
		$('#carousel_popup').fadeOut();
	});

	$(document).on('click', '.carousel_artist', function() {
		// if (edit_mode) {
		//   return false;
		// }
		if (!$(this).hasClass('disabled')) {
			$('#carousel_popup').fadeIn();
		}
	});

	$(document).on('click', '.dj_carousel_listing', function() {
		// if (edit_mode) {
		//   return false;
		// }
		if (!$(this).hasClass('disabled')) {
			$('#carousel_popup').fadeIn();
		}
	});

	$('#open_inivte_popup').on('click', function() {
		$('#invite_popup').fadeIn();
	});

	$('#copy_invite_link').on('click', function() {
		var $temp = $('<input>');
		$('body').append($temp);
		$temp.val($('#invite_link_string').text()).select();
		console.log($('#invite_link_string').text());
		document.execCommand('copy');
		$temp.remove();
		$('#copy_message').fadeIn(200);
		setTimeout(function() {
			$('#copy_message').fadeOut(500);
		}, 1000);
	});

	function refresh_carousel(disabled) {
		var users_artist_id = $('input[name=artist_id]').val();
		if ($('input[name=carousel_search]').val()) {
			searchstring = "AND name LIKE '%" + $('input[name=carousel_search]').val() + "%' ";
		} else {
			searchstring = '';
		}
		query = "SELECT acc_ID, artist_ID, name, call_url FROM artists WHERE artist_ID != '" + users_artist_id + "' " + searchstring + ' LIMIT 12';
		$.ajax({
			url: '/signed/php/dj_carousel/carousel_search.php',
			type: 'POST',
			data: {
				query: query,
				artist_id: artist_id,
				disabled: disabled
			},
			success: function(response) {
				$('.artist_search_listing').html(response);
				makePicsDraggable();
			}
		});
	}

	function makeCarousel() {
		allFilled = true;
		$('.top_wrapper>.dj_wrapper').each(function() {
			if ($(this).hasClass('empty')) {
				allFilled = false;
				$('.carousel_empty').fadeIn();
			}
		});
		if (allFilled) {
			$('.top_wrapper>.dj_wrapper.empty').fadeOut();
			$('.dj_carousel_listing').html('<div class="dj_carousel_hover"><div class="message">Bearbeiten</div></div><div class="inner_listing"></div>');
			$('.top_wrapper>.dj_wrapper').each(function() {
				dragged_artist_id = $(this)
					.children('.dj_pic')
					.attr('data-artistid');
				acc_id = $(this)
					.children('.dj_pic')
					.attr('data-accid');
				$('.dj_carousel_listing>.inner_listing').append('<div class="carousel_artist" style="background-image: url(/signed/src/user/' + acc_id + '/profile.jpg)"></div>');
			});
			$('#carousel_popup').fadeOut();
			$('.dj_carousel_listing').removeClass('empty');
		}
	}

	function makePicsDraggable() {
		$('.dj_pic.draggable').draggable({
			helper: 'clone',
			cursor: 'grabbing',
			start: function(e, ui) {},
			cursorAt: {
				left: 65,
				top: 37.5
			}
		});
	}
});
