$(function() {
	$('.popup_close_btn').on('click', function() {
		$(this)
			.closest('.popup_background')
			.fadeOut();
	});

	$('.popup_close_btn_nostyle').on('click', function() {
		$(this)
			.closest('.popup_background')
			.fadeOut();
	});

	$('.clickfadeout')
		.on('click', function() {
			$(this).fadeOut();
		})
		.children()
		.click(function(e) {
			e.stopPropagation();
		});
});
