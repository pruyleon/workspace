$(function() {
	var templates = $('.template_wrapper');
	var previous_template;
	var template;
	var next_template;
	var format = 'landscape';

	$('.preview').on('click', function() {
		var template_id = $(this)
			.closest('.template_wrapper')
			.attr('data-id');
		$('#menue_wrapper').fadeOut();
		loadPreview(template_id, format);
		$('#preview_wrapper').fadeIn();
		$('#choose_template_link').attr('href', '/builder?template=' + template_id);
	});

	$('.choose').on('click', function() {
		var template_id = $(this)
			.closest('.template_wrapper')
			.attr('data-id');
		console.log('Template: ' + template_id);
		$('#choose_template_link').attr('href', '/builder?template=' + template_id);
		$('#choose_template_link').trigger('click');
	});

	$('#previous_template').on('click', function() {
		var template_id = $('input[name=template_id]').val();
		template = $('.template_outer_wrapper').find('.template_wrapper[data-id=' + template_id + ']');
		previous_template = $(template).prev('.template_wrapper');
		$('input[name=template_id]').val($(previous_template).attr('data-id'));
		if (!previous_template.attr('data-id')) {
			previous_template = $('.template_wrapper').last();
		}
		loadPreview($(previous_template).attr('data-id'), format);
		$('#choose_template_link').attr('href', '/builder?template=' + $(previous_template).attr('data-id'));
	});

	$('#next_template').on('click', function() {
		var template_id = $('input[name=template_id]').val();
		template = $('.template_outer_wrapper').find('.template_wrapper[data-id=' + template_id + ']');
		next_template = templates.eq(templates.index(template) + 1);
		if (!next_template.attr('data-id')) {
			next_template = $('.template_wrapper').first();
		}
		$('input[name=template_id]').val($(next_template).attr('data-id'));
		loadPreview($(next_template).attr('data-id'), format);
		$('#choose_template_link').attr('href', '/builder?template=' + $(next_template).attr('data-id'));
	});

	//Switch
	var device = 'desktop';
	$(document).on('click', '.switch_button:not(.active)', function() {
		$('.switch_button.active').removeClass('active');
		$(this).addClass('active');
		//console.log($(this).attr("data-view"));
		if ($(this).attr('data-view') == 'mobile') {
			$('body')
				.delay(500)
				.addClass('mobile_background');
			$('#menue_wrapper')
				.delay(500)
				.addClass('mobile_background');
			$('.top_wrapper_fixed ')
				.delay(500)
				.addClass('mobile_background');
			$('#desktop_preview').fadeOut(500, function() {
				$('#mobile_preview').fadeIn(500);
				device = 'mobile';
			});
			$('#menue_wrapper').fadeIn();
		} else {
			$('#menue_wrapper').fadeOut();
			$('body')
				.delay(500)
				.removeClass('mobile_background');
			$('#menue_wrapper')
				.delay(500)
				.removeClass('mobile_background');
			$('.top_wrapper_fixed ')
				.delay(500)
				.removeClass('mobile_background');
			$('#mobile_preview').fadeOut(500, function() {
				$('#desktop_preview').fadeIn(500);
				device = 'desktop';
			});
		}
	});

	//Switcher Format
	var active;
	var inactive;
	$(document).on('click', '#format_switcher', function() {
		active = $('.wrapper>div[data-active=true]');
		if ($(active).is('#portrait')) {
			$('#format_switcher>img').attr('src', '/signed/src/icns/format_switch_landscape.svg');
			format = 'landscape';
		} else {
			$('#format_switcher>img').attr('src', '/signed/src/icns/format_switch_portrait.svg');
			format = 'portrait';
		}
		inactive = $('.wrapper>div[data-active=false]');
		$(inactive).attr('data-active', 'true');
		$(active).attr('data-active', 'false');
		$(active).fadeOut(500, function() {
			$(inactive).fadeIn();
		});
	});

	function loadPreview(template_id, format) {
		$('#preview_body').load(
			'/signed/php/style_preview/preview_desktop.php',
			{
				template_id: template_id,
				device: device,
				format: format
			},
			function() {
				$('#desktop_preview').html('<img src="/signed/php/profile_elements/preview_full/' + template_id + '.png" alt="Fehler ' + template_id + '">');
				$('.mobile_preview_body.portrait').html('<img src="/signed/php/profile_elements/preview_full/' + template_id + '_mobile.png">');
				$('.mobile_preview_body.landscape').html('<img src="/signed/php/profile_elements/preview_full/' + template_id + '.png">');
			}
		);
	}
});
