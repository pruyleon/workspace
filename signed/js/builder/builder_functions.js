/**
 * confirmExit - Bei verlassen warnen
 *
 * @return {type}  description
 */
function confirmExit() {
	return 'Sicher, dass du die Seite verlassen willst? Alle nicht gespeicherten Änderungen gehen hierdurch verloren!';
}
//window.onbeforeunload = confirmExit;

/**
 * jQuery - outerHTML Funktion direkt als jQuery function
 *
 * @return {Stiring}  gibt äußeres HTML des Element zurück
 */
jQuery.fn.outerHTML = function() {
	return jQuery('<div />')
		.append(this.eq(0).clone())
		.html();
};

/**
 * window - Drag n Drop von Dateien verhindern
 *
 * @param  {type} "dragover" description
 * @param  {type} function(e description
 * @return {type}            description
 */

window.addEventListener(
	'dragover',
	function(e) {
		e = e || event;
		e.preventDefault();
	},
	false
);
window.addEventListener(
	'drop',
	function(e) {
		e = e || event;
		e.preventDefault();
	},
	false
);

/**
 * makeFullScreen - Fullscreen des Builders wird gestartet
 *
 * @return {type}  none
 */
function makeFullScreen() {
	$('body').addClass('preview_mode transition');
	$('body')
		.get(0)
		.style.setProperty('--pixelunit', '100vw / 1366');
	// Presspack und Anfragepopup
	setTimeout(function() {
		$('body').removeClass('transition');
	}, 2000);
}

/**
 * exitFullScreen - beendet Fullscreen des Builders
 *
 * @return {type}  description
 */
function exitFullScreen() {
	$('body').addClass('preview_mode transition');
	$('body')
		.get(0)
		.style.setProperty('--pixelunit', '88vw / 1366');
	$('body').removeClass('preview_mode');
	//Presspack und Anfragepopup
	setTimeout(function() {
		$('body').removeClass('transition');
	}, 2000);
}

/**
 * lazyloadAll - Bilder mit Klasse lazyload wird die src des attributs data-lazy zugewiesen
 *
 * @return {type}  description
 */
function lazyloadAll() {
	if ($('.lazyload[data-lazy=null]').length > 0) {
		img = $('.lazyload[data-lazy=null]')[0];
		lazyloadImg(img);
	} else {
		console.log('All Images Loaded');
	}
}

/**
 * lazyloadImg - Lädt die src des Images mit Pfad aus data-lazy attribut
 *
 * @param  {type} img description
 * @return {type}     description
 */
function lazyloadImg(img) {
	src = $(img).attr('lazy-src');
	$(img).attr('src', src);
	img.addEventListener('load', function() {
		$(img).attr('data-lazy', 'loaded');
		lazyloadAll();
	});
	img.addEventListener('error', function() {
		alert('error');
	});
}

/**
 * refreshMobileLandscape - Gibt #mobile_landscape>.inner den Inhalt des Desktop Builderbodys und entfernt alle Bearbeitungsmöglichkeiten
 *
 * @return {type}  description
 */
function refreshMobileLandscape() {
	// disableEdit();
	$('#mobile_landscape>.inner').html($('#desktop').html());
	$('#mobile_landscape>.inner')
		.find('.element_menue')
		.remove();
	$('#mobile_landscape>.inner')
		.find('.add_profile_element_wrapper')
		.remove();
	$('#mobile_landscape>.inner')
		.find('.fixed_tooltip')
		.remove();
	$('#mobile_landscape>.inner')
		.find('.footer_filter')
		.remove();
	$('#mobile_landscape>.inner')
		.find('.profile_element_menue_bottom')
		.remove();

	$('#mobile_landscape>.inner')
		.find('.hover_blue_inlineshadow')
		.removeClass('hover_blue_inlineshadow');
	$('#mobile_landscape>.inner')
		.find('.carousel_footer_wrapper')
		.removeClass('blur');

	$('#mobile_landscape>.inner')
		.find('.genre_listing')
		.unbind('click');
	$('#mobile_landscape>.inner')
		.find('button')
		.unbind('click');
	$('#mobile_landscape>.inner')
		.find('div')
		.unbind('click');
	$('#mobile_landscape>.inner')
		.find('span')
		.unbind('click');
	$('#mobile_landscape>.inner')
		.find('.social_media_trigger')
		.unbind('click');
	$('#mobile_landscape>.inner')
		.find('.textinput_textarea')
		.unbind('click');
	$('#mobile_landscape>.inner')
		.find('.headline_input')
		.unbind('click');
	$('#mobile_landscape>.inner')
		.find('button, img, div, span')
		.unbind('hover');

	$('#mobile_landscape>.inner')
		.find('button')
		.addClass('disabled');
	$('#mobile_landscape>.inner')
		.find('.genre_listing')
		.addClass('disabled');
	$('#mobile_landscape>.inner')
		.find('.button')
		.addClass('disabled');
	$('#mobile_landscape>.inner')
		.find('.social_media_trigger')
		.addClass('disabled');
	$('#mobile_landscape>.inner')
		.find('.carousel_artist')
		.addClass('disabled');
	$('#mobile_landscape>.inner')
		.find('.dj_carousel_listing')
		.addClass('disabled');
	$('#mobile_landscape>.inner')
		.find('a')
		.addClass('disabled');
	$('#mobile_landscape>.inner')
		.find('.textinput_textarea')
		.addClass('disabled');
	$('#mobile_landscape>.inner')
		.find('.headline_input')
		.addClass('disabled');
	$('#mobile_landscape>.inner')
		.find('.name_city_wrapper')
		.addClass('disabled');
	$('#mobile_landscape>.inner')
		.find('.audio_element_wrapper.empty')
		.removeClass('empty');

	$('#mobile_landscape>.inner')
		.find('.carousel_footer_wrapper')
		.addClass('disabled');
	$('#mobile_landscape>.inner')
		.find('.builder_icon')
		.remove();

	$('#mobile_landscape>.inner')
		.find('.footer_wrapper')
		.fadeIn(0);

	//Slideshows!
	$('#mobile_landscape')
		.find('.slick-slider')
		.each(function() {
			slideshow_values = $(this)
				.closest('.input_parent')
				.children('input[name=value]')
				.val();
			slideshow_values = slideshow_values.split('|');
			slideshow_values.forEach(element => {
				slideshow_html += '<div class="slides" style="background-image: url(' + element + ')"></div>';
			});
			$(this).html(slideshow_html);
			$(this).removeClass('slick-slider slick-initialized');
			$(this).slick({
				autoplay: true,
				autoplaySpeed: 2000,
				infinite: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				speed: 1000,
				easing: 'linear',
				arrows: false,
				pauseOnHover: false,
				respondTo: 'slider',
				swipe: false
			});
		});
}
