$(function () {
	$('#presskit_options').on('click', function () {
		$('#presskit_options_menue').fadeToggle(300);
	});

	$('#presskit_options_menue>.options_line').on('click', function () {
		presskit_option = $(this).attr('data-action');
		if (presskit_option == 'new_presskit') {
			showTemplatePopup();
		} else if (presskit_option == 'save_as_presskit') {
			startSaveAs();
		} else if (presskit_option == 'save_presskit'){
			if (presskit_id == 'NULL') {
				startSaveAs();
			} else {
				savePresskit(presskit_id, presskit_name, false);
			}
		}
	});

	// Neues Presskit aus Template
	$('.choose_template').on('click', function () {
		template_id = getTemplateID($(this));
		template_name = encodeURI(getTemplateName($(this)));
		openPresskitFromTemplate(template_name, template_id);
	});

	$('#discard_changes').on('click', function () {
		discardChanges();
	});

	$('#save_changes').on('click', function () {
		//Checken ob Presskit bereits Speicherplatz zugewiesen ist
		if (presskit_id !== 'NULL') {
			//Ist Presskit veröffentlicht?
			if (is_published == 1) {
				startSaveAs();
			} else {
				savePresskit(presskit_id, 'name', false);
			}
		} else {
			startSaveAs();
		}
	});

	$('#save_presskit_as').on('click', function () {
		presskit_name = $('#save_as_name_input').val();
		//checken ob speicherslot verfügbar
		$.ajax({
			url: '/signed/php/misc/get_free_save_slots.php',
			type: 'POST',
			data: {
				user_id: user_id,
				artist_id: artist_id,
			},
			success: function (response) {
				free_save_slots = parseInt(response);
				if (free_save_slots > 0) {
					//Presskit speichern
					savePresskit(false, presskit_name, false);
				} else {
					//"Überschreiben"-Popup öffnen
					startOverwrite();
				}
			},
		});
	});

	// Slot überschreiben Action in Popup
	$(document).on('click', '.presskit_slot_wrapper.overwriteable', function (e) {
		if ($(e.target).closest('.slot_action').length == 0) {
			$(this).find('.slot_action').stop().fadeIn(300);
		}
	});

	$(document).on('mouseleave', '.presskit_slot_wrapper', function () {
		$(this).find('.slot_action').stop().fadeOut(300);
	});

	//Slot überschreiben Popup Action buttons
	$('.abort_slot_overwrite').on('click', function () {
		$(this).closest('.slot_action').fadeOut(300);
	});

	$(document).on('click', '.overwrite_slot', function () {
		presskit_id = $(this).closest('.presskit_slot_wrapper').attr('data-presskitid');
		presskit_name = $('#save_as_name_input').val();
		savePresskit(false, presskit_name, presskit_id);
	});
});
