window.onresize = recalcFontSizes;

recalcFontSizes();
/**
 * recalcFontSizes - Berechnet die Responsiven Schriftgrößen für alle Texte innerhalb des Builders
 *
 * @return{boolean} true
 */
function recalcFontSizes() {
	vw = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	vh = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
	vw /= 100;
	vh /= 100;
	pixelunit = (75 * vh * 1.77777) / 1366;
	elementmodepixelunit = (65 * vw - (100 * vw - 75 * vh * 1.7) / 2) / 1366;
	console.log('Viewport-height: ' + vh + '\nPixelunit: ' + pixelunit);

	$('.profile_element *').css({
		'font-size': 12 * pixelunit,
		'line-height': 23 * pixelunit + 'px'
	});

	console.log(12 * pixelunit + 'px');
}
