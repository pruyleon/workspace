$(function() {
	/*setTimeout(function(){
    $('.switch_button[data-view=mobile]').trigger('click');
  }, 1000);*/

	$(document).on('click', '#mobile_photo_edit_icn:not(.disabled)', function() {
		edit_trigger = 'mobile_header';
		$('#file_archive_popup').fadeIn(300);
	});

	$('#save_mobile_cropped_pic').on('click', function() {
		var result = $('.mobile_element_content')
			.croppie('result', {
				type: 'base64',
				size: 'original',
				format: 'png'
			})
			.then(function(dataImg) {
				$('.croppie-container').croppie('destroy');
				$('.mobile_element_content').css('background-image', 'url(' + dataImg + ')');
				// Menü machen
				$('.mobile_element_menue').html(mobile_pic_menue);
				$('.mobile_header_element>input[name=value]').val(dataImg);
				$('.mobile_header_element>input[name=type]').val('picture');
				$('.mobile_header_element>input[name=original]').val(url_file);
				$('.mobile_element_content>img').remove();
				$('#mobile_edit_menue').fadeOut(500);
				$('#mobile_photo').fadeOut(500);
				enableMobileEdit();
			});
	});

	$(document).on('click', '.mobile_element_menue>.action_wrapper>.element_dropdown', function() {
		$('.mobile_element_menue>.dropdown').fadeToggle(300);
	});

	$(document).on('click', '.mobile_element_menue>.dropdown>.dropdown_line', function() {
		action = $(this).attr('data-action');
		if (action == 'delete') {
			$('.mobile_element_menue').html(restored_mobile_menue);
			$('.mobile_element_content').css('background-image', 'none');
			$('.mobile_element_content').append('<img src="/signed/src/icns/filter/photo.svg" class="" id="mobile_photo_edit_icn" alt="">');
		} else if (action == 'position') {
			disableMobileEdit();
			url_file = $('.mobile_header_element>input[name="original"]').val();
			console.log(url_file);
			mobile_croppie = $('.mobile_element_content').croppie({
				url: url_file,
				viewport: {
					width: '100%',
					height: '100%'
				},
				showZoomer: true,
				mouseWheelZoom: false
			});
			$('.mobile_element_content').append(position_note_fixed);
			$('#mobile_cropper_slider').html('');
			croppie_slider = $('.croppie-container').children('.cr-slider-wrap');
			$(croppie_slider).appendTo('#mobile_cropper_slider');
			$('#mobile_photo').fadeIn(0);
			$('#mobile_edit_menue').fadeIn(500);
		} else if (action == 'upload') {
			edit_trigger = 'mobile_header';
			$('#file_archive_popup').fadeIn(300);
		}
		$('.mobile_element_menue>.dropdown').fadeOut(0);
	});

	$('#switch_mobile_format').on('click', function() {
		if ($(this).attr('data-format') == 'portrait') {
			$('.mobile_inner_wrapper').addClass('turned');
			$(this).attr('data-format', 'landscape');
			$(this)
				.children('img')
				.attr('src', '/signed/src/icns/format_switch_landscape.svg');
			$('#mobile_portrait').fadeOut(500, function() {
				$('#mobile_landscape').fadeIn(500);
			});
		} else {
			$('.mobile_inner_wrapper').removeClass('turned');
			$(this).attr('data-format', 'portrait');
			$('#mobile_landscape').fadeOut(500, function() {
				$('#mobile_portrait').fadeIn(500);
			});
			$(this)
				.children('img')
				.attr('src', '/signed/src/icns/format_switch_portrait.svg');
		}
	});

	//Mobiles Bearbeitungsmenü rein-/rausfaden
	$('#mobile_portrait').on('mouseenter', function() {
		$('.mobile_element_menue')
			.stop()
			.fadeIn(500);
	});

	$('#mobile_portrait').on('mouseleave', function() {
		$('.mobile_element_menue')
			.stop()
			.fadeOut(500);
	});
});
