$(function() {
	$('.builder_icon[data-type=slideshow]').fadeOut(0);
	$('.footer_filter').addClass('force_hide');
	$('.add_profile_element_wrapper').addClass('force_hide');

	$('#tutorial_start_button, #tutorial_popup_start_inner>div>.popup_close_btn').on('click', function() {
		$('#tutorial_start_popup').fadeOut(500);
		$('#tutorial_icons_popup').fadeIn(500);
	});

	$('#tutorial_icons_button, #tutorial_popup_icons_inner>div>.popup_close_btn').on('click', function() {
		$('#tutorial_icons_popup').fadeOut(500);
		disableEditWithTime(0);
		$('.builder_icon[data-type=photo]').attr('style', '');
		$('.builder_icon[data-type=photo]').addClass('tutorial_highlight');
	});

	$('#photo_cropper_trigger').on('click', function() {
		if (user_tutorial) {
			$('.builder_icon[data-type=photo]').removeClass('tutorial_highlight');
		}

	});

	$('#save_cropped_pic').on('click', function() {
		if (user_tutorial) {
			setTimeout(function() {
				disableEditWithTime(0);
			}, 10);
			setTimeout(function() {
				disableEditWithTime(0);
			}, 500);
			setTimeout(function() {
				$('#tutorial_name_popup').fadeIn(500);
			}, 1000);
		}
	});

	$('#tutorial_name_button').on('click', function() {
		$('#tutorial_name_popup').fadeOut(500);
		$('.name_city_wrapper').removeClass('disabled');
		$('.name_city_wrapper').addClass('tutorial_highlight');
	});

	$('.name_city_wrapper').on('click', function() {
		if (user_tutorial) {
			$('.name_city_wrapper').removeClass('tutorial_highlight');
		}

	});

	$('#save_name').on('click', function() {
		if (user_tutorial) {
			disableEditWithTime(0);
			setTimeout(function() {
				$('#tutorial_genre_popup').fadeIn(500);
			}, 750);
		}
	});

	$('#tutorial_genre_button').on('click', function() {
		$('#tutorial_genre_popup').fadeOut(500);
		$('.genre_listing').removeClass('disabled');
		$('.genre_listing').addClass('tutorial_highlight');
	});

	$(document).on('click', '.genre_listing', function() {
		if (user_tutorial) {
			$('.genre_listing').removeClass('tutorial_highlight');
		}
	});

	$(document).on('click', '.genre_button:not(.selected)', function() {
		if (user_tutorial) {
			//$('.genre_listing').removeClass('tutorial_highlight');
			if (genre_stage == 'final') {
				setTimeout(function() {
					$('#tutorial_element_popup').fadeIn(500);
				}, 750);
			}
		}
	});

	$('#tutorial_element_button').on('click', function() {
		scrollAmount = 0;
		window_position = $(window).scrollTop();
		window_height = $(window).height();
		el_top_pos = $('.profile_element').position().top;
		el_height = $('.profile_element').height();
		parent_top = $('#desktop').position().top;
		el_top_pos *= -1;
		el_offset = el_top_pos + (window_height - el_height) - 200;
		if (el_offset < 0) {
			el_offset *= -1;
			scrollAmount = '+=' + el_offset + 'px';
		} else {
			scrollAmount = '-=' + el_offset + 'px';
		}
		$('#desktop').animate(
			{
				scrollTop: scrollAmount,
				complete: function() {
					return true;
				}
			},
			800
		);
		$('#tutorial_element_popup').fadeOut(500);
		$('.add_profile_element_wrapper').removeClass('force_hide');
		$('.add_profile_element_wrapper').addClass('force_display');
		$('.profile_element').addClass('tutorial_blur');
		$('.footer_filter').removeClass('force_hide');
		$('.footer_filter').addClass('force_display');
		$('.carousel_footer_wrapper').addClass('force_display');
	});

	$(document).on('click', '.add_profile_element_plus', function() {
		if (user_tutorial) {
			$('#abort_element_mode').addClass('disabled');
			$('.add_profile_element_wrapper').removeClass('force_display');
			$('.footer_filter').removeClass('force_display');
			$('.carousel_footer_wrapper').removeClass('force_display');
			$('.profile_element').removeClass('tutorial_blur');
			setTimeout(function() {
				$('#tutorial_symbols_popup').fadeIn(500);
			}, 2500);
		}
	});

	$('#tutorial_symbols_button').on('click', function() {
		$('#tutorial_symbols_popup').fadeOut(500);
		$('#tutorial_filter_popup').fadeIn(500);
	});

	$('#tutorial_filter_button').on('click', function() {
		$('#tutorial_filter_popup').fadeOut(500);
	});

	$(document).on('click', '#choose_element', function() {
		if (user_tutorial) {
			setTimeout(function() {
				$('#tutorial_edit_popup').fadeIn(500);
				// Mülltonnenmenü einfaden
				$('.profile_element_menue_bottom').addClass('force_display');
				$('#abort_element_mode').removeClass('disabled');
			}, 500);
		}
	});

	$('#tutorial_edit_button').on('click', function() {
		$('#tutorial_edit_popup').fadeOut();
		finishTutorial('tutorial');
		user_tutorial = false;
	});

});
