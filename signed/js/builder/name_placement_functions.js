/**
 * namePositionRelative - Macht die positioneirung des Namens im Header relativ damit er beim resize responiv bleibt
 *
 * @return {boolean}  true
 */
function namePositionRelative() {
	var top_margin = parseFloat($('.name_city_wrapper').css('top'));
	var bottom_margin = parseFloat($('.name_city_wrapper').css('bottom'));
	var left_margin = parseFloat($('.name_city_wrapper').css('left'));
	var name_height = parseFloat($('.name_city_wrapper').css('height'));
	var container_height = parseFloat($('.profile_element.header').outerHeight());
	var container_width = parseFloat($('.profile_element.header').outerWidth());
	var top_relative = (top_margin / container_height) * 100;
	var bottom_relative = (bottom_margin / container_height) * 100;
	var left_relative = (left_margin / container_width) * 100;
	$('.name_city_wrapper').css('top', top_relative + '%');
	//$('.name_city_wrapper').css('bottom', bottom_relative + '%');
	$('.name_city_wrapper').css('bottom', 'unset');
	$('.name_city_wrapper').css('left', left_relative + '%');
	return true;
}
$(window).on('resize', function() {
	namePositionRelative();
});

/**
 * resizeLogo - Ändert die größe des Logos im Header
 *
 * @param  {int} multiplicator Multiplikator nach dem Vergrößert wird
 * @return {boolean}      true
 */
function resizeLogo(multiplicator) {
	$('.name_city_wrapper').css({
		'max-width': 20 * multiplicator + '%',
		'max-height': 40 * multiplicator + '%',
		'min-width': 'unset'
	});
	$('input[name=logo_multiplicator]').val(multiplicator);
	checkPositionName();
	return true;
}

/**
 * bindLogoPlacement - fügt Logo in Header-Namen ein
 *
 * @param  {string} pic_name dateiname des Logos
 * @param  {string} user_id  acc_ID des users (für Pfad zu Datei relevant)
 * @param  {string} type mobile oder normal
 * @return {boolean}          true
 */
function bindLogoPlacement(pic_name, user_id, type) {
	if (type == 'mobile') {
		$('#mobile_name_wrapper>.logo').html('<img src="/signed/src/user/' + user_id + '/archive/' + pic_name + '">');
		$('#upload_mobile_logo').text('neu hochladen');
		setTimeout(function() {
			//resizeLogo(parseFloat(parseFloat($('input[name=logo_multiplicator]').val()).toFixed(1)));
			return true;
		}, 100);
	} else {
		$('.name_city_wrapper>.logo').html('<img src="/signed/src/user/' + user_id + '/archive/' + pic_name + '">');
		$('#upload_logo').text('neu hochladen');
		setTimeout(function() {
			resizeLogo(parseFloat(parseFloat($('input[name=logo_multiplicator]').val()).toFixed(1)));
			return true;
		}, 100);
	}
}

/**
 * checkPositionName - Checkt ob der Namen irgendwo übersteht und justiert falls wahr die position
 *
 * @return {boolean}  true
 */
function checkPositionName() {
	//Check ob rechts außerhalb
	if ($('.name_city_wrapper').position().left + $('.name_city_wrapper').outerWidth() > $('.placement_border').position().left + $('.placement_border').outerWidth() && $('.placement_border').outerWidth() > 10) {
		$('.name_city_wrapper').css('left', ($('.placement_border').outerWidth() - $('.name_city_wrapper').outerWidth()) * 0.9);
	}
	//Checken ob unten Raus!
	if ($('.name_city_wrapper').position().top + $('.name_city_wrapper').outerHeight() > $('.placement_border').position().top + $('.placement_border').outerHeight()) {
		$('.name_city_wrapper').css('top', $('.placement_border').position().top + $('.placement_border').outerHeight() - ($('.name_city_wrapper').outerHeight() + 10));
	}
	return true;
}

