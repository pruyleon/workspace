const user_id = $('input[name=user_id]').val();
const artist_id = $('input[name=artist_id]').val();
const artist_name = $('input[name=artist_name]').val();
const genre_string = $('input[name=genre_string]').val();
const artist_city_country = $('input[name=artist_city_country]').val();
const artist_country = $('input[name=artist_country]').val();
const artist_city = $('input[name=artist_city]').val();

const social_array_names = ['empty', 'Facebook', 'Youtube', 'Twitter', 'Instagram'];
const social_array_links = ['facebook.com', 'youtube.com', 'twitter.com', 'instagram.com'];
const social_array = ['empty', 'facebook', 'youtube', 'twitter', 'instagram'];

var element_mode = false;
profile_element_width = $('.profile_element').width();
const target = document.querySelector('#desktop');
const target2 = document.querySelector('.element_listing_filter');

const text_editor_toolbar_html = '<!-- Add a bold button --><button class="ql-bold"></button><!-- Add subscript and superscript buttons --><img src="/signed/src/icns/text_color.svg" class="cursor_pointer" alt="Farbe wählen" id="text_color"><button class="ql-align" value=""><button class="ql-align" value="center"><button class="ql-align" value="justify"><button type="button" class="ql-list" value="ordered" name="button"></button><button type="button" class="ql-list" value="bullet" name="button"></button>';
const headline_editor_toolbar_html = '<img src="/signed/src/icns/text_color.svg" class="cursor_pointer" alt="Farbe wählen" id="headline_color"><button class="ql-bold"></button><!-- <img src="/signed/src/icns/text_color.svg" class="cursor_pointer" alt="Farbe wählen" id="text_color"> --><button class="ql-align" value=""><button class="ql-align" value="center"><button class="ql-align" value="justify">';

edit_mode = true;

var colorpicker_swatches = ['rgb(244, 67, 54)', 'rgb(233, 30, 99)', 'rgb(156, 39, 176)', 'rgb(103, 58, 183)', 'rgb(63, 81, 181)', 'rgb(33, 150, 243)', 'rgb(3, 169, 244)', 'rgb(0, 188, 212)', 'rgb(0, 150, 136)', 'rgb(76, 175, 80)', 'rgb(139, 195, 74)', 'rgb(205, 220, 57)', 'rgb(255, 235, 59)', 'rgb(255, 193, 7)'];

var positions_names = ['one', 'two', 'three', 'four'];

var parent_id;
var media_type;
var clicked_icon;
var active_side = 'left';
var color_string = '_white';
var color;
var color_hex;
const element_add_plus = '<div class="add_profile_element_wrapper"><div class="add_profile_element_plus" title="Neues Element hinzufügen"><img src="/signed/src/icns/plus_edgy.svg" alt="+"></div></div>';
const element_add_plus_disabled = '<div class="add_profile_element_wrapper disabled"><div class="add_profile_element_plus" title="Neues Element hinzufügen"><img src="/signed/src/icns/plus_edgy.svg" alt="+"></div></div>';
const video_upload_html = '<img src="/signed/src/icns/video_upload.svg" class="upload_img" alt=""> Video hochladen';

const position_note_fixed = '<div class="drag_positition_note" id="drag_position_photo">Ziehen um zu positionieren</div>';

var i;
var value;
var type;
var data;
var icons;
var original;
var data_type;
var data_action;
var action;
var id;
var element;
var prev_element;
var clicked;
var add_element;

var input_parent;
var menue_parent;

var coverformat;
var titles;
var covers;
var wave_colors;
var file_names;
var color_switcher;
var position;
var active_audio;
var title;
var audio_name;
var action;
var xhr;
//Video
var video_type;
//Abgeschlossene Prozent des Uploads
var percentComplete;
var $croppie;
var croppie_position;
var croppie_slider;
//Cropper-Ergebnis
var result;
var pickr_background;
var audio_wave_wrapper_string;
var audio_dropdown_string;
var length;
var start;
var end;
var audio_title;
var file_name;
var wave_json;
var video_file_name;
var name;
var name_container;
var src;
var slideshow_element;
var menue;
var url_file;
var url;
var multiplicator;

var mobile_edit_icons = '<img src="/signed/src/icns/filter/photo.svg" class="" id="mobile_photo_edit_icn" alt="">';

var footer_filter;
var fullscreen = false;

var filterTimeout;
var elementModeTimeout;

//Mobile
const mobile_pic_menue = '<div class="action_wrapper" data-type="picture" ><span class="reposition_name_mobile">NAME</span><span class="element_dropdown"> EDIT </span></div><div class="dropdown"><div class="dropdown_line" data-action="position">Neu positionieren</div><div class="dropdown_line" data-action="upload">Neu hochladen</div><div class="dropdown_line" data-action="delete">Entfernen</div></div>';
const restored_mobile_menue = '<div class="action_wrapper" data-type="" ><span class="reposition_name_mobile">NAME</span></div>';
var mobile_drag_initialized = false;

//File-Archiv
var archive_path = '/signed/src/user/' + user_id + '/archive/';
//gehoverte Archive-File
var hovered_archive_file;
//Name der gehoverten Archive-File
var archive_file;

// Tutorial
var user_tutorial;
if ($('input[name=user_tutorial]').val() == 'true') {
	user_tutorial = true;
} else {
	user_tutorial = false;
}

var mobile_tutorial;
if ($('input[name=mobile_tutorial]').val() == 'true') {
	mobile_tutorial = true;
} else {
	mobile_tutorial = false;
}

var mobile_tutorial_step = 1;

var icons_new;
// Genre
var genre_ids = $('input[name=genre_ids]').val();
var new_genre_ids = '';
genre_ids = genre_ids.split(',');
var genre_stage = 'first';

//Timeouts
edit_menue_timeout = '';

//Speichern
//TODO: Ändern!
var presskitIsSaved = true;

var presskit_option;
const component_names_array = ['value', 'type', 'config', 'icons', 'originals', 'name_info', 'name_size', 'name_color', 'name_type', 'name_size', 'name_uppercase', 'name_centered', 'name_uppercase', 'name_shadow', 'name_font', 'social_links', 'social_selected', 'logo_file_name', 'logo_file_size', 'thumbnails', 'titles', 'wave_colors', 'file_names'];
var presskit_id = $('input[name=presskit_id]').val();
var presskit_name = $('input[name=presskit_name]').val();
var is_published = $('input[name=is_published]').val();
var created = $('input[name=created]').val();
var last_edited = $('input[name=last_edited]').val();

var free_save_slots;

var current_element;
var profile_element_count;

var component_id = 0;
var element_ID;
var value;
var config;
var type;
var icons;
var originals;
var name_info;
var name_size;
var social_links;
var social_selected;
var logo_file_name;
var logo_file_size;
var thumbnails;
var titles;
var covers;
var wave_colors;
var file_names;
