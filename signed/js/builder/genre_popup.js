$(function() {
	$(document).on('click', '.genre_listing:not(.disabled)', function() {
		//Genre-Popup reset!
		genre_stage = 'first';
		$('#selected_genres').text('');
		new_genre_ids = '';
		$('.genre_button').removeClass('selected');
		$('#main_genre_popup').fadeIn(500);
	});

	$('#extend_genre_listing').on('click', function() {
		// $('.genre_wrapper.additional_genres').stop();
		$('.genre_wrapper.additional_genres')
			.stop()
			.slideToggle(500);
		$('#extend_genre_listing').toggleClass('turned');
	});

	$(document).on('click', '.genre_button:not(.selected)', function() {
		genre_id = $(this).attr('data-id');
		$(this).addClass('selected');
		if (genre_stage == 'first') {
			$('#selected_genres').text($(this).text());
			genre_stage = 'second';
			new_genre_ids = genre_id;
		} else if (genre_stage == 'second') {
			$('#selected_genres').text($('#selected_genres').text() + ' | ' + $(this).text());
			genre_stage = 'third';
			new_genre_ids += ',' + genre_id;
		} else if (genre_stage == 'third') {
			genre_stage = 'final';
			new_genre_ids += ',' + genre_id;
			$('#selected_genres').text($('#selected_genres').text() + ' | ' + $(this).text());
			//Ajax Call
			$.ajax({
				url: '/signed/php/tools/save_main_genres.php',
				type: 'POST',
				data: {
					genres: new_genre_ids,
					artist_id: artist_id
				},
				success: function(response) {
					$('.genre_listing').removeClass('empty_genre');
				}
			});
			setTimeout(function() {
				$('#main_genre_popup').fadeOut(500);
				$('#genres_inner').text($('#selected_genres').text());
			}, 300);
		}
	});
});
