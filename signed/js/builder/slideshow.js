$(function() {
	//Slideshow Menü
	$('.slideshow_menue_inner').on('click', function() {
		$(this).fadeOut();
	});

	$('.slideshow_menue_btn').on('click', function() {
		$(this)
			.parent()
			.children('.slideshow_menue_inner')
			.fadeToggle();
	});

	$(document).on('click', '#abort_cropped', function() {
		$('.cropper_wrapper.slideshow').fadeOut();
		$('.croppie_slider_wrapper.slideshow').html('');
		$(
			'.slideshow_element[position=' +
				$(this)
					.closest('.croppie_btn_wrapper')
					.attr('croppie_position') +
				']'
		).croppie('destroy');
		$(
			'.slideshow_element[position=' +
				$(this)
					.closest('.croppie_btn_wrapper')
					.attr('croppie_position') +
				']'
		).removeClass('active_cropping');
		croppie_position = 0;
	});

	$('.slideshow_menue_point.delete_pic').on('click', function() {
		var position = $(this)
			.closest('.slideshow_element_wrapper')
			.children('.slideshow_input')
			.attr('data-position');
		$(this)
			.closest('.slideshow_element')
			.css('background-image', 'url(/signed/src/icns/add_picture.svg)')
			.addClass('empty');
		$('.slides.' + position).css('background-image', 'none');
	});

	var value = 1000;
	//Slideshow Slider Effektdauer
	$('.effect_time')
		.slider({
			value: 5,
			min: 2,
			max: 17,
			step: 3,
			slide: function(event, ui) {},
			classes: {
				'ui-slider': 'effect_time_slider',
				'ui-slider-handle': 'effect_time_slider_handle',
				'ui-slider-range': 'slider_range'
			},
			change: function(event, ui) {
				value = ui.value * 1000;
				if ($('.slideshow_effect_select').val() == 2) {
					value -= 1000;
				}
				$('.slideshow_demo').slick(
					'slickSetOption',
					{
						autoplaySpeed: value
					},
					true
				);
			}
		})
		.each(function() {
			var opt = $(this).data().uiSlider.options;
			var vals = opt.max - opt.min;
			for (var i = 0; i <= vals; i += 3) {
				var el = $('<label class="font_weight_heavy"><div class="vertical_slider_line"></div><span>' + (i + opt.min) + '</span><br><span>sec</span></label>').css('left', (i / vals) * 100 + '%');
				$('.effect_time').append(el);
			}
		});

	//Slideshow
	$('#slideshow_embed').on('click', function() {
		$('#slideshow_popup').fadeIn();
	});

	//Slideshow-Demo im Slideshow-Popup
	$('.slideshow_demo').slick({
		autoplay: true,
		autoplaySpeed: 5000,
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		speed: 1000,
		easing: 'linear',
		arrows: false
	});
	//Cached
	if ($('.slideshow_effect_select').val() == 1) {
		$('.slideshow_demo').slick(
			'slickSetOption',
			{
				speed: 0
			},
			true
		);
	} else if ($('.slideshow_effect_select').val() == 2) {
		$('.slideshow_demo').slick(
			'slickSetOption',
			{
				speed: 1000
			},
			true
		);
	}

	//Slideshow Übergangseffekt
	$('.slideshow_effect_select').on('change', function() {
		if ($(this).val() == 1) {
			$('.slideshow_demo').slick(
				'slickSetOption',
				{
					speed: 0,
					autoplaySpeed: value + 1000
				},
				true
			);
		} else if ($(this).val() == 2) {
			$('.slideshow_demo').slick(
				'slickSetOption',
				{
					speed: 1000,
					autoplaySpeed: value
				},
				true
			);
		}
	});

	$('#abort_slideshow').on('click', function() {
		$('#slideshow_popup').fadeOut();
	});
});
