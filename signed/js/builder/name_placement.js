$(function() {
	const name_pickr = new Pickr({
		el: '#name_colorpicker',
		default: '#42445A',
		inline: true,
		swatches: colorpicker_swatches,
		showAlways: true,
		components: {
			preview: false,
			opacity: false,
			hue: true,
			interaction: {
				hex: true,
				rgba: false,
				hsva: false,
				input: true,
				clear: false,
				save: false
			}
		}
	});

	//Namensplatzierung
	$(document).on('click', '.reposition_name', function() {
		clearEditMenue();
		timeout = profileElementEditMenueScroll($(this));
		$('.profile_element_content.h1').addClass('active_placement');
		$('.profile_element_content.h1>.placement_border').addClass('active_placement');
		$('.name_city_wrapper').addClass('active_placement');
		setTimeout(function() {
			$('.name_city_wrapper').draggable({
				containment: '.placement_border',
				cursor: 'move',
				start: function() {
					removeDragInfos();
				}
			});
			$('.name_city_wrapper').draggable('enable');
			if ($('.name_city_wrapper').position().top > $('.placement_border').position().top + $('.placement_border').outerHeight()) {
				$('.name_city_wrapper').css('top', $('.placement_border').position().top + $('.placement_border').outerHeight() - $('.name_city_wrapper').outerHeight() - 20);
			}
		}, timeout);
		setTimeout(function() {
			openEditMenue();
			$('#placement_menue').fadeIn();
			appendPositionNoteFixed($('.profile_element.header'), 'name_placement_drag_note');
		}, timeout + 500);
		//Dragposition einblenden und nach zwei Sekunden ausfaden
		// setTimeout(function() {
		//   removeDragInfos();
		// }, 2000);
	});

	$(document).on('click', '.name_city_wrapper:not(.active_placement, .disabled)', function() {
		$(this)
			.closest('.profile_element')
			.find('.reposition_name')
			.trigger('click');
	});

	$('select[name=name_selector]').change(function() {
		val = $(this).val();
		console.log(val);
		if (val == 0) {
			$('.name_city_wrapper').fadeOut();
			$('.inner_edit.name').fadeOut();
			$('.inner_edit.logo').fadeOut();
		} else {
			$('.name_city_wrapper').fadeIn();
			if (val == 2) {
				$('.name_city_wrapper>.name').fadeOut(function() {
					resizeLogo(parseFloat(parseFloat($('input[name=logo_multiplicator]').val()).toFixed(1)));
					$('.name_city_wrapper>.logo').fadeIn();
				});
				$('.inner_edit.name').fadeOut(function() {
					$('.inner_edit.logo').fadeIn();
				});
			} else {
				$('.name_city_wrapper>.logo').fadeOut(function() {
					$('.name_city_wrapper').css({
						'max-width': 'unset',
						'min-width': 'max-content'
					});
					$('.name_city_wrapper>.name').fadeIn();
					if (val == 1) {
						$('.city_wrapper').html('');
					} else if (val == 3) {
						$('.city_wrapper').html(artist_city);
					} else if (val == 4) {
						$('.city_wrapper').html(artist_country);
					} else if (val == 5) {
						$('.city_wrapper').html(artist_city + ' | ' + artist_country);
					}
					if (val == 3 || val == 4 || val == 5) {
						$('.name_city_wrapper>.name>.city_wrapper').fadeIn(100);
						$('#name_center').css('visibility', 'unset');
					} else {
						$('.name_city_wrapper>.name>.city_wrapper').fadeOut(100);
						$('#name_center').css('visibility', 'hidden');
					}
				});
				$('.inner_edit.logo').fadeOut(function() {
					$('.inner_edit.name').fadeIn();
				});
			}
		}
		$(this)
			.closest('.profile_element')
			.find('input[name=name_type')
			.val(val);
		checkPositionName();
	});
	$('select[name=name_selector]').val('5');
	$('select[name=name_selector]').trigger('change');

	$('#name_uppercase').on('click', function() {
		console.log($('.name_wrapper').text());
		if ($(this).attr('data-state') == 'uppercase') {
			$(this).attr('data-state', 'lowercase');
			$('.name_wrapper').text(artist_name);
			$(this)
				.closest('.profile_element')
				.find('input[name=name_uppercase')
				.val(1);
		} else {
			$(this).attr('data-state', 'uppercase');
			$(this)
				.closest('.profile_element')
				.find('input[name=name_uppercase')
				.val(0);
			$('.name_wrapper').text(artist_name.toUpperCase());
		}
	});

	$('#name_center').on('click', function() {
		if ($(this).attr('data-state') == 'left') {
			$('#name_city_wrapper').addClass('text_align_center');
			$(this).addClass('active');
			$(this).attr('data-state', 'center');
			$(this)
				.closest('.profile_element')
				.find('input[name=name_centered')
				.val(1);
		} else {
			$('#name_city_wrapper').removeClass('text_align_center');
			$(this)
				.closest('.profile_element')
				.find('input[name=name_centered')
				.val(0);
			$(this).removeClass('active');
			$(this).attr('data-state', 'left');
		}
	});

	$('#make_name_shadow').on('click', function() {
		if ($(this).attr('data-state') == 'NULL') {
			$(this).attr('data-state', 'shadow');
			$('.name_city_wrapper>.name').addClass('text_shadow');
			$(this)
				.closest('.profile_element')
				.find('input[name=name_shadow')
				.val(1);
		} else {
			$(this).attr('data-state', 'NULL');
			$(this)
				.closest('.profile_element')
				.find('input[name=name_shadow')
				.val(0);
			$('.name_city_wrapper>.name').removeClass('text_shadow');
		}
	});

	$('#upload_logo').on('click', function() {
		edit_trigger = 'logo_placement';
		$('#file_archive_popup').fadeIn();
	});

	$('#descrease_logo_size').on('click', function() {
		multiplicator = parseFloat(parseFloat($('input[name=logo_multiplicator]').val()).toFixed(1));
		if (multiplicator < 1) {
			multiplicator -= 0.1;
		} else {
			multiplicator -= 0.3;
		}
		if (multiplicator < 0.2) {
			multiplicator = 0.2;
		}
		resizeLogo(multiplicator);
	});

	$('#inscrease_logo_size').on('click', function() {
		multiplicator = parseFloat(parseFloat($('input[name=logo_multiplicator]').val()).toFixed(1));
		if (multiplicator < 0.2) {
			multiplicator = 0.2;
		}
		if (multiplicator < 1) {
			multiplicator += 0.1;
		} else if (multiplicator <= 3) {
			multiplicator += 0.3;
		}
		if (multiplicator > 3) {
			multiplicator = 3;
		}
		resizeLogo(multiplicator);
	});

	$('input[name=namesize]').val('49');
	$('.name_resize_button').on('click', function() {
		resize_action = $(this).attr('data-action');
		name_size = parseInt($('input[name=namesize]').val());
		if (resize_action == 'increase') {
			name_size += 4;
		} else {
			name_size -= 4;
		}
		if (name_size > 85) {
			name_size = 85;
		} else if (name_size < 17) {
			name_size = 17;
		}
		$('.name_city_wrapper').attr('data-font-size', name_size);
		$('input[name=namesize]').val(name_size);
		checkPositionName();
	});

	//Colorswitch Name
	$('#name_color').on('click', function() {
		if (
			$('#placement_menue')
				.find('.picker')
				.attr('visible') == 'true'
		) {
			$('#placement_menue')
				.find('.picker')
				.removeClass('visible');
			$('#placement_menue')
				.find('.picker')
				.attr('visible', 'false');
		} else {
			$('#placement_menue')
				.find('.picker')
				.addClass('visible');
			$('#placement_menue')
				.find('.picker')
				.attr('visible', 'true');
		}
	});

	name_pickr.on('change', function() {
		$('.name_city_wrapper').css(
			'color',
			$('#placement_menue')
				.find('.pcr-result')
				.val()
		);
		$(this)
			.closest('.profile_element')
			.find('input[name=name_shadow')
			.val(
				$('#placement_menue')
					.find('.pcr-result')
					.val()
			);
	});

	//Font-Type
	$('select[name=name_font_type]').change(function() {
		$('.name_city_wrapper>.name').css('font-family', '"' + $(this).val() + '"');
		$(this)
			.closest('.profile_element')
			.find('input[name=name_font')
			.val($(this).val());
	});
	$('select[name=name_font_type]').val('Arial');

	$('#save_name').on('click', function() {
		$('.profile_element_content.h1').removeClass('active_placement');
		$('.profile_element_content.h1>.placement_border').removeClass('active_placement');
		$('.name_city_wrapper').removeClass('active_placement');
		$('#placement_menue').fadeOut();
		$('.name_city_wrapper').draggable('disable');
		//$('.name_city_wrapper').removeClass('ui-draggable ui-draggable-handle ui-draggable-disabled');
		$('.drag_positition_note').fadeOut(500, function() {
			$(this).remove();
		});
		//Positionierung relativ machen
		namePositionRelative();
		clearEditMenue();
		closeEditMenue();
	});
});
