// let para = document.querySelector('body * > .textinput_textarea');
// let compStyles = window.getComputedStyle(para);
//$('.value_display').html(compStyles.getPropertyValue('font-size') + '<br>' + compStyles.getPropertyValue('line-height') + '<br>' + parseFloat(compStyles.getPropertyValue('font-size')) / 12 + '<br>' + $('.textinput_textarea').width() / parseFloat(compStyles.getPropertyValue('font-size')));

//$('.value_display').html("Verhältnis: " + parseFloat(compStyles.getPropertyValue('font-size')) / $('.builder_content').width());
$(function() {
	$('#show_preview').on('click', function() {
		$('#fullscreen').addClass('hidden');
		makeFullScreen();
		$('#desktop * > .footer_wrapper').slideDown(500);
		//Preview-Beenden Button einblenden
		setTimeout(function() {
			$('#abort_preview').fadeIn(500);
		}, 1500);
		$('#abort_preview').removeClass('disabled');
		disableEdit();
	});

	$('#abort_preview').on('click', function() {
		exitFullScreen();
		$('#desktop * > .footer_wrapper').slideUp(500);
		//Preview-Beenden Button ausblenden
		$('#abort_preview').fadeOut(500);
		setTimeout(function() {
			enableEdit();
		}, 300);

		setTimeout(function() {
			$('#fullscreen').removeClass('hidden');
		}, 1000);
	});
});
