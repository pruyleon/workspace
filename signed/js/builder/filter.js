$(function() {
	//Alle Inputs "uncheck" -> Cache
	$('.filter_line>input').each(function() {
		$(this).prop('checked', false);
	});
	filterPreview();

	$(document).on('click', function() {
		//Reset Filter

		//Fadeout filter Popup
		$('.options_wrapper').fadeOut();
		$('#toggle_filter_options>span>img').removeClass('turned');
	});

	$('#close_filter').on('click', function() {
		$('.options_wrapper').fadeOut();
		$('#toggle_filter_options>span>img').removeClass('turned');
	});

	$(document).on('click', '.switch_button.size:not(.active)', function() {
		$('.switch_button.size.active').removeClass('active');
		$(this).addClass('active');
		filterPreview();
	});

	$('#toggle_filter_options').on('click', function(e) {
		//Upbubbleing verhindern
		e.stopPropagation();
		$('#toggle_filter_options>span>img').toggleClass('turned');
		$('.options_wrapper').fadeToggle();
	});

	$('.options_wrapper').on('click', function(e) {
		e.stopPropagation();
	});

	$('.filter_button').on('click', function() {
		$(this).toggleClass('selectedFilter');
		filter_input = $(this)
			.parent()
			.children('input')
			.get(0);
		// $(filter_input).attr("checked", !$(filter_input).attr("checked"));
		$(this)
			.parent()
			.children('input')
			.trigger('click');
	});

	$('.filter_line>input').on('change', function() {
		setTimeout(function() {
			$('#close_filter').trigger('click');
		}, 100);
		filterPreview();
	});

	$('input[name=filter]').each(function() {
		$(this).prop('checked', false);
	});

	$('#delete_filters').on('click', function() {
		$('.filter_button').removeClass('selectedFilter');
		$('input[name=filter]').each(function() {
			$(this).prop('checked', false);
		});
		filterPreview();
	});

	$('#apply_filters').on('click', function() {
		$('.options_wrapper').fadeOut();
		$('#toggle_filter_options>span>img').removeClass('turned');
	});

	function filterPreview() {
		$('.profile_element_preview').addClass('filter');
		var elementSize = $('.switch_button.size.active').attr('data-size');
		var filterArray = new Array();
		$('.filter_line>input').each(function() {
			if ($(this).prop('checked')) {
				filterArray.push($(this).val());
			}
		});
		var filterCount = filterArray.length;
		var filterCheck;
		var elementFilters;
		if (filterCount > 0) {
			$('#delete_filters').addClass('active');
			$('#toggle_filter_options').addClass('active');
			$('#element_filter_count').html('&emsp;(' + filterCount + ')');
			$('.profile_element_preview').each(function() {
				elementFilters = $(this).attr('data-filter');
				filterCheck = true;
				if ($(this).attr('data-format') != elementSize) {
					$(this).slideUp();
				} else {
					for (var i = 0; i < filterCount; i++) {
						if (!elementFilters.includes(filterArray[i])) {
							filterCheck = false;
						}
					}
					if (filterCheck) {
						$(this).slideDown(300);
					} else {
						$(this).slideUp(300);
					}
				}
			});
		} else {
			$('#delete_filters').removeClass('active');
			$('#toggle_filter_options').removeClass('active');
			$('#element_filter_count').html(''); //<img src="/signed/src/icns/dropdown_white.svg">
			$('.profile_element_preview').each(function() {
				if ($(this).attr('data-format') != elementSize) {
					$(this).slideUp();
				} else {
					$(this).slideDown();
				}
			});
		}
		setTimeout(function() {
			$('.profile_element_preview').removeClass('filter');
		}, 350);
	}
});
