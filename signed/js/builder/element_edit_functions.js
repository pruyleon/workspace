/**
 * getBase64 - macht aus einer Datei eine base64
 *
 * @param  {file} file Datei die zu base64 konvertiert werden soll
 * @return {type}      base64 string
 */
function getBase64(file) {
	var reader = new FileReader();
	reader.readAsDataURL(file);
	reader.onload = function() {};
	reader.onerror = function(error) {
		alert('Error: ', error);
	};
}

/**
 * profileElementMidScroll - Scrollt das Eltern-Profilelement in die Mitte
 *
 * @param  {Element} clicked Beliebiges Element in Profilelement
 * @return {int}         Scrollverzögerung in ms
 */
function profileElementMidScroll(clicked) {
	scrollAmount = 0;
	window_position = $(window).scrollTop();
	window_height = $(window).height();
	el_top_pos = $(clicked)
		.closest('.profile_element')
		.position().top;
	el_height = $(clicked)
		.closest('.profile_element')
		.height();
	parent_top = $('#desktop').position().top;
	el_top_pos *= -1;
	el_offset = el_top_pos + (window_height - el_height) / 2;
	if (el_offset < 0) {
		el_offset *= -1;
		scrollAmount = '+=' + el_offset + 'px';
	} else {
		scrollAmount = '-=' + el_offset + 'px';
	}
	if (el_offset < 50) {
		$('#desktop').animate(
			{
				scrollTop: scrollAmount,
				complete: function() {
					return 100;
				}
			},
			100
		);
		return 100;
	} else {
		$('#desktop').animate(
			{
				scrollTop: scrollAmount,
				complete: function() {
					return 800;
				}
			},
			800
		);
	}
}

/**
 * profileElementEditMenueScroll - Scrollt das Eltern-Profilelement in die Mitte
 *
 * @param  {Element} clicked Beliebiges Element in Profilelement
 * @return {Integer}         Verzögerung des Fadens
 */
function profileElementEditMenueScroll(clicked) {
	window_position = $(window).scrollTop();
	window_height = $(window).height();
	el_top_pos = $(clicked)
		.closest('.profile_element')
		.position().top;
	el_height = $(clicked)
		.closest('.profile_element')
		.height();
	element_size = $(clicked)
		.closest('.profile_element')
		.attr('data-id')
		.slice(-1);
	parent_top = $('#desktop').position().top;
	el_top_pos *= -1;
	if (fullscreen) {
		if (element_size == 'M' || element_size == 'S') {
			el_offset = el_top_pos + (window_height * 0.85 - el_height); //el_top_pos + (((window_height * 0.85 - el_height) / 2)) ;//- (window_height * 0.15));
		} else {
			el_offset = el_top_pos; // + ((el_height - window_height));
		}
	} else {
		el_offset = el_top_pos + (window_height * 0.85 - el_height);
	}
	if (el_offset < 0) {
		el_offset *= -1;
		scrollAmount = '+=' + el_offset + 'px';
	} else {
		scrollAmount = '-=' + el_offset + 'px';
	}
	if (
		$(clicked)
			.closest('.profile_element')
			.hasClass('header')
	) {
		el_offset = el_top_pos + (window_height - el_height * 1);
		scrollAmount = '-=' + el_offset + 'px';
	}
	if (el_offset < 50 && el_offset > -50) {
		$('#desktop').animate(
			{
				scrollTop: scrollAmount,
				complete: function() {
					// return 1000;
				}
			},
			100,
			'swing'
		);
		return 0;
	}
	$('#desktop').animate(
		{
			scrollTop: scrollAmount
		},
		800,
		'swing',
		function() {
			// return 1000;
		}
	);
	return 800;
}

/**
 * clearEditMenue - Fadet alle edit-menüs im Builder raus (unterer Bearbeitungsbalken)
 *
 * @return {boolean}  true
 */
function clearEditMenue() {
	$('.edit_menue').fadeOut(0);
	return true;
}

/**
 * openEditMenue - Fadet den unteren Bearbeitungsbalken rein und startet den Editiermodus
 *
 * @return {boolean}  true
 */
function openEditMenue() {
	disableEdit();
	$('.edit_menue_wrapper').fadeIn(500);
	return true;
}

/**
 * closeEditMenue - Schließt den unteren Bearbeitungsbalken und beendet den Editiermodus
 *
 * @return {boolean}  true
 */
function closeEditMenue() {
	$('.edit_menue_wrapper').fadeOut();
	enableEdit();
	return true;
}

/**
 * saveCropped - Speichert das gecroppte in base64-Format im nächsten value-Input und gibt dem Cropperdiv das ergebnis als hintergrund
 *
 * @param  {element} cropper_div  div in dem sich der Cropper befindet
 * @param  {element} clicked_icon das geklickte Icon (builder-icon oder Menü-Punkt)
 * @param  {boolean} save         Ob die Icons gespeichert wurden oder nicht
 * @return {boolean}              true
 */
function saveCropped(cropper_div, clicked_icon, save) {
	var result = $(cropper_div)
		.croppie('result', {
			type: 'base64',
			size: 'original',
			format: 'png'
		})
		.then(function(dataImg) {
			$('.croppie-container').croppie('destroy');
			$(cropper_div).css('background-image', 'url(' + dataImg + ')');
			$(cropper_div)
				.closest('.input_parent')
				.children('input[name=value]')
				.val(dataImg);
			$(cropper_div)
				.closest('.input_parent')
				.children('input[name=type]')
				.val('picture');
			$(cropper_div).html('');
			$(cropper_div)
				.closest('.input_parent')
				.children('input[name=originals]')
				.val(recently_uploaded);
			if (save) {
				makeMenue(cropper_div, 'picture');
				removeIcons(clicked_icon);
			}
			closeEditMenue();
			if (!user_tutorial) {
				enableEdit();
			}
			$('#loadingscreen').fadeOut(500);
			return true;
		});
}

function loaded() {
	console.log('loaded');
}

/**
 * destroyCropped - Zerstört aktiven Cropper
 *
 * @param  {Element} cropper_div div in dem sich der zu zerstörende Cropper befindet
 * @return {boolean}             true;
 */
function destroyCropped(cropper_div) {
	$(cropper_div).croppie('destroy');
	return true;
}

/**
 * newSlideshowCropper - Mach einen neuen Cropper im Slideshow-Popup
 *
 * @param  {Element} input   das geklickte File-Input im Popup (beinhaltet alle wichtigen Informationen als attribute)
 * @param  {String} user_id acc_ID des Users
 * @return {boolean}         true
 */
function newSlideshowCropper(input, user_id) {
	croppie_position = $(input).attr('position');
	url_file =
		'/signed/src/user/' +
		user_id +
		'/archive/' +
		$('.slideshow_element[position=' + croppie_position + ']')
			.parent()
			.children('.slideshow_input')
			.val();
	$('.slideshow_inner_wrapper').sortable('disable');
	var $croppie = $('.slideshow_element[position=' + croppie_position + ']').croppie({
		url: url_file,
		viewport: {
			width: '100%',
			height: '100%'
		},
		showZoomer: true
	});
	$('.slideshow_element[position=' + croppie_position + ']').addClass('active_cropping');
	$('.slideshow_element[position=' + croppie_position + ']').removeClass('empty');
	var croppie_slider = $('.croppie-container').children('.cr-slider-wrap');
	$(croppie_slider).appendTo('.croppie_slider_wrapper.slideshow');
	$('.cropper_wrapper.slideshow').fadeIn();
	$('.croppie_slider_wrapper.slideshow').prepend('<div class="cropper_desc">Foto positionieren: </div>');
	croppie_slider = $('.croppie_slider_wrapper.slideshow').find('.cr-slider-wrap');
	$(croppie_slider).prepend('<img src="/signed/src/icns/minus_gray.png">');
	$(croppie_slider).append('<img src="/signed/src/icns/plus_gray.png">');
	$('.croppie_slider_wrapper.slideshow').append("<div class='croppie_btn_wrapper' croppie_position='" + croppie_position + "'><button class='gray_btn menue_button' id='abort_cropped'>abbrechen</button><button class='blue_btn menue_button' id='save_cropped_slideshow'>Übernehmen</button></div>");
	appendPositionNoteFixed($('.slideshow_element[position=' + croppie_position + ']'), '');
	//Ladescreen rausfaden
	$('#loadingscreen').fadeOut(500);
	return true;
}

/**
 * makeSlideshowSortable - Macht die Slideshow-Wrapper im Slideshow-Popup sortierbar und Deklariert die Updatefunktion beim Sortieren
 *
 * @return {boolean}  true
 */
function makeSlideshowSortable() {
	$('.slideshow_inner_wrapper').sortable({
		update: function() {
			position = 0;
			$('.slideshow_element_wrapper').each(function() {
				$(this)
					.children('.slideshow_desc')
					.text(position + 1);
				$(this)
					.children('.slideshow_input')
					.attr('data-position', positions_names[position]);
				position++;
			});
		}
	});
	return true;
}
makeSlideshowSortable();

/**
 * clearSlideshow - Leert das Slideshow-Popup bzw füllt dieses mit den Daten des geklickten Elements (slideshow_div)
 *
 * @param  {Element} slideshow_div das geklickte div
 * @return {boolean}               true
 */
function clearSlideshow(slideshow_div) {
	if (slideshow_div == 'NULL') {
		data = [];
		originals = [];
	} else {
		if (
			!$(slideshow_div)
				.closest('.input_parent')
				.children('input[name=value]').length
		) {
			slideshow_input_wrapper = $(slideshow_div)
				.parent()
				.closest('.input_parent');
		} else {
			slideshow_input_wrapper = $(slideshow_div).closest('.input_parent');
		}
		var values = $(slideshow_div)
			.closest('.input_parent')
			.children('input[name=value]')
			.val();
		var originals = $(slideshow_div)
			.closest('.input_parent')
			.children('input[name=originals]')
			.val();
		originals = originals.split('|');
		data = values.split('|');
	}
	i = 0;
	//Datenübernahme
	$('.slideshow_element_wrapper').each(function() {
		if (data[i]) {
			if (data.length > i && data[i].length >= 1) {
				$(this)
					.children('input[name=picture_input]')
					.val(original[i]);
				$(this)
					.children('input[name=picture_input_base64]')
					.val(data[i]);
				$(this)
					.children('.slideshow_element')
					.css('background-image', 'url(' + data[i] + ')');
				$(this)
					.children('.slideshow_element')
					.removeClass('empty');
			} else {
				$(this)
					.children('input[name=picture_input]')
					.val('');
				$(this)
					.children('input[name=picture_input_base64]')
					.val('');
				$(this)
					.children('.slideshow_element')
					.addClass('empty');
				$(this)
					.children('.slideshow_element')
					.attr('style', '');
			}
		} else {
			$(this)
				.children('input[name=picture_input]')
				.val('');
			$(this)
				.children('input[name=picture_input_base64]')
				.val('');
			$(this)
				.children('.slideshow_element')
				.addClass('empty');
			$(this)
				.children('.slideshow_element')
				.attr('style', '');
		}
		i++;
	});
	return true;
}

/**
 * removeDragInfos - Entfernt alle "Ziehen um zu positionieren"-Nachrichten
 *
 * @return {boolean}  true
 */
function removeDragInfos() {
	$('.drag_positition_note').fadeOut(500);
	setTimeout(function() {
		$('.drag_positition_note').remove();
		return true;
	}, 1000);
}

/**
 * makeMenue - Erzeugt Menü für das Div, je nach Typ der Medien
 *
 * @param  {Element} div  Das element für welches ein Menü erstellt werden muss
 * @param  {String} type Der Type der Medien im Element, bestimmt welches Menü erzeugt wird
 * @return {boolean}      true
 */
function makeMenue(div, type) {
	var menue_parent = $(div).closest('.menue_parent');
	var menue = $(menue_parent).children('.element_menue');

	if ($(menue).hasClass('header')) {
		$(menue).html('<div class="action_wrapper" data-type="' + type + '" data-specs="header"><span class="reposition_name">NAME</span><span class="element_dropdown"> EDIT </span></div><div class="dropdown"></div>');
	} else {
		$(menue).html('<div class="action_wrapper" data-type="' + type + '"><span class="element_dropdown"> EDIT </span></div><div class="dropdown"></div>');
	}
	if (type == 'picture') {
		$(menue)
			.children('.dropdown')
			.html('<div class="dropdown_line" data-action="position">Neu positionieren</div><div class="dropdown_line" data-action="upload">Neu hochladen</div><div class="dropdown_line" data-action="delete">Entfernen</div>');
	} else {
		$(menue)
			.children('.dropdown')
			.html('<div class="dropdown_line" data-action="edit">Bearbeiten</div><div class="dropdown_line" data-action="delete">Entfernen</div>');
	}

	return true;
}

/**
 * restoreHeaderMenue - Erneuert das Menü des Headers
 *
 * @return {boolean}  true
 */
function restoreHeaderMenue() {
	$('.element_menue.header').html('<div class="action_wrapper" data-type="' + type + '" data-specs="header"><span class="reposition_name">NAME</span></div>'); //<span class="element_dropdown"> EDIT </span>
	return true;
}

/**
 * saveIcons - Speichert die Builder-Icons inkl. Wrapper im Icons-Input des Komponenten
 *
 * @param  {Element} clicked das geklickte Element (Menü oder Builder-Icon)
 * @return {boolean}         true wenn etwas zu speichern, false wenn nicht
 */
function saveIcons(clicked) {
	if ($(clicked).hasClass('dropdown_line')) {
		return false;
	}
	if ($(clicked).closest('.media_icns_wrapper').length) {
		$(clicked)
			.closest('.input_parent')
			.children('input[name=icons]')
			.val(
				$(clicked)
					.closest('.media_icns_wrapper')
					.outerHTML()
			);
	} else if ($(clicked).closest('.edit_icns_wrapper').length) {
		$(clicked)
			.closest('.input_parent')
			.children('input[name=icons]')
			.val(
				$(clicked)
					.closest('.edit_icns_wrapper')
					.outerHTML()
			);
	} else if ($(clicked).closest('.edit_icon_wrapper').length) {
		$(clicked)
			.closest('.input_parent')
			.children('input[name=icons]')
			.val(
				$(clicked)
					.closest('.edit_icon_wrapper')
					.outerHTML()
			);
	} else if ($(clicked).closest('.builder_icons_wrapper').length) {
		$(clicked)
			.closest('.input_parent')
			.children('input[name=icons]')
			.val(
				$(clicked)
					.closest('.builder_icons_wrapper')
					.outerHTML()
			);
	} else {
		console.error("Icons could not be saved in Input!\nNo Parent found!");
		return false;
	}
	return true;
}

/**
 * removeIcons - Entfernt den Wrapper des Builder-Icons inkl. Inhalt
 *
 * @param  {Element} clicked Das geklickte Element (builder_icon oder dropdown_line)
 * @return {boolean}         true wenn wrapper entfernt, false wenn dropdown_line geklickt
 */
function removeIcons(clicked) {
	if ($(clicked).hasClass('dropdown_line')) {
		return false;
	} else {
		$(clicked)
			.parent()
			.remove();
		return true;
	}
}

/**
 * restoreIcons - stellt die zuvor gelöschten Icons des Elements wieder her
 *
 * @param  {Element} div div in dem die Icons wiederhergestellt werden sollen
 * @return {boolean}     true wenn erfolgreich wiederhergestellt
 */
function restoreIcons(div) {
	if (
		$(div)
			.closest('.menue_parent')
			.children('.element_menue')
			.find('.reposition_name').length &&
		$(div)
			.closest('.menue_parent')
			.children('.element_menue')
			.find('.switch_color').length
	) {
		var color_switcher = $(div)
			.closest('.menue_parent')
			.children('.element_menue')
			.find('.switch_color')
			.outerHTML();
		var name_pos = $(div)
			.closest('.menue_parent')
			.children('.element_menue')
			.find('.reposition_name')
			.outerHTML();
		$(div)
			.closest('.menue_parent')
			.children('.element_menue')
			.html(name_pos + color_switcher);
	} else if (
		$(div)
			.closest('.menue_parent')
			.children('.element_menue')
			.find('.reposition_name').length
	) {
		var name_pos = $(div)
			.closest('.menue_parent')
			.children('.element_menue')
			.find('.switch_color')
			.outerHTML();
		$(div)
			.closest('.menue_parent')
			.children('.element_menue')
			.html(name_pos);
	} else if (
		$(div)
			.closest('.menue_parent')
			.children('.element_menue')
			.find('.switch_color').length
	) {
		var color_switcher = $(div)
			.closest('.menue_parent')
			.children('.element_menue')
			.find('.switch_color')
			.outerHTML();
		$(div)
			.closest('.menue_parent')
			.children('.element_menue')
			.html(color_switcher);
	} else {
		$(div)
			.closest('.menue_parent')
			.children('.element_menue')
			.html('');
		var color_switcher = false;
	}

	icons = $(div)
		.closest('.input_parent')
		.children('input[name=icons]')
		.val();
	$(div).html(icons);
	$(div)
		.closest('.input_parent')
		.find('.builder_icon')
		.each(function() {
			$(this).css('visibility', 'unset');
		});
	if (color_switcher) {
		$(div)
			.closest('.menue_parent')
			.children('.element_menue')
			.html('<div class="action_wrapper" data-type"">' + color_switcher + '</div>');
	}
	return true;
}

/**
 * removeBackground - Entfernt den Hintergrund des Elements
 *
 * @param  {Element} div Element, von dem Hintergrund entfernt werden soll
 * @return {boolean}     true
 */
function removeBackground(div) {
	$(div).css('background-image', 'none');
	return true;
}

/**
 * disableEditWithTime - Schaltet den Bearbeitungsmodus ein, alle Inputs und Co werden deaktiviert, Element-Menüs und Builder-Icons versteckt. Jegliche Interaktionsmöglichkeit außerhalb des gewollten wird deaktiviert
 *
 * @param  {type} effect_time Zeit des Rausfadens
 * @return {type}             description
 */
function disableEditWithTime(effect_time) {
	//TExtareas und Co
	$('.textinput_textarea').unbind('click');
	$('.headline_input').unbind('click');
	$('.textinput_textarea').addClass('disabled');
	$('.headline_input').addClass('disabled');
	$('.name_city_wrapper').addClass('disabled');
	$('.audio_element_wrapper.empty').addClass('temp_empty');
	$('.audio_element_wrapper.empty').removeClass('empty');
	//Blaue Hoverrahmen
	$('.hover_blue_inlineshadow').each(function() {
		$(this).addClass('hover_blue_inlineshadow_disabled');
		$(this).removeClass('hover_blue_inlineshadow');
	});
	//Menüs
	$('.element_menue').each(function() {
		$(this)
			.stop()
			.fadeOut(effect_time);
		$(this).addClass('disabled');
	});
	$('.edit_disable_fadeout')
		.stop()
		.fadeOut(effect_time);
	$('.builder_icon').css('visibility', 'hidden');

	//Plus-Icons ausblenden
	$('.add_profile_element_wrapper').addClass('disabled');

	//Presspack und Request Button + Tooltipp
	$('.presspack_btn').unbind('click');
	$('.request_button').unbind('click');
	$('.presspack_btn').addClass('disabled');
	$('.request_button').addClass('disabled');
	$('.presspack_tooltip')
		.stop()
		.fadeOut(effect_time);
	$('.request_tooltip')
		.stop()
		.fadeOut(effect_time);

	//Genre-Popup + Tooltipp
	$('.genre_listing').unbind('click');
	$('.genre_listing').addClass('disabled');

	//Social Media
	$('.social_media_trigger').unbind('click');
	$('.social_media_trigger').addClass('disabled');

	//Löschen verschieben
	$('.profile_element_menue_bottom')
		.stop()
		.fadeOut(effect_time);

	//Hover bei Buttons deaktivieren
	$('.builder_content * > button').addClass('disabled');
	//$('.edit_menue').find('button').addClass('disabled');
	$('.builder_content * > input').addClass('disabled');
	$('.builder_content * > input').prop('disabled', true);
	$('.builder_content * > a').addClass('disabled');
	// $('input[name=filter]').prop('disabled', false);
	// $('.pcr-result').removeClass('disabled');
	// $('.pcr-result').prop('disabled', false);
	$('.carousel_artist').addClass('disabled');
	$('.dj_carousel_listing').addClass('disabled');
	$('.carousel_footer_wrapper').addClass('disabled');
	$('.profile_element_menue_bottom').css('visibility', 'hidden');

	// $('#archive_input').removeClass('disabled');
	// $('.filter_button').removeClass('disabled');
	// $("#archive_input").prop('disabled', false);
	// $('input[name=user_id]').removeClass('disabled');
	// $("input[name=user_id]").prop('disabled', false);
	// $('#choose_element').removeClass('disabled');
	// $('#abort_element_mode').removeClass('disabled');

	//Language Dropdown
	$('.language_trigger').addClass('disabled');

	//Tooltips
	$('.fixed_tooltip').addClass('hidden');

	$('.edit_menue_inner * > button').removeClass('disabled');
	$('.edit_menue_inner * > input').removeClass('disabled');
	$('.edit_menue_inner * > input').prop('disabled', true);
	$('#mobile_edit_menue * > button').removeClass('disabled');
	$('#mobile_edit_menue * > input').removeClass('disabled');
	$('#mobile_edit_menue * > input').prop('disabled', true);

	edit_mode = false;
	return true;
}

/**
 * disableEdit - Standartzeit für DisableEdit
 *
 * @return {type}  description
 */
function disableEdit() {
	disableEditWithTime(500);
}

/**
 * enableEdit - Schaltet den Bearbeitungsmodus ein, Inputs werden reaktiviert, Interaktionsmöglichkeiten werden wieder freigeschalten
 *
 * @return {type}  description
 */
function enableEdit() {
	//Textareas und Co
	$('.textinput_textarea').bind('click');
	$('.headline_input').bind('click');
	$('.textinput_textarea').removeClass('disabled');
	$('.headline_input').removeClass('disabled');
	$('.name_city_wrapper').removeClass('disabled');
	$('.audio_element_wrapper.temp_empty').addClass('empty');
	$('.audio_element_wrapper.temp_empty').removeClass('temp_empty');

	//Blaue Hoverrahmen
	$('.hover_blue_inlineshadow_disabled').each(function() {
		$(this).removeClass('hover_blue_inlineshadow_disabled');
		$(this).addClass('hover_blue_inlineshadow');
	});
	//Menüs
	$('.element_menue').each(function() {
		$(this).removeClass('disabled');
	});
	$('.edit_disable_fadeout').fadeIn();
	$('.builder_icon').css('visibility', 'visible');

	//Plus-Icons ausblenden
	$('.add_profile_element_wrapper').removeClass('disabled');

	//Presspack und Request Button + Tooltipp
	$('.presspack_btn').bind('click');
	$('.request_button').bind('click');
	$('.presspack_btn').removeClass('disabled');
	$('.request_button').removeClass('disabled');
	$('.presspack_tooltip').fadeIn();
	$('.request_tooltip').fadeIn();
	//Social Media
	$('.social_media_trigger').bind('click');
	$('.social_media_trigger').removeClass('disabled');

	//Genre-Popup + Tooltipp
	$('.genre_listing').bind('click');
	$('.genre_listing').removeClass('disabled');

	//Hover bei Buttons deaktivieren
	$('button').removeClass('disabled');
	$('.edit_menue')
		.find('button')
		.removeClass('disabled');
	$('input').removeClass('disabled');
	$('input').prop('disabled', false);
	$('a').removeClass('disabled');
	$('.carousel_artist').removeClass('disabled');
	$('.dj_carousel_listing').removeClass('disabled');
	$('.carousel_footer_wrapper').removeClass('disabled');
	$('.profile_element_menue_bottom').attr('style', '');

	//Language Dropdown
	$('.language_trigger').removeClass('disabled');

	//Tooltips
	$('.fixed_tooltip').removeClass('hidden');
	edit_mode = true;
	return true;
}

/**
 * checkHeadlineLimit - Checkt das Limit aller Headlines, sodass nirgends überstehender/scrollbarer Content ist.
 *
 * @return {boolean}  true
 */
function checkHeadlineLimit() {
	var editor_width = $('.headline_input>.ql-editor').outerWidth();
	var string_width = $('.headline_input>.ql-editor>p').outerWidth();
	var string = $('.headline_input>.ql-editor>p').text();
	while (editor_width < string_width + 5) {
		editor_width = $('.headline_input>.ql-editor').outerWidth();
		string_width = $('.headline_input>.ql-editor>p').outerWidth();
		string = $('.headline_input>.ql-editor>p').text();
		headlineeditor.deleteText(string.length - 1, string.length);
	}
	return true;
}

/**
 * makeAudioCropper - macht einen Cropper zum Croppen eines Audio-Thumbnails
 *
 * @param  {int} active_audio      Aktives Audio im Popup (1-4)
 * @param  {String} recently_uploaded Dateiname der zu croppenden Datei
 * @param  {String} user_id           acc_ID des Users
 * @return {boolean}                   true
 */
function makeAudioCropper(active_audio, recently_uploaded, user_id) {
	enabledAudioEdit();
	var url_file = '/signed/src/user/' + user_id + '/archive/' + recently_uploaded;
	var $croppie = $('.audio_cover').croppie({
		url: url_file,
		viewport: {
			width: '100%',
			height: '100%'
		},
		showZoomer: true,
		mouseWheelZoom: false
	});
	$('.audio_upload_wrapper').removeClass('empty');
	appendPositionNoteFixed($('.audio_cover'), '');
	$('#cropper_slider_audio').fadeIn(0);
	$('.audio_cover>.cr-slider-wrap').append('<img src="/signed/src/icns/plus_gray.png" alt=" " class="right">');
	$('.audio_cover>.cr-slider-wrap').prepend('<img src="/signed/src/icns/minus_gray.png" alt=" " class="left">');
	$('.audio_cover>.cr-slider-wrap').append('<div><button type="button" class="gray_btn menue_button" id="abort_audio_cropping" name="button">Abbrechen</button><button type="button" class="blue_btn menue_button " id="save_audio_cropping" name="button">Übernehmen</button></div>');
	return true;
}

/**
 * enabledAudioEdit - Startet den Bearbeitungsmodus für das Audiopopup
 *
 * @return {boolean}  true
 */
function enabledAudioEdit() {
	$('.audio_edit_wrapper').fadeIn();
	$('.audio_upload_wrapper>.audio_cover>.audio_wave').fadeOut();
	$('.audio_upload_wrapper>.audio_cover>.background_filter').fadeOut();
	$('.audio_upload_wrapper>.audio_cover>.audio_menue').fadeOut();
	$('#audio_popup_inner>.bottom_right_btns').fadeOut();
	return true;
}

/**
 * disableAudioEdit - Beendet den Bearbeitungsmodus für das Audiopopup
 *
 * @return {boolean}  true
 */
function disableAudioEdit() {
	$('.audio_edit_wrapper').fadeOut();
	$('#audio_popup_inner>.bottom_right_btns').fadeIn();
	if (
		!$('.audio_cover')
			.parent()
			.hasClass('empty')
	) {
		$('.audio_upload_wrapper>.audio_cover>.audio_wave').fadeIn();
		$('.audio_upload_wrapper>.audio_cover>.background_filter').fadeIn();
		$('.audio_upload_wrapper>.audio_cover>.audio_menue').fadeIn();
	} else {
	}
	$('.audio_cover>.cr-slider-wrap').html(' ');
	return true;
}

/**
 * makeAudioMenue - Macht das Bearbeitungsmenü (Drei Punkte) im Audio-Popup
 *
 * @return {boolean}  true
 */
function makeAudioMenue() {
	$('.audio_cover').append('<div class="audio_menue"><div class="action_wrapper"><span class="element_dropdown"> <img src="/signed/src/icns/menue_dots_free.svg" alt=""> </span></div><div class="dropdown"><div class="audio_dropdown_line" data-action="position">neu positionieren</div><div class="audio_dropdown_line" data-action="upload">neu hochladen</div><div class="audio_dropdown_line" data-action="delete">entfernen</div></div></div>');
	$('.audio_cover>.audio_wave').fadeIn();
	return true;
}

/**
 * removeAudioMenue - Entfernt das Bearbeitungsmenü (Drei Punkte) im Audio-Popup
 *
 * @return {boolean}  true
 */
function removeAudioMenue() {
	$('.audio_cover')
		.children('.audio_menue')
		.remove();
	$('.audio_cover>.audio_wave').fadeOut();
	return true;
}

/**
 * makeAudioMenue - entfernt Bearbeitungsmenü (Drei Punkte) im Video-Popup
 *
 * @return {boolean}  true
 */
function makeVideoMenue() {
	$('#video_thumbnail')
		.children('.video_menue')
		.remove();
	$('#video_thumbnail')
		.children('.cr-slider-wrap')
		.remove();
	$('#video_thumbnail').append('<div class="video_menue"><div class="action_wrapper"><span class="element_dropdown"> <img src="/signed/src/icns/menue_dots_free.svg" alt=""> </span></div><div class="dropdown"><div class="video_dropdown_line" data-action="position">neu positionieren</div><div class="video_dropdown_line" data-action="upload">neu hochladen</div><div class="video_dropdown_line" data-action="delete">entfernen</div></div></div>');
	return true;
}

/**
 * makeAudioMenue - Startet den Bearbeitungsmodus im Video-Popup (Thumbnail)
 *
 * @return {boolean}  true
 */
function enableVideoEdit() {
	$('#video_popup_content>.bottom_right_btns').fadeOut();
	$('#video_thumbnail>.video_menue').fadeOut();
	return true;
}

/**
 * makeAudioMenue - Beendet den Bearbeitungsmodus im Video-Popup (Thumbnail)
 *
 * @return {boolean}  true
 */
function disableVideoEdit() {
	$('#video_popup_content>.bottom_right_btns').fadeIn();
	$('#video_thumbnail>.cr-slider-wrap').html(' ');
	$('#video_thumbnail>.video_menue').fadeIn();
	return true;
}

/**
 * makeVideoCropper - Macht cropper im Video-Popup
 *
 * @return {boolean}  true
 */
function makeVideoCropper() {
	var url_file = '/signed/src/user/' + $('input[name=user_id]').val() + '/archive/' + $('#video_thumbnail_file').val();
	if ($('#video_thumbnail').hasClass('empty')) {
		$('#video_thumbnail').removeClass('empty');
		$('#video_thumbnail').addClass('was_empty');
	}
	var croppie = $('#video_thumbnail').croppie({
		url: url_file,
		viewport: {
			width: '100%',
			height: '100%'
		},
		showZoomer: true,
		mouseWheelZoom: false
	});
	enableVideoEdit();
	$('#cropper_slider_audio').fadeIn(0);
	appendPositionNoteFixed($('#video_thumbnail'), '');
	$('#video_thumbnail>.cr-slider-wrap').append('<img src="/signed/src/icns/plus_gray.png" alt=" " class="right">');
	$('#video_thumbnail>.cr-slider-wrap').prepend('<img src="/signed/src/icns/minus_gray.png" alt=" " class="left">');
	$('#video_thumbnail>.cr-slider-wrap').append('<div><button type="button" class="gray_btn menue_button" id="abort_video_cropping" name="button">Abbrechen</button><button type="button" class="blue_btn menue_button " id="save_video_cropping" name="button">Übernehmen</button></div>');
	return true;
}

/**
 * clearAudioPopup - Leert das Audiopopup bzw. befüllt es mit den Daten des geklickten elements
 *
 * @return {boolean}  true
 */
function clearAudioPopup() {
	$('input[name=audio_title]').val('');
	$('#upload_audio>span').each(function() {
		$(this).text('Audiodatei hochladen');
	});
	for (var i = 1; i <= 5; i++) {
		clearAudioElement(i);
	}
	return true;
}

/**
 * clearAudioElement - Leert das Audio-Element im Audiopopup
 *
 * @param  {int} audio_wrapper_position Position des Audio-Wrappers
 * @return {boolean}                    true
 */
function clearAudioElement(audio_wrapper_position) {
	var audio_wrapper = $('.audio_wrapper[data-position=' + audio_wrapper_position + ']');
	$(audio_wrapper)
		.children('input')
		.val('');
	$(audio_wrapper)
		.children('input[name=wave_color]')
		.val('#FFFFFF,#CCCCCC');
	$(audio_wrapper)
		.children('.audio')
		.attr('style', '');
	$(audio_wrapper)
		.children('.title')
		.text('Titel');
	return true;
}

/**
 * makeAudioSlideshow - Macht Slideshow im Audio-Element
 *
 * @param  {Element} slideshow_div Div in dem Audioelement
 * @param  {String} audiotype      Typ des Audios (dropdown, bottom, usw)
 * @return {boolean}               true
 */
function makeAudioSlideshow(slideshow_div, audiotype) {
	//Slideshow machen
	slideoshow_div = $(playerparent).find('.full_size.background');
	if ($(slideshow_div).hasClass('slick-initialized')) {
		$(slideshow_div).slick('unslick');
	}
	//Slideshow Audio
	$(slideshow_div).slick({
		accessibbility: true,
		autoplay: false,
		dots: true,
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		easing: 'linear',
		arrows: true
	});
	return true;
}

/**
 * changeWaveColorPopup - Ändert die Wave-Color im Audiopopup
 *
 * @param  {String} wave_color   Farbe als HEX
 * @param  {String} side         Seite der Audio-Wave
 * @param  {int} active_audio    aktives Audio (1-4)
 * @return {boolean}             true
 */
function changeWaveColorPopup(wave_color, side, active_audio) {
	$('.audio_wave')
		.find('.' + side)
		.find('svg')
		.children('g')
		.children('g')
		.children('g')
		.attr('fill', wave_color);
	if (side == 'right') {
		wave_color_right = wave_color;
	} else {
		wave_color_left = wave_color;
	}
	$('.audio_wrapper[data-position=' + active_audio + ']')
		.find('input[name=wave_color]')
		.val(wave_color_left + ',' + wave_color_right);
	return true;
}

/**
 * stopAllPlayers - Stoppt alle Audio-Player
 *
 * @param  {Object-Array} wavesurfer Alle Wavesurferinstanzen in Object-Array
 * @param  {Element} player     geklickter Player
 * @return {boolean}            true
 */
function stopAllPlayers(wavesurfer, player) {
	Object.keys(wavesurfer).forEach(function(item, index) {
		if (item != player) {
			wavesurfer[item].pause();
		}
	});
	$('.playbutton').each(function() {});
	return true;
}

/**
 * ellipsizeTextBox - Kürzt die Texte in allen Elementen der angegebenene Klasse
 *
 * @param  {String} elementclass Klasse der Elemente in denen gekürzt werden soll
 * @return {boolean}              true
 */
function ellipsizeTextBox(elementclass) {
	var elements = document.getElementsByClassName(elementclass);
	for (var i = 0; i < elements.length; i++) {
		el = elements[i];
		var wordArray = el.innerHTML.split(' ');
		while (el.scrollHeight > el.offsetHeight) {
			wordArray.pop();
			el.innerHTML = wordArray.join(' ');
		}
	}
	return true;
}

/**
 * scanPresspackRequestSVGs - scant alle .color_switch_svg img tags und wandelt diese in inline svgs um
 * @return {boolean} true
 */
function scanPresspackRequestSVGs() {
	$('.color_switch_svg').each(function() {
		var $img = jQuery(this);
		var imgURL = $img.attr('src');
		var attributes = $img.prop('attributes');

		$.get(
			imgURL,
			function(data) {
				// Get the SVG tag, ignore the rest
				var $svg = jQuery(data).find('svg');
				// Remove any invalid XML tags
				$svg = $svg.removeAttr('xmlns:a');
				// Loop through IMG attributes and apply on SVG
				$.each(attributes, function() {
					$svg.attr(this.name, this.value);
				});
				// Replace IMG with SVG
				$img.replaceWith($svg);
			},
			'xml'
		);
	});
	return true;
}
scanPresspackRequestSVGs();

/**
 * appendPositionNoteFixed - Fügt dem container eine "Ziehen um zu Positionieren Nachricht hinzu" mit optionaler zusätzlicher Klasse
 *
 * @param  {object} container   jQuery Object, Container in dem
 * @param  {String} extra_class description
 * @return {boolean}             true
 */
function appendPositionNoteFixed(container, extra_class) {
	$(container).append('<div class="drag_positition_note ' + extra_class + '" id="drag_position_photo ">Ziehen um zu positionieren</div>');
	$(container)
		.find('.drag_positition_note')
		.fadeIn(500);
	return true;
}
