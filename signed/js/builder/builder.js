$(function() {
	$(window).on('resize', function() {
		profile_element_width = $('.profile_element').width();
		right_menue_size = $('#right_menue').width();
	});
	//Tutorial Overlay

	//Lazyload preview-bilder
	lazyloadAll();

	//Popups schließen wenn Click auf Background
	/*  $('.popup_background:not(.nofadeout)').on('click', function(e) {
      var clicked = $(this);
      var container = $('.popup_body');
      if (!container.is(e.target) && container.has(e.target).length === 0) {
        $(clicked).fadeOut();
      }
    });*/

	//Scrolleffekt Builder außerhalb builder_viewport
	$('#left_menue').scroll(function() {
		if (!$('#left_menue').hasClass('element_mode')) {
			target.prop('scrollTop', this.scrollTop);
		} else {
			target2.prop('scrollTop', this.scrollTop);
		}
	});

	right_menue = document.getElementsByClassName('right_menue')[0];
	right_menue.addEventListener('wheel', function(event) {
		target.scrollTop += event.deltaY * 10;
	});
	$('#right_menue').scroll(function() {
		target.prop('scrollTop', this.scrollTop);
	});

	// Zwischen Mobil- und Desktopansicht umschalten
	$(document).on('click', '.switch_button.view:not(.active)', function() {
		$('.switch_button.view.active').removeClass('active');
		$(this).addClass('active');
		if ($(this).attr('data-view') == 'mobile') {
			$('.blur_desktop').fadeOut(100);
			$('.builder_viewport').addClass('mobile');
			$('#desktop').css('z-index', 9);
			$('#mobile').css('z-index', 10);
			$('#mobile').fadeIn(500, function() {
				$('#desktop').fadeOut(100);
			});
			//namePositionRelative();

			// Landscape Mobile mit builder-html im Edit mode füllen
			refreshMobileLandscape();
			//Fullscreenbutton deaktivieren
			$('#fullscreen').addClass('deep_hide');
			$('#show_preview').addClass('deep_hide');
			$('.mobile_element_menue').fadeOut(0);
		} else {
			$('.builder_viewport').removeClass('mobile');
			$('.blur_desktop').fadeIn(1000);
			$('#mobile').css('z-index', 9);
			$('#desktop').css('z-index', 10);
			$('#desktop').fadeIn(500, function() {
				$('#mobile').fadeOut(100);
			});

			//enableEdit();
			$('#fullscreen').removeClass('deep_hide');
			$('#show_preview').removeClass('deep_hide');
		}
	});

	$('.slideshow_input').change(function() {
		$(this)
			.parent()
			.children('.slideshow_element')
			.removeClass('empty');
		$(this)
			.parent()
			.children('.slideshow_input_base64')
			.val('NULL');
		var slide_nr = $(this).attr('data-position');
		var input = $(this);
		var file = this.files[0];
		var reader = new FileReader();
		reader.onloadend = function() {
			$(input)
				.parent()
				.children('.slideshow_element')
				.css('background-image', 'url("' + reader.result + '")');
		};
		if (file) {
			reader.readAsDataURL(file);
		}
	});

	//Blauer Rahmen hover von Komponente
	$(document).on('mouseenter', '.hover_blue_inlineshadow', function(event) {
		$('.hover_blue_inlineshadow.hovered').removeClass('hovered');
		$(this).addClass('hovered');
	});

	$(document).on('mouseleave', '.hover_blue_inlineshadow', function() {
		if ($(this).parents('.hover_blue_inlineshadow').length) {
			$(this)
				.parents('.hover_blue_inlineshadow')
				.addClass('hovered');
			$(this)
				.parents('.profile_element_content')
				.children('.full_size_background.hover_blue_inlineshadow')
				.removeClass('hovered');
			$(this).removeClass('hovered');
		} else {
			$(this)
				.parents('.profile_element_content')
				.children('.full_size_background')
				.addClass('hovered');
			$(this).removeClass('hovered');
		}
	});

	//BLauer Rahmen hover von Komponente
	$(document).on('mouseenter', '.darkelement, .lightelement', function(event) {
		$('.lightelement.hovered_background').removeClass('hovered_background');
		$('.darkelement.hovered_background').removeClass('hovered_background');
		$(this).addClass('hovered_background');
	});

	$(document).on('mouseenter', '.right_menue, #left_menue', function(event) {
		$('.hovered_background').removeClass('hovered_background');
	});

	$('#desktop').on('mouseleave', function() {
		$('.element_menue').fadeOut(0);
		$('.hover_blue_inlineshadow.hovered').removeClass('hovered');
	});

	//Language
	$(document).on('click', '.language_trigger:not(.disabled)', function() {
		$('.language_dropdown').slideToggle();
	});

	$(document).on('click', function(e) {
		if (!$(e.target).is('.language_trigger') && !$(e.target).is('.language_trigger>img') && !$(e.target).is('.language_trigger>span')) {
			$('.language_dropdown').slideUp();
		}
	});

	$('.language_dropdown_line').on('click', function() {
		$('.language').text($(this).text());
	});

	/*Tooltips bei Hover über Icon*/
	$(document).on('mouseenter', '.tooltip_img', function() {
		$(this)
			.closest('.tooltip')
			.find('span[role=tooltip]')
			.fadeIn(); //:not(.url)
	});

	$(document).on('mouseleave', '.tooltip_img', function() {
		$(this)
			.closest('.tooltip')
			.find('span[role=tooltip]')
			.fadeOut(0); //:not(.url)
	});

	$('#fullscreen').on('click', function() {
		if ($(this).attr('data-screen') == 'edit') {
			makeFullScreen();
			$(this).attr('data-screen', 'fullscreen');
			$('#abort_preview').fadeOut(0);
			fullscreen = true;
		} else {
			exitFullScreen();
			$(this).attr('data-screen', 'edit');
			$('#abort_preview').fadeOut(1000);
			fullscreen = false;
		}
		setTimeout(function() {
			profile_element_width = $('.profile_element').width();
			right_menue_size = $('#right_menue').width();
		}, 2000);
	});

	$('#fullscreen').on('mouseenter', function() {
		$('#fullscreen').addClass('blue_hover');
		$('#right_menue').addClass('blue_hover');
		$('#fullscreen>img').attr('src', '/signed/src/icns/arrow_right_white.svg');
	});

	$('#fullscreen').on('mouseleave', function() {
		$('#fullscreen').removeClass('blue_hover');
		$('#right_menue').removeClass('blue_hover');
		$('#fullscreen>img').attr('src', '/signed/src/icns/arrow_right_gray.svg');
	});

	right_menue_size = $('#right_menue').width();
	$(document).on('mousemove', function(event) {
		x = event.pageX;
		y = event.pageY;
		if (x < right_menue_size + 20 && edit_mode == true) {
			$('#fullscreen').fadeIn(200);
		} else {
			$('#fullscreen').fadeOut(200);
		}
	});
	setTimeout(function() {
		$('.add_profile_element_wrapper').addClass('tutorial_highlight');
	}, 2000);
});
