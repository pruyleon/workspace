$(function() {
	/*Presspack*/
	$(document).on('click', '.presspack_popup_trigger:not(.disabled)', function() {
		$('#presspack_popup').fadeIn();
	});

	$(document).on('click', '.presspack_img.add_img', function() {
		if (
			!$(this)
				.parent()
				.hasClass('picture')
		) {
			$(this)
				.find('input')
				.trigger('click');
		}
	});

	$(document).on('click', '.presspack_img.add_img > input', function(e) {
		e.stopPropagation();
	});

	$(document).on('click', '.presspack_img.add_img > .img_menue_btn', function(e) {
		e.stopPropagation();
	});

	$(document).on('click', '.presspack_img.add_img > .presspack_menue', function(e) {
		e.stopPropagation();
	});

	$(document).on('click', '.presspack_img>.img_delete_btn', function() {
		input = this;
		fd = new FormData();
		fd.append('user_id', user_id);
		var type = $(input)
			.closest('.presspack_type')
			.attr('data-type');
		var name = $(this).attr('data-name');
		fd.append('name', name);
		fd.append('type', type);
		$.ajax({
			url: '/signed/php/presspack/delete_file_by_name.php',
			type: 'POST',
			data: fd,
			contentType: false,
			cache: false,
			processData: false,
			success: function(response) {
				console.log(response);
				$(input)
					.parent()
					.remove();
				if (response != 'failure') {
					$(this)
						.parent()
						.remove();
				}
			}
		});
	});

	$('.presspack_menue_btn.reupload').on('click', function() {
		$(this)
			.parent()
			.parent()
			.find('input')
			.trigger('click');
	});

	$(document).on('click', '.img_menue_btn', function() {
		$(this)
			.parent()
			.children('.presspack_menue')
			.fadeToggle();
	});

	$(document).on('change', '.presspack_img > input[type=file]', function(e) {
		input = this;
		fd = new FormData();
		fd.append('image', $(input)[0].files[0]);
		fd.append('user_id', user_id);
		var file = $(input)[0].files[0];
		var name = $(input).attr('name');
		fd.append('name', name);
		var parent = $(input).parent();
		$(parent).removeClass('add_img');
		var input = $(input);
		if ($(input).hasClass('empty')) {
			if (name == 'video') {
				var type = 'video';
			} else {
				var type = 'image';
			}
		}

		$.ajax({
			url: '/signed/php/presspack/upload_file.php',
			type: 'POST',
			data: fd,
			contentType: false,
			cache: false,
			processData: false,
			success: function(response) {
				console.log(response);
				if (response != 'failure') {
					$('.presspack_pic_list.' + name).append('<div class="presspack_img ' + name + ' add_img" data-type="' + name + '"><button type="button" class="button_img img_delete_btn" name="button" data-name=""> <img src="/signed/src/icns/delete_presspack.svg" alt="Menü"> </button><input type="file" class="empty hidden" name="' + name + '" accept="' + type + '/*" value=""></div>');
					var reader = new FileReader();
					reader.onloadend = function() {
						$(parent).css('background-image', 'url("' + reader.result + '")');
						$(parent)
							.children('button')
							.attr('data-name', response);
					};
					if (file) {
						reader.readAsDataURL(file);
					}
					//Bild in archiv-popup hinzufügen
					if (type != 'video' && type != 'misc') {
						addToArchive(response);
					}
					$('#file_archive_popup').fadeOut();
				}
			}
		});
	});

	$('.dropdown_desc').on('click', function() {
		$('#presspack_popup_content').animate(
			{
				scrollTop: $('#presspack_popup_content').prop('scrollHeight') + 50
			},
			300
		);
	});

	//Dropdown + Löschen
	$('.dropdown_desc').on('click', function() {
		if (!$(this).hasClass('inactive')) {
			$(this)
				.next('.manual_dropdown')
				.slideDown();
		}
	});

	//Ausfaden
	$(document).on('click', function(e) {
		if (!$(e.target).is('.dropdown_point') && !$(e.target).is('.manual_dropdown') && !$(e.target).is('.dropdown_desc')) {
			$('.manual_dropdown.presspack_rubrik').slideUp();
		}
		if (!$(e.target).is('.pcr-app') && !$(e.target).is('#text_color') && !$(e.target).is('.pcr-app *')) {
			$('#texteditor')
				.find('.picker')
				.fadeOut();
		}
	});

	$('.delete_presspack_type').on('click', function() {
		//Reinfaden Warning-Popup
		$('#delete_presspack_popup_outer').fadeIn();
		presspack_type = $(this)
			.parent()
			.parent()
			.attr('data-type');
		dropdown_point = $('.dropdown_point.' + presspack_type);
		presspack = $(this)
			.parent()
			.parent();
	});

	$('#delete_presspack').on('click', function() {
		$(dropdown_point).removeClass('hidden');
		$('.dropdown_desc').removeClass('inactive');
		$(presspack).fadeOut();
		$('#delete_presspack_popup_outer').fadeOut();
	});

	$('#presspack_delete_abort').on('click', function() {
		$('#delete_presspack_popup_outer').fadeOut();
	});

	var hidden_check;
	$('.dropdown_point').on('click', function() {
		$('.manual_dropdown').fadeOut();
		var type = $(this).attr('data-type');
		$('.presspack_type.' + type).fadeIn();
		$(this).addClass('hidden');
		hidden_check = false;
		$('.dropdown_point').each(function() {
			if (!$(this).hasClass('hidden')) {
				hidden_check = true;
			}
		});
		if (!hidden_check) {
			$('.dropdown_desc').addClass('inactive');
		}
	});

	$('#save_presspack').on('click', function() {
		$('#presspack_popup').fadeOut();
	});

	$('#abort_presspack').on('click', function() {
		$('#presspack_popup').fadeOut();
	});
});
