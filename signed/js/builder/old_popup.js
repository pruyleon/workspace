// Embedded-Popups
//Soundcloud
$('#soundcloud_embed').on('click', function() {
	$('#soundcloud_popup').fadeIn();
});

$(document).on('click', '.toggle_btn:not(.active)', function() {
	$(this)
		.parent()
		.find('.toggle_btn:not(active)')
		.removeClass('active');
	$(this).addClass('active');
});

$('#abort_soundcloud').on('click', function() {
	$('soundcloud_popup').fadeOut();
});

$('#close_soundcloud_info').on('click', function() {
	$('#soundcloud_info_popup').fadeOut();
});

$('#show_soundcloud_info').on('click', function() {
	$('#soundcloud_info_popup').fadeIn();
});

$('#soundcloud_light').on('click', function() {
	$('#soundcloud_darkmode').fadeOut(500, function() {
		$('#soundcloud_lightmode').fadeIn(500);
	});
});
$('#soundcloud_dark').on('click', function() {
	$('#soundcloud_lightmode').fadeOut(500, function() {
		$('#soundcloud_darkmode').fadeIn(500);
	});
});

//Mixcloud
$('#mixcloud_embed').on('click', function() {
	$('#mixcloud_popup').fadeIn();
});
$('#show_mixcloud_info').on('click', function() {
	$('#mixcloud_info_popup').fadeIn();
});

//Toggle
var mixcloud_artwork;
$('#mixcloud_light').on('click', function() {
	if ($('#mixcloud_artwork').is(':checked')) {
		mixcloud_artwork = '_artwork';
	} else {
		mixcloud_artwork = '';
	}
	$('.mixcloud_darkmode').fadeOut(500, function() {
		$('#mixcloud_lightmode' + mixcloud_artwork)
			.delay(500)
			.fadeIn(500);
	});
});
$('#mixcloud_dark').on('click', function() {
	if ($('#mixcloud_artwork').is(':checked')) {
		mixcloud_artwork = '_artwork';
	} else {
		mixcloud_artwork = '';
	}
	$('.mixcloud_lightmode').fadeOut(500, function() {
		$('#mixcloud_darkmode' + mixcloud_artwork)
			.delay(500)
			.fadeIn(500);
	});
});

var mixcloud_mode;
$('#mixcloud_artwork').on('change', function() {
	mixcloud_mode = $('.toggle_btn.mixcloud.active').attr('data-mode');
	if ($(this).is(':checked')) {
		mixcloud_artwork = '_artwork';
	} else {
		mixcloud_artwork = '';
	}
	$('.mixcloud_' + mixcloud_mode + 'mode').fadeOut(500, function() {
		$('#mixcloud_' + mixcloud_mode + 'mode' + mixcloud_artwork)
			.delay(500)
			.fadeIn(500);
	});
});

/*Spotify*/
$('#spotify_embed').on('click', function() {
	$('#spotify_popup').fadeIn();
});

$('#show_spotify_info').on('click', function() {
	$('#spotify_uri_info').fadeIn();
});

$('#spotify_uri').on('input', function() {
	spotify_uri = $('#spotify_uri').val();
	if (spotify_uri.indexOf('album') !== -1) {
		$('#spotify_embedded_player')
			.find('.track')
			.fadeOut(500, function() {
				$('#spotify_embedded_player')
					.find('.album')
					.fadeIn(500);
			});
	} else if (spotify_uri.indexOf('track') !== -1) {
		$('#spotify_embedded_player')
			.find('.album')
			.fadeOut(500, function() {
				$('#spotify_embedded_player')
					.find('.track')
					.fadeIn(500);
			});
	}
});

$('#show_spotify_info').on('click', function() {
	$('#spotify_info_popup').fadeIn();
});

$('#close_spotify_info').on('click', function() {
	$('#spotify_info_popup').fadeOut();
});

//Youtube
$('#youtube_embed').on('click', function() {
	$('#youtube_popup').fadeIn();
});

$('#abort_youtube').on('click', function() {
	$('#youtube_empty').fadeOut();
	$('#youtube_popup').fadeOut();
});

$('#save_soundcloud').on('click', function() {
	soundcloud_link = $('#soundcloud_link').val();
	if (soundcloud_link == '') {
		$('#soundcloud_empty').fadeIn();
	} else {
		$(getInputValue(clicked_icon)).val(soundcloud_link);
		$(getInputType(clicked_icon)).val('soundcloud');
		$(clicked_icon)
			.closest('.content')
			.html(getSoundcloudIFrame(soundcloud_link));
		$('#soundcloud_empty').fadeOut();
		$('#soundcloud_popup').fadeOut();
	}
});

$('#save_mixcloud').on('click', function() {
	mixcloud_link = $('#mixcloud_link').val();
	if (mixcloud_link == '') {
		$('#mixcloud_empty').fadeIn();
	} else {
		$(getInputValue(clicked_icon)).val(mixcloud_link);
		$(getInputType(clicked_icon)).val('mixcloud');
		$(clicked_icon)
			.closest('.content')
			.html(getMixcloudIFrame(mixcloud_link));
		$('#mixcloud_empty').fadeOut();
		$('#mixcloud_popup').fadeOut();
	}
});

var vid_id;
var youtube_link;
var autoplay;
$('#save_youtube').on('click', function() {
	youtube_link = $('#youtube_link').val();
	// speichern der Infos aus #youtube_uri
	if (youtube_link == '' && (youtube_link.indexOf('youtube') == -1 || youtube_link.indexOf('youtu.be') == -1)) {
		$('#youtube_empty').fadeIn();
	} else {
		vid_id = getYoutubeID(youtube_link);
		if ($('input[name=youtube_autoplay]').is(':checked')) {
			vid_id += '?start=1';
		}
		$(getInputValue(clicked_icon)).val(vid_id);
		$(getInputType(clicked_icon)).val('youtube');
		$(clicked_icon)
			.closest('.content')
			.html(getYoutubeIFrame(vid_id, autoplay));
		makeMenue($(clicked_icon));
		$('#youtube_empty').fadeOut();
		$('#youtube_popup').fadeOut();
	}
});

$('#save_spotify').on('click', function() {
	spotify_uri = $('#spotify_uri').val();
	// speichern der Infos aus #spotify_uri
	if (spotify_uri == '') {
		$('#spotify_empty').fadeIn();
	} else {
		$(clicked_icon)
			.closest('.content')
			.html(getSpotifyIFrame(spotify_uri));
		$('#spotify_empty').fadeOut();
		$('#spotify_popup').fadeOut();
	}
});

function getSoundcloudIFrame(link) {
	return '<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=' + link + '&color=%23231c32&auto_play=false&hide_related=false&show_comments=false&show_user=false&show_reposts=false&show_teaser=false"></iframe>';
}

function getYoutubeID(link) {
	if (link.indexOf('youtube') != -1) {
		link = link.split('=');
		vid_id = link[link.length - 1];
	} else if (link.indexOf('youtu.be') != -1) {
		link = link.split('/');
		vid_id = link[link.length - 1];
	} else {
		return false;
	}
	return vid_id;
}

function getMixcloudIFrame(link) {
	link = link.substring(0, link.length - 1);
	link = link.split('/');
	link = link[link.length - 2] + '%2F' + link[link.length - 1];
	var light = $('#mixcloud_light').hasClass('active') ? '&light=1' : '';
	var artwork = $('#mixcloud_artwork').is(':checked') ? '&hide_artwork=1' : '';
	return '<iframe width="100%" height="120" src="https://www.mixcloud.com/widget/iframe/?hide_cover=1' + light + artwork + '&mini=1&feed=%2F' + link + '%2F" frameborder="0" ></iframe>';
}

function getYoutubeIFrame(vid_id) {
	return '<iframe width="1600" height="900" src="https://youtube.com/embed/' + vid_id + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
}

function getSpotifyIFrame(id) {
	id = id.substring(8);
	id = id.replace(/:/g, '/');
	return '<iframe src="https://open.spotify.com/embed/' + id + '" width="300" height="20" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>';
}
