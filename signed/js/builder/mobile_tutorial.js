$(function() {
	console.log('Mobiles Tutorial: ' + mobile_tutorial);
	//Handy drehen und rausfaden
	$('#switch_mobile_format').trigger('click');
	$('#switch_mobile_format').fadeOut(0);

	$('.switch_button.view[data-view=mobile]').on('click', function() {
		setTimeout(() => {
			$('#mobile_tutorial_start_popup').fadeIn(500);
		}, 1500);
	});

	$('#mobile_tutorial_start_button').on('click', function() {
		$('#mobile_tutorial_start_popup').fadeOut(500);
		$('#switch_mobile_format').fadeIn(500, function() {
			$('#switch_mobile_format').addClass('tutorial_highlight');
		});
		mobile_tutorial_step = 2;
	});

	$('#switch_mobile_format').on('click', function() {
		console.log('Mobiles Tutorial: ' + mobile_tutorial + '\n' + 'Step: ' + mobile_tutorial_step);
		if (mobile_tutorial && mobile_tutorial_step == 2) {
			$('#switch_mobile_format').removeClass('tutorial_highlight');
			setTimeout(function() {
				$('#mobile_tutorial_portrait_popup').fadeIn(500);
			}, 1500);
		}
	});

	$('#mobile_tutorial_portrait_button').on('click', function() {
		$('#mobile_tutorial_portrait_popup').fadeOut(1000);
		$('#mobile_tutorial_hint_popup').fadeIn(1000);
	});

	$($('#mobile_tutorial_hint_button')).on('click', function() {
		$('#mobile_tutorial_hint_popup').fadeOut(500);
		$('#mobile_photo_edit_icn').addClass('tutorial_hightlight');
		finishTutorial('mobile_tutorial');
	});

	$('#mobile_photo_edit_icn').on('click', function() {
		if (mobile_tutorial) {
			$('#mobile_photo_edit_icn').removeClass('tutorial_hightlight');
			mobile_tutorial = false;
		}
	});
});
