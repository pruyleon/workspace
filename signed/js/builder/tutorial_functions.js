function finishTutorial(tutorial_name) {
	//Ajax call auf Tutorial
	$.ajax({
		url: '/signed/php/builder/inserts/complete_tutorial.php',
		type: 'POST',
		data: {
			user_id: user_id,
			tutorial_name: tutorial_name
		},
		success: function(response) {
            // console.log(response);
        }
	});
	// Iconsinput header display: none entfernen
	// display: none; visibility: hidden;
	if (tutorial_name == 'tutorial') {
		icons_new = $('.profile_element.header')
			.find('input[name=icons]')
			.val();
		icons_new = icons_new.replace(new RegExp('display: none;', 'g'), '');
		icons_new = icons_new.replace(new RegExp('visibility: hidden;', 'g'), '');
		$('.profile_element.header')
			.find('input[name=icons]')
			.val(icons_new);
	}
}
