$(function() {
	//Reinfaden des Plus und des Verschieben-Menüs
	$(document).on('mousemove', '.profile_element', function(event) {
		var offset = $(this).offset();
		x = event.pageX - offset.left;
		y = event.pageY - offset.top;
		x_perc = x / profile_element_width;
		if (y < 70 && x_perc > 0 && x_perc < 1) {
			$(this)
				.prev('.add_profile_element_wrapper:not(.disabled)')
				.stop()
				.fadeIn(300);
			if (footer_filter) {
				$('.carousel_footer_wrapper').addClass('hover');
				$('.footer_filter').addClass('hover');
			}
		} else if ($(this).height() - y < 70 && x_perc > 0 && x_perc < 1) {
			if ($('.no_add_plus:hover').length) {
				//Hover über Genre, Presspack, Anfrage oder Social aka .no_add_plus
				$(this)
					.next('.add_profile_element_wrapper:not(.disabled)')
					.stop()
					.fadeOut(300);
				if (footer_filter) {
					$('.carousel_footer_wrapper').removeClass('hover');
					$('.footer_filter').removeClass('hover');
				}
			} else {
				$(this)
					.next('.add_profile_element_wrapper:not(.disabled)')
					.stop()
					.fadeIn(300);
				if (footer_filter) {
					$('.carousel_footer_wrapper').addClass('hover');
					$('.footer_filter').addClass('hover');
				}
			}
		} else {
			if (footer_filter) {
				$('.carousel_footer_wrapper').removeClass('hover');
				$('.footer_filter').removeClass('hover');
			}
			$('.add_profile_element_wrapper').each(function() {
				$(this)
					.stop()
					.fadeOut(300);
			});
		}
		if (x_perc > 0.7 && x_perc < 1) {
			$(this)
				.find('.profile_element_menue_bottom')
				.stop()
				.fadeIn(300);
		} else {
			$(this)
				.find('.profile_element_menue_bottom')
				.stop()
				.fadeOut(300);
			$('#delete_element_popup').fadeOut(300);
		}
	});

	$(document).on('mousemove', '.carousel_footer_wrapper', function(event) {
		var dj_carousel_offset = $(this).offset();
		dj_carousel_x = event.pageX - dj_carousel_offset.left;
		dj_carousel_y = event.pageY - dj_carousel_offset.top;
		dj_carousel_x_perc = dj_carousel_x / profile_element_width;
		if (dj_carousel_y < 100) {
			$(this)
				.prev('.add_profile_element_wrapper:not(.disabled)')
				.stop()
				.fadeIn(300);
		} else if (!footer_filter) {
			$(this)
				.prev('.add_profile_element_wrapper:not(.disabled)')
				.stop()
				.fadeOut(100);
		}
	});

	$(document).on('mouseleave', '.builder_viewport', function() {
		if (footer_filter) {
			$('.carousel_footer_wrapper').removeClass('hover');
			$('.footer_filter').removeClass('hover');
		}
		$('.add_profile_element_wrapper').each(function() {
			$(this).fadeOut(0);
		});
	});

	$(document).on('mouseenter', '.footer_filter', function() {
		$(this)
			.parent()
			.prev('.add_profile_element_wrapper:not(.disabled)')
			.stop()
			.fadeIn(200);
	});

	$(document).on('mouseleave', '.profile_element', function(event) {
		$(this)
			.find('.profile_element_menue_bottom')
			.stop()
			.fadeOut(200);
	});

	//Rausfaden des Plus wenn hover auf kein Element
	$(document).on('mousemove', '.dj_carousel', function(event) {
		var offset = $(this).offset();
		y = event.pageY - offset.top;
		if (y > 20) {
			$('.add_profile_element_wrapper').each(function() {
				$(this)
					.stop()
					.fadeOut(100);
			});
		}
	});

	/* Bottom Menue Klicks */
	$(document).on('click', '.profile_element_menue_icon', function() {
		var action = $(this).attr('data-action');
		profile_element = $(this).closest('.profile_element');
		if (action == 'delete') {
			$('#delete_element_popup').fadeIn();
			active_bottom_menue = $(this);
			bottom_menue_offset = $(active_bottom_menue).offset();
			bottom_menue_top = bottom_menue_offset.top;
			if (bottom_menue_top == 0) {
				$('#delete_element_popup').fadeOut(300);
			} else {
				$('#delete_element_popup_inner').css('top', bottom_menue_top);
			}
		} else if (action == 'up') {
			previous = $(profile_element)
				.prevAll('.profile_element')
				.first();
			if (!$(previous).hasClass('header')) {
				$(profile_element).insertBefore(previous);
				$(profile_element)
					.get(0)
					.scrollIntoView();
				generateAddElementPlus(element_add_plus);
				profileElementMidScroll($(profile_element));
			}
		} else if (action == 'down') {
			next = $(profile_element)
				.nextAll('.profile_element')
				.first();
			$(profile_element)
				.get(0)
				.scrollIntoView();
			$(profile_element).insertAfter(next);
			generateAddElementPlus(element_add_plus);
			profileElementMidScroll($(profile_element));
		}
	});

	/* Element löschen Popup */
	$('#delete_element').on('click', function() {
		$(profile_element).remove();
		generateAddElementPlus(element_add_plus);
		$('#delete_element_popup').fadeOut();
	});

	active_bottom_menue = false;
	$('.builder_content').on('scroll', function() {
		if (active_bottom_menue) {
			bottom_menue_offset = $(active_bottom_menue).offset();
			bottom_menue_top = bottom_menue_offset.top;
			if (bottom_menue_top == 0) {
				$('#delete_element_popup').fadeOut(300);
			} else {
				$('#delete_element_popup_inner').css('top', bottom_menue_top);
			}
		}
	});

	$('#delete_element_popup_inner').on('mouseenter', function() {
		$(active_bottom_menue)
			.closest('.profile_element_menue_bottom')
			.stop()
			.fadeIn(0);
		$(active_bottom_menue)
			.closest('.profile_element')
			.find('.blue_profile_element_hover')
			.css('box-shadow', 'inset 0px 0px 0px 2px var(--red)');
		$(active_bottom_menue)
			.closest('.profile_element')
			.children('.blue_profile_element_hover')
			.stop()
			.fadeIn();
	});

	$('#delete_element_popup_inner').on('mouseleave', function() {
		$(active_bottom_menue)
			.closest('.profile_element')
			.find('.blue_profile_element_hover')
			.css('box-shadow', 'none');
		$(active_bottom_menue)
			.closest('.profile_element')
			.children('.blue_profile_element_hover')
			.stop()
			.fadeOut();
	});

	//Blauer Rahmen bei Hover Verschieben/Löschenmenü
	$(document).on('mouseenter', '.profile_element_menue_bottom>button', function() {
		$(this)
			.closest('.profile_element')
			.children('.blue_profile_element_hover')
			.stop()
			.fadeIn(300);
	});
	$(document).on('mouseleave', '.profile_element_menue_bottom>button', function() {
		$(this)
			.closest('.profile_element')
			.children('.blue_profile_element_hover')
			.stop()
			.fadeOut(300);
	});

	$(document).on('mouseenter', '.profile_element_menue_bottom>button[data-action=delete]', function() {
		$(this)
			.closest('.profile_element')
			.find('.blue_profile_element_hover')
			.css('box-shadow', 'inset 0px 0px 0px 2px var(--red)');
	});

	$(document).on('mouseleave', '.profile_element_menue_bottom>button[data-action=delete]', function() {
		$(this)
			.closest('.profile_element')
			.find('.blue_profile_element_hover')
			.css('box-shadow', 'inset 0px 0px 0px 2px var(--blue)');
	});

	$(document).on('mouseenter', '.menue_parent', function(event) {
		menue_parent = $(this);
		$(menue_parent)
			.children('.element_menue:not(.disabled)')
			.stop()
			.fadeIn(300);
	});

	$(document).on('mouseleave', '.menue_parent', function() {
		clearTimeout(edit_menue_timeout);
		$(this)
			.children('.element_menue')
			.stop()
			.fadeOut(300);
	});

	//Element-Mode Trigger auf add_profile_element_plus
	$(document).on('click', '.add_profile_element_plus', function() {
		if (!$(this).hasClass('disabled')) {
			footer_filter = false;
			//Filter und Elementlisting ausblenden!
			$('.filter_wrapper').fadeOut(0);
			$('.element_listing_filter').fadeOut(0);
			$('.footer_filter').fadeOut(500);
			$('.carousel_footer_wrapper').removeClass('blur');
			add_element = true;
			profile_element = $(this)
				.closest('.add_profile_element_wrapper')
				.prev('.profile_element');
			$('.builder_viewport, .blur_desktop, .spacer_desktop, #left_menue').addClass('element_mode');
			element_mode = true;
			$('.builder_menue_top, .builder_menue_bottom, .blur_desktop').fadeOut(500);
			$('#left_menue_content').fadeIn(500);
			setTimeout(function() {
				$('.builder_menue_middle').fadeIn(1000);
			}, 500);
			disableEdit();
			filterTimeout = setTimeout(function() {
				// $('.tutorial_overlay').fadeIn(500);
				// Element-Listing und Filter reinfaden
				$('.filter_wrapper').fadeIn(1000);
			}, 500);
			elementModeTimeout = setTimeout(function() {
				//Klick auf erstes sichtbares Element triggern (abhängig von Filter und gewählter Größe)
				$('.profile_element_preview:visible')
					.first()
					.removeClass('selected');
				$('.profile_element_preview:visible')
					.first()
					.trigger('click');
				if ($('.profile_element_preview:visible').length == 0) {
					$('.profile_element_preview[data-id=1M]')
						.first()
						.removeClass('selected');
					$('.profile_element_preview[data-id=1M]').trigger('click');
				}
			}, 1500);
			setTimeout(function() {
				$('.element_listing_filter').fadeIn(500);
			}, 2500);
		}
	});

	//Abbruch des Element-Modes
	$('#abort_element_mode').on('click', function() {
		if ($(this).hasClass('disabled')) {
			return;
		}
		add_element = false;
		$('.filter_wrapper').fadeOut(0);
		$('.builder_viewport, .blur_desktop, .spacer_desktop, #left_menue').removeClass('element_mode');
		element_mode = false;
		$('.builder_menue_top, .builder_menue_bottom, .blur_desktop').fadeIn(500);
		$('#left_menue_content').fadeOut(500);
		$('.builder_menue_middle').fadeOut(0);
		$('.profile_element.previewed').remove();
		generateElementMenues();
		enableEdit();
		profile_element_width = $('.profile_element').width();
		clearTimeout(filterTimeout);
		clearTimeout(elementModeTimeout);
		setTimeout(function() {
			$('.profile_element.previewed').remove();
		}, 2000);
	});

	//Klick auf Preview-JPG in linkem Menü
	$('.profile_element_preview').on('click', function() {
		if (!$(this).hasClass('selected') && add_element == true) {
			$('.profile_element.previewed').remove();
			$('.profile_element_preview.selected').removeClass('selected');
			$(this).addClass('selected');
			profileElementContent($(this).attr('data-id'), $(this).attr('data-format'), profile_element);
		}
	});

	//Wählen eines Elements
	//Erstellt neues Element mit Content der Element-Php
	$('#choose_element').on('click', function() {
		id = $('.profile_element_preview.selected').attr('data-id');
		prev_element = $('.profile_element.previewed').prevAll('.profile_element:first');
		console.log('Click triggered');
		clearTimeout(filterTimeout);
		clearTimeout(elementModeTimeout);
		$.ajax({
			url: '/signed/php/profile_elements/raw/' + id + '.php',
			type: 'POST',
			data: {
				ajax: 'true',
				lorem_ipsum_placeholder: $('input[name=lorem_ipsum_placeholder]').val(),
				headline_placeholder: $('input[name=headline_placeholder]').val(),
				artist_name: artist_name,
				genre_string: genre_string,
				artist_city_country: artist_city_country
			},
			success: function(response) {
				$('.profile_element.previewed').after(response);
				$('.profile_element.previewed').remove();
				$('.builder_viewport, .blur_desktop, .spacer_desktop, #left_menue').removeClass('element_mode');
				element_mode = false;
				$('.builder_menue_top, .builder_menue_bottom, .blur_desktop').fadeIn(500);
				$('.builder_menue_middle, #left_menue_content').fadeOut(500);
				profileElementEditMenueScroll($(prev_element).nextAll('.profile_element')[0]);
				enableEdit();
				generateElementMenues();
				profile_element_width = $('.profile_element').width();
				//SVGs scannen
				scanPresspackRequestSVGs();
			}
		});
	});

	//Klick auf gepreviewtes Element trigger Choose element
	$(document).on('click', '.profile_element.previewed', function() {
		$('#choose_element').trigger('click');
	});
});
