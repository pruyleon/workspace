function saveLog(html) {
	$('#save_log').append('<div>' + html + '</div>');
}

function savePresskit(presskit_id, presskit_name, delete_id) {
	//Checken ob headerelement vorhanden -> Nein: Korruptes Markup
	if ($('.profile_element.header').length == 0) {
		alert('Irgendwas stimmt ganz gewaltig nicht. Bitte kontaktiere einen Admin');
		return false;
	}

	$('#loadingscreen').fadeIn(300);
	profile_element_count = $('.profile_element').length;
	$('.presskit_name').text(presskit_name);
	if (presskit_id) {
		console.warn('Save Presskit');
		//Presskit speichern
		$.ajax({
			url: '/signed/php/builder/inserts/save_presskit.php',
			type: 'POST',
			data: {
				presskit_id: presskit_id,
				user_id: user_id,
			},
			success: function (response) {
				saveProfileElements(presskit_id);
			},
		});
	} else {
		// Neues Presskit erstellen
		console.warn('Create New Presskit');
		if (delete_id) {
			deletePresskit(delete_id);
		}
		createNewPresskit(presskit_name, 'saveProfileElements');
	}
}

function saveProfileElements(presskit_id) {
	counter = 0;
	$('.profile_element').each(function () {
		current_element = $(this);
		component_id++;

		element_id = $(this).attr('data-id');
		value = [];
		config = [];
		type = [];
		icons = [];
		originals = [];
		name_info = [];
		name_size = [];
		name_color = [];
		name_type = [];
		name_uppercase = [];
		name_centered = [];
		name_uppercase = [];
		name_shadow = [];
		name_font = [];
		social_links = [];
		social_selected = [];
		logo_file_name = [];
		logo_file_size = [];
		thumbnails = [];
		titles = [];
		covers = [];
		wave_colors = [];
		file_names = [];
		headlines = [];
		text_inputs = [];

		component_names_array.forEach((input) => {
			if ($(current_element).find('input[name=' + input + ']').length > 0) {
				//saveLog('<br>');
				$(current_element)
					.find('input[name=' + input + ']')
					.each(function () {
						//saveLog(input + '<br> &emsp;  -> ' + $(this).val() + '<br>');
						if (input == 'wave_colors') {
							temp = $(this).val();
							temp.split('|');
							wave_colors.push(temp);
						} else if (input == 'file_names') {
							file_names = $(this).val().split(',');
						} else if (input == 'titles') {
							titles = $(this).val().split(',');
						} else {
							window[input].push($(this).val());
						}
					});
			} else {
				// saveLog('NOT FOUND: input[name=' + input + ']');
				window[input].push('NULL');
			}
		});

		$(current_element)
			.find('.headline_input')
			.each(function () {
				$(this).find('.placeholder').remove();
				html = $(this).html();
				html = html.replace(/(\r\n|\n|\r|\/)/gm, '');
				headlines.push(html);
			});

		$(current_element)
			.find('.textinput_textarea')
			.each(function () {
				$(this).find('.placeholder').remove();
				html = $(this).html();
				html = html.replace(/(\r\n|\n|\r|\/)/gm, '');
				text_inputs.push(html);
			});

		$.ajax({
			url: '/signed/php/builder/inserts/save_component.php',
			type: 'POST',
			data: {
				presskit_id: presskit_id,
				user_id: user_id,
				artist_id: artist_id,
				component_id: component_id,
				element_id: element_id,
				value: value,
				config: config,
				type: type,
				icons: icons,
				originals: originals,
				name_info: name_info,
				name_size: name_size,
				name_color: name_color,
				social_links: social_links,
				social_selected: social_selected,
				logo_file_name: logo_file_name,
				logo_file_size: logo_file_size,
				name_type: name_type,
				name_uppercase: name_uppercase,
				name_centered: name_centered,
				name_uppercase: name_uppercase,
				name_shadow: name_shadow,
				name_font: name_font,
				thumbnails: thumbnails,
				titles: titles,
				covers: covers,
				wave_colors: wave_colors,
				file_names: file_names,
				headlines: headlines,
				text_inputs: text_inputs,
			},
			success: function (response) {
				// saveLog(response);
				//alert(response);
				counter++;
				if (counter == profile_element_count) {
					$('#loadingscreen').fadeOut(300);
					$('.popup_background').fadeOut(300);
					//TODO: Erfolgpopup
					refreshSlotListing();
				}
			},
		});
	});
}
