$(function() {
	//Presskit-Component
	var presskit_ID = 'random';
	var component_ID = 0;
	var element_ID;
	var value;
	var type;
	var icons;
	var originals;
	var name_info;
	var name_size;
	var social_links;
	var social_selected;
	var logo_file_name;
	var logo_file_size;
	var thumbnails;
	var wave_colors;
	var file_names;
	const component_names_array = ['presskit_ID', 'component_ID', 'element_ID', 'value', 'type', 'icons', 'originals', 'name_info', 'name_size', 'social_links', 'social_selected', 'logo_file_name', 'logo_file_size', 'thumbnails', 'wave_colors', 'file_names'];

	$('#save_presskit').on('click', function() {
		/*  alert("Klick")
      //Presskit
      //Presskit-ID
      //Presskit name
      //component_ID
      //artist_id
      //last_edited
      //Presskit in DB speichern bzw wenn noch nicht vorhanden
      $.ajax(function() {
        url: '/signed/php/builder/inserts/save_presskit.php'
      });*/
		$('.profile_element').each(function() {
			component_ID++;
			element_ID = $(this).attr('data-id');
			value = [];
			type = [];
			icons = [];
			originals = [];
			name_info = [];
			name_size = [];
			social_links = [];
			social_selected = [];
			logo_file_name = [];
			logo_file_size = [];
			thumbnails = [];
			wave_colors = [];
			file_names = [];

			current_element = $(this);

			//componenten_ID
			$(current_element)
				.find('input[name=value]')
				.each(function() {
					value.push($(this).val());
				});
			for (var i = 0; i < value.length; i++) {
				value[i] = value[i].replace(',', '&comma;');
			}

			$(current_element)
				.find('input[name=type]')
				.each(function() {
					type.push($(this).val());
				});
			for (var i = 0; i < type.length; i++) {
				type[i] = type[i].replace(',', '&comma;');
			}

			$(current_element)
				.find('input[name=icons]')
				.each(function() {
					icons.push($(this).val());
				});
			for (var i = 0; i < icons.length; i++) {
				icons[i] = icons[i].replace(',', '&comma;');
			}

			$(current_element)
				.find('input[name=originals]')
				.each(function() {
					originals.push($(this).val());
				});
			for (var i = 0; i < originals.length; i++) {
				originals[i] = originals[i].replace(',', '&comma;');
			}

			$(current_element)
				.find('input[name=name_info]')
				.each(function() {
					name_info.push($(this).val());
				});
			for (var i = 0; i < name_info.length; i++) {
				name_info[i] = name_info[i].replace(',', '&comma;');
			}

			$(current_element)
				.find('input[name=name_size]')
				.each(function() {
					name_size.push($(this).val());
				});
			for (var i = 0; i < name_size.length; i++) {
				name_size[i] = name_size[i].replace(',', '&comma;');
			}

			$(current_element)
				.find('input[name=social_links]')
				.each(function() {
					social_links.push($(this).val());
				});
			for (var i = 0; i < social_links.length; i++) {
				social_links[i] = social_links[i].replace(',', '&comma;');
			}

			$(current_element)
				.find('input[name=social_selected]')
				.each(function() {
					social_selected.push($(this).val());
				});
			for (var i = 0; i < social_selected.length; i++) {
				social_selected[i] = social_selected[i].replace(',', '&comma;');
			}

			$(current_element)
				.find('input[name=logo_file_name]')
				.each(function() {
					logo_file_name.push($(this).val());
				});
			for (var i = 0; i < logo_file_name.length; i++) {
				logo_file_name[i] = logo_file_name[i].replace(',', '&comma;');
			}

			$(current_element)
				.find('input[name=logo_file_size]')
				.each(function() {
					logo_file_size.push($(this).val());
				});
			for (var i = 0; i < logo_file_size.length; i++) {
				logo_file_size[i] = logo_file_size[i].replace(',', '&comma;');
			}

			$(current_element)
				.find('input[name=thumbnails]')
				.each(function() {
					thumbnails.push($(this).val());
				});
			for (var i = 0; i < thumbnails.length; i++) {
				thumbnails[i] = thumbnails[i].replace(',', '&comma;');
			}

			$(current_element)
				.find('input[name=wave_colors]')
				.each(function() {
					wave_colors.push($(this).val());
				});
			for (var i = 0; i < wave_colors.length; i++) {
				wave_colors[i] = wave_colors[i].replace(',', '&comma;');
			}

			$(current_element)
				.find('input[name=file_names]')
				.each(function() {
					file_names.push($(this).val());
				});
			for (var i = 0; i < file_names.length; i++) {
				file_names[i] = file_names[i].replace(',', '&comma;');
			}

			//TODO: Trennzeichen in Strings ersetzen , durch &comma; ersetzen!

			value = value.join(',');
			type = type.join(',');
			icons = icons.join(',');
			originals = originals.join(',');
			name_info = name_info.join(',');
			name_size = name_size.join(',');
			social_links = social_links.join(',');
			social_selected = social_selected.join(',');
			logo_file_name = logo_file_name.join(',');
			logo_file_size = logo_file_size.join(',');
			thumbnails = thumbnails.join(',');
			wave_colors = wave_colors.join(',');
			file_names = file_names.join(',');

			$.ajax({
				url: '/signed/php/builder/inserts/save_component.php',
				type: 'POST',
				data: {
					presskit_ID: presskit_ID,
					component_ID: component_ID,
					element_ID: element_ID,
					value: value,
					type: type,
					icons: icons,
					originals: originals,
					name_info: name_info,
					name_size: name_size,
					social_links: social_links,
					social_selected: social_selected,
					logo_file_name: logo_file_name,
					logo_file_size: logo_file_size,
					thumbnails: thumbnails,
					wave_colors: wave_colors,
					file_names: file_names
				},
				success: function(response) {
					console.log(response);
				}
			});
		});

		//Presskit gespeichert!
	});
});
