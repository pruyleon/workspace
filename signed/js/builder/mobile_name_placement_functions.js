/**
 * checkPositionMobileName - Checkt ob der Namen irgendwo übersteht und justiert falls wahr die position
 *
 * @return {boolean}  true
 */
function checkPositionMobileName() {
	//Check ob rechts außerhalb
	if ($('#mobile_name_wrapper').position().left + $('#mobile_name_wrapper').outerWidth() > $('.mobile_element_content>.placement_border').position().left + $('.mobile_element_content>.placement_border').outerWidth() && $('.mobile_element_content>.placement_border').outerWidth() > 10) {
		$('#mobile_name_wrapper').css('left', ($('.mobile_element_content>.placement_border').outerWidth() - $('#mobile_name_wrapper').outerWidth()) * 0.9);
	}
	//Checken ob unten Raus!
	if ($('#mobile_name_wrapper').position().top + $('#mobile_name_wrapper').outerHeight() > $('.mobile_element_content>.placement_border').position().top + $('.mobile_element_content>.placement_border').outerHeight()) {
		$('#mobile_name_wrapper').css('top', $('.mobile_element_content>.placement_border').position().top + $('.mobile_element_content>.placement_border').outerHeight() - ($('#mobile_name_wrapper').outerHeight() + 10));
	}
	return true;
}

/**
 * resizeMobileLogo - Ändert die größe des Logos im Header
 *
 * @param  {int} multiplicator Multiplikator nach dem Vergrößert wird
 * @return {boolean}      true
 */
function resizeMobileLogo(multiplicator) {
	width_multiplicator = 20 * multiplicator;
	if (width_multiplicator > 100) {
		width_multiplicator = 100;
	}

	height_multiplicator = 20 * multiplicator;
	if (height_multiplicator > 100) {
		height_multiplicator = 100;
	}

	$('#mobile_name_wrapper').css({
		'max-width': width_multiplicator + '%',
		'max-height': height_multiplicator + '%',
		'min-width': 'unset'
	});
	$('input[name=mobile_logo_multiplicator]').val(multiplicator);
	$('#mobile_name_wrapper').draggable('option', 'containment', $('.mobile_element_content>.placement_border'));
	width = $('#mobile_name_wrapper').width();
	checkPositionMobileName();
	return true;
}
