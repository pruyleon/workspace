/**
 * @description enableMobileEdit - Mobilen Bearbeitungsmodus starten
 * @return {boolean} true
 */
function disableMobileEdit() {
	// disableEdit();
	// $('.edit_disable_fadeout')
	// 	.stop()
	// 	.fadeOut(500);
	//Blaue Hoverrahmen
	$('.hover_blue_inlineshadow').each(function() {
		$(this).addClass('hover_blue_inlineshadow_disabled');
		$(this).removeClass('hover_blue_inlineshadow');
	});
	$('#mobile_photo_edit_icn').addClass('disabled');
	$('#mobile_photo_edit_icn').fadeOut(200);
	$('.mobile_element_menue').addClass('deep_hide');
	$('#mobile_name_wrapper').addClass('disabled');
	$('.reposition_name_mobile').addClass('disabled');
	$('#mobile_name_wrapper').removeClass('hover_blue_inlineshadow');
	return true;
}

/**
 * enableMobileEdit2 - Mobilen Bearbeitungsmodus beenden
 * @return {boolean} true
 */
function enableMobileEdit() {
	// enableEdit();
	$('.edit_disable_fadeout')
		.stop()
		.fadeIn(500);
	//Blaue Hoverrahmen
	$('.hover_blue_inlineshadow_disabled').each(function() {
		$(this).removeClass('hover_blue_inlineshadow_disabled');
		$(this).addClass('hover_blue_inlineshadow');
	});
	$('#mobile_photo_edit_icn').removeClass('disabled');
	$('.mobile_element_menue').removeClass('deep_hide');
	$('#mobile_photo_edit_icn').fadeIn(200);
	$('.mobile_element_menue').fadeIn(200);
	$('#mobile_name_wrapper').removeClass('disabled');
	$('.reposition_name_mobile').removeClass('disabled');
	$('#mobile_name_wrapper').addClass('hover_blue_inlineshadow');
	return true;
}
