$(function() {
	const mobile_name_pickr = new Pickr({
		el: '#mobile_name_colorpicker',
		default: '#42445A',
		inline: true,
		swatches: colorpicker_swatches,
		showAlways: true,
		components: {
			preview: false,
			opacity: false,
			hue: true,
			interaction: {
				hex: true,
				rgba: false,
				hsva: false,
				input: true,
				clear: false,
				save: false
			}
		}
	});

	mobile_name_pickr.on('change', function() {
		$('#mobile_name_wrapper').css(
			'color',
			$('#mobile_name')
				.find('.pcr-result')
				.val()
		);
	});

	//Namensplatzierung
	//reposition_name_mobile
	$(document).on('click', '.reposition_name_mobile:not(.disabled)', function() {
		$('#mobile_name_wrapper').draggable({
			cursor: 'move',
			containment: '.mobile_element_content>.placement_border'
		});
		if (mobile_drag_initialized) {
			$('#mobile_name_wrapper').draggable('enable');
		}

		disableMobileEdit();
		$('#mobile_name_wrapper').addClass('active_drag');
		$('.mobile_element_content').addClass('active_placement');
		$('.mobile_element_content>.placement_border').addClass('active_placement');

		//Name edit Menue
		$('#mobile_edit_menue>.full_absolute').fadeOut();
		$('#mobile_edit_menue>#mobile_name').fadeIn(0);
		setTimeout(function() {
			$('#mobile_edit_menue').fadeIn(500);
		}, 500);
	});

	$(document).on('click', '#mobile_name_wrapper:not(.active_drag)', function() {
		$('.reposition_name_mobile').trigger('click');
	});

	// Name / Logo Select
	$('select[name=mobile_name_selector]').val('1');
	$('select[name=mobile_name_selector]').change(function() {
		if ($(this).val() == 0) {
			$('#mobile_name_wrapper').fadeOut();
			$('.inner_edit.mobile_name').fadeOut();
			$('.inner_edit.mobile_logo').fadeOut();
		} else {
			$('#mobile_name_wrapper').fadeIn();
			if ($(this).val() == 1) {
				$('#mobile_name_wrapper>.logo').fadeOut(function() {
					//'max-height': '10%',
					$('#mobile_name_wrapper').css({
						'max-width': 'unset',
						'min-width': 'max-content'
					});
					$('#mobile_name_wrapper>.name').fadeIn();
				});
				$('.inner_edit.mobile_logo').fadeOut(function() {
					$('.inner_edit.mobile_name').fadeIn();
				});
			} else if ($(this).val() == 2) {
				$('#mobile_name_wrapper>.name').fadeOut(function() {
					resizeMobileLogo(parseFloat(parseFloat($('input[name=mobile_logo_multiplicator]').val()).toFixed(1)));
					$('#mobile_name_wrapper>.logo').fadeIn();
				});
				$('.inner_edit.mobile_name').fadeOut(function() {
					$('.inner_edit.mobile_logo').fadeIn();
				});
			}
		}
	});
	$('select[name=mobile_name_selector]').trigger('change');

	$('#mobile_name_uppercase').on('click', function() {
		if ($(this).attr('data-state') == 'uppercase') {
			$(this).attr('data-state', 'lowercase');
			$('#mobile_name_wrapper>.name').text(artist_name);
		} else {
			$(this).attr('data-state', 'uppercase');
			$('#mobile_name_wrapper>.name').text(artist_name.toUpperCase());
		}
	});

	$('#make_mobile_name_shadow').on('click', function() {
		if ($(this).attr('data-state') == 'NULL') {
			$(this).attr('data-state', 'shadow');
			$('#mobile_name_wrapper').addClass('text_shadow');
		} else {
			$(this).attr('data-state', 'NULL');
			$('#mobile_name_wrapper').removeClass('text_shadow');
		}
	});

	$('input[name=mobile_namesize]').val(56);
	$('.mobile_name_resize_button').on('click', function() {
		resize_action = $(this).attr('data-action');
		name_size = parseInt($('input[name=mobile_namesize]').val());
		if (resize_action == 'increase') {
			name_size += 7;
		} else {
			name_size -= 7;
		}
		if (name_size > 84) {
			name_size = 84;
		} else if (name_size < 14) {
			name_size = 14;
		}
		$('#mobile_name_wrapper').attr('data-font-size', name_size);
		$('input[name=mobile_namesize]').val(name_size);
		checkPositionMobileName();
	});

	$('#mobile_name_color').on('click', function(e) {
		e.stopPropagation();
		if (
			$('#mobile_name')
				.find('.picker')
				.attr('visible') == 'true'
		) {
			$('#mobile_name')
				.find('.picker')
				.removeClass('visible');
			$('#mobile_name')
				.find('.picker')
				.attr('visible', 'false');
		} else {
			$('#mobile_name')
				.find('.picker')
				.addClass('visible');
			$('#mobile_name')
				.find('.picker')
				.attr('visible', 'true');
		}
	});

	// Logo
	$('#upload_mobile_logo').on('click', function() {
		edit_trigger = 'mobile_logo_placement';
		$('#file_archive_popup').fadeIn();
	});

	$('#descrease_mobile_logo_size').on('click', function() {
		multiplicator = parseFloat(parseFloat($('input[name=mobile_logo_multiplicator]').val()).toFixed(1));
		if (multiplicator < 1) {
			multiplicator -= 0.1;
		} else {
			multiplicator -= 0.3;
		}
		if (multiplicator < 0.2) {
			multiplicator = 0.2;
		}
		resizeMobileLogo(multiplicator);
	});

	$('#inscrease_mobile_logo_size').on('click', function() {
		multiplicator = parseFloat(parseFloat($('input[name=mobile_logo_multiplicator]').val()).toFixed(1));
		if (multiplicator < 0.2) {
			multiplicator = 0.2;
		}
		if (multiplicator < 1) {
			multiplicator += 0.1;
		} else if (multiplicator <= 5) {
			multiplicator += 0.3;
		}
		if (multiplicator > 5) {
			multiplicator = 5;
		}
		resizeMobileLogo(multiplicator);
	});

	$('#save_mobile_name').on('click', function() {
		$('#mobile_edit_menue>.full_absolute').fadeOut();
		$('#mobile_edit_menue>#mobile_name').fadeOut(500);
		$('#mobile_edit_menue').fadeOut(500);
		$('#mobile_name_wrapper').removeClass('active_placement');
		$('#mobile_name_wrapper').removeClass('active_drag');
		$('#mobile_name_wrapper').draggable('disable');
		$('.mobile_element_content').removeClass('active_placement');
		$('.mobile_element_content>.placement_border').removeClass('active_placement');
		//$('#mobile_name_wrapper').removeClass('ui-draggable ui-draggable-handle ui-draggable-disabled');
		mobile_drag_initialized = true;
		enableMobileEdit();
	});
});
