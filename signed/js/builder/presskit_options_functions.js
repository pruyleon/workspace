/**
 * @description getTemplateID - liefert tempalteID des geklickten Templates zurück
 * @param {*} clicked geklicktes Child des template_wrappers
 * @returns
 */
function getTemplateID(clicked) {
	return $(clicked).closest('.template_wrapper').attr('data-templateid');
}

/**
 * @description getTemplateID - liefert tempaltename des geklickten Templates zurück
 * @param {*} clicked geklicktes Child des template_wrappers
 * @returns
 */
function getTemplateName(clicked) {
	return $(clicked).closest('.template_wrapper').attr('data-templatename');
}

/**
 * @description openPresskitFromTemplate - öffnet Template Popup
 * @param {*} template_name
 * @param {*} template_id
 * @returns boolean - neu geöffnet oder nicht
 */
function openPresskitFromTemplate(template_name, template_id) {
	if (checkIfSaved()) {
		openPresskittemplate(template_name, template_id);
	} else {
		// Änderungen speichern Popup
		showSaveChangesPopup();
		// Speichern unter
		return false;
	}
	return true;
}

function openPresskittemplate(template_name, template_id) {
	window.location.href = '/builder/' + template_name;
	return true;
}

/**
 * @description
 * @returns
 */

function freeSaveSlots() {
	$.ajax({
		url: '/signed/php/misc/get_free_save_slots.php',
		type: 'POST',
		data: {
			user_id: user_id,
			artist_id: artist_id,
		},
		success: function (response) {
			free_save_slots = parseInt(response);
			console.log('Freie Slots: ' + free_save_slots);
			return free_save_slots;
		},
	});
}
function createNewPresskit(presskit_name, operation) {
	$.ajax({
		url: '/signed/php/builder/inserts/create_new_presskit.php',
		type: 'POST',
		data: {
			user_id: user_id,
			artist_id: artist_id,
			presskit_name: presskit_name
		},
		success: function (response) {
			//Presskit erstellt response = ID des Presskits
			// alert(response);
			if (operation == 'saveProfileElements') {
				saveProfileElements(response)
			}
			//Globale Variable!
			presskit_id = response;
			return response;
		},
	});
}

/**
 * @description checkIfSaved - Checkt ob das Presskit ungespeicherte Änderungen beinhält
 * @returns
 */
function checkIfSaved() {
	return presskitIsSaved;
}

/**
 * @description - Anzeigen des Template Popups
 * @returns
 */
function showTemplatePopup() {
	$('#choose_template_popup').fadeIn(300);
	return true;
}

/**
 * @description - Anzeigen des "Änderungen speichern?" Popups
 * @returns
 */
function showSaveChangesPopup() {
	$('#save_changes_popup').fadeIn(300);
	return true;
}

function discardChanges() {
	if (presskit_option == 'new_presskit') {
		openPresskittemplate(template_name, template_id);
	} else {
		alert('Soweit bin ich noch nicht maxim...');
	}
	return true;
}

function presskitNotSaved() {
	presskitIsSaved = false;
}

function startSaveAs() {
	if ($('.popup_background:visible').length > 0) {
		$('.popup_background:visible').fadeOut(500, function () {
			$('#save_as_popup').fadeIn(500);
		});
	} else {
		$('#save_as_popup').fadeIn(500);
	}
}

function startOverwrite() {
	$('.popup_background:visible').fadeOut(500, function () {
		$('#overwrite_slot_popup').fadeIn(500);
	});
}

function deletePresskit(delete_id) {
	$.ajax({
		url: '/signed/php/builder/inserts/delete_presskit.php',
		type: 'POST',
		data: {
			presskit_id: delete_id,
			artist_id: artist_id,
			user_id: user_id
		},
		success: function (response) {
			// alert(response);
		},
	});
}

function refreshSlotListing(){
	$.ajax({
		url: '/signed/php/builder/incl/presskit_slots_listing.php',
		type: 'POST',
		data: {
			artist_id: artist_id,
			wrapper_class: "",
			row_class: "",
			slot_class: ""
		},
		success: function (response) {
			$('#slot_overwrite_listing').html(response);
		},
	});
}