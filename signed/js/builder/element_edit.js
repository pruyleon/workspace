$(function () {
	//Text-Inputs beschneiden
	ellipsizeTextBox('textinput_textarea');

	//Disabled-Klasse Klick auf Links unterdrücken
	$('a').click(function (e) {
		e.preventDefault();
		if ($(this).hasClass('disabled')) {
			return false;
		} else {
			window.location.href = $(this).attr('href');
		}
	});

	setTimeout(function () {
		namePositionRelative();
	}, 1000);

	//Footer-Filter wenn nur Header im Presskit und noch nicht im Elementmode gewesen +  Blurklasse blur  auf carousel_footer_wrapper
	if ($('.profile_element').length == 1) {
		footer_filter = true;
		$('.footer_filter').fadeIn(100);
		$('.carousel_footer_wrapper').addClass('blur');
	} else {
		footer_filter = false;
	}

	// Bestimmt welche Art von Builder-Icon geklickt wurde und triggert entsprechende Funktion
	// data_action = media || background
	// data_type = audio || photo || slideshow || video || color
	$(document).on('click', '.builder_icon', function (e) {
		parent_id = getParentsElementDataID($(this));
		data_type = getDataType($(this));
		data_action = $(this).attr('data-action');
		clicked_icon = $(this);
		//Element in Mitte Scrollen
		if (data_type == 'photo' || data_type == 'color') {
			timeout = profileElementEditMenueScroll(clicked_icon);
		} else {
			profileElementMidScroll(clicked_icon);
		}
		if (data_action == 'media') {
			cropper_div = $(this).closest('.content');
			if (data_type == 'photo') {
				$('#file_archive_popup').fadeIn();
				edit_trigger = 'photo';
			} else if (data_type == 'slideshow') {
				$('#' + data_type + '_popup').fadeIn();
				edit_trigger = 'slideshow';
				slideshow_div = $(this).closest('.content');
				format = $(clicked_icon).closest('.format_parent').attr('data-format');
				$('#slideshow_popup').attr('data-format', format);
				$('.slideshow_element').each(function () {
					$(this).attr('data-format', format);
					$(this).parent().attr('data-format', format);
				});
				clearSlideshow('NULL');
			} else if (data_type == 'audio') {
				clearAudioPopup();
				//Format in Popup übertragen
				coverformat = $(clicked_icon).closest('.audioplayer_parent').attr('data-coverformat');
				$('#audio_popup').attr('data-coverformat', coverformat);
				$('.audio_wrapper:first').trigger('click');
				//Audio-Popup leeren
				audio_type = $(this).closest('audio_element_wrapper').attr('data-audiotype');
				$('#' + data_type + '_popup').fadeIn();
			} else if (data_type == 'video') {
				//Video-Popup leeren
				$('#video_thumbnail').addClass('empty');
				$('#video_thumbnail').removeClass('was_empty');
				$('#video_thumbnail').attr('style', '');
				$('input[name=video_file_name]').val('');
				$('input[name=video_thumbnail_file]').val('');
				$('input[name=video_thumbnail_base64]').val('');
				$('#upload_video').html(video_upload_html);
				$('#upload_video').addClass('empty');
				$('#video_thumbnail').children('.video_menue').remove();
				$('.video_popup_mode_toggle[data-type=video]').trigger('click');

				format = $(this).closest('.format_parent').attr('data-format');
				$('#video_popup').attr('data-format', format);
				$('#' + data_type + '_popup').fadeIn();
				video_div = $(this).closest('.video_div');
			} else {
				gif_background = $(this).closest('.content');
				$('#' + data_type + '_link').val(getInputValueVal($(this)));
				$('#' + data_type + '_popup').fadeIn();
			}
		} else if ($(this).attr('data-action') == 'background') {
			if (data_type == 'color') {
				if ($(this).closest('.background').length) {
					pickr_background = $(this).closest('.background');
				} else {
					pickr_background = $(this).closest('.profile_element_content').children('.background');
				}
				if ($(clicked_icon).closest('.profile_element').find('.background_filter').length) {
					$(clicked_icon).closest('.profile_element').find('.background_filter').fadeOut();
				}
				clearEditMenue();
				disableEdit();
				setTimeout(function () {
					$('.edit_menue#color').fadeIn(0);
					openEditMenue();
				}, timeout);
				saveIcons(clicked_icon);
			} else if (data_type == 'photo') {
				$('#file_archive_popup').fadeIn();
				edit_trigger = 'photo';
				cropper_div = $(this).closest('.background');
			} else if (data_type == 'slideshow') {
				$('#' + data_type + '_popup').fadeIn();
				edit_trigger = 'slideshow';
				slideshow_div = $(this).closest('.background');
				format = $(clicked_icon).closest('.format_parent').attr('data-format');
				$('#slideshow_popup').attr('data-format', format);
				$('.slideshow_element').each(function () {
					$(this).attr('data-format', format);
					$(this).parent().attr('data-format', format);
				});
				cropper_div = $(this).closest('.profile_element_content').children('.full_size_background');
				clearSlideshow('NULL');
			} else if (data_type == 'video') {
				format = $(this).closest('.menue_parent').attr('data-format');
				$('#video_popup').attr('data-format', format);
				//Video-Popup leeren
				$('#video_thumbnail').addClass('empty').css('background-image', '');
				$('#upload_video').html(video_upload_html);
				$('#upload_video').addClass('empty');
				$('input[name=video_file_name]').val('');
				$('input[name=video_thumbnail_base64]').val('');
				$('#' + data_type + '_popup').fadeIn();
				$('.video_popup_mode_toggle[data-type=video]').trigger('click');
				video_div = $(this).closest('.video_div');
			}
		}
	});

	//Bearbeiten Menüs Elemente und Componenten
	// action = edit || delete || position || upload
	// type = audio || photo || slideshow || video || color
	$(document).on('click', '.element_menue>.dropdown>.dropdown_line', function () {
		action = $(this).attr('data-action');
		menue_parent = $(this).closest('.menue_parent');
		input_parent = $(this).closest('.input_parent');
		type = $(input_parent).children('input[name=type]').val();
		data = $(input_parent).children('input[name=value]').val();
		icons = $(input_parent).children('input[name=icons]').val();
		original = $(input_parent).children('input[name=originals]').val();
		clicked_icon = $(this);
		if (action == 'edit') {
			profileElementEditMenueScroll(clicked_icon);
			if (type == 'color') {
				if ($(this).closest('.input_parent').hasClass('background')) {
					pickr_background = $(this).closest('.background');
				} else if ($(this).closest('.menue_parent').parent().hasClass('background')) {
					// NOTE: Notwendig?
					pickr_background = $(this).parent().closest('.menue_parent');
				} else {
					pickr_background = $(this).closest('.input_parent').find('.background');
				}
				if ($(clicked_icon).closest('.profile_element').find('.background_filter').length) {
					$(clicked_icon).closest('.profile_element').find('.background_filter').fadeOut();
				}
				openEditMenue();
				$('.edit_menue#color').fadeIn();
				$('.pcr-result').val(data);
				pickr.setColor(data, true);
			} else if (type == 'slideshow') {
				if ($(this).closest('.menue_parent').find('.content').length) {
					slideshow_div = $(this).closest('.menue_parent').find('.content');
				} else if ($(this).closest('.menue_parent').children('.background').length > 0) {
					// NOTE: Notwendig?
					slideshow_div = $(this).closest('.menue_parent').children('.background');
				} else {
					slideshow_div = $(this).closest('.menue_parent').find('.background');
				}
				format = $(clicked_icon).attr('data-format');
				$('#slideshow_popup').attr('data-format', format);
				data = data.split('|');
				original = original.split('|');
				$('.slideshow_element').each(function () {
					$(this).attr('data-format', format);
					$(this).parent().attr('data-format', format);
				});
				clearSlideshow(slideshow_div);
				//Slideshow Popup einfaden
				$('#slideshow_popup').fadeIn();
			} else if (type == 'audio') {
				input_parent = $(this).closest('.input_parent');
				//Format in Popup übertragen
				coverformat = $(this).closest('.audioplayer_parent').attr('data-coverformat');
				$('#audio_popup').attr('data-coverformat', coverformat);
				//Daten aus Inputs holen und Arrays spliten
				titles = $(input_parent).find('input[name=titles]').val().split(',');
				covers = $(input_parent).find('input[name=covers]').val().split('|');
				wave_colors = $(input_parent).find('input[name=wave_colors]').val().split('|');
				file_names = $(input_parent).find('input[name=file_names]').val();
				file_names = file_names.split(',');
				//console.log("Titles: " + titles + "\n Wave_colos: " + wave_colors + "\n file Names: " + file_names);
				i = 0;
				//Daten in Popup übertragen
				$('#audio_popup * > .audio_wrapper').each(function () {
					if (titles[i] && titles[i].length >= 1) {
						$(this).removeClass('empty');
						$(this).children('input[name=cover]').val(covers[i]);
						$(this).children('input[name=file_name]').val(file_names[i]);
						$(this).children('input[name=audio_name]').val(file_names[i]);
						$(this).children('input[name=title]').val(titles[i]);
						$(this).children('input[name=wave_color]').val(wave_colors[i]);
						$(this)
							.children('.audio')
							.css('background-image', 'url(' + covers[i] + ')');
					} else {
						$(this).addClass('empty');
						$(this).children('input[name=cover]').val('');
						$(this).children('input[name=file_name]').val('');
						$(this).children('input[name=audio_name]').val('');
						$(this).children('input[name=title]').val('');
						$(this).children('input[name=wave_color]').val('');
						$(this).children('.audio').attr('style', '');
					}
					i++;
				});
				//File-Inputs leeren
				$('#audio_file_1').val('');
				$('#audio_file_2').val('');
				$('#audio_file_3').val('');
				$('#audio_file_4').val('');
				$('#audio_pic_1').val('');
				$('#audio_pic_2').val('');
				$('#audio_pic_3').val('');
				$('#audio_pic_4').val('');
				//Klick auf ersten .audio_wrapper simulieren
				$('.audio_wrapper[data-position=1]').trigger('click');
				//Popup reinfaden
				$('#audio_popup').fadeIn();
				$('.audio_wrapper');
			} else if (type == 'video') {
				video_div = $(clicked_icon).closest('.menue_parent').find('.video_div');
				thumbnails = $(input_parent).children('input[name=thumbnails]').val();
				format = $(clicked_icon).closest('.menue_parent').attr('data-format');
				if (data.length > 30) {
					video_name = data.substring(0, 27) + '...';
				} else {
					video_name = data;
				}
				$('#video_popup').attr('data-format', format);
				$('#video_popup').find('input[name=video_file_name]').val(data);
				$('#video_popup').find('input[name=video_thumbnail_base64]').val(thumbnails);
				$('#video_thumbnail').css('background', 'url(' + thumbnails + ')');
				if (thumbnails.length < 1) {
					$('#video_thumbnail').addClass('empty');
				} else {
					$('#video_thumbnail').removeClass('empty');
				}
				$('#video_thumbnail').removeClass('was_empty');
				$('#upload_video').html(video_name + '<img src="/signed/src/icns/trash.svg" id="delete_video">');
				$('#upload_video').removeClass('empty');
				$('.video_popup_mode_toggle[data-type=video]').trigger('click');

				makeVideoMenue();
				disableVideoEdit();
				$('#video_popup').fadeIn();
			} else if (type == 'gif') {
				if ($(menue_parent).find('.background').length !== 0) {
					gif_background = $(input_parent).find('.background');
				} else {
					gif_background = $(input_parent).find('.content');
				}
				$('#gif_popup').fadeIn(250);
			}
		} else if (action == 'delete') {
			if (type == 'color') {
				color_html = '';
				if ($(clicked_icon).closest('.input_parent').hasClass('background')) {
					pickr_background = $(clicked_icon).closest('.background');
				} else {
					pickr_background = $(clicked_icon).closest('.input_parent').find('.background');
				}
				icons = icons.replace('visibility: hidden', '');
				//restoreIcons(pickr_background);
				$(pickr_background).attr('style', ' ');
				$(input_parent).children('input[name=type]').val('');
				$(input_parent).children('input[name=value]').val('');
				html = $(input_parent).find('.icon_wrapper').html();

				$(pickr_background).html(icons);

				$(pickr_background).find('.builder_icon').attr('style', ' ');

				if ($(pickr_background).closest('.menue_parent').children('.element_menue').find('.switch_color').length) {
					color_switcher = $(pickr_background).closest('.menue_parent').children('.element_menue').find('.switch_color').outerHTML();
				} else {
					$(pickr_background).closest('.menue_parent').children('.element_menue').html('');
					color_switcher = false;
				}
				if (color_switcher) {
					$(pickr_background)
						.closest('.menue_parent')
						.children('.element_menue')
						.html('<div class="action_wrapper" data-type"">' + color_switcher + '</div>');
				}
			} else if (type == 'slideshow') {
				icons = icons.replace('style="visibility: hidden;"', '');
				$(input_parent).children('.content').attr('style', '');
				$(input_parent).children('.slick-initialized').addClass('hover_blue_inlineshadow');
				$(input_parent).children('.slick-initialized').removeClass('slick-slider');
				$(input_parent).children('.slick-initialized').removeClass('slick-initialized');
				$(input_parent).children('input[name=type]').val('');
				$(input_parent).children('input[name=originals]').val('');
				$(input_parent).children('input[name=value]').val('');
				if ($(input_parent).children('.content').length) {
					$(input_parent).children('.content').html(icons);
				} else if ($(input_parent).children('.background').length) {
					$(input_parent).children('.background').html(icons);
				}
				//$(input_parent).children('.content').html(icons);
			} else if (type == 'picture') {
				cropper_div = $(this).closest('.menue_parent').children('.content');
				removeBackground(cropper_div);
				cropper_div = $(this).closest('.menue_parent').children('.background');
				removeBackground(cropper_div);
				if ($(clicked_icon).closest('.input_parent').children('.content').length > 0) {
					restoreIcons($(clicked_icon).closest('.input_parent').children('.content'));
				}
				if ($(clicked_icon).closest('.input_parent').children('.background').length > 0) {
					restoreIcons($(clicked_icon).closest('.input_parent').children('.background'));
				} else {
					restoreIcons($(clicked_icon).closest('.input_parent'));
				}
			} else if (type == 'audio') {
				icons = icons.replace('style="visibility: hidden;"', '');
				playerparent = $(clicked_icon).closest('.menue_parent').children('.audioplayer_parent');
				audio_type = $(playerparent).attr('data-audiotype');
				audio_class = $(playerparent).attr('data-audioclass');
				if (audio_type == 'audio_full_cover') {
					$(playerparent).closest('.profile_element').find('.background_filter').fadeOut();
					$(playerparent).closest('.menue_parent').find('.element_menue').html('');
				} else {
					$(clicked_icon).closest('.inner_profile_element_content').find('.full_size.background').attr('style', ' ');
					$(playerparent).parent().find('.background_filter').remove();
					$(playerparent).addClass('hover_blue_inlineshadow');
				}
				if (audio_type == 'audio_full_cover') {
					audio_url = 'audio_cover';
				} else {
					audio_url = audio_type;
					icons = '<div class="full_size background hover_blue_inlineshadow">' + icons + '</div>';
				}
				$.ajax({
					url: '/signed/php/profile_elements/raw/comps/' + audio_url + '.php',
					type: 'POST',
					data: {
						audio_class: audio_class,
					},
					success: function (response) {
						player = response;
						$(playerparent).html(icons + response);
					},
				});
			} else if (type == 'video') {
				icons = icons.replace('style="visibility: hidden;"', '');
				$(input_parent).children('.content').attr('style', '');
				$(input_parent).children('input[name=type]').val('');
				$(input_parent).children('input[name=originals]').val('');
				$(input_parent).children('input[name=value]').val('');
				$(input_parent).children('.content').html(icons);
				$(input_parent).children('.element_menue').html('');
			} else if (type == 'gif') {
				gif_background = $(this).closest('.menue_parent').children('.content');
				removeBackground(gif_background);
				gif_background = $(this).closest('.menue_parent').children('.background');
				removeBackground(gif_background);
				if ($(clicked_icon).closest('.input_parent').children('.content').length > 0) {
					restoreIcons($(clicked_icon).closest('.input_parent').children('.content'));
				}
				if ($(clicked_icon).closest('.input_parent').children('.background').length > 0) {
					restoreIcons($(clicked_icon).closest('.input_parent').children('.background'));
				} else {
					restoreIcons($(clicked_icon).closest('.input_parent'));
				}
			}

			//Header Menue cleanen
			if ($(menue_parent).children('.element_menue').hasClass('header')) {
				restoreHeaderMenue();
			}
		} else if (action == 'position') {
			profileElementEditMenueScroll(clicked_icon);
			if ($(clicked_icon).closest('.profile_element').find('.background_filter').length) {
				$(clicked_icon).closest('.profile_element').find('.background_filter').fadeOut();
			}
			recently_uploaded = $(input_parent).children('input[name=originals]').val();
			if ($(input_parent).find('.background').length !== 0) {
				cropper_div = $(input_parent).find('.background');
			} else {
				cropper_div = $(input_parent).find('.content');
			}
			$('#photo_cropper_trigger').trigger('click');
		} else if (action == 'upload') {
			edit_trigger = 'photo';
			if ($(menue_parent).find('.background').length !== 0) {
				cropper_div = $(input_parent).find('.background');
			} else {
				cropper_div = $(input_parent).find('.content');
			}
			$('#file_archive_popup').fadeIn();
		}
		$(this).closest('.dropdown').fadeOut();
	});

	//Colorpicker-Instanzen
	//Hintergrundfarbe
	const pickr = new Pickr({
		el: '#colorpicker',
		default: '#42445A',
		inline: true,
		swatches: colorpicker_swatches,
		showAlways: true,
		components: {
			preview: false,
			opacity: false,
			hue: true,
			interaction: {
				hex: true,
				rgba: false,
				hsva: false,
				input: true,
				clear: false,
				save: true,
			},
		},
		strings: {
			save: 'Übernehmen',
		},
	});

	//Textfarbe
	const text_pickr = new Pickr({
		el: '#text_colorpicker',
		default: '#42445A',
		inline: true,
		swatches: colorpicker_swatches,
		showAlways: true,
		components: {
			preview: false,
			opacity: false,
			hue: true,
			interaction: {
				hex: true,
				rgba: false,
				hsva: false,
				input: true,
				clear: false,
				save: false,
			},
		},
	});

	const headline_pickr = new Pickr({
		el: '#headline_colorpicker',
		default: '#42445A',
		inline: true,
		swatches: colorpicker_swatches,
		showAlways: true,
		components: {
			preview: false,
			opacity: false,
			hue: true,
			interaction: {
				hex: true,
				rgba: false,
				hsva: false,
				input: true,
				clear: false,
				save: false,
			},
		},
	});

	const audio_wave_pickr = new Pickr({
		el: '#audio_wave_colorpicker',
		appClass: 'audio_wave_colorpicker',
		default: '#42445A',
		swatches: colorpicker_swatches,
		showAlways: false,
		inline: true,
		components: {
			preview: false,
			opacity: false,
			hue: true,
			interaction: {
				hex: true,
				rgba: false,
				hsva: false,
				input: true,
				clear: false,
				save: false,
			},
		},
	});

	$('.picker_buttons>button').on('click', function () {
		$(this).closest('.picker').removeClass('visible');
	});

	/*AUDIO-HANDLING*/
	$(document).on('click', '.audio_element_wrapper.empty', function () {
		$(this).closest('.audioplayer_parent').find('.builder_icon[data-type=audio]').trigger('click');
	});

	$('.sortable_audios').sortable({
		stop: function () {
			position = 1;
			$('.audio_wrapper').each(function () {
				$(this).attr('data-position', position);
				$(this).find('.position_ind').text(position);
				position++;
			});
			//Position des Aktiven
			$('.audio_upload_wrapper').attr('data-active', $('.audio_wrapper.active').attr('data-position'));
		},
	});
	$('.sortable_audios').disableSelection();
	active_audio = 1;
	$('input[name=audio_title]').on('keyup', function () {
		active_audio = $('.audio_upload_wrapper').attr('data-active');
		title = $(this).val();
		if (title.length > 0) {
			$('.audio_wrapper[data-position=' + active_audio + ']')
				.find('.title')
				.text(title);
		} else {
			$('.audio_wrapper[data-position=' + active_audio + ']')
				.find('.title')
				.text('Titel');
		}
	});

	$('.audio_wrapper').on('click', function () {
		$('.audio_wrapper.active').removeClass('active');
		//Werte in Vorheriges Schreiben
		title = $('.audio_upload_wrapper').find('input[name=audio_title]').val();
		audio_name = $('.audio_upload_wrapper').find('.audio_title').val();
		$('.audio_wrapper[data-position=' + active_audio + ']')
			.find('input[name=audio_name]')
			.val(audio_name);
		if (title.length > 0) {
			$('.audio_wrapper[data-position=' + active_audio + ']')
				.find('.title')
				.text(title);
		}
		$('.audio_wrapper[data-position=' + active_audio + ']')
			.find('input[name=title]')
			.val(title);

		//Werte aus geklickten in upload_wrapper übernehmen
		active_audio = $(this).attr('data-position');
		audio_name = $(this).find('input[name=audio_name]').val();
		title = $(this).find('input[name=title]').val();
		cover = $(this).find('input[name=cover]').val();
		//Uploadstatus
		$('#upload_audio>span').fadeOut(0);
		$('#upload_audio>#audio_upload_status_' + active_audio).fadeIn(100);
		//Wavecolor
		wave_color = $(this).find('input[name=wave_color]').val();
		wave_color = wave_color.split(',');
		wave_color_left = wave_color[0];
		wave_color_right = wave_color[1];
		active_side = 'left';
		audio_wave_pickr.setColor(wave_color_left);
		$('.audio_wave').find('.left').find('svg').children('g').children('g').children('g').attr('fill', wave_color_left);
		$('.audio_wave').find('.right').find('svg').children('g').children('g').children('g').attr('fill', wave_color_right);
		$('.audio_upload_wrapper').attr('data-active', $(this).attr('data-position'));
		$('.audio_upload_wrapper').find('input[name=audio_name]').val(audio_name);
		$('.audio_upload_wrapper').find('input[name=title]').val(title);
		$('.audio_upload_wrapper').find('input[name=audio_title]').val(title);
		if (cover != '' && cover) {
			$('.audio_cover').css('background-image', 'url(' + cover + ')');
			$('.audio_upload_wrapper').removeClass('empty');
			//Audio Menü hinzufügen
			makeAudioMenue();
		} else {
			$('.audio_cover').css('background-image', 'url(/signed/src/icns/filter/photo.svg)');
			$('.audio_upload_wrapper').addClass('empty');
			$('.audio_cover>.background_filter').fadeOut(0);
			//Audio-Menü entfernen
			removeAudioMenue();
		}
		if ($(this).find('input[name=file_name]').val()) {
			$('#upload_audio>#audio_upload_status_' + active_audio).html($(this).find('input[name=file_name]').val() + '<img id="delete_audio" src="/signed/src/icns/trash.svg">');
		} else {
			$('#upload_audio>#audio_upload_status_' + active_audio).html('Audiodatei hochladen');
		}
		$(this).addClass('active');
	});

	//Audio Menü Steuerung
	$(document).on('click', '.audio_menue>.action_wrapper>.element_dropdown', function (e) {
		$(this).closest('.audio_menue').children('.dropdown').fadeToggle();
		e.stopPropagation();
	});

	$(document).on('click', '.audio_dropdown_line', function () {
		action = $(this).attr('data-action');
		if (action == 'delete') {
			$('.audio_cover').css('background-image', 'url(/signed/src/icns/filter/photo.svg)');
			$('.audio_upload_wrapper').addClass('empty');
			$('.audio_cover>.background_filter').fadeOut(0);
			removeAudioMenue();
			$('.audio_wrapper[data-position=' + active_audio + ']').addClass('empty');
			$('.audio_wrapper[data-position=' + active_audio + ']')
				.children('.audio')
				.css('background-image', 'url(/signed/src/icns/add_audio.svg)');
			$('.audio_wrapper[data-position=' + active_audio + ']')
				.find('input[name=cover]')
				.val('');
		} else if (action == 'position') {
			recently_uploaded = $('#audio_pic_' + active_audio).val();
			makeAudioCropper(active_audio, recently_uploaded, user_id);
		} else {
			edit_trigger = 'audio';
			$('#file_archive_popup').fadeIn();
		}
	});

	//Audio löschen des Inputs
	$(document).on('click', '#delete_audio', function (e) {
		e.stopPropagation();
		$('#audio_file_' + active_audio).val('');
		$('#audio_upload_status_' + active_audio).html('Audiodatei hochladen');
		$('.audio_wrapper[data-position=' + active_audio + ']')
			.find('input[name=file_name]')
			.val('');
	});

	//Audiodatei upload + Progressbar
	$('#upload_audio').on('click', function (e) {
		if ($(e.target).attr('id') != 'delete_audio') {
			$('#audio_file_' + active_audio).prop('value', '');
			$('#audio_file_' + active_audio).trigger('click');
		}
	});

	$('#audio_file_1').on('change', function () {
		data = new FormData($('#audio_form')[0]);
		data.append('file', $('#audio_file_' + active_audio)[0].files[0]);
		$('#upload_audio>#audio_upload_status_1').html('Wird verarbeitet...');
		$.ajax({
			url: '/signed/php/builder/audio/upload.php',
			type: 'POST',
			data: data,
			contentType: false,
			cache: false,
			processData: false,
			xhr: function () {
				xhr = new window.XMLHttpRequest();
				xhr.upload.addEventListener(
					'progress',
					function (evt1) {
						if (evt1.lengthComputable) {
							percentComplete = evt1.loaded / evt1.total;
							percentComplete = Math.round(percentComplete * 100);
							$('#upload_audio>#audio_upload_status_1').html('<div class="progressbar left"><div class="done left"></div></div>' + percentComplete + '%');
							$('#upload_audio>#audio_upload_status_1>.progressbar>.done').css('width', percentComplete + '%');
						}
					},
					false
				);
				return xhr;
			},
			success: function (response) {
				if (response != 'failure') {
					if (response.length > 30) {
						audio_name = response.substring(0, 27) + '...';
					} else {
						audio_name = response;
					}
					$('#upload_audio>#audio_upload_status_1').html(audio_name + '<img src="/signed/src/icns/trash.svg" id="delete_audio">');
					$('.audio_wrapper[data-position=' + active_audio + ']')
						.find('input[name=file_name]')
						.val(response);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert(errorThrown);
			},
		});
	});

	$('#audio_file_2').on('change', function () {
		data = new FormData($('#audio_form')[0]);
		data.append('file', $('#audio_file_' + active_audio)[0].files[0]);
		$('#upload_audio>#audio_upload_status_2').html('Wird verarbeitet...');
		$.ajax({
			url: '/signed/php/builder/audio/upload.php',
			type: 'POST',
			data: data,
			contentType: false,
			cache: false,
			processData: false,
			xhr: function () {
				xhr = new window.XMLHttpRequest();
				xhr.upload.addEventListener(
					'progress',
					function (evt2) {
						if (evt2.lengthComputable) {
							percentComplete = evt2.loaded / evt2.total;
							percentComplete = Math.round(percentComplete * 100);
							$('#upload_audio>#audio_upload_status_2').html('<div class="progressbar left"><div class="done left"></div></div>' + percentComplete + '%');
							$('#upload_audio>#audio_upload_status_2>.progressbar>.done').css('width', percentComplete + '%');
						}
					},
					false
				);
				return xhr;
			},
			success: function (response) {
				if (response != 'failure') {
					if (response.length > 30) {
						audio_name = response.substring(0, 27) + '...';
					} else {
						audio_name = response;
					}
					$('#upload_audio>#audio_upload_status_2').html(audio_name + '<img src="/signed/src/icns/trash.svg" id="delete_audio">');
					$('.audio_wrapper[data-position=' + active_audio + ']')
						.find('input[name=file_name]')
						.val(response);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert(errorThrown);
			},
		});
	});

	$('#audio_file_3').on('change', function () {
		data = new FormData($('#audio_form')[0]);
		data.append('file', $('#audio_file_' + active_audio)[0].files[0]);
		$('#upload_audio>#audio_upload_status_3').html('Wird verarbeitet...');
		$.ajax({
			url: '/signed/php/builder/audio/upload.php',
			type: 'POST',
			data: data,
			contentType: false,
			cache: false,
			processData: false,
			xhr: function () {
				xhr = new window.XMLHttpRequest();
				xhr.upload.addEventListener(
					'progress',
					function (evt3) {
						if (evt3.lengthComputable) {
							percentComplete = evt3.loaded / evt3.total;
							percentComplete = Math.round(percentComplete * 100);
							$('#upload_audio>#audio_upload_status_3').html('<div class="progressbar left"><div class="done left"></div></div>' + percentComplete + '%');
							$('#upload_audio>#audio_upload_status_3>.progressbar>.done').css('width', percentComplete + '%');
						}
					},
					false
				);
				return xhr;
			},
			success: function (response) {
				if (response != 'failure') {
					if (response.length > 30) {
						audio_name = response.substring(0, 27) + '...';
					} else {
						audio_name = response;
					}
					$('#upload_audio>#audio_upload_status_3').html(audio_name + '<img src="/signed/src/icns/trash.svg" id="delete_audio">');
					$('.audio_wrapper[data-position=' + active_audio + ']')
						.find('input[name=file_name]')
						.val(response);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert(errorThrown);
			},
		});
	});

	$('#audio_file_4').on('change', function () {
		data = new FormData($('#audio_form')[0]);
		data.append('file', $('#audio_file_' + active_audio)[0].files[0]);
		$('#upload_audio>#audio_upload_status_4').html('Wird verarbeitet...');
		$.ajax({
			url: '/signed/php/builder/audio/upload.php',
			type: 'POST',
			data: data,
			contentType: false,
			cache: false,
			processData: false,
			xhr: function () {
				xhr = new window.XMLHttpRequest();
				xhr.upload.addEventListener(
					'progress',
					function (evt4) {
						if (evt4.lengthComputable) {
							percentComplete = evt4.loaded / evt4.total;
							percentComplete = Math.round(percentComplete * 100);
							$('#upload_audio>#audio_upload_status_4').html('<div class="progressbar left"><div class="done left"></div></div>' + percentComplete + '%');
							$('#upload_audio>#audio_upload_status_4>.progressbar>.done').css('width', percentComplete + '%');
						}
					},
					false
				);
				return xhr;
			},
			success: function (response) {
				if (response != 'failure') {
					if (response.length > 30) {
						audio_name = response.substring(0, 27) + '...';
					} else {
						audio_name = response;
					}
					$('#upload_audio>audio_upload_status_4').html(audio_name + '<img src="/signed/src/icns/trash.svg" id="delete_audio">');
					$('.audio_wrapper[data-position=' + active_audio + ']')
						.find('input[name=file_name]')
						.val(response);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert(errorThrown);
			},
		});
	});

	//Audio Cropping
	$(document).on('click', '.audio_upload_wrapper.empty>.audio_cover', function (e) {
		edit_trigger = 'audio';
		$('#file_archive_popup').fadeIn();
	});

	$(document).on('click', '#abort_audio_cropping', function () {
		//Checken ob hintergrund, wenn nicht .empty auf audio_cover
		if ($('.audio_wrapper[data-position=' + active_audio + ']').hasClass('empty')) {
			$('.audio_cover').parent().addClass('empty');
		}
		disableAudioEdit();
		$('.audio_cover').croppie('destroy');
	});

	$('.audio_cover.croppie-container').on('mouseenter', function () {
		setTimeout(function () {
			removeDragInfos();
		}, 2000);
	});

	$(document).on('click', '#save_audio_cropping', function () {
		result = $('.audio_cover')
			.croppie('result', {
				type: 'base64',
				size: 'original',
				format: 'png',
			})
			.then(function (dataImg) {
				$('#audio_pic_' + active_audio).val(recently_uploaded);
				$('.audio_wrapper[data-position=' + active_audio + ']')
					.children('input[name=cover]')
					.val(dataImg);
				$('.audio_wrapper[data-position=' + active_audio + ']').removeClass('empty');
				$('.audio_wrapper[data-position=' + active_audio + ']>.audio').css('background-image', 'url(' + dataImg + ')');
				$('.audio_cover').css('background-image', 'url(' + dataImg + ')');
				$('.audio_cover>.background_filter').fadeIn(500);
				$('.audio_cover').croppie('destroy');
				disableAudioEdit();
				makeAudioMenue();
			});
	});

	/*Colorpicker Audiowave*/
	$('.audio_wave>.left').on('click', function () {
		active_side = 'left';
		setTimeout(function () {
			audio_wave_pickr.setColor(wave_color_left);
			audio_wave_pickr.show();
		}, 100);
		$('.audio_button_cover').fadeIn();
	});

	$('.audio_wave>.right').on('click', function () {
		active_side = 'right';
		setTimeout(function () {
			audio_wave_pickr.setColor(wave_color_right);
			audio_wave_pickr.show();
		}, 100);
		$('.audio_button_cover').fadeIn();
	});

	audio_wave_pickr.on('change', function () {
		wave_color = $('.audio_upload_wrapper>.pcr-app').find('.pcr-result').val();
		changeWaveColorPopup(wave_color, active_side, active_audio);
	});

	audio_wave_pickr.on('init', function () {
		audio_wave_pickr.hide();
		$('.audio_upload_wrapper>.pcr-app').attr('style', '');
	});

	/*Audio Speichern*/
	audioplayer_id = 0;
	wavesurfer = [];
	audio_desc_array = ['a', 'b', 'c', 'd'];
	$('#save_audio').on('click', function () {
		//daten (type & Icons) speichern
		presskitNotSaved();
		saveIcons(clicked_icon);
		$('.audio_wrapper[data-position=1]').trigger('click');
		if ($(clicked_icon).closest('.audioplayer_parent').attr('data-audioplayer_id') == 'NULL') {
			audioplayer_id++;
			$(clicked_icon).closest('.audioplayer_parent').attr('data-audioplayer_id', audioplayer_id);
			clicked_audioplayer_id = audioplayer_id;
		} else {
			clicked_audioplayer_id = $(clicked_icon).closest('.audioplayer_parent').attr('data-audioplayer_id');
		}
		if (!clicked_audioplayer_id) {
			clicked_audioplayer_id = $(clicked_icon).closest('.profile_element').find('.audioplayer_parent').attr('data-audioplayer_id');
		}
		audio_names = [];
		covers = [];
		file_names = [];
		titles = [];
		wave_colors = [];
		$('.audio_wrapper').each(function () {
			if ($(this).find('input[name=file_name]').val() && $(this).find('input[name=title]').val() && $(this).find('input[name=cover]').val()) {
				audio_names.push($(this).find('input[name=audio_name]').val());
				covers.push($(this).find('input[name=cover]').val());
				file_names.push($(this).find('input[name=file_name]').val());
				titles.push($(this).find('input[name=title]').val());
				wave_colors.push($(this).find('input[name=wave_color]').val());
			}
		});

		if (audio_names.length < 1 && covers.length < 1 && file_names.length < 1 && titles.length < 1) {
			//Fehlermeldung
			$('#audio_error_message').fadeIn(0);
			setTimeout(function () {
				$('#audio_error_message').fadeOut(200);
			}, 5000);
			return false;
		}

		$('#loadingscreen').fadeIn(300);
		covers_string = covers.join('|');
		wave_colors_string = wave_colors.join('|');
		$(clicked_icon).closest('.input_parent').find('input[name=covers]').val(covers_string);
		$(clicked_icon).closest('.input_parent').find('input[name=titles]').val(titles.join(','));
		$(clicked_icon).closest('.input_parent').find('input[name=file_names]').val(file_names.join(','));
		$(clicked_icon).closest('.input_parent').find('input[name=wave_colors]').val(wave_colors_string);
		playerparent = $(clicked_icon).closest('.input_parent').children('.audioplayer_parent');
		playerwrapper = $(clicked_icon).closest('.menue_parent').find('.audio_element_wrapper'); //find('.audioplayer_parent').
		audiotype = $(playerparent).attr('data-audiotype');

		if (audiotype != 'audio_full_cover') {
			$(clicked_icon)
				.closest('.inner_profile_element_content')
				.find('.full_size.background')
				.css('background-image', 'url(' + covers[0] + ')');
		}

		//Dropdown
		audio_wave_wrapper = $(playerwrapper).find('.audio_wave_wrapper');
		$(audio_wave_wrapper).html('');
		loaded = 0;

		//Dropdown für audio_full_dropdown
		dropdown = $(playerwrapper).find('.audio_dropdown').find('.dropdown');
		$(dropdown).html('');

		input_parent = $(playerwrapper).closest('.input_parent');
		type = $(input_parent).children('input[name=type]').val('audio');
		//Menü erzeugen
		makeMenue(playerparent, 'audio');
		if (audiotype == 'audio_full_bottom') {
			slideshow_div = $(playerparent);
			$(playerparent).html('<div class="full_size background " data-audioplayerid="' + clicked_audioplayer_id + '"></div><div class="full_size background_filter hidden"></div><div class="audio_slideshow slideshow_full_bottom"></div>');
			slideshow_div = $(playerparent).find('.audio_slideshow');
			$(playerparent).removeClass('hover_blue_inlineshadow');
		} else if (audiotype == 'audio_full_dropdown') {
			$(playerwrapper)
				.find('.playbutton')
				.attr('data-active_playerid', clicked_audioplayer_id + 'a');
			$(playerwrapper).find('.title').text(titles[0]);
			$(playerparent).find('.background').html('');
			$(playerparent)
				.find('.background')
				.css('background-image', 'url(' + covers[0] + ')');
			if ($(playerwrapper).parent().children('.background_filter').length >= 0) {
				$(playerwrapper).parent().prepend('<div class="background_filter hover_blue_inlineshadow"></div>');
			}
			$(playerparent).removeClass('hover_blue_inlineshadow');
			//div für Player erstellen
			$(playerparent).find('.audio_volume_slider_wrapper').remove();
			$(playerparent).append('<div class="audio_volume_slider_wrapper" data-audioplayer_id="NULL"><div class="input_slider_wrapper"><input type="range" min="0" max="1" value="1" step="0.02" name="" class="volume_slider"></div><div class="audio_volume_slider_img"><img src="/signed/src/icns/audio_volume.svg" alt=""></div></div>');
		} else if (audiotype == 'audio_full_cover') {
			$(playerwrapper)
				.find('.playbutton')
				.attr('data-active_playerid', clicked_audioplayer_id + 'a');
			$(playerwrapper).find('.title').text(titles[0]);
			$(playerparent)
				.find('.cover')
				.css('background-image', 'url(' + covers[0] + ')');
			$(playerparent).find('.builder_icons_wrapper').fadeOut();
			/*if ($(playerwrapper).parent().children('.background_filter').length >= 0) {
        $(playerwrapper).parent().prepend('<div class="background_filter hover_blue_inlineshadow"></div>');
      }*/
			$(playerwrapper).closest('.inner_profile_element_content').find('.background_filter').fadeIn(0);
			$(playerparent).find('.audio_volume_slider_wrapper').remove();
			$(playerparent).append('<div class="audio_volume_slider_wrapper" data-audioplayer_id="NULL"><div class="input_slider_wrapper"><input type="range" min="0" max="1" value="1" step="0.02" name="" class="volume_slider"></div><div class="audio_volume_slider_img"><img src="/signed/src/icns/audio_volume.svg" alt=""></div></div>');
		}

		$(playerwrapper).removeClass('empty');

		if (audiotype == 'audio_full_cover' || audiotype == 'audio_full_dropdown') {
			audio_wave_wrapper_string = '';
			audio_dropdown_string = '';
			for (i = 0; i < titles.length; i++) {
				if (i == 0) {
					dropdown_class = 'heavy';
				} else {
					dropdown_class = 'light';
				}
				active_playerinstance = clicked_audioplayer_id + audio_desc_array[i];
				audio_wave_wrapper_string += '<div class="wavesurfer_player player_' + active_playerinstance + '" data-playerid="' + active_playerinstance + '"><div class="wave"></div></div>';
				audio_dropdown_string += '<div class="dropdown_line ' + dropdown_class + '" data-position="' + i + '" data-playerid="' + active_playerinstance + '"><img src="/signed/src/icns/play_' + dropdown_class + '.svg">' + titles[i] + '</div>';
			}
			$(audio_wave_wrapper).html(audio_wave_wrapper_string);
			$(dropdown).html(audio_dropdown_string);
		}

		$(playerparent).find('.builder_icons_wrapper').remove();
		//Schleife durch Arrays
		for (i = 0; i < titles.length; i++) {
			active_playerinstance = clicked_audioplayer_id + audio_desc_array[i];
			//Unterscheidung der einzelnen Audio Typen
			if (audiotype == 'audio_full_bottom') {
				$(playerwrapper).removeClass('hover_blue_inlineshadow');
				$(slideshow_div).append('<div class="audio_slider_wrapper playerid" data-audioplayer_id="' + active_playerinstance + '" style="background-image: url(' + covers[i] + ')"><div class="audio_volume_slider_wrapper" data-audioplayer_id="' + active_playerinstance + '"><div class="input_slider_wrapper"><input type="range" min="0" max="1" value="1" step="0.02" name="" class="volume_slider"></div><div class="audio_volume_slider_img"><img src="/signed/src/icns/audio_volume.svg" alt=""></div></div><div class="background_filter hover_blue_inlineshadow"></div><div class="audio_element_wrapper"><div class="audio_title"><img src="/signed/src/icns/playbutton_gray.svg" class="playbutton" data-active_playerid="' + active_playerinstance + '" data-state="pause">' + titles[i] + '</div><div class="audio_wave_wrapper"><div class="wavesurfer_player player_' + active_playerinstance + '"  data-playerid="' + active_playerinstance + '"><div class="wave"></div></div></div></div></div>');
			} else if (audiotype == 'audio_full_dropdown') {
			} else if (audiotype == 'audio_full_cover') {
			}
			playercontainer = '.wavesurfer_player.player_' + active_playerinstance + '>.wave';
			//Player erstellen
			if (wavesurfer[active_playerinstance]) {
				wavesurfer[active_playerinstance].destroy();
			}
			temp_wave_colors = wave_colors[i].split(',');
			wave_color_left = temp_wave_colors[0];
			wave_color_right = temp_wave_colors[1];
			if (wavesurfer[active_playerinstance]) {
				wavesurfer[active_playerinstance].destroy();
			}
			wavesurfer[active_playerinstance] = WaveSurfer.create({
				container: playercontainer,
				progressColor: wave_color_left,
				waveColor: wave_color_right,
				responsive: 1,
				minPxPerSec: 0,
				barWidth: 2,
				scrollParent: false,
				autoCenter: true,
				fillParent: true,
				pixelRatio: 1,
				hideScrollbar: true,
				mediaType: 'audio',
				backend: 'MediaElement',
			});

			audiolowestload = 0;
			wavesurfer[active_playerinstance].load('../../../signed/src/user/' + user_id + '/audio/' + file_names[i]);

			length = 300;
			start = 0;
			end = 300;

			//Ready-Inits der Player
			if (wavesurfer[clicked_audioplayer_id + 'a'] && i == 0) {
				wavesurfer[clicked_audioplayer_id + 'a'].on('waveform-ready', function () {
					loaded++;
					if (loaded >= titles.length) {
						$('#audio_popup').fadeOut(0);
						$('#loadingscreen').fadeOut(300);
						//$(playerparent).find('.background_filter').fadeIn();
						if (audiotype == 'audio_full_bottom') {
							//Slideshow Audio
							makeAudioSlideshow(slideshow_div, 'audio_full_bottom');
						}
						$(playerwrapper).attr('data-audioplayer_id', clicked_audioplayer_id + 'a');
					}
					audio_title = $('#audio_title_1').val();
					file_name = $('#file_name_1').val();
					wave_json = wavesurfer[clicked_audioplayer_id + 'a'].backend.getPeaks(length, start, end);
					$.ajax({
						url: '/signed/php/builder/audio/wave_upload.php',
						type: 'POST',
						data: {
							wave_json: wave_json,
							user_id: user_id,
							artist_id: artist_id,
							audio_title: audio_title,
							file_name: file_name,
						},
						success: function (response) {},
						error: function (jqXHR, textStatus, errorThrown) {
							alert(errorThrown);
						},
					});
				});
			}

			if (wavesurfer[clicked_audioplayer_id + 'b'] && i == 1) {
				wavesurfer[clicked_audioplayer_id + 'b'].on('waveform-ready', function () {
					loaded++;
					if (audiotype == 'audio_full_dropdown') {
						$('.wavesurfer_player.player_' + clicked_audioplayer_id + 'b').fadeOut(0);
					} else if (audiotype == 'audio_full_cover') {
						$('.wavesurfer_player.player_' + clicked_audioplayer_id + 'b').fadeOut(0);
					}
					if (loaded >= titles.length) {
						$('#audio_popup').fadeOut(0);
						$('#loadingscreen').fadeOut(300);
						if (audiotype == 'audio_full_bottom') {
							makeAudioSlideshow(slideshow_div, 'audio_full_bottom');
						}
					}
					audio_title = $('#audio_title_2').val();
					file_name = $('#file_name_2').val();
					wave_json = wavesurfer[clicked_audioplayer_id + 'b'].backend.getPeaks(length, start, end);
					$.ajax({
						url: '/signed/php/builder/audio/wave_upload.php',
						type: 'POST',
						data: {
							wave_json: wave_json,
							user_id: user_id,
							artist_id: artist_id,
							audio_title: audio_title,
							file_name: file_name,
						},
						success: function (response) {},
						error: function (jqXHR, textStatus, errorThrown) {
							alert(errorThrown);
						},
					});
					wavesurfer[clicked_audioplayer_id + 'a'].backend.getPeaks(length, start, end);
				});
			}

			if (wavesurfer[clicked_audioplayer_id + 'c'] && i == 2) {
				wavesurfer[clicked_audioplayer_id + 'c'].on('waveform-ready', function () {
					$('.wavesurfer_player.player_' + clicked_audioplayer_id + 'c').fadeOut(0);
					loaded++;
					if (audiotype == 'audio_full_dropdown') {
						$('.wavesurfer_player.player_' + clicked_audioplayer_id + 'b').fadeOut(0);
					} else if (audiotype == 'audio_full_cover') {
						$('.wavesurfer_player.player_' + clicked_audioplayer_id + 'b').fadeOut(0);
					}
					if (loaded >= titles.length) {
						$('#audio_popup').fadeOut(0);
						$('#loadingscreen').fadeOut(300);
						if (audiotype == 'audio_full_bottom') {
							makeAudioSlideshow(slideshow_div, 'audio_full_bottom');
						}
					}
					audio_title = $('#audio_title_3').val();
					file_name = $('#file_name_3').val();
					wave_json = wavesurfer[clicked_audioplayer_id + 'c'].backend.getPeaks(length, start, end);
					$.ajax({
						url: '/signed/php/builder/audio/wave_upload.php',
						type: 'POST',
						data: {
							wave_json: wave_json,
							user_id: user_id,
							artist_id: artist_id,
							audio_title: audio_title,
							file_name: file_name,
						},
						success: function (response) {},
						error: function (jqXHR, textStatus, errorThrown) {
							alert(errorThrown);
						},
					});
					wavesurfer[clicked_audioplayer_id + 'a'].backend.getPeaks(length, start, end);
				});
			}

			if (wavesurfer[clicked_audioplayer_id + 'd'] && i == 3) {
				wavesurfer[clicked_audioplayer_id + 'd'].on('waveform-ready', function () {
					$('.wavesurfer_player.player_' + clicked_audioplayer_id + 'd').fadeOut(0);
					loaded++;
					if (audiotype == 'audio_full_dropdown') {
						$('.wavesurfer_player.player_' + clicked_audioplayer_id + 'b').fadeOut(0);
					} else if (audiotype == 'audio_full_cover') {
						$('.wavesurfer_player.player_' + clicked_audioplayer_id + 'b').fadeOut(0);
					}
					if (loaded >= titles.length) {
						$('#audio_popup').fadeOut(0);
						$('#loadingscreen').fadeOut(300);
						if (audiotype == 'audio_full_bottom') {
							makeAudioSlideshow(slideshow_div, 'audio_full_bottom');
						}
					}
					audio_title = $('#audio_title_4').val();
					file_name = $('#file_name_4').val();
					wave_json = wavesurfer[clicked_audioplayer_id + 'd'].backend.getPeaks(length, start, end);
					$.ajax({
						url: '/signed/php/builder/audio/wave_upload.php',
						type: 'POST',
						data: {
							wave_json: wave_json,
							user_id: user_id,
							artist_id: artist_id,
							audio_title: audio_title,
							file_name: file_name,
						},
						success: function (response) {},
						error: function (jqXHR, textStatus, errorThrown) {
							alert(errorThrown);
						},
					});
					wavesurfer[clicked_audioplayer_id + 'a'].backend.getPeaks(length, start, end);
				});
			}
		}
	});

	/*Audio abbrechen*/
	$('#abort_audio').on('click', function () {
		$('#audio_popup').fadeOut();
	});

	$(document).on('click', '.slick-arrow', function () {
		if ($(this).closest('.audio_slideshow').find('.audio_element_wrapper').length > 0) {
			stopAllPlayers(wavesurfer);
		}
	});

	$(document).on('click', '.audio_dropdown>.dropdown>.dropdown_line', function () {
		if (wavesurfer) {
			stopAllPlayers(wavesurfer);
		}
		clicked = $(this);
		position = $(clicked).attr('data-position');
		playerid = $(clicked).attr('data-playerid');
		wavesurfer[$(clicked).closest('.audio_element_wrapper').find('.playbutton').attr('data-active_playerid')].pause();
		$('.playbutton').attr('data-state', 'pause');
		$('.playbutton').attr('src', '/signed/src/icns/playbutton_gray.svg');
		title = $(clicked).text();
		audiotype = $(clicked).closest('.audio_element_wrapper').attr('data-audiotype');
		$(clicked).closest('.audio_dropdown').find('.title').text(title);
		covers = $(clicked).closest('.input_parent').find('input[name=covers]').val();
		covers = covers.split('|');
		//Umschalten der Fett/nicht fett Klasse & Icon
		$(clicked)
			.parent()
			.children('.dropdown_line')
			.each(function () {
				$(this).removeClass('heavy');
				$(this).children('img').attr('src', '/signed/src/icns/play_light.svg');
			});
		$(clicked).addClass('heavy');
		$(clicked).children('img').attr('src', '/signed/src/icns/play_heavy.svg');
		if (audiotype == 'audio_full_dropdown') {
			$(clicked)
				.closest('.audio_element_wrapper')
				.find('.wavesurfer_player:not(.player_' + playerid + ')')
				.fadeOut(0);
			$(clicked)
				.closest('.audio_element_wrapper')
				.find('.wavesurfer_player.player_' + playerid)
				.fadeIn(200);
			$(clicked).closest('.audio_element_wrapper').find('.playbutton').attr('data-active_playerid', playerid);
			$(clicked)
				.closest('.inner_profile_element_content')
				.find('.full_size.background')
				.css('background-image', 'url(' + covers[position] + ')');
		} else if (audiotype == 'audio_full_cover') {
			$(clicked)
				.closest('.audio_element_wrapper')
				.find('.wavesurfer_player:not(.player_' + playerid + ')')
				.fadeOut(0);
			$(clicked)
				.closest('.audio_element_wrapper')
				.find('.wavesurfer_player.player_' + playerid)
				.fadeIn(200);
			$(clicked).closest('.audio_element_wrapper').find('.playbutton').attr('data-active_playerid', playerid);
			$(clicked)
				.closest('.audio_element_wrapper')
				.find('.cover')
				.css('background-image', 'url(' + covers[position] + ')');
		}
	});

	//Audioplayer Controlls
	$(document).on('click', '.playbutton', function () {
		player = $(this).attr('data-active_playerid');
		playbutton = $(this);
		if (wavesurfer) {
			stopAllPlayers(wavesurfer, player);
			$('.playbutton').each(function () {
				$(this).attr('data-state', 'play');
				$(this).attr('src', '/signed/src/icns/playbutton_gray.svg');
			});
		}
		if (wavesurfer[player]) {
			wavesurfer[player].playPause();
			if (wavesurfer[player].isPlaying()) {
				$(playbutton).attr('data-state', 'play');
				$(playbutton).attr('src', '/signed/src/icns/pausebutton_gray.svg');
			} else {
				$(playbutton).attr('data-state', 'pause');
				$(playbutton).attr('src', '/signed/src/icns/playbutton_gray.svg');
			}
		}
	});

	//Audioplayer Volume Slider
	$(document).on('click', '.audio_volume_slider_img', function () {
		$(this).closest('.audio_volume_slider_wrapper').find('.input_slider_wrapper').fadeToggle();
		active_player = $(this).closest('.audioplayer_parent').attr('data-audioplayer_id');
	});

	$(document).on('input', '.volume_slider', function () {
		if ($(this).closest('.audio_volume_slider_wrapper').attr('data-audioplayer_id') != 'NULL') {
			active_player = $(this).closest('.audio_volume_slider_wrapper').attr('data-audioplayer_id');
		} else if ($(this).closest('.audioplayer_parent').find('.audio_element_wrapper').length > 0) {
			active_player = $(this).closest('.audioplayer_parent').find('.audio_element_wrapper').attr('data-audioplayer_id');
		} else {
			active_player = $(this).closest('.audioplayer_parent').attr('data-audioplayer_id');
		}
		console.log('1. ' + active_player);
		wavesurfer[active_player].setVolume($(this).val());
	});

	$(document).on('mouseenter', '.audio_element_wrapper', function () {
		$(this).parent().find('.hover_blue_inlineshadow').addClass('hovered');
	});

	/* COLOR-HANDLING */
	$(document).on('click', '#save_color', function () {
		presskitNotSaved();
		if ($(clicked_icon).closest('.profile_element').find('.background_filter').length) {
			$(clicked_icon).closest('.profile_element').find('.background_filter').fadeIn();
		}
		if (saveIcons(clicked_icon)) {
			save = true;
		} else {
			save = false;
		}
		$(clicked_icon).closest('.input_parent').children('input[name=type]').val('color');
		color = pickr.getColor();
		color = color.toHEX().toString();
		$(clicked_icon).closest('.input_parent').children('input[name=value]').val(color);
		$('.pcr-save').trigger('click');
		removeIcons(clicked_icon);
		if (save) {
			if ($(pickr_background).closest('.element_menue').length == 0) {
				makeMenue($(pickr_background), 'color');
			} else {
				makeMenue($(pickr_background).find('.menue_parent'), 'color');
			}
			removeIcons(clicked_icon);
		}
		closeEditMenue();
	});

	pickr.on('change', function () {
		$(pickr_background).css('background-color', $('#color').find('.pcr-result').val());
	});

	pickr.on('save', function () {
		closeEditMenue();
		color = pickr.getColor();
		color = color.toHEX();
		//$(clicked_icon).closest('.input_parent').children('input[name=value]').val(color);
	});

	/* VIDEO-HANDLING */
	//Toggle Button Video Popup Video <-> Youtube
	$(document).on('click', '.video_popup_mode_toggle:not(.active)', function () {
		$('.video_popup_mode_toggle.active').removeClass('active');
		$(this).addClass('active');
		video_type = $(this).attr('data-type');
		if (video_type == 'video') {
			$('#video_popup_youtube_wrapper').fadeOut(200, function () {
				$('#video_popup_video_wrapper').fadeIn(200);
			});
		} else {
			$('#video_popup_video_wrapper').fadeOut(200, function () {
				$('#video_popup_youtube_wrapper').fadeIn(200);
			});
		}
	});

	//Klick auf Video-Thumbnail
	$(document).on('click', '#video_thumbnail.empty', function (e) {
		edit_trigger = 'video';
		$('#file_archive_popup').fadeIn();
	});

	//Upload der Datei
	$('#video_file').on('change', function () {
		if (this.files[0].size > 50000000) {
			$('#video_too_big_message').fadeIn(250);
			return false;
		}
		$('#video_too_big_message').fadeOut(0);
		data = new FormData($('#video_form')[0]);
		$.ajax({
			url: '/signed/php/builder/video/upload.php',
			type: 'POST',
			data: data,
			contentType: false,
			cache: false,
			processData: false,
			xhr: function () {
				xhr = new window.XMLHttpRequest();
				xhr.upload.addEventListener(
					'progress',
					function (evt) {
						if (evt.lengthComputable) {
							percentComplete = evt.loaded / evt.total;
							percentComplete = Math.round(percentComplete * 100);
							$('#upload_video').html('<div class="progressbar left"><div class="done left"></div></div>'); //' + percentComplete + '%'
							$('#upload_video').removeClass('empty');
							$('#upload_video>.progressbar>.done').css('width', percentComplete + '%');
						}
					},
					false
				);
				return xhr;
			},
			success: function (response) {
				if (response != 'failure') {
					if (response.length > 75) {
						video_name = response.substring(0, 72) + '...';
					} else {
						video_name = response;
					}
					$('#upload_video').html(video_name + '<img src="/signed/src/icns/trash.svg" id="delete_video">');
					$('#video_popup_inner').find('input[name=video_file_name]').val(response);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert(errorThrown);
			},
		});
	});

	$(document).on('click', '#save_video_cropping', function (e) {
		$('#video_thumbnail').removeClass('empty');
		$('#video_thumbnail').removeClass('was_empty');
		e.preventDefault();
		result = $('#video_thumbnail')
			.croppie('result', {
				type: 'base64',
				size: 'original',
				format: 'png',
			})
			.then(function (dataImg) {
				$('#video_thumbnail').val(dataImg);
				$('#video_thumbnail').removeClass('empty');
				$('#video_thumbnail').css('background-image', 'url(' + dataImg + ')');
				$('#video_thumbnail_base64').val(dataImg);
				$('#video_thumbnail').croppie('destroy');
				$('.croppie-container').croppie('destroy');
				makeVideoMenue();
				disableVideoEdit();
			});
	});

	$(document).on('click', '#abort_video_cropping', function () {
		//Checken ob .was_empty klasse (erster Init des Croppers), wenn nicht .empty auf audio_cover
		if ($('#video_thumbnail').hasClass('was_empty')) {
			$('#video_thumbnail').addClass('empty');
			$('#video_thumbnail').removeClass('was_empty');
		}
		disableVideoEdit();
		$('#video_thumbnail').croppie('destroy');
	});

	$('#video_thumbnail').on('mouseenter', function () {
		setTimeout(function () {
			removeDragInfos();
		}, 1000);
	});

	//Video-Thumbnail Menü
	$(document).on('click', '.video_menue>.dropdown>.video_dropdown_line', function () {
		action = $(this).attr('data-action');
		if (action == 'position') {
			makeVideoCropper();
		} else if (action == 'upload') {
			edit_trigger = 'video';
			$('#file_archive_popup').fadeIn();
		} else {
			$('#video_thumbnail').addClass('empty');
			$('#video_thumbnail').attr('style', '');
			$('#video_thumbnail_base64').val(' ');
			$('#video_thumbnail_file').val(' ');
			$('#video_thumbnail>.video_menue').remove();
		}
		$('.video_menue>.dropdown').fadeOut();
	});

	$(document).on('click', '.video_menue>.action_wrapper>.element_dropdown', function () {
		$(this).closest('.video_menue').find('.dropdown').fadeToggle();
	});

	//Video-Bearbeitung Abbrechen
	$('#abort_video').on('click', function () {
		$('#video_popup').fadeOut();
	});

	//Video-Datei löschen
	$(document).on('click', '#delete_video', function (e) {
		e.stopPropagation();
		$('#upload_video').html(video_upload_html);
		$('#upload_video').addClass('empty');
	});

	$('#upload_video').on('click', function (e) {
		if ($(e.target).attr('id') != 'delete_video') {
			$('#video_file').prop('value', '');
			$('#video_file').trigger('click');
		}
	});

	//Video Speichern
	$('#save_video').on('click', function () {
		presskitNotSaved();
		saveIcons(clicked_icon);
		video_file_name = $('input[name=video_file_name]').val();
		video_thumbnail = 'NULL';
		video_thumbnail = $('#video_thumbnail_base64').val();
		input_parent = $(video_div).closest('.input_parent');
		if (!video_file_name || video_file_name.length < 1 || !video_thumbnail || video_thumbnail.length < 10) {
			$('#video_error_message').fadeIn();
		} else {
			$(input_parent).find('input[name=value]').val(video_file_name);
			$(input_parent).find('input[name=thumbnails]').val(video_thumbnail);
			$(input_parent).find('input[name=type]').val('video');
			if (video_thumbnail.length > 0) {
				video_thumbnail = 'preload="none" poster="' + video_thumbnail + '"';
			} else {
				video_thumbnail = 'preload="auto"';
			}
			$(video_div).removeClass('hover_blue_inlineshadow');
			//$(video_div).html("Video lädt. Dies kann einige Sekunden dauern.<br>Bitte warten...");
			$(video_div).html('<div class="full_loadscreen"></div>');
			setTimeout(function () {
				$(video_div).html('<div class="video-js"><div class="vjs-big-play-button"></div><video class="hover_blue_inlineshadow" ' + video_thumbnail + '><source src="/signed/src/user/' + user_id + '/video/' + video_file_name + '" type="video/mp4"></video></div>');
			}, 2000);
			makeMenue(video_div, 'video');
			$(video_div).find('.controlBar').fadeOut(1000);
			$('#video_popup').fadeOut(500);
		}
	});

	$(document).on('click', '.vjs-big-play-button', function () {
		video = $(this).parent().children('video');
		$(this).parent().children('video')[0].play();
		$(this).fadeOut();
		$(this).parent().children('video').attr('controls', 'controls');
	});

	//Cropper
	$('#photo_cropper_trigger').on('click', function () {
		if ($(clicked_icon).closest('.profile_element').find('.background_filter').length) {
			$(clicked_icon).closest('.profile_element').find('.background_filter').fadeOut();
		}
		clearEditMenue();
		saveIcons(clicked_icon);
		// Gerade gecropptes speichern und Cropper zerstören
		$('.edit_menue#cropper').fadeIn();
		$('#cropper_slider').html('');
		if (data_action == 'media') {
			$(cropper_div).html('');
		}

		$croppie = $(cropper_div).croppie({
			url: '/signed/src/user/' + user_id + '/archive/' + recently_uploaded,
			viewport: {
				width: '100%',
				height: '100%',
			},
			showZoomer: true,
			mouseWheelZoom: false,
		});
		appendPositionNoteFixed(cropper_div, '');
		croppie_slider = $('.croppie-container').children('.cr-slider-wrap');
		$(croppie_slider).appendTo('#cropper_slider');
		$('#cropper_slider').append('<img src="/signed/src/icns/plus_white.svg" class="right">');
		$('#cropper_slider').prepend('<img src="/signed/src/icns/minus_white.svg" class="left">');
		disableEdit();
		setTimeout(function () {
			openEditMenue();
		}, 1500);
		setTimeout(function () {
			$('.cr-slider').removeClass('disabled').removeAttr('disabled');
		}, 1600);
	});

	$('#save_cropped_pic').on('click', function (e) {
		presskitNotSaved();
		$('#loadingscreen').fadeIn(50);
		e.stopPropagation();
		if (saveIcons(clicked_icon)) {
			save = true;
		} else {
			save = false;
		}
		if ($(clicked_icon).closest('.profile_element').find('.background_filter').length) {
			$(clicked_icon).closest('.profile_element').find('.background_filter').fadeIn();
		}
		setTimeout(function () {
			saveCropped(cropper_div, clicked_icon, save);
		}, 200);
	});

	$(document).on('click', '#abort_cropped', function () {
		if ($('.slideshow_element.croppie-container').hasClass('was_empty')) {
			//restoreSlideshowIcons(cropper_div);
			$('.slideshow_element.croppie-container').addClass('empty').removeClass('was_empty');
			$('.drag_positition_note').remove();
		}
		destroyCropped($('.slideshow_element.croppie-container'));
	});

	//Slideshow
	$(document).on('click', '.slideshow_element.empty:not(.slideshow_menue)', function (e) {
		$('.slideshow_menue_inner').fadeOut();
		slideshow_element_clicked = $(this);
		$('#file_archive_popup').fadeIn();
	});

	$('.slideshow_menue_point.upload_pic').on('click', function () {
		$('.slideshow_menue_inner').fadeOut();
		slideshow_element_clicked = $(this).closest('.slideshow_element');
		$('#file_archive_popup').fadeIn();
	});

	$('.slideshow_menue_point.delete_pic').on('click', function () {
		$('.slideshow_menue_inner').fadeOut();
		$(this).closest('.slideshow_element_wrapper').children('input[name=picture_input]').val(' ');
		$(this).closest('.slideshow_element_wrapper').children('input[name=picture_input_base64]').val(' ');
	});

	//Menü-FadeIn/Out
	$('.slideshow_element')
		.on('mouseenter', function () {
			if (!$('.slideshow_element.active_cropping').length) {
				$(this).find('.slideshow_menue').fadeIn();
			}
		})
		.on('mouseleave', function () {
			$(this).find('.slideshow_menue').fadeOut();
			$(this).find('.slideshow_menue>.slideshow_menue_inner').fadeOut();
		});

	$('.slideshow_menue_point.position_pic').on('click', function () {
		$('.cropper_wrapper.slideshow').fadeIn();
		$('.slideshow_element.active_cropping').croppie('destroy');
		$('.slideshow_element.active_cropping').removeClass('active_cropping');
		$('.croppie_slider_wrapper.slideshow').html('');
		menue = $(this);
		slideshow_element = $(this).closest('.slideshow_element');
		$(slideshow_element).addClass('active_cropping');
		croppie_position = $(this).closest('.slideshow_element').attr('position');
		$(this)
			.closest('.slideshow_element_wrapper')
			.children('.slideshow_input')
			.each(function () {
				url_file = '/signed/src/user/' + user_id + '/archive/' + $(this).closest('.slideshow_element_wrapper').children('.slideshow_input').val();
				if (!$(menue).closest('.slideshow_element').hasClass('empty')) {
					$('.slideshow_inner_wrapper').sortable('disable');
					$croppie = $(menue)
						.closest('.slideshow_element')
						.croppie({
							url: url_file,
							viewport: {
								width: '100%',
								height: '100%',
							},
							showZoomer: true,
						});
					croppie_slider = $('.croppie-container').children('.cr-slider-wrap');
					$(croppie_slider).appendTo('.croppie_slider_wrapper.slideshow');
					$('.croppie_slider_wrapper.slideshow').prepend('<div class="cropper_desc">Foto positionieren: </div>');
					$('.croppie_slider_wrapper.slideshow').append("<div class='croppie_btn_wrapper' croppie_position='" + croppie_position + "'><button class='blue_btn menue_button' id='save_cropped_slideshow'>Übernehmen</button><button class='gray_btn menue_button' id='abort_cropped'>Abbrechen</button></div>");
				} else {
				}
			});
	});

	$(document).on('click', '#save_cropped_slideshow', function () {
		clicked = $(this);
		$('.slideshow_inner_wrapper').sortable('enable');
		slideshow_element = $('.slideshow_element[position=' + $(clicked).closest('.croppie_btn_wrapper').attr('croppie_position') + ']');
		result = $(slideshow_element)
			.croppie('result', {
				type: 'base64',
				size: 'original',
				format: 'png',
			})
			.then(function (dataImg) {
				$(slideshow_element).parent().children('.slideshow_input_base64').val(dataImg);
				$(slideshow_element).css('background-image', 'url(' + dataImg + ')');
				$(slideshow_element).croppie('destroy');
				$(slideshow_element).removeClass('active_cropping');
				$(slideshow_element).removeClass('was_empty');
				$('.cropper_wrapper.slideshow').fadeOut();
				$('.croppie_slider_wrapper.slideshow').html('');
				croppie_position = 0;
			});
	});

	// TODO: Speed und Effekttyp speichern
	$('#save_slideshow').on('click', function () {
		presskitNotSaved();
		//Checken ob überhaupt Element mit geladenem Foto vorhanden
		if ($('.slideshow_element:not(.empty)').length == 0) {
			return false;
		}
		if (saveIcons(clicked_icon)) {
			save = true;
		} else {
			save = false;
		}
		slideshow_array = [];
		slideshow_paths = [];
		slideshow_html = '';
		$('.slideshow_input_base64').each(function () {
			url_file = $(this).closest('.slideshow_element_wrapper').children('.slideshow_input').val();
			if ($(this).val().length > 10 && url_file) {
				slideshow_html += '<div class="slides" style="background-image: url(' + $(this).val() + ')"></div>'; //
				slideshow_array.push($(this).val());
				slideshow_paths.push(url_file);
			}
		});
		if (slideshow_array.length > 0) {
			if ($(slideshow_div).hasClass('slick-initialized')) {
				$(slideshow_div).slick('unslick');
			}
			$(slideshow_div).html(slideshow_html);

			if ($('.slideshow_effect_select').val() == '1') {
				speed = 0;
			} else {
				speed = 1000;
			}
			$(slideshow_div).parent('.input_parent').children('input[name=value]').val(slideshow_array.join('|'));
			$(slideshow_div).parent('.input_parent').children('input[name=originals]').val(slideshow_paths.join('|'));
			$(slideshow_div).parent('.input_parent').children('input[name=type]').val('slideshow');
			if (save) {
				makeMenue(slideshow_div, 'slideshow');
				removeIcons(clicked_icon);
			}
			$(slideshow_div).slick({
				autoplay: true,
				autoplaySpeed: $('.effect_time').slider('option', 'value') * 1000,
				infinite: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				speed: speed,
				easing: 'linear',
				arrows: false,
				pauseOnHover: false,
				respondTo: 'slider',
				swipe: false,
			});
			$(slideshow_div).removeClass('hover_blue_inlineshadow');
			$(slideshow_div).find('.slides').addClass('hover_blue_inlineshadow');
			$('#slideshow_popup').fadeOut();
		} else {
		}
	});

	/*GIF Upload*/
	$('#upload_gif').on('click', function () {
		$('#gif_input').trigger('click');
	});

	$('#gif_input').on('change', function (e) {
		$('#loadingscreen').fadeIn(50);
		//Formdata aus gif popup
		data = new FormData($('#gif_form')[0]);
		data.append('gif', $('#gif_input')[0].files[0]);
		data.append('user_id', user_id);
		$('#gif_popup').fadeOut(500);
		// Upload GIF in Presspack Temp Ordner
		$.ajax({
			url: '/signed/php/tools/upload_to_temp_presspack.php',
			type: 'POST',
			data: data,
			processData: false,
			contentType: false,
			success: function (response) {
				path = '/signed/src/user/' + user_id + '/temp_presspack/' + response;
				console.log(path);
				if (saveIcons(clicked_icon)) {
					save = true;
				} else {
					save = false;
				}
				//Menü erstellen
				if (save) {
					makeMenue(gif_background, 'gif');
					removeIcons(clicked_icon);
				}
				//gif als Hintergrund nehmen
				$(gif_background).css('background-image', 'url(' + path + ')');
				$(gif_background).closest('.input_parent').children('input[name=value]').val(response);
				$(gif_background).closest('.input_parent').children('input[name=type]').val('gif');
				$(gif_background).html('');
				$('#gif_popup').fadeOut(500);
				$('#loadingscreen').fadeOut(500);
			},
			error: function (jqXHR, textStatus, errorThrown) {
				console.log(jqXHR);
			},
		});
	});

	/*Presspack*/
	$(document).on('click', '.picture>.presspack_img.add_img', function () {
		presspack_clicked = $(this);
		edit_trigger = 'presspack';
		$('#file_archive_popup').fadeIn();
	});

	/*Dropdown Element-Menü*/
	$(document).on('click', '.element_menue>.action_wrapper>.element_dropdown', function (e) {
		$(this).closest('.element_menue').children('.dropdown').fadeToggle();
		$(this).closest('.audio_menue').children('.dropdown').fadeOut();
		e.stopPropagation();
	});

	//Social media  // Social Media
	$(document).on('click', '.social_media_trigger:not(.disabled)', function () {
		social_clicked = $(this);
		$('#socials_popup').fadeIn();
		sociallinks = $(social_clicked).closest('.input_parent').children('input[name=sociallinks]').val();
		socialselected = $(social_clicked).closest('.input_parent').children('input[name=socialselected]').val();
		if (socialselected) {
			socialselected = socialselected.split(',');
			i = 0;
			$('.social_link_select').each(function () {
				$(this).val(socialselected[i]);
				if (sociallinks[i] != 0) {
					$(this).parent().removeClass('disabled');
				} else {
					$(this).parent().addClass('disabled');
				}
				if (!socialselected[i]) {
					$(this).val('0');
					$(this).parent().addClass('disabled');
				}
				i++;
			});
		} else {
			j = 1;
			$('.social_link_select').each(function () {
				if (j == 2) {
					$(this).val('4');
				} else if (j == 3) {
					$(this).val('2');
				} else {
					$(this).val('1');
				}
				j++;
			});
		}
		if (socialselected) {
			sociallinks = sociallinks.split(',');
			i = 0;
			$('.social_link_input').each(function () {
				sociallinks[i] = sociallinks[i].replace(' ', '');
				if (social_array_links.includes(sociallinks[i])) {
					$(this).val('');
					$(this).attr('placeholder', 'Link einfügen');
				} else {
					$(this).val(sociallinks[i]);
				}
				i++;
			});
		} else {
			$('.social_link_input').each(function () {
				$(this).val('');
				$(this).attr('placeholder', 'Link einfügen');
			});
		}
	});

	$('.social_preview').on('click', function () {
		url = $(this).parent().children('input').val();
		if (url.substring(8, 0) == 'https://' || url.substring(7, 0) == 'http://') {
			window.open(url, '_blank');
		} else if (url.length > 4) {
			url = 'https://' + url;
			window.open(url, '_blank');
		}
	});

	$('#save_social').on('click', function () {
		presskitNotSaved();
		count = 0;
		sociallinks = [];
		socialselected = [];
		social_html = [];
		if ($(social_clicked).hasClass('wide')) {
			social_full = true;
		} else {
			social_full = false;
		}
		$('.social_link_wrapper').each(function () {
			selected = $(this).children('.social_link_select').val();
			link = $(this).children('.social_link_input').val();
			if (selected != 0) {
				sociallinks.push(link);
				socialselected.push(selected);
				count++;
			}
		});
		if (count < 2) {
			check = false;
		} else if (count > 2) {
			size = '';
		} else {
			size = 'half';
		}
		html = '';
		for (i = 0; i < socialselected.length; i++) {
			if (social_full) {
				if (size == 'half') {
					html += '<div class="social_wrapper half"><div class="color_change"><img src="/signed/src/icns/switch_icons/' + social_array[socialselected[i]] + color_string + '.svg" alt="" class=""> <span>' + social_array_names[socialselected[i]] + '</span></div></div>';
				} else {
					if (i == 0) {
						html += '<div class="social_wrapper first"><div class="color_change"><img src="/signed/src/icns/switch_icons/' + social_array[socialselected[i]] + color_string + '.svg" alt="" class=""> <span>' + social_array_names[socialselected[i]] + '</span></div></div>';
					} else if (i == 1) {
						html += '<div class="social_wrapper second"><div class="color_change"><img src="/signed/src/icns/switch_icons/' + social_array[socialselected[i]] + color_string + '.svg" alt="" class=""> <span>' + social_array_names[socialselected[i]] + '</span></div></div>';
					} else if (i == 2) {
						html += '<div class="social_wrapper third"><div class="color_change"><img src="/signed/src/icns/switch_icons/' + social_array[socialselected[i]] + color_string + '.svg" alt="" class=""> <span>' + social_array_names[socialselected[i]] + '</span></div></div>';
					}
				}
			} else {
				html += '<img src="/signed/src/icns/switch_icons/' + social_array[socialselected[i]] + color_string + '.svg" class="black_white_header_social" alt="' + social_array_names[socialselected[i]] + '">';
			}
		}
		sociallinks = sociallinks.join(',');
		socialselected = socialselected.join(',');
		$(social_clicked).html(html);
		$(social_clicked).closest('.input_parent').children('input[name=sociallinks]').val(sociallinks);
		$(social_clicked).closest('.input_parent').children('input[name=socialselected]').val(socialselected);
		$('#socials_popup').fadeOut();
	});

	$('#abort_social').on('click', function () {
		$('#socials_popup').fadeOut();
	});

	$('select[name=social_3_select]').change(function () {
		if ($(this).val() == 0) {
			$(this).parent().addClass('disabled');
		} else {
			$(this).parent().removeClass('disabled');
		}
	});

	//Texteditor
	active_texteditor = false;
	var texteditor_options = {
		theme: 'snow',
		modules: {
			toolbar: '.text_editor_toolbar',
		},
	};

	var ColorClass = Quill.import('attributors/class/color');
	var SizeStyle = Quill.import('attributors/style/size');
	Quill.register(ColorClass, true);
	Quill.register(SizeStyle, true);
	//Click auf .textinput_textarea texteditor initialisieren wenn nicht bereits einer aktiv ist.
	$(document).on('click', '.textinput_textarea:not(.disabled)', function () {
		timeout = profileElementEditMenueScroll($(this));
		if (!active_headlineeditor && !active_texteditor) {
			$('.text_editor_toolbar').html(text_editor_toolbar_html);
			texteditor = $(this);
			placeholder = $(texteditor).find('.placeholder').outerHTML();
			$(texteditor).find('.placeholder').remove();
			$(texteditor).removeClass('inactive');
			setTimeout(function () {
				if (!active_texteditor) {
					$(texteditor).html(placeholder);
					$(texteditor).removeClass('ql-container ql-snow');
				}
			}, 500);
			//Editor initalisieren
			editor = new Quill($(texteditor).get(0), texteditor_options);
			active_texteditor = true;
			//textinput blaue hover-klasse deaktivieren
			$(texteditor).removeClass('hover_blue_inlineshadow');
			//Menü leeren
			clearEditMenue();
			//Toolbar-Menü einfaden
			$('#texteditor').fadeIn(0);
			//Menü einfaden
			disableEdit();
			setTimeout(function () {
				openEditMenue();
			}, timeout);
			//$('.ql-editor').closest('.profile_element_content')[0].scrollIntoView();
			setTimeout(function () {
				$('.ql-editor').get(0).focus();
			}, 200);
		}
	});

	$(document).on('keydown', '.textinput_textarea>.ql-editor', function () {
		height = $(this).height();
		amount_of_lines =
			height /
			$(this)
				.css('line-height')
				.replace(/[^-\d\.]/g, '');
	});

	$('#save_text').on('click', function () {
		presskitNotSaved();
		$('#loadingscreen').fadeIn(50);
		value = editor.container.firstChild.innerHTML;
		$('.ql-editor').get(0).focus({
			preventScroll: true,
		});
		$('.ql-editor').trigger('click');
		setTimeout(function () {
			$('.ql-container').html(value);
			if (value == '<p></p>' || value == '<p><br></p>') {
				$('.ql-container').html(placeholder);
			}
			$('.ql-container').addClass('inactive');
			$('.ql-container').removeClass('ql-container ql-snow');
			active_texteditor = false;
			$(texteditor).addClass('hover_blue_inlineshadow');
			editor = '';
			//Farbe speichern
			closeEditMenue();
			clearEditMenue();
			//Text automatisch kürzen
			ellipsizeTextBox('textinput_textarea');
			$('#loadingscreen').fadeOut(500);
		}, 300);
	});

	$(document).on('click', '#text_color', function () {
		if ($('#texteditor').find('.picker').attr('visible') == 'true') {
			$('#texteditor').find('.picker').removeClass('visible');
			$('#texteditor').find('.picker').attr('visible', 'false');
		} else {
			$('#texteditor').find('.picker').addClass('visible');
			$('#texteditor').find('.picker').attr('visible', 'true');
		}
	});

	$(document).on('click', '#headline_color', function () {
		if ($('#headlineeditor').find('.picker').attr('visible') == 'true') {
			$('#headlineeditor').find('.picker').removeClass('visible');
			$('#headlineeditor').find('.picker').attr('visible', 'false');
		} else {
			$('#headlineeditor').find('.picker').addClass('visible');
			$('#headlineeditor').find('.picker').attr('visible', 'true');
		}
	});

	text_pickr.on('change', function () {
		$('.ql-container')
			.closest('.text_color_parent')
			.css('color', $('#texteditor').find('.pcr-result').val() + '!important');
		$('.ql-container').closest('.text_color_parent').find('.color_switch_svg').attr('fill', $('#texteditor').find('.pcr-result').val());
		$('.ql-container').closest('.text_color_parent').find('.color_switch_svg').children('g').attr('fill', $('#texteditor').find('.pcr-result').val());
		$('.ql-container').closest('.text_color_parent').find('.color_switch_svg').children('g').children('g').attr('fill', $('#texteditor').find('.pcr-result').val());
	});

	headline_pickr.on('change', function () {
		$('.ql-container')
			.closest('.text_color_parent')
			.css('color', $('#headlineeditor').find('.pcr-result').val() + '!important');
		$('.ql-container').closest('.text_color_parent').find('.color_switch_svg').attr('fill', $('#texteditor').find('.pcr-result').val());
		$('.ql-container').closest('.text_color_parent').find('.color_switch_svg').children('g').attr('fill', $('#texteditor').find('.pcr-result').val());
		$('.ql-container').closest('.text_color_parent').find('.color_switch_svg').children('g').children('g').attr('fill', $('#texteditor').find('.pcr-result').val());
	});

	//Headline Editor
	var Keyboard = Quill.import('modules/keyboard');
	var bindingsOptions = {
		enter: {
			key: 13,
			handler: function () {
				//Enter bei headlineeditor triggert speichern Button
				$('#save_headline').trigger('click');
			},
		},
	};
	var headlineeditor_options = {
		theme: 'snow',
		modules: {
			toolbar: '.headline_editor_toolbar',
			keyboard: {
				bindings: bindingsOptions,
			},
		},
	};
	active_headlineeditor = false;
	$(document).on('click', '.headline_input:not(.disabled)', function () {
		timeout = profileElementEditMenueScroll($(this));
		if (!active_headlineeditor && !active_texteditor) {
			$('.headline_editor_toolbar').html(headline_editor_toolbar_html);
			placeholder = $(this).find('.placeholder').outerHTML();
			$(this).removeClass('inactive');
			if ($(this).find('.placeholder').length > 0) {
				$(this).find('.placeholder').remove();
				$(this).html('<p><strong> </strong></p>');
			}
			editor_html = $(this).html();
			active_headlineeditor = true;
			headlineeditor = new Quill($(this).get(0), headlineeditor_options);
			$(this).removeClass('hover_blue_inlineshadow');
			clearEditMenue();
			texteditor = $(this);
			$('#headlineeditor').fadeIn();
			headlineeditor.on('text-change', function (delta, old, source) {
				checkHeadlineLimit();
			});
			$('.ql-editor').closest('.profile_element_content')[0].scrollIntoView();
			setTimeout(function () {
				$('.ql-editor').addClass('headline_ql');
				$('.ql-editor').get(0).focus();
			}, 200);
			timeout = profileElementEditMenueScroll($(this));
			disableEdit();
			setTimeout(function () {
				openEditMenue();
			}, timeout);
		}
	});

	$(document).on('click', '.headline_input.disabled:not(.ql-container)', function (e) {
		if (active_headlineeditor || active_texteditor) {
			if (active_headlineeditor) {
				$('#save_headline').trigger('click');
			} else if (active_texteditor) {
				$('#save_text').trigger('click');
			}
			setTimeout(function () {
				$(e.target).trigger('click');
			}, 500);
		}
	});

	$(document).on('click', '.textinput_textarea.disabled:not(.ql-container)', function (e) {
		if (active_headlineeditor || active_texteditor) {
			if (active_headlineeditor) {
				$('#save_headline').trigger('click');
			} else if (active_texteditor) {
				$('#save_text').trigger('click');
			}
			setTimeout(function () {
				$(e.target).trigger('click');
			}, 500);
		}
	});

	$('#save_headline').on('click', function () {
		presskitNotSaved();
		$('#loadingscreen').fadeIn(50);
		value = headlineeditor.container.firstChild.innerHTML;
		$('.ql-editor').get(0).focus({
			preventScroll: true,
		});
		$('.ql-editor').trigger('click');
		setTimeout(function () {
			$('.ql-container').html(value);
			$('.ql-container').addClass('inactive');
			$('.ql-container').removeClass('ql-container ql-snow');
			active_headlineeditor = false;
			$(texteditor).addClass('hover_blue_inlineshadow');
			headlineeditor = '';
			//Farbe speichern
			closeEditMenue();
			clearEditMenue();
			//Text automatisch kürzen
			ellipsizeTextBox('.headline_input');
			$('#loadingscreen').fadeOut(500);
		}, 200);
	});

	//Color-Switch
	$(document).on('click', '.switch_color', function () {
		element = $(this).closest('.profile_element');
		if ($(this).attr('data-color') == 'black') {
			color_string = '_white';
			color = 'white';
			color_hex = '#FFFFFF';
			$(this).attr('data-color', 'white');
		} else {
			color_string = '';
			color = 'black';
			color_hex = '#000000';
			$(this).attr('data-color', 'black');
		}
		//Klasse .color_change
		$(element)
			.find('.color_change')
			.css('color', color_hex + '!important');
		$(element)
			.find('.color_change')
			.find('img')
			.each(function () {
				src = $(this).attr('src');
				src = src.replace('.svg', '');
				src = src.replace('.png', '');
				if ($(this).attr('data-fileformat') == 'png') {
					fileformat = '.png';
				} else {
					fileformat = '.svg';
				}
				if ($(this).attr('src').indexOf('_white') > 0) {
					$(this).attr('src', src.replace('_white', '') + fileformat);
				} else {
					$(this).attr('src', src + '_white' + fileformat);
				}
			});
	});

	/*"Ziehen um zu positionieren" löschen*/
	$(document).on('mouseenter', '.cr-boundary', function () {
		setTimeout(function () {
			removeDragInfos();
		}, 2000);
	});

	$(document).on('click', '#save_cropped_pic, #save_cropped_slideshow, #save_audio_cropping, #save_video_cropping, .slideshow_element.empty', function () {
		removeDragInfos();
	});

	//Allgemeine Klicks
	$(document).on('click', function (e) {
		//Colorpicker
		if ($('.pcr-app').has(e.target).length == 0 && !$(e.target).is('#name_color') && !$(e.target).is('#text_color') && !$(e.target).is('#headline_color')) {
			$('.picker').removeClass('visible');
			$('.picker').attr('visible', 'false');
		}
		//Colorpicker Wave
		if (!$(e.target).closest('.pcr-app').length && !$(e.target).closest('.audio_wave').length) {
			audio_wave_pickr.hide();
			$('.audio_button_cover').fadeOut();
		}

		//Elementmenü
		if (!$(e.target).is('.element_menue') && !$(e.target).is('.element_menue>.dropdown') && !$(e.target).is('.element_menue>.dropdown>.dropdown_line')) {
			$('.element_menue>.dropdown').fadeOut(0);
		}

		//Elementmenü 2
		if (!$(e.target).is('.action_wrapper') && !$(e.target).is('.action_wrapper>.dropdown') && !$(e.target).is('.action_wrapper>.dropdown>.dropdown_line')) {
			$('.action_wrapper>.dropdown').fadeOut(0);
		}

		//Videomenü
		if (!$(e.target).closest('.mobile_element_menue').length) {
			//&& !$(e.target).closest('.audio_wave').length
			$('.mobile_element_menue>.dropdown').fadeOut();
		}

		//Audiomenü
		if ($(e.target).is('.audio_dropdown') || $(e.target).is('.audio_dropdown>.title') || $(e.target).is('.audio_dropdown>img')) {
			$(e.target).closest('.audio_dropdown').children('.dropdown').fadeToggle();
		} else {
			$('.audio_dropdown>.dropdown').fadeOut();
		}
		if (!$(e.target).is('.audio_menue') && !$(e.target).is('.audio_menue>.dropdown') && !$(e.target).is('.audio_menue>.dropdown>.audio_dropdown_line')) {
			$('.audio_menue>.dropdown').fadeOut();
		}
		if (!$(e.target).is('.audio_menue') && !$(e.target).is('.audio_menue>.dropdown') && !$(e.target).is('.audio_menue>.dropdown>.audio_dropdown_line')) {
			$('.audio_menue>.dropdown').fadeOut();
		}
		if (!$(e.target).closest('.audio_volume_slider_wrapper').length) {
			//&& !$(e.target).closest('.audio_wave').length
			$('.audio_volume_slider_wrapper>.input_slider_wrapper').fadeOut();
		}

		//Videomenü
		if (!$(e.target).closest('.video_menue').length) {
			//&& !$(e.target).closest('.audio_wave').length
			$('.video_menue>.dropdown').fadeOut();
		}

		if (!$(e.target).is('#presskit_options')) {
			$('#presskit_options_menue').fadeOut(300);
		}
	});
});
