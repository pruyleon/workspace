/**
 * Fügt Datei dem Archiv-Popup hinzu
 * @param {String} image Name der Datei
 * @return {String} Pfad der Datei
 */
function addToArchive(image) {
	var addString = '<div class="archive_file" data-name="' + image + '" style="background-image: url(/signed/src/user/' + user_id + '/archive/' + image + ')"><div class="content"><div class="delete_file"><img src="/signed/src/icns/garbage_white.svg" alt="löschen"></div></div></div>';
	$('.files').prepend(addString);
	return '/signed/src/user/' + user_id + '/archive/' + image;
}

/**
 * checkArchiveFilesLength - Fadet die "Keine Dateien im Archiv" Nachricht rein oder raus, je nachdem ob Sie vorhanden ist oder nicht
 *
 * @return {boolean}  true wenn vorhanden, false wenn nicht
 */
function checkArchiveFilesLength() {
	if ($('.archive_file').length == 0) {
		$('.no_archive_files').fadeIn();
		return false;
	} else {
		$('.no_archive_files').fadeOut();
		return true;
	}
}
checkArchiveFilesLength();

/**
 * Gibt die ID des Parent-Elements wieder
 * @param {jquery-Object} element beliebiges Child eines Profilelements
 * @return {String} data-id attr. des Elements (= Element-ID)
 */
function getParentsElementDataID(element) {
	return $(element)
		.closest('.profile_element')
		.attr('data-id');
}

/**
 * getDataType - gibt data-type attr des element zurück
 *
 * @param  {jquery-Object} element Beliebiges element in einem Profilelement
 * @return {String}         data-type attr des element
 */
function getDataType(element) {
	return $(element).attr('data-type');
}

/**
 * getInputValue - gibt den Wert des nächsten value-Inputs zurück
 *
 * @param  {Element} element beliebiges Element in einem Profilelement
 * @return {string}         wert des Inputs
 */
function getInputValue(element) {
	return $(element)
		.closest('.input_parent')
		.children('input[name=value]');
}

/**
 * getInputType - gibt den Wert des nächsten type-Inputs zurück
 *
 * @param  {Element} element beliebiges Element in einem Profilelement
 * @return {string}         wert des Inputs
 */
function getInputType(element) {
	return $(element)
		.closest('.input_parent')
		.children('input[name=type]');
}

//NOTE: deprecated?
function getInputValueVal(element) {
	return $(element)
		.closest('.input_parent')
		.children('input[name=value]')
		.val();
}

//NOTE: deprecated?
function getInputValueType(element) {
	return $(element)
		.closest('.input_parent')
		.children('input[name=type]')
		.val();
}

/**
 * addToArchive - Fügt Datei zum File-Archiv hinzu
 *
 * @param  {String} image   Dateiname der hinzuzufügenden Datei
 * @param  {String} user_id acc_ID des Users
 * @return {boolean}         true
 */
function addToArchive(image, user_id) {
	var addString = '<div class="archive_file" data-name="' + image + '" style="background-image: url(/signed/src/user/' + user_id + '/archive/' + image + ')"><div class="content"><div class="delete_file"><img src="/signed/src/icns/garbage_white.svg" alt="löschen"></div></div></div>';
	$('.files').prepend(addString);
	return true;
}

/**
 * profileElementContent - Gibt dem Profilelement den inhalt der Raw-Preview-JPG
 *
 * @param  {String} id              ID des Elements das eingefügt werden soll (wichtig für Pfad)
 * @param  {char} size                größe des einzufügenden Elements (S, M oder L)
 * @param  {Element} profile_element Element, Nach dem das neue eingefügt wird
 * @return {boolean}                 true
 */
function profileElementContent(id, size, profile_element) {
	var html = element_add_plus_disabled + '<div class="profile_element previewed blink_blue size_' + size + '" data-id="' + id + '"><div class="previewed_element_hover_effect "></div><img src="/signed/php/profile_elements/preview_raw/' + id + '.jpg "><div class="preview_blue_blink "></div></div>';
	$(profile_element).after(html);
	setTimeout(function() {
		img = $(document)
			.find('.profile_element.previewed')
			.find('img')
			.get(0);
		if (img.complete) {
			profileElementMidScroll($('.profile_element.previewed').find('img'));
		} else {
			img.addEventListener('load', loaded);
			img.addEventListener('error', function() {
				alert('Load Error 217. Please contact Admin at admin@mypressk.it');
			});
		}
	}, 200);

	setTimeout(function() {
		$('.profile_element.previewed>.preview_blue_blink').remove();
	}, 3000);
	return true;
}

/**
 * generateElementMenues - Generiert die Verschieben und löschen Menüs und triggert generateAddElementPlus();
 *
 * @return {boolean}  true
 */
function generateElementMenues() {
	$('.profile_element:not(.header)').each(function() {
		//Mülltonnen entfernen hinzufügen
		$(this)
			.next('.profile_element_menue_bottom')
			.remove();
		//Mülltonnen Menü hinzufügen
		//$(this).append('<div class="profile_element_menue_bottom"><img class="profile_element_menue_icon" title="Gesamtes Element nach oben verschieben" data-action="up" src="/signed/src/icns/up_white.svg"><br><img class="profile_element_menue_icon" title="Gesamtes Element löschen" data-action="delete" src="/signed/src/icns/garbage_white.svg"><br><img class="profile_element_menue_icon" title="Gesamtes Element nach unten verschieben" data-action="down" src="/signed/src/icns/down_white.svg"></div>');
		$(this).append('<div class="profile_element_menue_bottom"><button class="profile_element_menue_icon" title="Gesamtes Element nach oben verschieben" data-action="up" src="/signed/src/icns/up_white.svg"></button><br><button class="profile_element_menue_icon" title="Gesamtes Element löschen" data-action="delete" src="/signed/src/icns/garbage_white.svg"></button><br><button class="profile_element_menue_icon" title="Gesamtes Element nach unten verschieben" data-action="down" src="/signed/src/icns/down_white.svg"></button></div>');
		//Blauen Hoverrahmen hinzufügen
		$(this).append('<div class="blue_profile_element_hover"></div>');
	});
	$('.profile_element_menue_bottom').fadeOut(0);
	//Element Plus hinzufügen
	generateAddElementPlus(element_add_plus);
	return true;
}
generateElementMenues();

/**
 * generateAddElementPlus - Generiert die plus-Menüs zum einfügen von Elementen
 *
 * @param  {Element} element_add_plus String der Add-Element-Plus HTML Code enthält
 * @return {boolean}                  true
 */

function generateAddElementPlus(element_add_plus) {
	/*Einfügen der Plus zum hinzufügen der Elemente*/
	$('.add_profile_element_wrapper').remove();
	$('.profile_element').each(function() {
		$(this).after(element_add_plus);
	});
	return true;
}
generateAddElementPlus(element_add_plus);
