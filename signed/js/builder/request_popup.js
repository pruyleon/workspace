$(function() {
	//Anfragepopup
	$(document).on('click', '.request_popup_trigger:not(.disabled)', function() {
		$('#request_popup').fadeIn();
	});

	//Frage in Anfragepopup hinzufügen
	$('#add_question').on('click', function() {
		$('.additional_questions_append').append('<div class="full_width additional_question_wrapper"><div class="full_width"><input type="text" name="additional_question" class="additional_question_input" placeholder="Frage eingeben... "><div class="right"><input type="checkbox" name="required" checked> Pflichtfeld <button type="button" class="button_img delete_question" name="button"> <img src="/signed/src/icns/trash.svg" alt="löschen"> </button></div></div><div class="full_width"><input type="text" name="additional_answer" disabled class="additional_answer_input" value=""></div></div>');
		$('.request_info_wrapper').animate(
			{
				scrollTop: $('.request_info_wrapper').prop('scrollHeight')
			},
			1000
		);
	});

	$(document).on('click', '.delete_question', function() {
		$(this)
			.closest('.additional_question_wrapper')
			.remove();
	});

	$('#save_request').on('click', function() {
		$('#request_popup').fadeOut(500);
	});

	$('#abort_request').on('click', function() {
		$('#request_popup').fadeOut();
	});

	$('.slideshow_menue').on('click', function(e) {
		e.stopPropagation();
	});
});
