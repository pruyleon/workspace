<html lang="de">

<head>
  <meta charset="utf-8">
  <?php
  include('./php/login_state.php');
  only_logged();
  include("../src/header.php");
  include("./php/gen.php");
  $user_id = USER;
  $artist_id = getArtistID($user_id);
  $artist_name = getArtistNameAccID($user_id);
  ?>
  <title>Complete your infos <?php echo $user_id ?></title>
  <link rel="stylesheet" href="/signed/css/complete_infos.css">
</head>

<body>
  <input type="hidden" name="artist_id" value="<?php echo $artist_id ?>">
  <input type="hidden" name="user_id" value="<?php echo $user_id ?>">
  <input type="hidden" name="city" value="NULL">
  <input type="hidden" name="country_code" value="NULL">
  <div id="background"></div>
  <div class="content">
    <div class="inner_content">
      <h3>BEVOR ES LOS GEHT</h3>
      <div class="input_wrapper">
        <div class="input_desc">
          Artist name:
        </div>
        <input type="text" name="artist_name" value="" placeholder="Name eingeben...">
      </div>
      <div class="input_wrapper">
        <div class="input_desc">
          mypressk.it/
        </div>
        <input type="text" name="url" value="" placeholder="URL Endung eingeben… "> <img src="" id="url_check_icn" alt="">
      </div>
      <div class="input_wrapper">
        <div class="input_desc">
          Land & Stadt
        </div>
        <input type="text" name="address" id="addressinput" value="" placeholder="Anschrift eingeben … ">
      </div>
      <button type="button" class="blue_btn normal_button" id="continue" name="button">Weiter</button>
    </div>

  </div>
</body>
<script src="/signed/js/complete_infos/complete_infos.js" charset="utf-8"></script>
<!-- Google Autocomplete -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyFQvVybLBsn1iwUcmf09O-fdeSY-xf78&libraries=places" type="text/javascript"></script>
<script src="/signed/js/complete_infos/google_autocomplete.js" charset="utf-8"></script>

</html>