<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png" href="/src/icns/favicon.png" sizes="96x96">
<meta name="robots" content="none" />
<meta name="copyright" content="Syncronight UG (haftungsbeschränkt)" />
<meta name="audience" content="Alle" />
<meta name="expires" content="NEVER" />
<meta name="page-type" content="Kommerzielle Organisation" />
<meta name="language" content="deutsch, de" />
<meta name="publisher" content="Syncronight UG (haftungsbeschränkt)" />
<meta name="distribution" content="global" />
<meta name="page-topic" content="Pressekit" />
<meta name="page-type" content="Pressekit Builder" />
<meta name="revisit-after" content="30 days" />

<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
<!--
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
-->
<link type="text/css" rel="stylesheet" href="/css/gen.css">

<!--  JQuery -->
<script src="/src/jQuery/jquery.min.js"></script>
<!--  JQueryUI-->
<script src="/src/jQuery/jquery-ui.min.js"></script>
  <script>
    if (!$("link[href='/src/jQuery/jquery-ui.css']").length){
      $('<link rel="stylesheet" href="/src/jQuery/jquery-ui.css">').appendTo("head");
    }
  </script>

<?php
  include($_SERVER['DOCUMENT_ROOT'].'/signed/php/db.php');
?>
