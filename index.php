<html>
  <head>
    <base href="/">

    <!-- <link type="text/css" rel="stylesheet" href="/css/gen.css"> -->
    <?php include('./src/header.php'); ?>
        <title>mypressk.it</title>

        <link type="text/css" rel="stylesheet" href="./css/frontend/index.css">
        <link rel="stylesheet" href="/signed/css/popups.css">
        <script type="text/javascript">
          $(function(){
            $('#close_popup').on('click', function(){
              $('#login_popup').fadeOut();
            });

            $('#login_btn').on('click', function(){
              $('#login_popup').fadeIn();
            });

            $('.clickfadeout').on('click', function(){
              $(this).fadeOut();
            }).children().click(function(e) {
              e.stopPropagation();
            });
          });
        </script>

    </head>

    <body>
      <div id="background">
        <video playsinline autoplay loop muted>
          <source src="/src/demo/stoinks.mp4" type="video/mp4">
        </video>
        </div>
        <div class="logo_space">
          <img src="/signed/src/logos/logo_full_color.png" alt="">
        </div>
        <div class="login_button">
          <a href="#"> <button type="button" id="login_btn" name="button">Login</button> </a>
        </div>
        <div class="legal_links_wrapper">
          <a class="legal_link" href="/impressum.php" title="Impressum aufrufen">Impressum</a>
           <span>|</span>
          <a class="legal_link" href="/dataprotection.php" title="Datenschutz einsehen">Datenschutz</a>
           <!-- -
          <a class="legal_link" href="/legal/agb" title="AGB anzeigen">AGB</a> -->
        </div>

        <div id="login_popup" class="popup popup_background clickfadeout">
          <div class="popup_content" id="login_popup_content">
            <div class="popup_header">
              Login
            </div>
            <button type="button" id="close_popup" class="popup_close_btn" name="button"> <img src="/signed/src/icns/close.svg" alt="schließen"> </button>
            <form id="loginform" action="./php/account/login.php" method="POST">
              <div class="login_inputs_wrapper">
                <input type="text" name="usermail" value="" placeholder="E-Mail eingeben...">
                <input type="password" name="userpw" value="" placeholder="Passwort eingeben...">
                <input type="submit" id="SbmBtn" value="Anmelden" />
              </div>
            </form>
          </div>

        </div>



</body></html>
