<?php
ini_set('mssql.charset', 'UTF-8');

function db_connect() {

    static $connection;

    if(!isset($connection)) {

      if (!defined('SQL_INI')) {
          define('SQL_INI', $_SERVER['DOCUMENT_ROOT'].'/php/sql.ini');
      }

        if(file_exists(SQL_INI)){
            $config = parse_ini_file(SQL_INI);
        } else return mysqli_connect_error();


        $connection = mysqli_connect($config['server'],$config['username'],$config['password'],$config['dbname']);
        mysqli_set_charset($connection, "utf8");

    }

    // If connection was not successful, handle the error
    if($connection === false) {
        // Handle error - notify administrator, log to a file, show an error screen, etc.
        return mysqli_connect_error();
    }
    return $connection;
}

// Daten Einlesesn / aktualisieren
function db_query($query) {
    // Connect to the database
    $connection = db_connect();

    // Query the database
    $result = mysqli_query($connection,$query);

    return $result;
}


// Daten auslesen
function db_select($query) {
    $rows = array();
    $result = db_query($query);

    // If query failed, return `false`
    if($result === false) {
        return false;
    }

    // If query was successful, retrieve all the rows into an array
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }
    return $rows;
}

// User Input validieren
function db_escape($value) {
    $connection = db_connect();
    return mysqli_real_escape_string($connection,$value);
}

function db_check($IDcode, $DB_col, $DB = "account"){

    $qrStr = "SELECT * FROM `".$DB."` WHERE `".$DB_col."`='".db_escape($IDcode)."'";
    $test_query = db_select($qrStr);

    if($test_query === false) return false;
    else{
        if(sizeof($test_query) > 0) return true;
        else return false;
    }

    $fresh_array = array();
    $sqlquery = "SELECT `".$DB_col."` FROM `".$DB."`";
    $sqlResult = db_select($sqlquery);

    if(sizeof($sqlResult) > 0 && $sqlResult != false){

        foreach($sqlResult as $snglrow){
            array_push($fresh_array, $snglrow[$DB_col]);
        }

        if(in_array($IDcode, $fresh_array) == true){
            // ID vorhanden
            return true;

        }else return false;

    }else return false;

}

?>
