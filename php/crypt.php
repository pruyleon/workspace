<?php
function encrypt($val, $ky)
{
    // you may change these values to your own
    $secret_iv = $ky;

    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash('sha256', $ky);
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    $output = base64_encode(openssl_encrypt($val, $encrypt_method, $key, 0, $iv));

    return $output;
}

// Decrypting
function decrypt($val, $ky)
{
    // you may change these values to your own
    $secret_iv = $ky;

    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash('sha256', $ky);
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    
    $output = openssl_decrypt(base64_decode($val), $encrypt_method, $key, 0, $iv);
    
    return $output;
}
