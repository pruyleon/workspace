<html lang="de" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>Registrieren</title>
  <base href="/" />
  <?php
  include('../../src/header.php');
  include('../../signed/php/gen.php');
  // PFAD AUS INI laden
  $iniPath = '../path.ini';
  if (file_exists($iniPath)) {
    $iniContent = parse_ini_file($iniPath);
    $mainURL = $iniContent['HostName'];
    $secure = $iniContent['Secure'] == true ? "https://" : "http://";
  }
  $secure = "http://";
  //$redirect_link = "/complete_infos";//$secure
  //echo $redirect_link;
  ?>

  <link type="text/css" rel="stylesheet" href="./css/frontend/register_artist.css" />

  <script src="/js/register/artist.js" charset="utf-8"></script>

</head>

<body>
  <div id="background"></div>
  <div class="content">
    <div class="inner_content">
      <h3>Jetzt registrieren</h3>
      <div class="input_wrapper">
        <div class="input_desc">
          E-Mail:
        </div>
        <input type="text" id="mail_address" value="" placeholder="">
      </div>
      <div class="full_width">
        <div class="input_wrapper">
          <div class="input_desc">
            Passwort:
          </div>
          <input type="password" id="password" class="register_input half">
        </div>
        <div class="input_wrapper">
          <div class="input_desc">
            Passwort wiederholen:
          </div>
          <input type="password" id="password_repeat" class="register_input half">
        </div>
      </div>
      <div class="register_checkboxes_wrapper">
        <div class="checkbox_inner_wrapper" id="agb_wrapper">
          <input type="checkbox" id="agb_checkbox" value="1">
          <span class="checkbox_desc">Mit meiner Anmeldung akzeptiere ich die <a href="/legal/agb" target="_blank" class="clean_link_visible">mypressk.it-AGB</a> </span>
        </div>
        <div class="checkbox_inner_wrapper" id="data_wrapper">
          <input type="checkbox" id="dataprotect_checkbox" value="1">
          <span class="checkbox_desc">Ich willige in die Verarbeitung und Nutzung meiner Daten gemäß der <a href="/legal/agb" target="_blank" class="clean_link_visible">mypressk.it-Datenschutzerklärung</a> ein</span>
        </div>
      </div>
      <div class="error_wrapper"></div>
      <button type="button" class="blue_btn normal_button" id="register" name="button">Weiter</button>

    </div>
  </div>

  <div class="" id="popup_load_screen">
    <div class="popup_content load_popup">
      <img src="/signed/src/icns/loading_screen.gif" class="load_img">
    </div>
  </div>
  <form class="" id="login_form" action="../../php/account/login.php" method="post">
    <input type="text" placeholder="E-Mail Adresse eingeben..." class="login_input" name="usermail" id="usermail" value="" />
    <input type="password" placeholder="Passwort eingeben..." class="login_input" name="userpw" id="userpw" />
  </form>

</body>

</html>