<?php
  include('../../../../signed/php/db.php');
  include('../../../sql.php');

  //User_id bzw acc_ID generieren
  $genID = generateUniqueID();
  $artist_id = genArtistID();

  $name                     = $_POST['name'];
  $prename                  = $_POST['prename'];
  $artist_name              = $_POST['artist_name'];
  $mail_address             = $_POST['mail_address'];
  $password                 = $_POST['password'];
  $address_form             = $_POST['address_form'];
  $company_name             = $_POST['company_name'];
  $street                   = $_POST['street'];
  $address_addition         = $_POST['address_addition'];
  $country                  = $_POST['country'];
  $plz                      = $_POST['plz'];
  $city                     = $_POST['city'];
  $street_company           = $_POST['street_company'];
  $address_addition_company = $_POST['address_addition_company'];
  $country_company          = $_POST['country_company'];
  $plz_company              = $_POST['plz_company'];
  $city_company             = $_POST['city_company'];
  $position                 = $_POST['position'];
  $club_name                = $_POST['club_name'];


  $account_query = "INSERT INTO `account`(`acc_ID`, `form_of_address`, `position`, `name`, `prename`, `company`, `mail`, `address`, `PLZ`, `city`, `country`, `address_addition`, `password`, `acc_type`, `mail_active`) VALUES ('$genID', '$address_form', '$position', '$name', '$prename', '$company_name', '$mail_address', '$street_company', '$plz_company', '$city_company', '$country_company', '$address_addition_company', AES_ENCRYPT('$password', '$genID'), '2', '0')";
  mysqli_query($con, $account_query);
  $insert_query = "INSERT INTO `host_location` (acc_ID, loc_name, address, address_addition, plz, city, country, bill_street, bill_city, bill_email) VALUES ('$genID', '$club_name', '$street', '$address_addition', '$plz', '$city', '$country', '$street_company', '$city_company', '$mail')";
  mysqli_query($con, $insert_query);

  // Ordner erstellen und Profilbild ablegen
  $copyPath = "../../../../src/pic/default_profile.jpg";
  $userPath = '../../../../signed/src/user/'.$genID.'/';
  $profileName = "profile.jpg";
  if(!is_dir($userPath)) mkdir($userPath, 0755, true);

  if(is_file($userPath.$profileName)) unlink($userPath.$profileName);

  copy($copyPath, $userPath.$profileName);

  echo "user_ID=$genID";
  // Weiterleiten
  //header('Location: ../account/verify_mail.php?token='.$linkToken);
  //exit();


  function genArtistID(){
      $chars = "123456789abcdefghijklmnopqrstuvwxyz";
      $returnID = "";

      for($n = 0; $n<10; $n++){
          $addChar = $chars[rand(0, strlen($chars)-1)];
          $returnID .= $addChar;
      }

      if(db_check($returnID, 'artist_ID', 'artists') == true) return genArtistID();

      return $returnID;
  }

  function generateUniqueID(){

      $chars = "123456789abcdefghijklmnopqrstuvwxyz";
      $returnID = "";
      for($n = 0; $n<12; $n++){
          if($n % 3 == 0 && $n > 0)$returnID .= "-";
          $returnID .= $chars[rand(0, strlen($chars)-1)];
      }

      if(db_check($returnID, 'acc_ID') == true) return generateUniqueID();

      return $returnID;
  }

  //echo $insert_query;
 ?>
