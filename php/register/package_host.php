<?php
  $user_id = $_GET['user_ID'];
 ?>
<html lang="de" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Preispaket wählen</title>
    <base href="/" />
    <link type="text/css" rel="stylesheet" href="./css/frontend/register_host.css" />
    <?php
      include('../../src/header.php');
      include('../../signed/php/db_jquery.php');
      // PFAD AUS INI laden
      $iniPath = '../path.ini';
      if(file_exists($iniPath)){
          $iniContent = parse_ini_file($iniPath);
          $mainURL = $iniContent['HostName'];
          $secure = $iniContent['Secure'] == true ? "https://" : "http://";
      }
      $redirect_link = "".$secure.$mainURL."/mail.php?";
      ?>

      <script>
        $(function(){
          var redirect_link = "<?php echo $redirect_link ?>";
          var user_id = "<?php echo $user_id ?>";
          $('.package').on('click', function(){
            var clickedPackage = $(this);
            $('.package').each(function(){
              $(this).removeClass('selected_package');
            });
            $(clickedPackage).addClass('selected_package');
          });

          $('#submit_package').on('click', function(){

            $('#auth_checkbox_desc').css("color", "#000000");
            $('.error_wrapper').html('');
            var auth_check = true;
            var iban_check = true;
            if(!$('#auth_checkbox').is(':checked')){
              auth_check = false;
              $('#auth_checkbox_desc').css("color", "red");
            }
            if ($('#iban').val().length < 20) {
              iban_check = false;
              $('.error_wrapper').append("<br>Bitte überprüfe deine IBAN!");
            }

            var package_type = $('.selected_package').attr('type');
            var payment_intervall = $('#payment_intervall').val();
            var iban = $('#iban').val();

            if (iban_check && auth_check) {
              $('#popup_load_screen').fadeIn();
              $.ajax({
                url: "/php/register/host/inserts/package.php",
                type: "post",
                data: {
                  user_id: user_id,
                  iban: iban,
                  package_type: package_type,
                  payment_intervall: payment_intervall
                }, success: function(response){
                  //alert(response);
                  //alert(redirect_link+response);
                  $('#popup_load_screen').fadeOut();
                  window.location.replace(redirect_link+response);
                }
              });
            }



          });
        });
      </script>

  </head>
  <body>
    <div class="login_top_line">
      <div class="nav_logo_wrapper">
        <a href="/home"><img src="/src/icns/logo_white.svg" alt="SYNCRONIGHT" class="nav_logo"></a>
      </div>
      <div class="login_wrapper">
        <span class="nav_link_wrapper"><a href="/register/artist" class="nav_link" >Du bist ein Künstler?</a></span>
        <span class="nav_link_wrapper"><a class="nav_link" href="/login">Einloggen</a></span>
      </div>
    </div>

    <div class="master_wrapper package_select">

      <div class="register_wrapper">
        <div class="package_wrapper">
          <h2 class="block_line_header">
            Paketwahl
          </h2>
          <div class="package_inner_wrapper">
            <div class="package"  type="1">
              <div class="package_desc">
                <div class="package_name">
                  Basic
                </div>
                <div class="package_price">
                  5€
                </div>
              </div>
              <div class="package_specs">
                Rechnungsgenerator <br>
                Vertragsgenerator<br>
                Presse PDF nur URL
              </div>
            </div>
            <div class="package selected_package"  type="2">
              <div class="package_desc">
                <div class="package_name">
                  Professional
                </div>
                <div class="package_price">
                  40€
                </div>
              </div>
              <div class="package_specs">
                Rechnungsgenerator <br>
                Vertragsgenerator<br>
                Presse PDF<br>
                Listing in der Suche<br>
                Tourmanager
              </div>
            </div>
            <div class="package" type="3">
              <div class="package_desc">
                <div class="package_name">
                  High End
                </div>
                <div class="package_price">
                  60€
                </div>
              </div>
              <div class="package_specs">
                Rechnungsgenerator <br>
                Vertragsgenerator<br>
                Presse PDF<br>
                Listing in der Suche<br>
                Tourmanager <br>
                Promovideo <br>
                bevorzugtes Listing
              </div>
            </div>
          </div>
        </div>
        <div class="voucher_wrapper">
          <div class="block_line">
            <div class="input_wrapper_half left">
              <div class="input_wrapper_inner">
                <div class="input_header">
                  Gutscheincode
                </div>
                <input type="text" id="voucher" class="register_input half package_input">
              </div>

            </div>
          <div class="input_wrapper_half right">


          </div>
        </div>


        </div>
        <div class="payment_wrapper">

          <div class="block_line">
            <h2 class="block_line_header">
              Zahlungsinformationen
            </h2>
            <div class="input_wrapper_half left">
              <div class="input_wrapper_inner">
                <div class="input_header">
                  IBAN
                </div>
                <input type="text" id="iban" class="register_input half nedded_input">
              </div>

            </div>
          <div class="input_wrapper_half right">
            <div class="input_wrapper_inner">
              <div class="input_header">
                Zahlungsweise
              </div>
              <select class="payment_selector" id="payment_intervall">
                <option value="1">monatlich</option>
                <option value="2">vierteljährlich</option>
                <option value="3">jährlich</option>
              </select>
            </div>

          </div>
        </div>
        </div>

        <div class="checkboxes_wrapper">
          <div class="checkbox_wrapper">
            <input type="checkbox" id="auth_checkbox" value="1">
          </div>
          <div class="checkbox_desc" id="auth_checkbox_desc">
            Ich ermächtige die Syncronight UG , Zahlungen von meinem Konto mittels Lastschrift einzuziehen. Zugleich weise ich mein Kreditinstitut an, die von der Syncronight UG auf mein Konto gezogenen Lastschriften einzulösen. Dieses Rahmenmandat bezieht sich auf alle gegenwärtigen und zukünftigen Vertragsverhältnisse mit der Syncronight UG.
          </div>
        </div>

        <div class="error_wrapper">

        </div>

        <div class="continue_btn_wrapper">
          <button type="button" class="continue_btn btn_normal_small btn_blue" id="submit_package">Registrierung abschließen</button>
        </div>


      </div>
    </div>

    <div class="" id="popup_load_screen">
      <div class="popup_content load_popup">
        <img src="/signed/src/icns/load_screen.gif" class="load_img">
      </div>
    </div>

  </body>
</html>
