<?php

function generateUniqueID()
{
    $chars = "123456789abcdefghijklmnopqrstuvwxyz";
    $returnID = "";
    for ($n = 0; $n<12; $n++) {
        if ($n % 3 == 0 && $n > 0) {
            $returnID .= "-";
        }
        $returnID .= $chars[rand(0, strlen($chars)-1)];
    }
        
    if (db_check($returnID, 'acc_ID') == true) {
        return generateUniqueID();
    }
    
    return $returnID;
}
