<html lang="de" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Registrieren</title>
    <base href="/" />
    <?php
      include('../../src/header.php');
      include('../../signed/php/db_jquery.php');
      // PFAD AUS INI laden
      $iniPath = '../path.ini';
      if(file_exists($iniPath)){
          $iniContent = parse_ini_file($iniPath);
          $mainURL = $iniContent['HostName'];
          $secure = $iniContent['Secure'] == true ? "https://" : "http://";
      }
      $redirect_link = "".$secure.$mainURL."/php/register/package_host.php?";
  ?>

    <link type="text/css" rel="stylesheet" href="./css/frontend/register_host.css" />

    <script>
      $(function(){
        var redirect_link = "<?php echo $redirect_link ?>";

        $('#same_address').on('change', function(){
          if ($(this).is(':checked')) {
            $('#street_company').val($('#street').val());
            $('#country_company').val($('#country').val());
            $('#plz_company').val($('#plz').val());
            $('#city_company').val($('#city').val());
            $('#address_addition_company').val($('#address_addition').val());
          }
        });


        $('.continue_btn').on('click', function(){
          $('.error_wrapper').html(" ");
          $('#agb_wrapper').css("color", "black");
          $('#data_wrapper').css("color", "black");
          if (!$('#agb_checkbox').is(':checked')) {
            $('#agb_wrapper').css("color", " red");
          }
          if (!$('#dataprotect_checkbox').is(':checked')) {
            $('#data_wrapper').css("color", " red");
          }

          var mail_address             = $('#mail_address').val();
          var password                 = $('#password').val();
          var password_repeat          = $('#password_repeat').val();
          var address_form             = $('#address_form').val();
          var prename                  = $('#prename').val();
          var name                     = $('#name').val();
          var company_name             = $('#company_name').val();
          var street                   = $('#street').val();
          var address_addition         = $('#address_addition').val();
          var country                  = $('#country').val();
          var plz                      = $('#plz').val();
          var city                     = $('#city').val();
          var club_name                = $('#club_name').val();
          var position                 = $('#position').val();
          var company_name             = $('#company_name').val();
          var street_company           = $('#street_company').val();
          var address_addition_company = $('#address_addition_company').val();
          var country_company          = $('#country_company').val();
          var plz_company              = $('#plz_company').val();
          var city_company             = $('#city_company').val();

          if (address_form == "") {
            address_form = "0";
          }

          var datacheck = true;
          var mail_check = true;
          var mailcheck2 = false;
          var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          mail_check = regex.test(mail_address);
          if (mail_check == false) {
            $('.error_wrapper').append("<br>Bitte gebe eine gültige Email Adresse an!");
          }
          $.ajax({
            url: "/php/register/check_email.php",
            data: {mail_address: mail_address},
            type: "post",
            success: function(response){
              if (response == "1") {
                mailcheck2 = true;
              } else {
                $('.error_wrapper').append("<br>Die Mailadresse ist bereits vergeben");
                mailcheck2 = false;
              }
                if (password != password_repeat) {
                  var password_check = false;
                  $('.error_wrapper').append("<br>Die Passwörter stimmen nicht überein");
                } else {
                  var password_check = true;
                }

                if (password.length < 7 ) {
                  $('.error_wrapper').append("<br>Dein Passwort muss mindestens 7 Zeichen lang sein!");
                }


                datacheck = true;
                $('.nedded_input').each(function(){
                  if ($(this).val() == "" || $(this).val() == " " ) {
                    $(this).css('border-color', 'red');
                    datacheck = false;
                  } else {
                    $(this).css('border-color', '#CCCCCC');
                  }
                });
                if (datacheck == false) {
                  $('.error_wrapper').append("<br>Bitte fülle alle Pflichtfelder aus!");
                }

                if ($('#dataprotect_checkbox').is(':checked') && $('#agb_checkbox').is(':checked') && mail_check == true && password_check == true && datacheck == true && mailcheck2 == true) {
                  $('#popup_load_screen').fadeIn();
                  $.ajax({
                    url: "/php/register/host/inserts/host.php",
                    type: "post",
                    data: {
                      mail_address: mail_address,
                      password: password,
                      password_repeat: password_repeat,
                      address_form: address_form,
                      prename: prename,
                      name: name,
                      street: street,
                      address_addition: address_addition,
                      country: country,
                      plz: plz,
                      city: city,
                      club_name: club_name,
                      position: position,
                      company_name: company_name,
                      street_company: street_company,
                      address_addition_company: address_addition_company,
                      country_company: country_company,
                      plz_company: plz_company,
                      city_company: city_company
                    }, success: function(response){
                      //alert(response);
                      //alert(redirect_link+response);
                      $('#popup_load_screen').fadeOut();
                      window.location.replace(redirect_link+response);
                    }
                  });
                }


            }
          });





        });
      });

    </script>

  </head>

  <body>
    <div class="login_top_line">
      <div class="nav_logo_wrapper">
        <a href="/home"><img src="/src/icns/logo_white.svg" alt="SYNCRONIGHT" class="nav_logo"></a>
      </div>
      <div class="login_wrapper">
        <span class="nav_link_wrapper"><a href="/register/artist" class="nav_link" >Du bist ein Künstler?</a></span>
        <span class="nav_link_wrapper"><a class="nav_link" href="/login">Einloggen</a></span>
      </div>
    </div>

    <div class="master_wrapper">
      <div class="sell">
        <img src="/src/pic/artist.jpg" class="sell_img"alt="">
      </div>


      <div class="register_wrapper">

        <div class="register_header font_size_big font_weight_heavy">
          Jetzt registrieren
        </div>
        <div class="block bottom_line">
          <div class="block_line">
            <div class="input_wrapper_half left">
              <div class="input_wrapper_inner">
                <div class="input_header">
                  Clubname (öffentlich)
                </div>
                <input type="text" id="club_name" class="register_input half nedded_input">
              </div>

            </div>
            <div class="input_wrapper_half right">
              <div class="input_wrapper_inner">

              </div>

            </div>
          </div>
          <div class="block_line">
            <div class="input_wrapper_half left">
              <div class="input_wrapper_inner">
                <div class="input_header">
                  Straße und Hausnummer
                </div>
                <input type="text" id="street" class="register_input half nedded_input">
              </div>

            </div>
          <div class="input_wrapper_half right">
            <div class="input_wrapper_inner">
              <div class="input_header">
                Ergänzende Angaben
              </div>
              <input type="text" id="address_addition" class="register_input half">
            </div>

          </div>
        </div>

        <div class="block_line">
          <div class="input_wrapper_half left">
            <div class="input_wrapper_inner">
              <div class="input_header">
                Land
              </div>
              <select class="country_select"  id="country">
                <option value="DE">Deutschland</option>
                <option value="AT">Österreich</option>
                <option value="CH">Schweiz</option>
              </select>
            </div>

          </div>
          <div class="input_wrapper_half right">
            <div class="input_wrapper_inner">
              <div class="plz_wrapper">
                <div class="input_header">
                  PLZ
                </div>
                <input type="text" id="plz" class="register_input plz nedded_input">
              </div>
              <div class="city_wrapper">
                <div class="input_header">
                  Ort
                </div>
                <input type="text" id="city" class="register_input city nedded_input">
              </div>
            </div>


            </div>
          </div>
        </div>



        <div class="block bottom_line">
          <div class="block_line">

            <div class="input_wrapper_half left">
              <div class="input_wrapper_inner">
                <div class="input_header">
                  E-Mail
                </div>
                <input type="text" id="mail_address" class="register_input half nedded_input">
              </div>

            </div>
          </div>
          <div class="block_line">
            <div class="input_wrapper_half left">
              <div class="input_wrapper_inner">
                <div class="input_header">
                  Passwort
                </div>
                <input type="password" id="password" class="register_input half nedded_input">
              </div>

            </div>
          <div class="input_wrapper_half right">
            <div class="input_wrapper_inner">
              <div class="input_header">
                Passwort wiederholen
              </div>
              <input type="password" id="password_repeat" class="register_input half nedded_input">
            </div>

          </div>
        </div>
    </div>


    <div class="block bottom_line">
      <div class="second_block">
        <div class="block_line">
          <div class="input_wrapper_half left">
            <div class="input_wrapper_inner">
              <div class="input_header">
                Anrede
              </div>
              <select class="form_select" id="address_form">
                <option value="0"> </option>
                <option value="1">Herr</option>
                <option value="2">Frau</option>
              </select>
            </div>

          </div>

          <div class="input_wrapper_half right">
            <div class="input_wrapper_inner">
              <div class="input_header">
                Deine Position
              </div>
              <select class="form_select big" id="position">
                <option value="0">Manager</option>
                <option value="1">Inhaber</option>
                <option value="2">Booker</option>
              </select>
            </div>

          </div>

        </div>
        <div class="block_line">
          <div class="input_wrapper_half left">
            <div class="input_wrapper_inner">
              <div class="input_header">
                Vorname
              </div>
              <input type="text" id="prename" class="register_input half nedded_input">
            </div>

          </div>
          <div class="input_wrapper_half right">
            <div class="input_wrapper_inner">
              <div class="input_header">
                Nachname
              </div>
              <input type="text" id="name" class="register_input half nedded_input">
            </div>

          </div>
        </div>
      </div>

      </div>

      <div class="block last">
        <div class="block_line">
          <div class="input_wrapper_half left">
            <div class="input_wrapper_inner">
              <div class="input_header">
                Name des Unternehmens (optional)
              </div>
              <input type="text" id="company_name" class="register_input half">
            </div>

          </div>
          <div class="input_wrapper_half right">
            <div class="input_wrapper_inner">

            </div>

          </div>
        </div>
        <div class="block_line">
          <div class="checkbox_inner_wrapper" id="same_address_wrapper">
            <input type="checkbox" id="same_address" value="1">
            <span class="checkbox_desc">Anschrift gleich der Clubaddresse</span>
          </div>

        </div>
        <div class="block_line">
          <div class="input_wrapper_half left">
            <div class="input_wrapper_inner">
              <div class="input_header">
                Straße und Hausnummer
              </div>
              <input type="text" id="street_company" class="register_input half nedded_input">
            </div>

          </div>
        <div class="input_wrapper_half right">
          <div class="input_wrapper_inner">
            <div class="input_header">
              Ergänzende Angaben
            </div>
            <input type="text" id="address_addition_company" class="register_input half">
          </div>

        </div>
      </div>

        <div class="block_line">
          <div class="input_wrapper_half left">
            <div class="input_wrapper_inner">
              <div class="input_header">
                Land
              </div>
              <select class="country_select"  id="country_company">
                <option value="DE">Deutschland</option>
                <option value="AT">Österreich</option>
                <option value="CH">Schweiz</option>
              </select>
            </div>

          </div>
          <div class="input_wrapper_half right">
            <div class="input_wrapper_inner">
              <div class="plz_wrapper">
                <div class="input_header">
                  PLZ
                </div>
                <input type="text" id="plz_company" class="register_input plz nedded_input">
              </div>
              <div class="city_wrapper">
                <div class="input_header">
                  Ort
                </div>
                <input type="text" id="city_company" class="register_input city nedded_input">
              </div>
            </div>

            </div>
            </div>
          </div>



    </div>


  <div class="register_checkboxes_wrapper">
    <div class="checkbox_inner_wrapper" id="agb_wrapper">
      <input type="checkbox" id="agb_checkbox" value="1">
      <span class="checkbox_desc">Mit meiner Anmeldung akzeptiere ich die <a href="/legal/agb" target="_blank" class="clean_link_visible">Syncronight-AGB</a> </span>
    </div>
    <div class="checkbox_inner_wrapper" id="data_wrapper">
      <input type="checkbox" id="dataprotect_checkbox" value="1">
      <span class="checkbox_desc">Ich willige in die Verarbeitung und Nutzung meiner Daten gemäß der <a href="/legal/agb" target="_blank" class="clean_link_visible">Syncronight-Datenschutzerklärung</a> ein</span>
    </div>
  </div>

  <div class="error_wrapper">

  </div>

  <div class="continue_btn_wrapper">
    <button type="button" class="continue_btn btn_normal_small btn_blue">weiter</button>
  </div>


    </div>
  </div>

  <div class="" id="popup_load_screen">
    <div class="popup_content load_popup">
      <img src="/signed/src/icns/load_screen.gif" class="load_img">
    </div>
  </div>

  </body>
</html>
