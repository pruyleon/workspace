<?php
include('../../../../signed/php/db.php');
include('../../../sql.php');

include('../../../cookie.php');


require '../../../crypt.php';


$mail_address = $_POST['mail_address'];
$password = $_POST['password'];

//User_id bzw acc_ID generieren
$genID = generateUniqueID();
$artist_id = genArtistID();

$account_query = "INSERT INTO `account`(`acc_ID`, `mail`, `password`, `acc_type`, `mail_active`) VALUES ('$genID', '$mail_address', AES_ENCRYPT('$password', '$genID'), '0', '1')";
mysqli_query($con, $account_query);
// echo $account_query;

$insert_query = "INSERT INTO `artists` (acc_ID, artist_ID) VALUES ('$genID', '$artist_id')";
mysqli_query($con, $insert_query);
// echo $insert_query;

// Ordner erstellen und Profilbild ablegen
$copyPath = "../../../../src/pic/default_profile.jpg";
$userPath = '../../../../signed/src/user/' . $genID . '/';
$profileName = "profile.jpg";
if (!is_dir($userPath)) {
    mkdir($userPath, 0755, true);
}
$archivePath = '../../../../signed/src/user/' . $genID . '/archive/';
if (!is_dir($archivePath)) {
    mkdir($archivePath, 0755, true);
}
$archivePath = '../../../../signed/src/user/' . $genID . '/presspack/';
if (!is_dir($archivePath)) {
    mkdir($archivePath, 0755, true);
}
$archivePath = '../../../../signed/src/user/' . $genID . '/video/';
if (!is_dir($archivePath)) {
    mkdir($archivePath, 0755, true);
}
$archivePath = '../../../../signed/src/user/' . $genID . '/audio/';
if (!is_dir($archivePath)) {
    mkdir($archivePath, 0755, true);
}
$archivePath = '../../../../signed/src/user/' . $genID . '/temp_presspack/';
if (!is_dir($archivePath)) {
    mkdir($archivePath, 0755, true);
}
$archivePath = '../../../../signed/src/user/' . $genID . '/presspack/picture/';
if (!is_dir($archivePath)) {
    mkdir($archivePath, 0755, true);
}
$archivePath = '../../../../signed/src/user/' . $genID . '/presspack/logo/';
if (!is_dir($archivePath)) {
    mkdir($archivePath, 0755, true);
}
$archivePath = '../../../../signed/src/user/' . $genID . '/presspack/video/';
if (!is_dir($archivePath)) {
    mkdir($archivePath, 0755, true);
}
$archivePath = '../../../../signed/src/user/' . $genID . '/presspack/misc/';
if (!is_dir($archivePath)) {
    mkdir($archivePath, 0755, true);
}
$archivePath = '../../../../signed/src/user/' . $genID . '/presspack/bio/';
if (!is_dir($archivePath)) {
    mkdir($archivePath, 0755, true);
}

if (is_file($userPath . $profileName)) {
    unlink($userPath . $profileName);
}

copy($copyPath, $userPath . $profileName);
echo $genID;
$user_id = $genID;

//Bestätigungsmail
// Bestätigungsmail senden
$linkToken = urlencode(encrypt($user_id, "linkKEY"));
$mailToken = urlencode(encrypt($user_id, "mailKEY"));

// PFAD AUS INI laden
$iniPath = '../../../path.ini';
if (file_exists($iniPath)) {
    $iniContent = parse_ini_file($iniPath);
    $mainURL = $iniContent['HostName'];
    $secure = $iniContent['Secure'] == true ? "https://" : "http://";
}

$msg = '<html><head><style type="text/css">body{background-color: #727272; color: #FFFFFF; padding-top: 50px;} .logo_wrapper{width: 100%; margin-bottom: 50px;} .header_logo{min-width: 50%; max-width: 300px; margin: auto;} .mail_header{margin-bottom: 10px;} .mail_body{padding: 20px;} .link_wrapper{margin-top: 30px;margin-bottom: 30px;} .link{color: #FFFFFF;}</style></head><body>';
$msg .= '<div class="logo_wrapper"><img src="' . $secure . $mainURL . '/src/icns/logo_white.svg" class="header_logo" alt="Logo"></div>';
$msg .= '<div class="mail_body"><div class="mail_header">Hallo,</div><div class="mail_content">Herzlich willkommen bei mypresskit!<br>Klicke auf den untenstehenden Link, um Deine E-Mail Adresse zu bestätigen und deine Registrierung abzuschließen.</div><div class="link_wrapper"><a href="' . $secure . $mainURL . '/php/account/verify_mail.php?validate=' . $mailToken . '&user_type=1&user_id=' . $user_id . '" class="link">E-Mail Adresse bestätigen</a></div>Viele Grüße, <br>Dein <a href="https://mypressk.it" class="link">mypressk.it</a>-Team</div></body></html>';

require '../../../../src/phpmailer/PHPMailer.php';
require "../../../../src/phpmailer/SMTP.php";
require "../../../../src/phpmailer/Exception.php";

$mail = new PHPMailer\PHPMailer\PHPMailer();
$mail->IsSMTP(); // enable SMTP

$mail->SMTPDebug = false; // debugging: 1 = errors and messages, 2 = messages only
$mail->SMTPAuth = true; // authentication enabled
$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
$mail->Host = "mail.mypressk.it";
$mail->Port = 465; // or 587
$mail->IsHTML(true);
$mail->CharSet = "UTF-8";
$mail->Username = "welcome@mypressk.it";
$mail->Password = "126Millionendigga$$$";
$mail->SetFrom("welcome@mypressk.it");
$mail->Subject = "Bitte bestätige deine Mailadresse";
$mail->Body = $msg;
$mail->AddAddress("$mail_address");

if (!$mail->Send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    //echo "Message has been sent";
}

function genArtistID()
{
    $chars = "123456789abcdefghijklmnopqrstuvwxyz";
    $returnID = "";

    for ($n = 0; $n < 10; $n++) {
        $addChar = $chars[rand(0, strlen($chars) - 1)];
        $returnID .= $addChar;
    }

    if (db_check($returnID, 'artist_ID', 'artists') == true) {
        return genArtistID();
    }

    return $returnID;
}

function generateUniqueID()
{
    $chars = "123456789abcdefghijklmnopqrstuvwxyz";
    $returnID = "";
    for ($n = 0; $n < 12; $n++) {
        if ($n % 3 == 0 && $n > 0) {
            $returnID .= "-";
        }
        $returnID .= $chars[rand(0, strlen($chars) - 1)];
    }

    if (db_check($returnID, 'acc_ID') == true) {
        return generateUniqueID();
    }

    return $returnID;
}

function genSessionID()
{
    $chars = "123456789abcdefghijklmnopqrstuvwxyz123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $returnID = "";
    for ($n = 0; $n < 12; $n++) {
        if ($n % 3 == 0 && $n > 0) {
            $returnID .= "-";
        }
        $returnID .= $chars[rand(0, strlen($chars) - 1)];
    }

    return $returnID;
}

//  echo $insert_query;
