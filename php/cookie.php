<?php
function makeCookie($cookieGroup, $name, $value, $expire = 0)
{
    // Cookie Version
    $cookieDom = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
    setcookie($cookieGroup.'['.$name.']', $value, $expire, "/", $cookieDom, false);

    // Session Version
    // NOTE: Benötigt eventuell Überarbeitung
    /*if (defined(session_status)) {
      if(session_status != PHP_SESSION_ACTIVE){
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
      }
    } else {
      if (session_status() == PHP_SESSION_NONE) {
          session_start();
      }
    }*/

    $_SESSION[$cookieGroup] = true;
    $session_key = $cookieGroup."_".$name;
    $_SESSION[$session_key] = $value;
}

function getCookie($cookieGroup, $name = false)
{
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }
    // NOTE: Benötigt eventuell Überarbeitung
    /*if (defined(session_status)) {

      if(session_status != PHP_SESSION_ACTIVE){
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
      }
    } else {
      if (session_status() == PHP_SESSION_NONE) {
          session_start();
      }
    }*/


    if ($name == false) {
        if (!isset($_COOKIE[$cookieGroup]) && !isset($_SESSION[$cookieGroup])) {
            return false;
        } else {
            return true;
        }
    } else {
        if (isset($_COOKIE[$cookieGroup])) {
            if (isset($_COOKIE[$cookieGroup][$name])) {
                return $_COOKIE[$cookieGroup][$name];
            }
        } else {
            $session_key = $cookieGroup."_".$name;
            if (isset($_SESSION[$session_key])) {
                return $_SESSION[$session_key];
            } else {
                return false;
            }
        }
    }
}
