<?php
// login überprüfung
if(isset($_POST['tk1']) && $_POST['tk1'] != null && isset($_POST['tk2']) && $_POST['tk2'] != null){
    $tk1 = $_POST['tk1'];
    $tk2 = $_POST['tk2'];
    
    require('../crypt.php');
    $tk1_decrypt = decrypt(urldecode($tk1), "PW_Reset_Key");
    $userID = decrypt(urldecode($tk2), $tk1_decrypt);
    
    // Datenbank überprüfung
    include('../sql.php');
    $db_token = db_select("SELECT AES_DECRYPT(`password`, '".$userID."'), `session` FROM `account` WHERE `acc_ID`='".$userID."'");
    if($db_token != false && sizeof($db_token) == 1){
        
        $dbPW = $db_token[0]["AES_DECRYPT(`password`, '".$userID."')"];

        
        if($db_token[0]['session'] != $tk1_decrypt){
            // Token stimmen nicht überein
            jumphome();
            return;
        }else{
            // Passwort ändern
            $pw1 = $_POST['user_pw_1'];
            $pw2 = $_POST['user_pw_2'];
            
            if($pw1 != $pw2){
                jumphome(2);
                return;
            }else{
                
                if($dbPW == $pw1){
                    jumphome(1);
                    return;
                }else{
                    // Passwort aktualisieren
                    $action = db_query("UPDATE `account` SET `password`=AES_ENCRYPT('".$pw1."', '".$userID."') WHERE `acc_ID`='".$userID."'");
                    if($action != false){
                        // Passwort erfolgreich geändert
                        header('Location: ../../login/6');
                    }
                    
                }
            }
        }
        
    }else{
        jumphome();
        return;
    }
}else{
    jumphome();
    return;
}

function jumphome($t = 0){
    if(!isset($_POST['tk1']) || !isset($_POST['tk2'])){
        header('Location: ../../login/0');
    } else {
        header('Location: ../../set_pw/'.$_POST['tk1'].'/'.$_POST['tk2'].'/'.$t);
    }
    
    exit();
}
?>