<?php
include('../sql.php');

if(isset($_POST['usermail']) && $_POST['usermail'] != null){
    $userInput = db_escape($_POST['usermail']);
}else{
    jumpPW(1);
    return;
}

// Überprüfen ob Mail vorhanden
if(!db_check($userInput, 'mail')){
    jumpPW(1);
    return;
}

// Code generieren
    // User aus db laden
    $dbQuery = db_select("SELECT `acc_ID`, `prename` FROM `account` WHERE `mail`='".$userInput."'");
    if($dbQuery == false || sizeof($dbQuery) > 1){
    //  Passwort nicht gefunden
        jumpPW();
        return;
    }else{
        $dbUser = $dbQuery[0]['acc_ID'];
        $preName = $dbQuery[0]['prename'];
    }

// Token generieren und In Datenbank Session eintragen
$gen_PW_token = genPW_reset_id();

$result = db_query("UPDATE `account` SET `session`='".$gen_PW_token."' WHERE `acc_ID`='".$dbUser."'");
if(!$result){
//  Fehler bei der Session in DB
    jumpPW();
    return;
}
require('../crypt.php');
$mail_token_key = urlencode(encrypt($gen_PW_token, "PW_Reset_Key"));
$mail_token_value = urlencode(encrypt($dbUser, $gen_PW_token));

// Mail verfassen

$msg = 'du möchtes dein Passwort zurücksetzen? Klicke auf den untenstehenden Link, um ganz einfach ein neues Passwort festzulegen.

<a href="'.$_SERVER['HTTP_HOST'].'/set_pw/'.$mail_token_key.'/'.$mail_token_value.'" style="padding: 5px 15px;background-color: #BEA5FF;display:table;margin: 0 auto;color: #FFFFFF;text-decoration: none;">Passwort zurücksetzen</a>';

require_once('../mail/mail.php');
if(send_mail($userInput, $preName, "Passwort zurücksetzen", $msg) == false){
    // Fehlerseite weiterleiten
    jumpPW();
    return;
}else{

    header('Location: ../../login/5');
}



// Weiterleiten

function jumpPW($err = 0){
    header('Location: ../../reset/'.$err);
    exit();
}

function genPW_reset_id(){

    $chars = "123456789abcdefghijklmnopqrstuvwxyz123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $returnID = "";
    for($n = 0; $n<12; $n++){
        if($n % 3 == 0 && $n > 0)$returnID .= "-";
        $returnID .= $chars[rand(0, strlen($chars)-1)];
    }

    return $returnID;
}
?>
