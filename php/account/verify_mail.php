<?php
include('../sql.php');
include('../cookie.php');
require('../crypt.php');

if(isset($_GET['validate']) && $_GET['validate'] != null){
    $trueUser = decrypt(urldecode($_GET['validate']), "mailKEY");
    $mailMode = true;
}else if(isset($_GET['token']) && $_GET['token'] != null){
    $trueUser = decrypt(urldecode($_GET['token']), "linkKEY");
    $mailMode = false;
}else{
    // Fehler -> Kein Account verbunden
    tostart();
    return;
}

// checken ob Account vorhanden
if(db_check($trueUser, 'acc_ID')){

    if($mailMode){
        // Mailadresse bestätigen
        $check = db_select("SELECT `mail_active`, `mail` FROM `account` WHERE `acc_ID`='".$trueUser."'");
        makeCookie('user', 'fill', $check[0]['mail']);
        if($check[0]['mail_active'] == 1){
            // Bereits aktiviert
            // -> Weiterleitung auf mail.php?info=0 + Login Button
            header('Location: ../../mail/0');
            exit();
        }else{
            $try = db_query("UPDATE `account` SET `mail_active`='1' WHERE `acc_ID`='".$trueUser."'");
            if($try == true){
                // Mail erfolgreich aktiviert
                // Weiterleitung auf mail.php?info=1 + Login Button
                header('Location: ../../mail/1');
                exit();
            }else{
                tostart();
                return;
            }
        }

    }else{
        // Mail angefragt
        // Weiterleitung auf mail.php?info=2 + Resend button ($_GET['token'])
        header('Location: ../../mail/2/'.$_GET['token']);
        exit();
    }
}else{
    // Fehler -> Account nicht in Datenbank
    tostart();
    return;
}

function tostart(){
    header('location: ../../login/0');
    exit();
}

?>