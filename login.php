<?php
include('./signed/php/login_state.php');
if (checkLogin()) {
  header('Location: '.URL_ROOT.'dashboard');
}

    $prefill = "";

    if(isset($_GET['token']) && $_GET['token'] != null){
        $userCode = urldecode(strip_tags($_GET['token']));
        $userID = decrypt($userCode, "linkKEY");

        if(db_check($userID, 'acc_ID') == true){
            $userData =db_select("SELECT `mail` FROM `account` WHERE `acc_ID`='".$userID."'");
            if($userData != false && isset($userData[0]['mail'])){
                $prefill = $userData[0]['mail'];

                makeCookie("user", "fill", $prefill);
            }
        }
    }

    if($prefill == "" && isset($_COOKIE['user']['fill']) && $_COOKIE['user']['fill'] != null){
        $prefill = $_COOKIE['user']['fill'];
    }
?>

<html>

    <head>
        <base href="/" />
        <?php include('./src/header.php'); ?>
        <title>mypressk.it - login</title>
        <link type="text/css" rel="stylesheet" href="css/frontend/login.css" />
    </head>

    <body>
      <div class="login_top_line">
        <div class="nav_logo_wrapper">
          <a href="/home"><img src="/src/icns/logo_white.svg" alt="SYNCRONIGHT" class="nav_logo"></a>
        </div>
        <div class="login_wrapper">
          <span class="nav_link_wrapper"><a class="nav_link" href="/login">Einloggen</a></span>
        </div>
      </div>

        <div class="header_line">
            <p id="login_headline">Login</p>
            <div class="inner_wrapper">
              <form id="loginform" action="./php/account/login.php" method="POST">
                <!--   onsubmit="return checkInput();" -->

                  <div class="message_negativ" id="errNote">
                  <?php
                      $msg = [
                          "Ein technischer Fehler ist aufgetreten.",
                          "Die angegebenen E-Mail Adresse existiert nicht.",
                          "Das eingegebene Passwort ist falsch.",
                          "Die aktuelle Sitzung ist abgelaufen",
                          "Erfolgreich abgemeldet",
                          "Eine Mail zum zurücksetzten des Passworts wurde soeben versandt.",
                          "Dein Passwort wurde erfolgreich geändert.",
                          "Der Link ist bereits abgelaufen.",
                          "Bitte bestätige deine E-Mail-Adresse vor dem Login.",
                          "Dir Fehlen die nötigen Berechtigungen für diesen Bereich. Bitte melde dich erneut an.",
                          "Die angegebene Mailadresse ist bereits registriert. Bitte melde dich hier an."
                      ];

                      if(isset($_GET['re']) && $_GET['re'] != null){

                          if(isset($msg[strip_tags($_GET['re'])])){
                              echo $msg[strip_tags($_GET['re'])];
                              $showMSG = true;
                          }
                      }
                  ?>
                  </div>
                  <?php
                      if(isset($showMSG) && $showMSG){
                          echo '<style type="text/css" rel="stylesheet">#errNote{display:block;}</style>';
                      }

                       if(isset($_GET['re']) && $_GET['re'] == 8){

                           if(isset($userCode)){
                              $userToken = $userCode;
                           }else{
                              if(isset($_COOKIE['user']['fill']) && $_COOKIE['user']['fill'] != null){
                                  $userMail = urldecode($_COOKIE['user']['fill']);
                                  $db_return = db_select("SELECT `acc_ID` FROM `account` WHERE `mail`='".$userMail."'");
                                  if($db_return != false && sizeof($db_return) == 1){
                                      $userToken = encrypt(urlencode($db_return[0]['acc_ID']), "linkKEY");
                                  }
                              }
                           }
                           echo isset($userToken) ? '<div id="resendMail">Bestätigungsmail <a href="./php/register/resend.php?token='.$userToken.'&second=true">Erneut senden</a></div>' : '';
                       }
                  ?>


                  <div class="input_wrapper">
                    <input type="text" placeholder="E-Mail Adresse eingeben..." class="login_input" name="usermail" id="usermail" value="<?php echo $prefill; ?>"/>
                  </div>

                  <div class="input_wrapper">
                    <input type="password" placeholder="Passwort eingeben..." class="login_input" name="userpw" id="userpw"/>
                  </div>
                  <div class="stay_logged_wrapper">
                    <input type="checkbox" id="remember" name="remember" value="0" checked/>
                    angemeldet bleiben
                  </div>
                  <!--  onchange="setRemeber()" -->


                  <input type="submit" class="button neutral space btn_blue btn_normal" id="SbmBtn" value="Anmelden" />
              </form>
              <div class="bottom_links">
                <a href="./reset" id="new_pw" class="space_small">Passwort vergessen? </a>
                <a href="../register/artist"> noch nicht registriert?</a>
              </div>
            </div>
        </div>

        <!-- <?php include('./signed/incl/footer/foot.php'); ?> -->
    </body>

</html>
